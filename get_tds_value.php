<?php 
require_once("connect.php");

$pan_letter=escapeString($conn,strtoupper($_POST['pan_letter']));
$freight=escapeString($conn,strtoupper($_POST['freight']));
$pay_to=escapeString($conn,strtoupper($_POST['pay_to']));
$adv_bal=escapeString($conn,strtoupper($_POST['adv_bal']));

if($adv_bal=='BAL')
{
	$get_tds_value=Qry($conn,"SELECT tds_value FROM rrpl_database.tds_value WHERE pan_letter='$pan_letter'");

	if(!$get_tds_value){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($get_tds_value)==0)
	{
		echo "<script>
			alert('Invalid PAN Card !');
			$('#tds_amount').val('');
			$('#unloading').val('');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		$row_tds_value = fetchArray($get_tds_value);
		$tds_amount = sprintf("%0.2f",ceil($freight*$row_tds_value['tds_value']/100));
		
		echo "<script>
			$('#tds_amount').val('$tds_amount');
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
else
{
if($pay_to=='NULL')
{
		
$owner_name=escapeString($conn,strtoupper($_POST['owner_name']));
$owner_pan=escapeString($conn,strtoupper($_POST['owner_pan']));
$broker_name=escapeString($conn,strtoupper($_POST['broker_name']));
$broker_pan=escapeString($conn,strtoupper($_POST['broker_pan']));
$tds_deduct=escapeString($conn,strtoupper($_POST['tds_deduct']));

$get_tds_value_owner=Qry($conn,"SELECT tds_value FROM rrpl_database.tds_value WHERE pan_letter='".substr($owner_pan,3,1)."'");
if(!$get_tds_value_owner){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
}
		
if(numRows($get_tds_value_owner)==0)
{
	$get_tds_value_broker=Qry($conn,"SELECT tds_value FROM rrpl_database.tds_value WHERE pan_letter='".substr($broker_pan,3,1)."'");

	if(!$get_tds_value_broker){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
	}
	
	if(numRows($get_tds_value_broker)==0)
	{
		echo "<script>
			alert('Owner and Broker Both PAN Card is not Valid. !');
			$('#tds_amount').val('0');
			$('#adv_to').val('');
			$('#party_name').val('');
			$('#party_pan).val('');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		$row_tds_value = fetchArray($get_tds_value_broker);
		$tds_amount = sprintf("%0.2f",ceil($freight*$row_tds_value['tds_value']/100));
		
		echo "<script>
			alert('broker !');
			$('#tds_amount').val('$tds_amount');
			$('#party_name').val('$broker_name');
			$('#party_pan).val('$broker_pan');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
}
else
{
	$row_tds_value = fetchArray($get_tds_value_owner);
	$tds_amount = sprintf("%0.2f",ceil($freight*$row_tds_value['tds_value']/100));
	
	echo "<script>
		$('#tds_amount').val('$tds_amount');
		$('#party_name').val('$owner_name');
		$('#party_pan').val('$owner_pan');
		$('#loadicon').hide();
	</script>";
	exit();
}

}
else
{
$get_tds_value=Qry($conn,"SELECT tds_value FROM rrpl_database.tds_value WHERE pan_letter='$pan_letter'");
if(!$get_tds_value){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
	}

if(numRows($get_tds_value)==0)
{
	echo "<script>
		alert('PAN Card is not Valid !');
		$('#tds_amount').val('0');
		$('#adv_to').val('');
		$('#loadicon').hide();
	</script>";
}
else
{
	$row_tds_value = fetchArray($get_tds_value);
	$tds_amount = sprintf("%0.2f",ceil($freight*$row_tds_value['tds_value']/100));
	
	echo "<script>
		$('#tds_amount').val('$tds_amount');
		$('#loadicon').hide();
	</script>";
	exit();
}
}
}
?>