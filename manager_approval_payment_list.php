<?php
include "header_manager.php";

if(!isset($_SESSION['rrpl_ship_manager']))
{
	echo "<script>
		window.location.href='./manager_approval.php';
	</script>";
	exit();
}
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		RTGS Payment Approval :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
	
<div id="result_form"></div> 		
			
<div class="row">
	<div class="form-group col-md-12 table-responsive">
		<table id="example" class="table table-bordered table-striped" style="font-size:12px;">
          <thead>
          <tr>
				<th>#</th>
				<th>Vou No.</th>
				<th>Company</th>
				<th>Amount</th>
				<th>Account Details</th>
				<th>PAN No.</th>
				<th>Truck No.</th>
				<th>Adv/Bal</th>
				<th>Ref_No.</th>
				<th>Approve</th>
			</tr>
			</thead>
			<tbody>
          
            <?php
              $sql = Qry($conn,"SELECT id,fno,com,amount,acname,acno,bank_name,ifsc,pan,tno,type,crn FROM rrpl_database.rtgs_fm 
			  WHERE approval='' AND branch='$branch'");
              
			  if(!$sql){
				  ScriptError($conn,$page_name,__LINE__);
				  exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			{
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
                echo 
                "<tr>
				  <td>$sn</td>
				  <td>
					<form action='view_vou.php' target='_blank' method='POST'>
						<input type='hidden' value='$row[fno]' name='vou_no'>	
						<button type='submit'>$row[fno]</button>
					</form>	
				</td>
				  <td>$row[com]</td>
				  <td>$row[amount]</td>
				  <td>
					<b>AcHolder :</b> $row[acname] <br>
					<b>AcNo :</b> $row[acno] <br>
					<b>Bank :</b> $row[bank_name] <br>
					<b>Ifsc :</b> $row[ifsc]
				</td>
				<td>$row[pan]</td>
				<td>$row[tno]</td>
				<td>$row[type]</td>
				<td>$row[crn]</td>
				<td>
					<button type='button' id='Btn1$row[id]' onclick=Approve('$row[id]') class='btn btn-xs btn-success'>Approve</button>
				</td>
				 </tr>
				";
			$sn++;		
              }
//<button type='button' onclick=ByRoad('$row[unq_id]','$row[id]','$row[name]','$row[bl_weight]','$row[bal_weight]') class='btn btn-xs btn-warning'>By Road</button>			  
			}
            ?>
		</tbody>	
        </table>
      </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
	
<div id="result1"></div>

<script>	
function Approve(id)
{
	$('#Btn1'+id).attr('disabled',true);
	$('#Btn1'+id).html('Processing..');
		
	jQuery.ajax({
		url: "manager_approval_mark_approve.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
		$("#result1").html(data);
		},
		error: function() {}
	});
}

$(document).ready(function() {
    $('#example').DataTable();
} );

</script>

<?php
include "footer.php";
?>