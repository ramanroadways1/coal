<?php
include_once "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Search EWay Bill by LR No :
      </h4>
	  
	  <style>
		label{font-size:13px;}
		.form-control{text-transform:uppercase;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<div id="result_form"></div> 
   
        <div class="row">
			
			 <div class="form-group col-md-12">


				<div class="form-group col-md-12">
				 </div>
				 
                 <div class="form-group col-md-3">
                     <label>Enter LR Number <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" type="text" class="form-control" id="lrno" required />
                 </div>
				 
				<div class="form-group col-md-12">   
					<button type="button" onclick="SearchEwayBill($('#lrno').val())" id="button1" class="btn btn-success">Search</button>
                 </div> 
				 
		 </div>
		  
			<div class="form-group col-md-12">
			
				<div class="form-group col-md-12 table-responsive" id="tab_result">
				 </div>
				 
			</div>
			
         </div>
     
</div>

		</div>
			</div> 
          </div>
        </div>       
    </section>
	
		<script>
		function SearchEwayBill(lrno)
		{
			if(lrno=='')
			{
				alert('Enter LR Number First !');
			}
			else
			{
				$("#loadicon").show();
					jQuery.ajax({
					url: "search_ewb_by_lrno.php",
					data: 'lrno=' + lrno,
					type: "POST",
					success: function(data) {
					$("#tab_result").html(data);
					},
					error: function() {}
				});
			}
		}
		</script>
<?php
include "footer.php";
?>
