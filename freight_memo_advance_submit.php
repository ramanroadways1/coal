<?php
require_once("./connect.php");

$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$freight=escapeString($conn,strtoupper($_POST['freight']));
$adv_to=escapeString($conn,strtoupper($_POST['adv_to']));
$loading=escapeString($conn,strtoupper($_POST['loading']));
$others=escapeString($conn,strtoupper($_POST['others']));
$tds_amount=escapeString($conn,strtoupper($_POST['tds_amount']));
$tds_deduct=escapeString($conn,strtoupper($_POST['tds_deduct']));
$total_freight=escapeString($conn,strtoupper($_POST['total_freight']));

$cash_adv=escapeString($conn,strtoupper($_POST['cash_adv']));
$diesel_adv=escapeString($conn,strtoupper($_POST['diesel_adv']));
$rtgs_adv=escapeString($conn,strtoupper($_POST['rtgs_adv']));

$total_adv=escapeString($conn,strtoupper($_POST['total_adv']));
$balance_amount=escapeString($conn,strtoupper($_POST['balance_amount']));

$fm_id=escapeString($conn,strtoupper($_POST['fm_id']));
$fm_number=escapeString($conn,strtoupper($_POST['fm_number']));
$owner=escapeString($conn,strtoupper($_POST['owner']));
$owner_pan=escapeString($conn,strtoupper($_POST['owner_pan']));
$broker=escapeString($conn,strtoupper($_POST['broker']));
$broker_pan=escapeString($conn,strtoupper($_POST['broker_pan']));
$owner_id=escapeString($conn,strtoupper($_POST['owner_id']));
$broker_id=escapeString($conn,strtoupper($_POST['broker_id']));
$company=escapeString($conn,strtoupper($_POST['company']));
$truck_no=escapeString($conn,strtoupper($_POST['truck_no']));
$vou_date=escapeString($conn,strtoupper($_POST['vou_date']));
$gps_deposit=escapeString($conn,strtoupper($_POST['gps_deposit']));
$commission_amount = escapeString($conn,($_POST['commission_model']));

if($broker_id=="2616" AND $commission_amount!=400)
{
	echo "<script>
			alert('ERROR : Invalid Commission Amount !');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if($others<300)
{
	echo "<script>
			alert('ERROR : Invalid Other Amount !');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if($adv_to=='OWNER')
{
	$party_name=$owner;
	$party_pan=$owner_pan;
	$party_type=2;
}
else if($adv_to=='BROKER')
{
	$party_name=$broker;
	$party_pan=$broker_pan;
	$party_type=1;
}
else
{
	if($owner_pan=='')
	{
		if($broker_pan=='')
		{
			echo "<script>
				alert('ERROR : ATLEAST ONE PARTY PAN CARD REQUIRED.');
				$('#submit_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
		else
		{
			$party_name=$broker;
			$party_pan=$broker_pan;
			$party_type=1;
		}
	}
	else
	{
		$party_name=$owner;
		$party_pan=$owner_pan;
		$party_type=2;
	}
}

$chk_gps_deposit = Qry($conn,"SELECT id,install_type,first_install FROM gps_device_record WHERE frno='$fm_number'");

if(!$chk_gps_deposit){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_gps_deposit)==0)
{
	$chk_fix_gps_deposit = Qry($conn,"SELECT id FROM gps_device_record WHERE uninstall_date=0 AND 
	tno='$truck_no' AND install_type='FIX'");

	if(!$chk_fix_gps_deposit){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}

	if(numRows($chk_fix_gps_deposit)==0)
	{
		echo "<script>
			alert('ERROR : GPS record not found.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$gps_deposit_db = 0;
}
else
{
	$row_gps = fetchArray($chk_gps_deposit);

	$gps_deposit_id = $row_gps['id'];

	if($row_gps['install_type']=='TRIP')
	{
		$gps_deposit_db = 3000;
	}
	else if($row_gps['install_type']=='FIX')
	{
		if($row_gps['first_install']=="1"){
			$gps_deposit_db = 3000;
		}
		else{
			$gps_deposit_db = 0;
		}
	}
	else
	{
		echo "<script>
			alert('ERROR : Invalid GPS installation type.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

if($gps_deposit!=$gps_deposit_db)
{
	echo "<script>
		alert('ERROR : GPS deposit not verified.');
		$('#submit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_owner_dec = Qry($conn,"SELECT tno,up6 AS declaration FROM rrpl_database.mk_truck WHERE id='$owner_id'");

if(!$chk_owner_dec){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_owner_dec)==0){
	
	echo "<script>
		alert('ERROR : Vehicle not found.');
		$('#submit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_owner_dec = fetchArray($chk_owner_dec);

if($row_owner_dec['tno']!=$truck_no)
{
	echo "<script>
		alert('ERROR : Vehicle number not verified.');
		$('#submit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
	
if($row_owner_dec['declaration']=='')
{
	$tds_deduct_db = "1";
}
else
{
	$tds_deduct_db = "0";
}

if($tds_deduct!=$tds_deduct_db)
{
	echo "<script>
		alert('Tds value is not valid !!');
		$('#submit_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($tds_deduct>0)
{
	$pan_latter = substr($party_pan,3,1);
	
	$get_tds_value=Qry($conn,"SELECT tds_value FROM rrpl_database.tds_value WHERE pan_letter='$pan_latter'");

	if(!$get_tds_value){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}
		
	if(numRows($get_tds_value)==0)
	{
		$tds_amount_db = 0;
	}
	else
	{
		$row_tds_value = fetchArray($get_tds_value);
		$tds_amount_db = sprintf("%0.2f",ceil($freight*$row_tds_value['tds_value']/100));
	}
	
	if($tds_amount!=$tds_amount_db)
	{
		echo "<script>
			alert('ERROR : TDS amount is not valid.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

if($total_freight!=$freight+$loading-$others-$commission_amount-$tds_amount-$gps_deposit)
{
	echo "<script>
			alert('ERROR : TOTAL FREIGHT AMOUNT NOT MATCHING.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if($total_adv!=$cash_adv+$diesel_adv+$rtgs_adv)
{
	echo "<script>
			alert('ERROR : ADVANCE AMOUNT NOT MATCHING.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
}

if($balance_amount<0)
{
	echo "<script>
			alert('ERROR : Balance Amount is Negative. Please CHECK !!');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if($total_adv==0 AND $adv_to!='NULL')
{
	echo "<script>
			alert('ERROR : Advance Amount is Zero. Please Select Advance to NULL option.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if($cash_adv>0)
{
	$fetch_bal=Qry($conn,"SELECT balance,balance2 FROM rrpl_database.user WHERE username='$branch'");
	if(!$fetch_bal){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($fetch_bal)==0)
	{
		echo "<script>
			alert('ERROR : Branch not found.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_balance=fetchArray($fetch_bal);
	
	$rrpl_cash=$row_balance['balance'];
	$rr_cash=$row_balance['balance2'];
	
	if($company=='RRPL' AND $row_balance['balance']>=$cash_adv)
	{
		$dr_col="debit";
		$balance_col="balance";
		$new_bal=$rrpl_cash-$cash_adv;
		// echo $new_bal;
	}
	else if($company=='RAMAN_ROADWAYS' AND $row_balance['balance2']>=$cash_adv)
	{
		$dr_col="debit2";
		$balance_col="balance2";
		$new_bal=$rr_cash-$cash_adv;
		// echo $new_bal;
	}
	else
	{
		echo "<script>
			alert('BALANCE ERROR : INSUFFICIENT BALANCE IN : $company.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

if($diesel_adv>0)
{
	$chk_diesel = Qry($conn,"SELECT SUM(amount) as total_dsl FROM diesel_cache WHERE fm_no='$fm_number' AND type='ADV'");
	if(!$chk_diesel){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($chk_diesel)==0)
	{
		echo "<script>
			alert('Error : Diesel entry not found.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_sum_diesel = fetchArray($chk_diesel);
	
	if($row_sum_diesel['total_dsl']!=$diesel_adv)
	{
		echo "<script>
			alert('Error : Please check diesel amount. Diesel Amount : $diesel_adv, Entered : $row_sum_diesel[total_dsl]');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$chk_ril_stock = Qry($conn,"SELECT phy_card,amount FROM diesel_cache WHERE fm_no='$fm_number' AND ril_card='1' AND type='ADV'");
	
	if(!$chk_ril_stock){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($chk_ril_stock)>0)
	{
		while($row_ril = fetchArray($chk_ril_stock))
		{
			$get_balance = Qry($conn,"SELECT balance FROM diesel_api.dsl_ril_stock WHERE cardno='$row_ril[phy_card]' ORDER BY id DESC LIMIT 1");
			if(!$get_balance){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
			
			$row_balance = fetchArray($get_balance);
			
			if($row_balance['balance']<$row_ril['amount'])
			{
				echo "<script>
					alert('Card not in stock : $row_ril[phy_card]. Available balance is : $row_balance[balance] !');
					$('#loadicon').hide();
					$('#submit_button').attr('disabled', false);
				</script>";
				exit();	
			}
		}
	}
}

if($rtgs_adv>0)
{
	if($adv_to=='BROKER')
	{
		$qry_ac=Qry($conn,"SELECT acname,accno as acno,bank,ifsc FROM rrpl_database.mk_broker WHERE id='$broker_id'");
		if(!$qry_ac){
			ScriptError($conn,$page_name,__LINE__);
			exit();
		}
		
		if(numRows($qry_ac)==0)
		{
			echo "<script>
				alert('ERROR : Account not found : Broker.');
				$('#submit_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	else if($adv_to=='OWNER')
	{
		$qry_ac=Qry($conn,"SELECT acname,accno as acno,bank,ifsc FROM rrpl_database.mk_truck WHERE id='$owner_id'");
		if(!$qry_ac){
			ScriptError($conn,$page_name,__LINE__);
			exit();
		}
		
		if(numRows($qry_ac)==0)
		{
			echo "<script>
				alert('ERROR : Account not found : Owner.');
				$('#submit_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	
	$row_ac=fetchArray($qry_ac);
	
	$acname=$row_ac['acname'];
	$acno=$row_ac['acno'];
	$bank=$row_ac['bank'];
	$ifsc=$row_ac['ifsc'];
	
	if($acname=='')
	{
		$acname=escapeString($conn,strtoupper($_POST['ac_holder']));
		$acno=escapeString($conn,strtoupper($_POST['ac_number']));
		$bank=escapeString($conn,strtoupper($_POST['bank_name']));
		$ifsc=escapeString($conn,strtoupper($_POST['ifsc_code']));
		
		if($acname=='')
		{
			echo "<script>
				alert('ERROR : Account details is empty !!');
				$('#submit_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
		if($adv_to=='BROKER')
		{
			$update_id=$broker_id;
			$tab_name="rrpl_database.mk_broker";
		}
		else if($adv_to=='OWNER')
		{
			$update_id=$owner_id;
			$tab_name="rrpl_database.mk_truck";
		}
			
		$update_ac_details="YES";
	}
	else
	{
		$update_ac_details="NO";
	}
}
else
{
	$update_ac_details="NO";
}

StartCommit($conn);
$flag = true;	
$rtgs_flag = false;	

if($cash_adv>0)
{
	$insert_cash=Qry($conn,"INSERT INTO rrpl_database.cashbook(branch,branch_user,company,vou_date,date,vou_no,vou_type,narration,
	`$dr_col`,`$balance_col`,timestamp) VALUES ('$branch','$branch_sub_user','$company','$vou_date','$date','$fm_number','Freight_Memo',
	'ADVANCE_CASH','$cash_adv','$new_bal','$timestamp')");
	
	if(!$insert_freight_memo){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$update_new_bal=Qry($conn,"UPDATE rrpl_database.user SET `$balance_col`=`$balance_col`-'$cash_adv' WHERE username='$branch'");

	if(!$update_new_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($diesel_adv>0)
{
	$insert_diesel=Qry($conn,"INSERT INTO diesel(vou_no,vou_type,card_pump,card_no,fuel_company,date,branch,branch_user,timestamp) 
	SELECT '$fm_number','ADVANCE',card_pump,phy_card,fuel_company,'$date','$branch','$branch_sub_user','$timestamp' FROM diesel_cache WHERE 
	fm_no='$fm_number' AND type='ADV'");
	
	if(!$insert_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_diesel_main_Db=Qry($conn,"INSERT INTO rrpl_database.diesel_fm(branch,branch_user,fno,com,qty,rate,disamt,tno,type,dsl_by,
	bill_no,dcard,veh_no,dcom,dsl_nrr,pay_date,fm_date,ril_card,dsl_mobileno,timestamp,stockid) SELECT '$branch','$branch_sub_user',
	'$fm_number','$company',qty,rate,amount,'$truck_no','ADVANCE',card_pump,bill_no,card_no,phy_card,fuel_company,narration,'$date',
	'$vou_date',ril_card,mobile_no,'$timestamp',stockid FROM diesel_cache WHERE fm_no='$fm_number' AND type='ADV'");
	
	if(!$insert_diesel_main_Db){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_ril_amount = Qry($conn,"SELECT phy_card,amount FROM diesel_cache WHERE fm_no='$fm_number' AND ril_card='1' AND type='ADV'");
	
	if(!$get_ril_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_ril_amount)>0)
	{
		while($row_ril2 = fetchArray($get_ril_amount))
		{
			$update_ril_stock = Qry($conn,"UPDATE diesel_api.dsl_ril_stock SET balance=balance-'$row_ril2[amount]' WHERE 
			cardno='$row_ril2[phy_card]' ORDER BY id DESC LIMIT 1");
			
			if(!$update_ril_stock){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
	
	$sel_data_for_log = Qry($conn,"SELECT id,branch,disamt,tno,dsl_by,dcard,veh_no,dcom,timestamp,dsl_mobileno FROM 
	rrpl_database.diesel_fm WHERE timestamp='$timestamp' AND fno='$fm_number' AND type='ADVANCE'");
	
	if(!$sel_data_for_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	while($row_log=fetchArray($sel_data_for_log))
	{
		if($row_log['dsl_by']=='CARD')
		{
			$card_live_bal=Qry($conn,"select balance from diesel_api.dsl_cards where cardno='$row_log[dcard]'");
			if(!$card_live_bal){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			$row_card_bal=fetchArray($card_live_bal); 
			$live_bal = $row_card_bal['balance']; 

			$qry_log=Qry($conn,"insert into diesel_api.dsl_logs (user,status,content,timestamp,trucktype,truckno,
			dslcomp,dsltype,cardno,vehno,cardbal,amount,mobileno,reqid) VALUES 
			('$row_log[branch]','SUCCESS','New Request Added','$row_log[timestamp]','MARKET','$row_log[tno]','$row_log[dcom]',
			'CARD','$row_log[dcard]','$row_log[veh_no]','$live_bal','$row_log[disamt]','$row_log[dsl_mobileno]','$row_log[id]')");
			
			if(!$qry_log){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
			
	$dlt_cache = Qry($conn,"DELETE FROM diesel_cache WHERE fm_no='$fm_number' AND type='ADV'");
	
	if(!$dlt_cache){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($rtgs_adv>0)
{
		if(date('m')>3){ $year = date('y'); } else { $year = (date('y')-1); }
												
		if($company=='RRPL')
		{
			$crn="RRPL".$year;
			
			$ramanc=Qry($conn,"SELECT crn FROM rrpl_database.rtgs_fm where crn like '$crn%' ORDER BY id DESC LIMIT 1");
			if(!$ramanc){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($ramanc)!=0)
			{
				$rowcrn=fetchArray($ramanc);
				$count=$rowcrn['crn'];	
				$newstring = substr($count,6,13);
				$newstr = ++$newstring;
			}
			else
			{
				$newstr=1;
			}	
		}
		else if($company=='RAMAN_ROADWAYS')
		{	
			$crn="RR".$year;
			
			$ramanc = Qry($conn,"SELECT DISTINCT crn FROM rrpl_database.rtgs_fm where crn like '$crn%' ORDER BY id DESC LIMIT 1");
			
			if(!$ramanc){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($ramanc)!=0)
			{
				$rowcrn = fetchArray($ramanc);
				$count = $rowcrn['crn'];	
				$newstring = substr($count,4);
				$newstr = ++$newstring;
			}
			else
			{
				$newstr = 1;
			}	
		}
		
	$crnnew = $crn.sprintf("%'.06d",$newstr);
		
	$insert_rtgs=Qry($conn,"INSERT INTO rtgs(vou_no,vou_type,branch,amount,acname,acno,bank,ifsc,pan,tno,crn,date,timestamp) VALUES 
	('$fm_number','ADVANCE','$branch','$rtgs_adv','$acname','$acno','$bank','$ifsc','$party_pan','$truck_no','$crnnew','$date','$timestamp')");
	
	if(!$insert_rtgs){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_rtgs_main_Db=Qry($conn,"INSERT INTO rrpl_database.rtgs_fm (fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,
	pay_date,fm_date,tno,type,crn,timestamp) VALUES ('$fm_number','$branch','$company','$total_freight','$rtgs_adv','$acname','$acno',
	'$bank','$ifsc','$party_pan','$date','$vou_date','$truck_no','ADVANCE','$crnnew','$timestamp')");
	
	if(!$insert_rtgs_main_Db){
		$flag = false;
		$rtgs_flag = true;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	

$rtgs_adv_set="0";
}
else{
	$rtgs_adv_set="1";
}

$insert_advance_fm=Qry($conn,"INSERT INTO freight_memo_adv(fm_no,adv_party,party_name,party_pan,loading,other,commission_amount,tds,gps_deposit,total_freight,
total_adv,cash,diesel,rtgs,branch,branch_user,date,timestamp,rtgs_adv) VALUES ('$fm_number','$party_type','$party_name','$party_pan',
'$loading','$others','$commission_amount','$tds_amount','$gps_deposit','$total_freight','$total_adv','$cash_adv','$diesel_adv','$rtgs_adv','$branch','$branch_sub_user',
'$date','$timestamp','$rtgs_adv_set')");

if(!$insert_advance_fm){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_freight_memo=Qry($conn,"UPDATE freight_memo SET advance='1' WHERE id='$fm_id'");
if(!$update_freight_memo){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$get_fm_date = Qry($conn,"SELECT f.tno,f.date,f.owner,f.broker,f.lrnos,f.branch,l.lr_date 
FROM freight_memo AS f 
LEFT JOIN lr_entry as l ON l.id=f.lr_ids 
WHERE f.id='$fm_id'");

$row_fm = fetchArray($get_fm_date);

$insert_pod_pending = Qry($conn,"INSERT INTO rrpl_database._pending_lr_list(vou_id,vou_no,bid,oid,branch,lr_date,create_date,lrno,timestamp) 
VALUES ('$fm_id','$fm_number','$row_fm[broker]','$row_fm[owner]','$row_fm[branch]','$row_fm[lr_date]','$row_fm[date]',
'$row_fm[lrnos]','$timestamp')");

if(!$insert_pod_pending){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_gps_deposit)>0 AND $row_gps['install_type']=='FIX' AND $row_gps['first_install']=="1")
{
	$update_gps_install_done = Qry($conn,"UPDATE gps_device_record SET first_install='0' WHERE id='$gps_deposit_id'");
	if(!$update_gps_install_done){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($update_ac_details=='YES')
{
	if($adv_to=='OWNER')
	{
		$update_ac=Qry($conn,"UPDATE rrpl_database.mk_truck SET acname='$acname',accno='$acno',bank='$bank',ifsc='$ifsc' WHERE 
		id='$update_id'");
		if(!$update_ac){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$update_ac=Qry($conn,"UPDATE rrpl_database.mk_broker SET acname='$acname',accno='$acno',bank='$bank',ifsc='$ifsc' WHERE 
		id='$update_id'");
		if(!$update_ac){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
			
	$update_ac_log=Qry($conn,"INSERT INTO rrpl_database.ac_update(o_b,ac_for,at_the_time_of,acname,acno,bank,ifsc,branch,
	branch_user,timestamp) VALUES ('$adv_to','$update_id','ADVANCE','$acname','$acno','$bank','$ifsc','$branch','$branch_sub_user',
	'$timestamp')");
		
	if(!$update_ac_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('SUCCESS : ADVANCE PAID.');
		$('#AdvForm')[0].reset();
		$('#AdvanceId$fm_id').attr('disabled',true);
		$('#AdvanceId$fm_id').html('ADVANCE PAID');
		$('#AdvanceId$fm_id').attr('class','btn btn-xs bg-green');
		$('#loadicon').hide();
		document.getElementById('close_modal_button').click();
		// window.location.href='./freight_memo_adv_pending.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	if($rtgs_flag)
	{
		echo "<script type='text/javascript'>
			alert('Please try again after 5-10 seconds..');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		echo "<script type='text/javascript'>
			alert('Error While Processing Request.');
			$('#submit_button').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
?>