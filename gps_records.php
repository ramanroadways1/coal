<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		GPS Device Records :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
		
<div class="row">
 <div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered" style="font-size:13px;">
          <thead>
		  <tr>
				<th>#</th>
				<th>Vou. No.</th>
				<th>Vehicle No.</th>
				<th>Total Trips</th>
				<th>Install date</th>
				<th>Trip/Fix</th>
				<th>Rcv Device</th>
			</tr>
          </thead>
          <tbody>
            <?php
              $sql = Qry($conn,"SELECT g.id,g.frno,g.tno,g.install_date,g.install_type,COUNT(f.id) as total_trips 
			  FROM gps_device_record AS g 
			  LEFT OUTER JOIN freight_memo as f ON f.tno=g.tno 
			  WHERE g.uninstall_date=0 AND f.timestamp>=g.timestamp GROUP by g.tno");
              
			 if(!$sql){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			}
			  
			if(numRows($sql)==0)
			{
					echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
		}
		   else
			{
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				echo "<tr>
				  <td>$sn</td>
				  <td>$row[frno]</td>
				  <td>$row[tno]</td>
				  <td>$row[total_trips]</td>
				 <td>".date("d-m-y",strtotime($row['install_date']))."</td>				 
				 <td>$row[install_type]</td>";
				 
				 if($row['install_type']=='FIX')
				 {
					 echo "<td>
					 <input type='hidden' id='veh_no_$row[id]' value='$row[tno]'>
					 <button type='button' id='gps_btn_$row[id]' onclick=RcvDevice('$row[id]') class='btn btn-xs btn-primary'>Rcv Device</button>
					</td>";
				 }
				 else
				 {
					 echo "<td></td>";
				 }
				 echo "</tr>";
			$sn++;		
              }
			}
            ?>
		</tbody>	
        </table>
      </div>

  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
	
<div id="load_result1"></div>

<script>	
function RcvDevice(id)
{
	var tno = $('#veh_no_'+id).val();
	
	if (confirm('Are you sure to Receive GPS device of Vehicle: '+tno+' ?'))
	{
		$('#gps_btn_'+id).attr('disabled',true);
		$("#loadicon").show();
			jQuery.ajax({
			url: "rcv_gps_device.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#load_result1").html(data);
			},
			error: function() {}
		});
	}
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>