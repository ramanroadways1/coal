<?php
require_once("./connect.php");

$timestamp=date("Y-m-d H:i:s");
$date=date("Y-m-d");

$weight=escapeString($conn,($_POST['weight']));
$company=escapeString($conn,($_POST['company']));
$truck_no=escapeString($conn,($_POST['truck_no']));
$shipment_id=escapeString($conn,($_POST['shipment_id']));

if($weight=='' || $weight==0)
{
	echo "<script type='text/javascript'>
		alert('PLEASE CHECK WEIGHT or No LR Added Yet.');
		$('#create_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($shipment_id=='')
{
	echo "<script type='text/javascript'>
		alert('ERROR : SHIPMENT NOT FOUND.');
		$('#create_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($company=='')
{
	echo "<script type='text/javascript'>
		alert('ERROR : COMPANY NOT FOUND.');
		$('#create_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($truck_no=='')
{
	echo "<script type='text/javascript'>
		alert('ERROR : TRUCK NUMBER NOT FOUND.');
		$('#create_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(count(array_filter($_POST['lrid']))>0)
{
		foreach(array_filter($_POST['lrid']) as $lrids=>$value) 
		{
			if($_POST['ship_ids'][$lrids]!=$shipment_id)
			{
				echo "<script>
					alert('Multiple Shipment Found in LR.');
					$('#create_button').attr('disabled',false);
					$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$lr_ids[]=$_POST['lrid'][$lrids];
			$lrnos[]=$_POST['lrnos'][$lrids];
			$DbWeight[]=$_POST['chrg_wt'][$lrids];
		}
		
		$lrIds=implode(',',array_filter($lr_ids));
		$lrnos=implode(',',array_filter($lrnos));
		$DbWeight=array_sum($DbWeight);
		
		if($weight!=$DbWeight)
		{
			echo "<script>
					alert('Weight not matching.');
					$('#create_button').attr('disabled',false);
					$('#loadicon').hide();
				</script>";
			exit();
		}
		
}
else
{
	echo "<script>
		alert('No LR found.');
		$('#create_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$get_lr_ids = Qry($conn,"SELECT id FROM rrpl_database.lr_sample WHERE FIND_IN_SET(lrno,'$lrnos')");
if(!$get_lr_ids){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_lr_ids)!=count(array_filter($_POST['lrid'])))
{
	echo "<script>
		alert('LR record not found.');
		$('#create_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

	$lrno_ids= array();
			
	while($row_lr_ids=fetchArray($get_lr_ids))
	{
		$lrno_ids[] = "'".$row_lr_ids['id']."'";
	}
	
	$lrno_ids=implode(',', $lrno_ids);
	
$fetch_bal_weight=Qry($conn,"SELECT bal_weight FROM shipment WHERE id='$shipment_id'");

if(!$fetch_bal_weight){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($fetch_bal_weight)==0)
{
	echo "<script>
		alert('Shipment not found.');
		$('#create_button').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_bal_weight=fetchArray($fetch_bal_weight);

$balance_weight=sprintf("%.2f",($row_bal_weight['bal_weight']-$DbWeight));

if($balance_weight<0)
{
	echo "<script>
			alert('Shipment Balance Weight ERROR : Bal Weight Aailable is : $row_bal_weight[bal_weight].');
			$('#create_button').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
	exit();
}

$branch = $_SESSION['rrpl_ship'];

$get_branch_code = Qry($conn,"SELECT branch_code FROM rrpl_database.user WHERE username='$branch'");

if(!$get_branch_code){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_branch_code) == 0)
{
	errorLog("Branch not found. $branch.",$conn,$page_name,__LINE__);
	exit();
}

$row_branch_code = fetchArray($get_branch_code);

if(strlen($row_branch_code['branch_code']) != 3)
{
	errorLog("Branch code not found. $branch.",$conn,$page_name,__LINE__);
	exit();
}

		
		$branch2 = strtoupper($row_branch_code['branch_code'])."OLRSP".date("dmy");
        
		$ramanc=Qry($conn,"SELECT fm_no FROM own_truck_form where fm_no like '$branch2%' ORDER BY id DESC LIMIT 1");
		if(!$ramanc){
			ScriptError($conn,$page_name,__LINE__);
			exit();
		}
		
		$row=fetchArray($ramanc);
		if(numRows($ramanc)>0)
		{
			$count=$row['fm_no'];
			$newstring = substr($count,14);
			$newstr = ++$newstring;
			$memoid=$branch2.$newstr;
		}
		else
		{
			$memoid=$branch2."1";
		}

StartCommit($conn);
$flag = true;
		
$insert_freight_memo = Qry($conn,"INSERT INTO own_truck_form(fm_no,truck_no,company,date,weight,lr_ids,lrnos,shipment_id,branch,
branch_user,timestamp) VALUES ('$memoid','$truck_no','$company','$date','$weight','$lrIds','$lrnos','$shipment_id','$branch',
'$branch_sub_user','$timestamp')");

if(!$insert_freight_memo){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$vou_id=getInsertID($conn);

$update_lr_done=Qry($conn,"UPDATE lr_entry SET done='1' WHERE id IN($lrIds)");

if(!$update_lr_done){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_lr_done2=Qry($conn,"UPDATE rrpl_database.lr_sample SET crossing='NO' WHERE id IN($lrno_ids)");

if(!$update_lr_done2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$dlr_lr_temp = Qry($conn,"DELETE FROM rrpl_database.lr_sample_pending WHERE lr_id IN($lrno_ids)");

if(!$dlr_lr_temp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_dispatch = Qry($conn,"INSERT INTO dispatch_road(shipment_id,vou_id,vou_no,lrnos,tno,vou_date,weight,branch,branch_user,timestamp) 
VALUES ('$shipment_id','$vou_id','$memoid','$lrnos','$truck_no','$date','$weight','$branch','$branch_sub_user','$timestamp')");

if(!$insert_dispatch){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_shipment_table=Qry($conn,"UPDATE shipment SET bal_weight='$balance_weight',by_road=by_road+'$DbWeight' WHERE 
id='$shipment_id'");

if(!$update_shipment_table){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_into_main_Db=Qry($conn,"INSERT INTO rrpl_database.freight_form_lr(frno,company,branch,branch_user,date,create_date,truck_no,
lrno,mother_lr_id,fstation,tstation,consignor,consignee,from_id,to_id,con1_id,con2_id,wt12,weight,crossing,timestamp) SELECT '$memoid',l.company,
l.branch,'$branch_sub_user',l.lr_date,'$date',l.tno,l.lrno,l.main_lr_id,loc1.name,loc2.name,con1.name,con2.name,l.from_loc,l.to_loc,l.con1,l.con2,
l.actual_wt,l.charge_wt,'NO','$timestamp' FROM ship.lr_entry as l 
LEFT OUTER JOIN rrpl_database.station as loc1 ON loc1.id=from_loc
LEFT OUTER JOIN rrpl_database.station as loc2 ON loc2.id=to_loc
LEFT OUTER JOIN rrpl_database.consignor as con1 ON con1.id=con1
LEFT OUTER JOIN rrpl_database.consignee as con2 ON con2.id=con2 
WHERE l.id IN($lrIds)");

if(!$insert_into_main_Db){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('Own Truck Form : $memoid. Created Successfully !');
			window.location.href='./own_truck_form.php';
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',true);
	</script>";
	exit();
}
?>