<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Shipment status :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
	
<script type="text/javascript">
$(document).ready(function (e) {
$("#RailForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_rail").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_dispatch_rail.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#RoadForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_road").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_dispatch_road.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>		

<div id="result_form"></div> 		
			
<div class="row">
 <div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered" style="font-size:13px;">
          <thead>
		  <tr>
				<th>#</th>
				<th>Shipment<br>Number</th>
				<th>ShipName</th>
				<th>Total BL</th>
				<th>Total BE</th>
				<th>Sipment<br>Date</th>
				<th>BL Wt.<br>(in MTS)</th>
				<th>Draft Wt.<br>(in MTS)</th>
				<th>Bal Wt.<br>(in MTS)</th>
				<th>Date <br> of Arrival</th>
				<th>Time <br> of Arrival</th>
			</tr>
          </thead>
          <tbody>
            <?php
              $sql = Qry($conn,"SELECT shipment.id,unq_id,voyage_no,total_bl,total_be,bl_weight,date,name,draft_weight,bal_weight,
			  date_of_arrival,time_of_arrival FROM shipment,vessel_name where rcv=1 AND vessel_name.id=vessel_name AND 
			  shipment.branch='$branch' AND shipment.active='1' ORDER BY shipment.id ASC");
              
			 if(!$sql){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
					echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			{
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
                echo "<tr>
				  <td>$sn</td>
				  <td>$row[unq_id]</td>
				  <td>$row[name]<br>($row[voyage_no])</td>
				  <td>$row[total_bl]</td>
				  <td>$row[total_be]</td>
				 <td>".date("d-m-y",strtotime($row['date']))."</td>				 
				 <td>$row[bl_weight]</td>
				 <td>".sprintf("%.2f",$row['draft_weight'])."</td>
				  <td>".sprintf("%.2f",$row['bal_weight'])."</td>
				 <td>".date("d-m-y",strtotime($row['date_of_arrival']))."</td>
				  <td>".date("h:i A",strtotime($row['time_of_arrival']))."</td>
				<input type='hidden' id='ShipNameBox$row[id]' value='$row[name]'>
				
				 </tr>
				";
			$sn++;		
              }
			  // <td>
					// <button type='button' onclick=ByRail('$row[unq_id]','$row[id]','$row[bl_weight]','$row[bal_weight]') class='btn btn-xs btn-primary'>By Rail</button>
				// </td>
//<button type='button' onclick=ByRoad('$row[unq_id]','$row[id]','$row[name]','$row[bl_weight]','$row[bal_weight]') class='btn btn-xs btn-warning'>By Road</button>			  
			}
            ?>
		</tbody>	
        </table>
      </div>

  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
	
<button type="button" id="RailModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#RailModal"></button>
<button type="button" id="RoadModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#RoadModal"></button>

<div class="modal" id="RailModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="bg-primary modal-header">
        <h5 class="modal-title">Dispatch By Railway : <span id="ship_name_rail"></span></h5>
      </div>
	  
	<form id="RailForm" autocomplete="off">
      <div class="modal-body">
		 <div class="row">
			<div class="form-group col-md-6">
				<label>RR Number <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9-]/,'')" type="text" name="rr_no" class="form-control" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>FRN Number <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9-]/,'')" type="text" name="frn_no" class="form-control" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>RR Date <font color="red">*</font></label>
				<input type="date" name="rr_date" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
			
			<div class="form-group col-md-6">
				<label>RR Weight (in MTS) <font color="red">*</font></label>
				<input type="number" min="1" step="any" name="rr_weight" class="form-control" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>No of Weagons <font color="red">*</font></label>
				<input type="number" max="59" min="1" name="no_of_wgn" class="form-control" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Upload Weagons Images <font color="red">*</font></label>
				<input type="file" name="upload[]" multiple="multiple" class="form-control" required />
			</div>
		</div>
			<input type="hidden" id="ship_id_rail" name="ship_id">	
			<input type="hidden" id="weight_rail" name="weight">	
			<input type="hidden" id="bal_weight_rail" name="bal_weight">
			<input type="hidden" id="shipment_no_rail" name="shipment_no">			
      </div>

      <div class="modal-footer">
        <button type="submit" id="submit_rail" class="btn btn-primary">Submit</button>
        <button type="button" id="close_modal_rail" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<div id="load_lr"></div>

<script>	
function LoadLR(lrno)
{
		$("#loadicon").show();
		jQuery.ajax({
		url: "fetch_lr_entry.php",
		data: 'lrno=' + lrno,
		type: "POST",
		success: function(data) {
			$("#load_lr").html(data);
		},
		error: function() {}
	});
}
</script>

<div class="modal" id="RoadModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="bg-primary modal-header">
        <h4 class="modal-title">Dispatch By Road : <span id="ship_name_road"></span></h4>
      </div>
	<form id="RoadForm" autocomplete="off">
      <div class="modal-body">
		 <div class="row">
		 
			<div class="form-group col-md-6">
				<label>Truck Type <font color="red">*</font></label>
				<select onchange="TruckType(this.value)" name="truck_type" class="form-control" required>
					<option value="">---SELECT---</option>
					<option value="MARKET">MARKET TRUCK</option>
					<option value="OWN">OWN TRUCK</option>
				</select>
			</div>
			
			<script>
			function TruckType(elem)
			{
				$('#loadicon').show();
				jQuery.ajax({
				url: "fetch_voucher_for_road_dispatch.php",
				data: 'truck_type=' + elem,
				type: "POST",
				success: function(data) {
				$("#vou_result").html(data);
				$('#loadicon').hide();
				},
				error: function() {}
				});
			}
			</script>
			
			<div class="form-group col-md-6">
				<label>Vou Number <font color="red">*</font></label>
				<select id="vou_result" onchange="FetchLR(this.value)" name="vou_no2" id="vou_no2" class="form-control" required>
					<option value="">---SELECT---</option>
				</select>
			</div>
			
			<script>
			function FetchLR(elem)
			{
				if(elem=='')
				{
					var id = "";
					var weight = "";
					var type = "";
					var tno = "";
					var vou_no = "";
					var vou_date = "";
					var lrnos = "";
				}
				else
				{
					var string = elem.split("_");
					var id = string[0];
					var weight = string[1];
					var type = string[2];
					var tno = string[3];
					var vou_no = string[4];
					var vou_date = string[5];
					var lrnos = string[6];
				}
					
				$('#truck_no').val(tno);
				$('#fm_weight').val(weight);
				$('#vou_id').val(id);
				$('#lr_type').val(type);
				$('#vou_no').val(vou_no);
				$('#vou_date').val(vou_date);
				$('#lrnos').val(lrnos);
			}
			</script>
			
			<div class="form-group col-md-6">
				<label>Voucher Number <font color="red">*</font></label>
				<input type="text" name="vou_no" id="vou_no" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Date <font color="red">*</font></label>
				<input type="text" name="vou_date2" id="vou_date" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Truck Number <font color="red">*</font></label>
				<input type="text" name="truck_no" id="truck_no" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-6">
				<label>LR Weight (in MTS) <font color="red">*</font></label>
				<input type="number" min="1" name="fm_weight" id="fm_weight" readonly class="form-control" required />
			</div>
			
		</div>
			<input type="hidden" id="lrnos" name="lrnos">	
			<input type="hidden" id="vou_id" name="vou_id">	
			<input type="hidden" id="lr_type" name="lr_type">	
			
			<input type="hidden" id="ship_id_road" name="ship_id">	
			<input type="hidden" id="weight_road" name="weight">
			<input type="hidden" id="bal_weight_road" name="bal_weight">
			<input type="hidden" id="shipment_no_road" name="shipment_no">
      </div>

      <div class="modal-footer">
        <button type="submit" id="submit_road" class="btn btn-primary">Submit</button>
        <button type="button" id="close_modal_road" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<script>	
$(document).ready(function() {
    $('#example').DataTable();
} );

// function ByRail(shipment_no,id,weight,bal_weight)
// {
	// var shipname = $('#ShipNameBox'+id).val();
	
	// $('#ship_id_rail').val(id);
	// $('#weight_rail').val(weight);
	// $('#bal_weight_rail').val(bal_weight);
	// $('#ship_name_rail').html(shipname +", Shipment No: "+shipment_no);
	// $('#shipment_no_rail').val(shipment_no);
	// document.getElementById('RailModalButton').click();
// }

// function ByRoad(shipment_no,id,name,weight,bal_weight)
// {
	// $('#ship_id_road').val(id);
	// $('#weight_road').val(weight);
	// $('#bal_weight_road').val(bal_weight);
	// $('#ship_name_road').html(name +", Shipment No: "+shipment_no);
	// $('#shipment_no_road').val(shipment_no);
	// document.getElementById('RoadModalButton').click();
// }
</script>

<?php
include "footer.php";
?>