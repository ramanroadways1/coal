<?php
require_once("./connect.php");

$menu_page_name = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Raman Sea Trans : Raman Group</title>
	<link rel="shortcut icon" type="image/png" href="../favicon.png"/>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  
	  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
	  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
	  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
	  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
	  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
	  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
		<link href="../google_font.css" rel="stylesheet">

<style>
/* width */
::-webkit-scrollbar {
  width: 5px;
  height: 5px;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 15px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: gray; 
  border-radius: 15px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: maroon; 
}
</style>
 
 <script>
<!--
//Disable right mouse click Script
var message="Function Disabled!";
///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}
function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}
if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}
document.oncontextmenu=new Function("return false")

function disableCtrlKeyCombination(e)
{
        //list all CTRL + key combinations you want to disable
        var forbiddenKeys = new Array('a', 'c', 'x', 'v');
        var key;
        var isCtrl;
        if(window.event)
        {
                key = window.event.keyCode;     //IE
                if(window.event.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        else
        {
                key = e.which;     //firefox
                if(e.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        //if ctrl is pressed check if other key is in forbidenKeys array
        if(isCtrl)
        {
                for(i=0; i<forbiddenKeys.length; i++)
                {
                        //case-insensitive comparation
                        if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
                        {
                                alert('Key combination CTRL + ' +String.fromCharCode(key)+' has been disabled.');
                                return false;
                        }
                }
        }
        return true;
}
// --> 
</script>

<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}
</style>

<style>	
.ui-autocomplete.ui-widget {
  font-family:'Open Sans', sans-serif !important;
  font-size: 13px;
}
</style>	

<style type="text/css">
 .no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url(../loader.gif) center no-repeat #fff;
}

.treeview-menu>li>a{
	font-size:12.5px;
}
</style>

<script type="text/javascript">
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
  });
</script>

</head>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);" class="hold-transition skin-blue sidebar-mini">
 
 <div class="se-pre-con"></div>
<div class="wrapper">

  <header class="main-header">

	<a href="./" class="logo" style="background:#FFF">
		<span class="logo-mini"><img src="../logo_small2.png" style="width:100%;height:50px" class="" /></span>
		<span class="logo-lg" id="logo_desktop"><img src="../logo_main1.png" style="margin-top:5px;width:100%;height:40px" class="img-responsive" /></span>
		<span class="logo-lg" id="logo_mobile"><center><img src="../logo_main1.png" style="margin-top:5px;width:50%;height:40px" class="img-responsive" /></center></span>
	</a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
		 <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../avtar2.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo  $_SESSION['rrpl_ship_user']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="../avtar2.png" class="img-circle" alt="User Image">
				<p>
                   <?php echo $_SESSION['rrpl_ship_user']; ?>
                </p>
              </li>
              <!-- Menu Body -->
            
              <!-- Menu Footer-->
              <li class="user-footer">
          
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
       
      </div>
      
	  <ul style="font-size:13px;" class="sidebar-menu" data-widget="tree">          
             
 	<li class="<?php if($menu_page_name=="index.php") {echo "active";} ?>">
        <a href="./"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
     </li>
	 
	<li class="<?php if($menu_page_name=="view_rcvd.php") {echo "active";} ?>">
		<a href="./view_rcvd.php"> <i class="fa fa-television"></i> <span>Shipment Status</span> </a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="rcv_pod.php") {echo "active";} ?>">
		<a href="./rcv_pod.php"> <i class="fa fa-file"></i> <span>Receive POD</span> </a>
     </li>
	 
	<li class="<?php if($menu_page_name=="pod_rcvd.php") {echo "active";} ?>">
		<a href="./pod_rcvd.php"> <i class="fa fa-television"></i> <span>Received POD</span> </a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="reports.php") {echo "active";} ?>">
		<a href="./reports.php"> <i class="fa fa-file"></i> <span>Reports</span> </a>
     </li>
	
	<li>
		<a href="logout.php"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a>
      </li>
		
      </ul>
    </section>
  </aside>