<?php
include_once "header.php";
?>
<div class="content-wrapper">

<section class="content-header">
       <h4>Receive Proof of Delivery (POD) :</h4>
	  
<style>
label{font-size:13px;}
	  
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
</style>
</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#upload_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_rcv_pod.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>			

<div id="result_form"></div> 
   
<script type="text/javascript">
function FetchFMNumber()
{
	var lrno=$('#lrno').val();
	var truck_type=$('#truck_type').val();
	
	if(lrno!='' && truck_type!='')
	{
			$("#loadicon").show();
			jQuery.ajax({
			url: "fetch_fm_no_for_lr.php",
			data: 'lrno=' + lrno + '&type=' + truck_type,
			type: "POST",
			success: function(data) {
				$("#result_form2").html(data);
			},
			error: function() {}
		});
	}
}
</script>  

   
<script type="text/javascript">
function FetchLRData()
{
	var type = $('#truck_type2').val();
	var fm_no = $('#fm_no').val();
	
	if(fm_no!='' && type!='')
	{
			$("#loadicon").show();
			jQuery.ajax({
			url: "fetch_lr_for_pod_rcv.php",
			data: 'fm_no=' + fm_no + '&type=' + type,
			type: "POST",
			success: function(data) {
				$("#result_table").html(data);
			},
			error: function() {}
		});
	}
	else
	{
		alert('Select Voucher Number First.');
	}
}

function ClaimSelection(elem,lr_id)
{
	var lrno = $('#claim_branch_lrno_'+lr_id).val();
	var vou_no = $('#claim_branch_vou_no_'+lr_id).val();
	
	if(elem=='YES')
	{
			$("#loadicon").show();	
				jQuery.ajax({
				url: "./load_claim_modal.php",
				data: 'lrno=' + lrno + '&vou_no=' + vou_no + '&lr_id=' + lr_id,
				type: "POST",
				success: function(data) {
				$("#result_form").html(data);
			},
			 error: function() {}
			});
	}
	else if(elem=='NO')
	{
		$("#loadicon").show();	
		jQuery.ajax({
		url: "./update_claim_status.php",
		data: 'lrno=' + lrno + '&vou_no=' + vou_no + '&lr_id=' + lr_id,
		type: "POST",
		success: function(data) {
			$("#result_form").html(data);
		},
		 error: function() {}
		});
	}
}
</script>  
   
			<div class="form-group col-md-12">
			
			
			 <div class="row">
				
				<div class="form-group col-md-4">
                    <label>Vehicle Type <font color="red">*</font></label>
                    <select id="truck_type" onchange="$('#lrno').val('')" class="form-control" required>
						<option value="">--SELECT--</option>
						<option value="MARKET">MARKET TRUCK</option>
						<option value="OWN">OWN TRUCK</option>
					</select>
                 </div>
				 
				<div class="form-group col-md-4">
                    <label>Enter LR Number <font color="red">*</font></label>
                    <input type="text" oninput="" onblur="FetchFMNumber()" class="form-control" id="lrno" required />
                </div>
				 
				 <div id="result_form2" class="form-group col-md-4">
				 </div>
				 
				 <input type="hidden" id="truck_type2">
				 
				 <div class="form-group col-md-12">
                      <button type="button" disabled onclick="FetchLRData()" id="get_button" class="btn btn-danger">Get Details</button>
                     <button type="button" onclick="location.reload();" class="btn btn-warning pull-right">Reset</button>
                 </div>
				 
             </div>
			 
			<div id="func_result"> </div>
			 
			<div class="row">
				<div class="form-group col-md-12 table-responsive" style="overflow:auto" id="result_table"></div>
			</div>
			
		 </div>
</div>
	</div>
			</div> 
          </div>
        </div>       
    </section>
<?php
include "footer.php";
?>