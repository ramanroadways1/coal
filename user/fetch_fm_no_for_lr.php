<?php
require_once("connect.php");

$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$type = escapeString($conn,($_POST['type']));

if($type=='MARKET')
{
	$fetch_fm_no = Qry($conn,"SELECT fm_no FROM freight_memo WHERE FIND_IN_SET('$lrno',lrnos)");
}
else if($type=='OWN')
{
	$fetch_fm_no = Qry($conn,"SELECT fm_no FROM own_truck_form WHERE FIND_IN_SET('$lrno',lrnos)");
}

if(!$fetch_fm_no){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($fetch_fm_no)==0)
{
	echo "
	<script>
		alert('NO RESULT FOUND..');
		$('#lrno').attr('disabled',false);
		$('#truck_type2').val('');
		$('#truck_type').attr('disabled',false);
		$('#get_button').attr('disabled','false');
		$('#result_form2').html('');
		$('#loadicon').hide();
	</script>";
	exit();
}
else if(numRows($fetch_fm_no)==1)
{
	$row=fetchArray($fetch_fm_no);
	
	echo "
		<label>Select Voucher Number <font color='red'>*</font></label>
		<select id='fm_no' class='form-control' required>
			<option value=''>--SELECT--</option>
			<option value='$row[fm_no]'>$row[fm_no]</option>
		</select>
	<script>
		$('#truck_type2').val('$type');
		$('#lrno').attr('disabled',true);
		$('#truck_type').attr('disabled',true);
		$('#get_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	echo "
		<label>Select Voucher Number <font color='red'>*</font></label>
		<select id='fm_no' class='form-control' required>
			<option value=''>--SELECT--</option>";
			
	while($row=fetchArray($fetch_fm_no))
	{
		echo "<option value='$row[fm_no]'>$row[fm_no]</option>";
	}
	
	echo "
	</select>
	
	<script>
		$('#truck_type2').val('$type');
		$('#lrno').attr('disabled',true);
		$('#truck_type').attr('disabled',true);
		$('#get_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
}
?>