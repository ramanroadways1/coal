<?php
require_once './connect.php'; 

$date = date('Y-m-d');
$timestamp = date("Y-m-d H:i:s");

$lr_id = escapeString($conn,($_POST['lr_id']));

$chk_record = Qry($conn,"SELECT lrno,is_claim_updated FROM lr_entry WHERE main_lr_id='$lr_id'");
	
if(!$chk_record){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> 	
		alert('Error !');
		$('#claim_branch_$lr_id').val('');
		$('#loadicon').hide();
	</script>";
	exit();
}
	
if(numRows($chk_record)==0)
{
	echo "<script> 	
		alert('LR not found !');
		$('#claim_branch_$lr_id').val('');
		$('#loadicon').hide();	
	</script>";
	exit();
}

$row_pod = fetchArray($chk_record);

if($row_pod['is_claim_updated']!="0")
{
	echo "<script> 	 
		alert('Error : Claim status already updated !');
		$('#claim_branch_$lr_id').val('');
		$('#loadicon').hide();	
	</script>";
	exit();
}
	
$update_claim_status = Qry($conn,"UPDATE lr_entry SET is_claim_updated='2' WHERE main_lr_id='$lr_id'");
	
if(!$update_claim_status){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script> 	
		alert('Error !');
		$('#claim_branch_$lr_id').val('');
		$('#loadicon').hide();
	</script>";
	exit();
}
	
	echo "<script>
		alert('Claim status updated !');
		$('#claim_branch_$lr_id').attr('disabled',true);	
		$('#loadicon').hide();	
	</script>"; 
	exit();

?>