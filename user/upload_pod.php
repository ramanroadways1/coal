<?php
require_once('./connect.php');

$branch_sub_user = $_SESSION['user_code'];
$lr_entry_id = escapeString($conn,strtoupper($_POST['lr_entry_id']));
$id = escapeString($conn,strtoupper($_POST['unq_id']));
$lr_id = escapeString($conn,strtoupper($_POST['lr_id']));
$del_date = escapeString($conn,strtoupper($_POST['unload_date']));
$vou_no = escapeString($conn,strtoupper($_POST['vou_no']));
$lrno = escapeString($conn,strtoupper($_POST['lrno']));
$truck_no = escapeString($conn,strtoupper($_POST['truck_no']));
$lr_branch = escapeString($conn,strtoupper($_POST['lr_branch']));
$lr_date = escapeString($conn,strtoupper($_POST['lr_date']));
$consignor = escapeString($conn,strtoupper($_POST['consignor']));
$con1_id = escapeString($conn,strtoupper($_POST['con1_id']));
$fm_id = escapeString($conn,strtoupper($_POST['fm_id']));
$fm_branch = escapeString($conn,strtoupper($_POST['fm_branch']));
$freight_amount = escapeString($conn,strtoupper($_POST['freight_amount']));
$unloading_weight = escapeString($conn,strtoupper($_POST['unload_weight']));
$trip_no = escapeString($conn,strtoupper($_POST['trip_no']));
$trip_id = escapeString($conn,strtoupper($_POST['trip_id']));
$lr_weight = escapeString($conn,strtoupper($_POST['lr_weight']));
$truck_type = escapeString($conn,strtoupper($_POST['truck_type']));
$deduction = escapeString($conn,($_POST['other_deduction']));
$lr_bill_party_id = escapeString($conn,($_POST['billparty_id']));
$lr_bill_branch = escapeString($conn,($_POST['billing_branch']));
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$diff=sprintf("%0.2f",(($lr_weight-$unloading_weight)/$lr_weight*100));
 
// echo "<script>
			// alert('Error : Trip id or Trip number not found.');
			// $('#loadicon').hide();
			// $('#rcv_button$id').attr('disabled', false);
		// </script>";
		// exit();

if($truck_type!='MARKET')
{
	if($trip_id=='' || $trip_id==0 || $trip_no=='' || $trip_no==0)
	{
		echo "<script>
			alert('Error : Trip id or Trip number not found.');
			$('#loadicon').hide();
			$('#rcv_button$id').attr('disabled', false);
		</script>";
		exit();
	}
}

if($truck_type=='MARKET')
{
	$table_name="freight_memo";
	$gps_col_name="gps_install_type";
}
else
{
	$table_name="own_truck_form";
	$gps_col_name="id";
}


$chk_claim = Qry($conn,"SELECT id FROM lr_entry WHERE id='$lr_entry_id' AND is_claim_updated='0'");

if(!$chk_claim){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_claim)>0)
{
	echo "<script>
		alert('Update claim first !');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

$chk_gps_type = Qry($conn,"SELECT fm_no,`$gps_col_name` as gps_install_type FROM `$table_name` WHERE id='$fm_id'");

if(!$chk_gps_type){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_gps_type)==0)
{
	echo "<script>
		alert('Voucher not found !');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

$row_fm = fetchArray($chk_gps_type);

if($row_fm['fm_no']!=$vou_no)
{
	echo "<script>
		alert('Voucher number not verified !');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

if($row_fm['gps_install_type']=='' AND $truck_type=='MARKET')
{
	echo "<script>
		alert('GPS record not found !');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

if($truck_type=='MARKET')
{
	if($row_fm['gps_install_type']=='TRIP')
	{
		if(!isset($_POST['gps_selection']))
		{
			echo "<script>
				alert('GPS device option not selected !');
				$('#loadicon').hide();
				$('#rcv_button$id').attr('disabled', false);
			</script>";
			exit();
		}
		
		if($_POST['gps_selection']=='YES'){
			$gps_amount = "0";
		}
		else{
			$gps_amount = "3000";
		}
	}
	else
	{
		$gps_amount = "0";
	}
}
else
{
	$gps_amount = "0";
}

$chk_pod = Qry($conn,"SELECT id FROM rcv_pod WHERE fm_no='$vou_no' AND lrno='$lrno'");

if(!$chk_pod){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_pod)>0)
{
	echo "<script>
		alert('POD already received for LR number : $lrno ($vou_no) !');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

if($truck_type=='MARKET')
{
	if($con1_id=='' || $con1_id==0)
	{
		echo "<script>
			alert('Consignor not found !');
			$('#loadicon').hide();
			$('#rcv_button$id').attr('disabled', false);
		</script>";
		exit();
	}
}
else
{
	if((substr($lrno,3,1)!='M') AND ($con1_id=='' || $con1_id==0))
	{
		echo "<script>
			alert('Consignor not found !');
			$('#loadicon').hide();
			$('#rcv_button$id').attr('disabled', false);
		</script>";
		exit();
	}
}

if(substr($lrno,3,1)=='M')
{
	$get_bill_branch_mb = Qry($conn,"SELECT billing_branch FROM rrpl_database.mkt_bilty WHERE bilty_no='$vou_no'");
	if(!$get_bill_branch_mb){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($get_bill_branch_mb)==0)
	{
		echo "<script>
			alert('Market bilty not found !');
			$('#loadicon').hide();
			$('#rcv_button$id').attr('disabled', false);
		</script>";
		exit();
	}
	
	$row_bill_branch_mb = fetchArray($get_bill_branch_mb);
	$billing_branch = $row_bill_branch_mb['billing_branch'];
	$billing_party_id = "-1";
	
	if($billing_branch==$branch){
		$self_var=1;
	}
	else{
		$self_var=0;
	}
}
else
{
	$bill_func = GetBillPartyBranch($conn,$branch,$con1_id,$lr_bill_branch,$lr_bill_party_id);

	$billing_branch = $bill_func[0];
	$billing_party_id = $bill_func[1];
	$self_var = $bill_func[2];
}

if(count($_FILES['upload_file']['name'])==0)
{
	echo "<script>
		alert('No attachment found : POD Copy !!');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

if(count($_FILES['unload_slip']['name'])==0)
{
	echo "<script>
		alert('No attachment found : Unloading Slip !!');
		$('#loadicon').hide();
		$('#rcv_button$id').attr('disabled', false);
	</script>";
	exit();
}

if(count($_FILES['upload_file']['name'])>0)
{
	if($con1_id=='56')
	{
		$valid_types = array("application/pdf");
		$valid_types_text = "application/pdf";
	}
	else
	{
		$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");
		$valid_types_text = "image/jpg, image/jpeg, image/bmp, image/gif, image/png, application/pdf";
	}
	
	foreach($_FILES['upload_file']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			echo "<script>
				alert('Uploaded file format is not allowed. Supported formats are : $valid_types_text !');
				$('#loadicon').hide();
				$('#rcv_button$id').attr('disabled', false);
			</script>";
			exit();
		}
    }
}

if(count($_FILES['unload_slip']['name'])>0)
{
	if($con1_id=='56')
	{
		$valid_types = array("application/pdf");
		$valid_types_text = "application/pdf";
	}
	else
	{
		$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");
		$valid_types_text = "image/jpg, image/jpeg, image/bmp, image/gif, image/png, application/pdf";
	}
	
	foreach($_FILES['unload_slip']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			echo "<script>
				alert('Uploaded file format is not allowed. Supported formats are : $valid_types_text !');
				$('#loadicon').hide();
				$('#rcv_button$id').attr('disabled', false);
			</script>";
			exit();
		}
    }
}

	for($i=0; $i<count($_FILES['upload_file']['name']);$i++) 
	{
		$fix_Name = date('dmYHis').mt_rand().$i;
		$sourcePath = $_FILES['upload_file']['tmp_name'][$i];
		
		if($truck_type=='MARKET'){
			$shortname = "pod_copy/".$fix_Name.".".pathinfo($_FILES['upload_file']['name'][$i],PATHINFO_EXTENSION);
			$targetPath = "../../b5aY6EZzK52NA8F/pod_copy/".$fix_Name.".".pathinfo($_FILES['upload_file']['name'][$i],PATHINFO_EXTENSION);
		}
		else{
			$shortname = "pod/".$fix_Name.".".pathinfo($_FILES['upload_file']['name'][$i],PATHINFO_EXTENSION);
			$targetPath = "../../diary/close_trip/pod/".$fix_Name.".".pathinfo($_FILES['upload_file']['name'][$i],PATHINFO_EXTENSION);
		}
		
		if($_FILES['upload_file']['type'][$i]!='application/pdf')
		{		
			ImageUpload(800,800,$_FILES['upload_file']['tmp_name'][$i]);
		}
		
		   if(move_uploaded_file($sourcePath, $targetPath))
		   {
				$files[] = $shortname;
		   }
		   else
		   {
				echo "<script> 
					alert('Upload Failed : POD copy !');
					$('#loadicon').hide();
					$('#rcv_button$id').attr('disabled', false);
				</script>";
				exit();		
		   }
	}

	$file_name_pod = implode(',',$files);
	
	for($i2=0; $i2<count($_FILES['unload_slip']['name']);$i2++) 
	{
		$fix_Name2 = date('dmYHis').mt_rand().$i2;
		$sourcePath2 = $_FILES['unload_slip']['tmp_name'][$i2];
		$shortname2 = "unloading_slip/".$fix_Name2.".".pathinfo($_FILES['unload_slip']['name'][$i2],PATHINFO_EXTENSION);
		$targetPath2 = "../unloading_slip/".$fix_Name2.".".pathinfo($_FILES['unload_slip']['name'][$i2],PATHINFO_EXTENSION);
		
			if($_FILES['unload_slip']['type'][$i2]!='application/pdf')
			{		
				ImageUpload(800,800,$_FILES['unload_slip']['tmp_name'][$i2]);
			}
		
		   if(move_uploaded_file($sourcePath2, $targetPath2))
		   {
				$files_slip[] = $shortname2;
		   }
		   else
		   {
				echo "<script> 
					alert('Upload Failed : Unloading Slip !');
					$('#loadicon').hide();
					$('#rcv_button$id').attr('disabled', false);
				</script>";
				exit();		
		   }
	}

	$file_name_unload_slip = implode(',',$files_slip);	
	
StartCommit($conn);
$flag = true;	

if($truck_type=='OWN')
{
	$insert_pod_diary = Qry($conn,"INSERT INTO dairy.rcv_pod(trip_id,tno,vou_no,lrno,trip_no,date,copy,branch,branch_user,
	consignor_id,fromstation,tostation,lr_date,del_date,billing_party,billing_ofc,self,exdate,timestamp) VALUES ('$trip_id','$truck_no',
	'$vou_no','$lrno','$trip_no','$date','$file_name_pod','$branch','$branch_sub_user','$con1_id','$branch','$billing_branch',
	'$lr_date','$del_date','$billing_party_id','$billing_branch','$self_var','$timestamp','$timestamp')");

	if(!$insert_pod_diary){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$chk_claim_lr = Qry($conn,"SELECT id FROM rrpl_database.claim_records WHERE lrno='$lrno' AND vou_no='$vou_no'");

if(!$chk_claim_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_claim_lr)>0)
{
	$claim_branch="1";
	$row_claim = fetchArray($chk_claim_lr);
	$claim_id = $row_claim['id'];
}
else
{
	$claim_branch="2";
}

$insert_pod = Qry($conn,"INSERT INTO rrpl_database.rcv_pod(trip_id,tno,trip_no,frno,veh_type,lrno,lr_id,consignor_id,branch,
branch_user,fm_date,fm_amount,pod_branch,pod_copy,pod_date,del_date,deduction,billing_party,billing_ofc,claim_branch,
timestamp,self,exdate,fromstation,tostation) VALUES ('$trip_id','$truck_no','$trip_no','$vou_no','$truck_type','$lrno',
'$lr_id','$con1_id','$branch','$branch_sub_user','$lr_date','$freight_amount','$fm_branch','$file_name_pod',
'".date('Y-m-d')."','$del_date','$deduction','$billing_party_id','$billing_branch','$claim_branch','$timestamp','$self_var',
'".date('Y-m-d')."','$branch','$billing_branch')");

if(!$insert_pod){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$pod_id = getInsertID($conn);

if($claim_branch=="1")
{
	$update_pod_id_claim = Qry($conn,"UPDATE rrpl_database.claim_records_desc SET pod_id='$pod_id' WHERE claim_id='$claim_id'");
	
	if(!$update_pod_id_claim){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_pod_id_claim2 = Qry($conn,"UPDATE rrpl_database.claim_records SET pod_id='$pod_id' WHERE id='$row_claim[id]'");
	
	if(!$update_pod_id_claim2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_pod_id_claimTxn = Qry($conn,"UPDATE rrpl_database.claim_book_trans SET pod_id='$pod_id' WHERE claim_id='$claim_id'");
	
	if(!$update_pod_id_claimTxn){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$insert_pod2 = Qry($conn,"INSERT INTO rcv_pod(fm_no,pod_type,lrno,deduction,lr_weight,rcvd_weight,weight_diff,unloading_slip,
pod_copy,unload_date,gps_charge,branch,branch_user,timestamp) VALUES ('$vou_no','$truck_type','$lrno','$deduction',
'$lr_weight','$unloading_weight','$diff','$file_name_unload_slip','$file_name_pod','$del_date','$gps_amount','$branch',
'$branch_sub_user','$timestamp')");

if(!$insert_pod2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(substr($lrno,3,1)=='M')
{
	$update_pod_date = Qry($conn,"UPDATE rrpl_database.mkt_bilty SET pod_date='$date' WHERE bilty_no='$lrno'");
	
	if(!$update_pod_date){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$update_pod_date = Qry($conn,"UPDATE rrpl_database.lr_sample SET pod_rcv_date='".date('Y-m-d')."' WHERE id='$lr_id'");
	
	if(!$update_pod_date){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$update_pod_date_lr_entry = Qry($conn,"UPDATE lr_entry SET pod_rcv_date='$timestamp' WHERE id='$lr_entry_id'");
	
	if(!$update_pod_date_lr_entry){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($truck_type=='MARKET')
{
	$col_name_pod = "market_pod_date";
	$table_name_pod2 = "freight_memo";
}
else
{
	$col_name_pod = "pod_rcv_date_own";
	$table_name_pod2 = "own_truck_form";
	
	$updatePodCount = Qry($conn,"UPDATE dairy.trip SET pod=pod+1,pod_date='$timestamp' WHERE id='$trip_id'");

	if(!$updatePodCount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
 
$update_pod_date_fm_coal = Qry($conn,"UPDATE `$table_name_pod2` SET pod='1',pod_date='".date('Y-m-d')."' WHERE id='$fm_id'");
if(!$update_pod_date_fm_coal){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($truck_type=='MARKET')
{
	$delete_from_pending_pod_list = Qry($conn,"DELETE FROM rrpl_database._pending_lr_list WHERE vou_id='$fm_id'");
	if(!$delete_from_pending_pod_list){ 
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$updatePodCount = Qry($conn,"UPDATE rrpl_database._pending_lr SET lr_pod_pending=lr_pod_pending-1 WHERE branch='$fm_branch'");
	if(!$updatePodCount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('POD Copy Received. LR Number : $lrno !');
		$('#rcv_button$id').attr('disabled',true);	
		$('#unload_date_id_$id').attr('disabled',true);	
		$('#unload_wt_id_$id').attr('disabled',true);	
		$('#unload_slip_id_$id').attr('disabled',true);	
		$('#pod_copy_id_$id').attr('disabled',true);	
		$('#gps_sel_id_$id').attr('disabled',true);	
		$('#loadicon').hide();	
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Error While Processing Request. !');
		$('#rcv_button$id').attr('disabled',false);	
		$('#loadicon').hide();	
	</script>";
	exit();
}
?>
