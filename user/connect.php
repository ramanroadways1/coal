<?php
session_start();

include($_SERVER['DOCUMENT_ROOT']."/_connect.php");
date_default_timezone_set('Asia/Kolkata');
$timestamp=date("Y-m-d H:i:s");

if(!isset($_SESSION['rrpl_ship_user']))
{
	echo "<script>
		window.location.href='/';
	</script>";
	exit();
}

$branch = $_SESSION['rrpl_ship_user'];
$branch_sub_user = $_SESSION['user_code'];

$conn=mysqli_connect($host,$username,$password,"ship");

if(!$conn){
    echo "<span style='font-family:Verdana'>Database Connection failed: <b><font color='red'>" .mysqli_connect_error()."</b></font></span>";
	exit();
}
?>