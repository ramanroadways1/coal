<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Received POD Summary :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
			
<div class="row">
 
	<div class="form-group col-md-12">
		
		<div class="form-group col-md-3">
			<label>LR Number</label>
			<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" id="lrno" name="lrno" class="form-control" placeholder="LR Number">
		</div>

		<div class="form-group col-md-3">
			<label>From Date</label>
			<input type="date" name="from_date" id="from_date" class="form-control" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
		</div>
		
		<div class="form-group col-md-3">
			<label>To Date</label>
			<input type="date" name="to_date" id="to_date" class="form-control" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
		</div>
		
		 <div class="form-group col-md-3">   
			<label>&nbsp;</label>
			<br />
            <button type="button" onclick="LoadPODs()" id="get_button" class="btn btn-md btn-primary">Get records !</button>
        </div> 
		
	</div>
	
	<div class="form-group col-md-12">
		<div class="form-group col-md-12 table-responsive" id="pod_result">
		</div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<script>
function LoadPODs()
{
	var lrno = $('#lrno').val();
	var from_date = $('#from_date').val();
	var to_date = $('#to_date').val();
	
	$("#loadicon").show();
	jQuery.ajax({
			url: "get_pods.php",
			data: 'lrno=' + lrno + '&from_date=' + from_date + '&to_date=' + to_date,
			type: "POST",
			success: function(data) {
			$("#pod_result").html(data);
			   $('#example').DataTable({ 
                 "destroy": true,
              });
			},
			error: function() {}
	});
}
</script>	

<?php
include "footer.php";
?>