<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		View Received Shipment :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
		
			
<div class="row">
 <div class="form-group col-md-12 table-responsive">
	 
        <table id="example" class="table table-bordered" style="font-size:13px;">
          <thead>
		  <tr>
				<th>#</th>
				<th>Shipment<br>Number</th>
				<th>ShipName</th>
				<th>Voyage<br>No</th>
				<th>Number <br>of BL</th>
				<th>Sipment<br>Date</th>
				<th>BL Wt.<br>(in MTS)</th>
				<th>Draft Wt.<br>(in MTS)</th>
				<th>Bal Wt.<br>(in MTS)</th>
				<th>Date <br> of Arrival</th>
				<th>Time <br> of Arrival</th>
			</tr>
          </thead>
          <tbody>
            <?php
              $sql = Qry($conn,"SELECT shipment.id,unq_id,voyage_no,total_bl,bl_weight,date,name,draft_weight,bal_weight,
			  date_of_arrival,time_of_arrival FROM shipment,vessel_name where rcv=1 AND vessel_name.id=vessel_name AND 
			  shipment.active='1' ORDER BY shipment.id ASC");
              
			 if(!$sql){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
					echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			{
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
                echo "<tr>
				  <td>$sn</td>
				  <td>$row[unq_id]</td>
				  <td>$row[name]</td>
				  <td>$row[voyage_no]</td>
				  <td>$row[total_bl]</td>
				 <td>".date("d-m-y",strtotime($row['date']))."</td>				 
				 <td>$row[bl_weight]</td>
				 <td>".sprintf("%.2f",$row['draft_weight'])."</td>
				  <td>".sprintf("%.2f",$row['bal_weight'])."</td>
				 <td>".date("d-m-y",strtotime($row['date_of_arrival']))."</td>
				  <td>".date("h:i A",strtotime($row['time_of_arrival']))."</td>
				 </tr>";
			$sn++;		
              }
			}
            ?>
		</tbody>	
        </table>
      </div>

  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<script>	
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>