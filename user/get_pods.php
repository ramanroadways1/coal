<?php
require_once("connect.php");

$lrno=escapeString($conn,strtoupper($_POST['lrno']));
$from_date=escapeString($conn,strtoupper($_POST['from_date']));
$to_date=escapeString($conn,strtoupper($_POST['to_date']));

$where_condition = "";

if($lrno!='')
{
	$where_condition.="lrno='$lrno'";
}
else if($from_date!='' AND $to_date!='')
{
	$where_condition.="date(timestamp) BETWEEN '$from_date' AND '$to_date'";
}
else
{
	echo "<center><font color='red'>Atleast one search parameter is required !! LR number or dates.</font></center>
	<script>$('#loadicon').hide();</script>";
	exit();
}

$sql = Qry($conn,"SELECT * FROM rcv_pod WHERE $where_condition AND branch='$_SESSION[rrpl_ship_user]'");
              
if(!$sql){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}
			  
if(numRows($sql)==0)
{
	echo "<center><font color='red'>No records found for given search criteria.</font></center>
	<script>$('#loadicon').hide();</script>";
	exit();
}
?>
<table id="example" class="table table-bordered table-striped" style="font-size:12px;">
          <thead>
          <tr>
				<th>#</th>
				<th>Vou No</th>
				<th>LR_No</th>
				<th>LR Wt.</th>
				<th>Unload Wt.</th>
				<th>Weight Diff.</th>
				<th>Unload Slip</th>
				<th>POD Copy</th>
				<th>Unload Date</th>
				<th>Rcvd On</th>
			</tr>
          </thead>
          <tbody>
          <?php
           $sn=1;
			  
			while($row = fetchArray($sql))
			{
                echo 
                "<tr>
				  <td>$sn</td>
				  <td>$row[fm_no]</td>
				  <td>$row[lrno]</td>
				  <td>$row[lr_weight]</td>
				  <td>$row[rcvd_weight]</td>
				  <td>$row[weight_diff]</td>
				  <td><a target='_blank' style='color:#FFF' class='btn btn-xs bg-primary' href='view_unload_slip.php?file=$row[unloading_slip]'>View</a></td>
				  <td><a target='_blank' style='color:#FFF' class='btn btn-xs bg-primary' href='view_pod_copy.php?file=$row[pod_copy]'>View</a></td>
				  <td>".date("d-m-y",strtotime($row['unload_date']))."</td>				 
				  <td>".date("d-m-y H:i A",strtotime($row['timestamp']))."</td>				 
			   </tr>";
			 $sn++;	
            }
			?>
			</tbody>
 </table>
 
 <script>$('#loadicon').hide();</script>
 
 
<script>	
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>