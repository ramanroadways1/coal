<?php 
require_once("connect.php");

$output = '';

$report=escapeString($conn,$_POST['report']);

$from_date=escapeString($conn,$_POST['from_date']);
$to_date=escapeString($conn,$_POST['to_date']);
$bid=escapeString($conn,$_POST['bid']);
$oid=escapeString($conn,$_POST['oid']);
$destination=escapeString($conn,$_POST['destination']);

if($report=='DISPATCH')
{
$query = "SELECT v.name as ship_name,s.voyage_no,l.lrno,l.tno,l.lr_type,l.lr_date,l.branch as lr_branch,l.company,stn1.name as from_loc,l.from_pincode,
stn2.name as to_loc,l.to_pincode,l.approx_distance,con1.name as consignor,con2.name as consignee,l.allow_slip_no,l.trip_no,l.actual_wt,
l.charge_wt,l.bill_rate,l.bill_amount,l.t_type,l.po_no,l.do_no,l.boe_date,l.inv_no,l.ship_no,l.item,l.hsn_code,l.articles,l.goods_desc,
l.goods_value,l.igst_rate,l.igst_amount,l.total_value,l.loading_slip,ewb.eway_bill_no,ewb.gen_on,pod.rcvd_weight,pod.unloading_slip,
pod.pod_copy,pod.unload_date,pod.branch as pod_rcv_branch,pod.timestamp as pod_rcv_date
FROM lr_entry as l 
LEFT OUTER JOIN shipment as s ON s.id=l.shipment_id
LEFT OUTER JOIN vessel_name as v ON v.id=s.vessel_name
LEFT OUTER JOIN rrpl_database.station as stn1 ON stn1.id=l.from_loc
LEFT OUTER JOIN rrpl_database.station as stn2 ON stn2.id=l.to_loc
LEFT OUTER JOIN rrpl_database.consignor as con1 ON con1.id=l.con1
LEFT OUTER JOIN rrpl_database.consignee as con2 ON con2.id=l.con2
LEFT OUTER JOIN eway_bill_log as ewb ON ewb.lrno=l.lrno
LEFT OUTER JOIN rcv_pod as pod ON pod.lrno=l.lrno
WHERE lr_date BETWEEN '$from_date' AND '$to_date' AND l.to_loc='$destination' ORDER BY l.id ASC";

$result = Qry($conn,$query);

if(!$result)
{
	echo mysqli_error($conn);
	exit();
}

if(numRows($result) > 0)
 {
$output .= '
   <table border="1">  
                    <tr>  
						 <th>Ship Name</th>  
                         <th>LR_No</th>  
                         <th>Truck_No</th>  	
                         <th>LR_Type</th>  
                         <th>LR_Date</th>  
                         <th>Branch</th>  
                         <th>Company</th>  
                         <th>From_Loc</th>  
                         <th>From<br>PinCode</th>  
                         <th>To_Loc</th>  
						 <th>To<br>PinCode</th>  
						 <th>Distance</th>  
						 <th>Cosignor</th>  
						 <th>Cosignee</th>  
						 <th>Allow<br>Slip_No</th>  
						 <th>Trip_No</th>  
						 <th>Actual<br>Weight</th>  
						 <th>Charge<br>Weight</th>  
						 <th>Billing<br>Rate</th>  
						 <th>Billing<br>Amount</th>  
						 <th>Truck<br>Type</th>  
						 <th>PO<br>Number</th>  
						 <th>DO/BOE<br>Number</th>  
						 <th>Invoice<br>No</th>  
						 <th>BOE<br>Date</th>  
						 <th>Item</th>  
						 <th>HSN Code</th>  
						 <th>Articles</th>  
						 <th>Goods<br>Description</th>  
						 <th>Goods<br>Value</th>  
						 <th>Igst<br>Rate</th>  
						 <th>Igst<br>Value</th>  
						 <th>Total<br>Value</th>  
						 <th>Loading<br>Slip</th>  
						 <th>Ewb<br>Number</th>  
						 <th>Generated<br>On</th>  
						 <th>Unloading<br>Date</th>  
						 <th>Unloading<br>Weight</th>  
						 <th>Unloading<br>Slip</th>  
						 <th>POD<br>Copy</th>  
						 <th>POD<br>Branch</th>  
						 <th>Rcvd<br>On</th>  
                    </tr>
  ';
  while($row = fetchArray($result))
  {
	$location_name=$row['to_loc']; 
	$output .= '
    <tr>  
		<td>'.$row["ship_name"].'('.$row["voyage_no"].')'.'</td>  
		<td>'.$row["lrno"].'</td>  
		<td>'.$row["tno"].'</td>  
		<td>'.$row["lr_type"].'</td>  
		<td>'.$row["lr_date"].'</td>  
		<td>'.$row["lr_branch"].'</td>  
		<td>'.$row["company"].'</td>  
		<td>'.$row["from_loc"].'</td>  
		<td>'.$row["from_pincode"].'</td>  
		<td>'.$row["to_loc"].'</td>  
		<td>'.$row["to_pincode"].'</td>  
		<td>'.$row["approx_distance"].'</td>  
		<td>'.$row["consignor"].'</td>  
		<td>'.$row["consignee"].'</td>  
		<td>'."'".$row["allow_slip_no"].'</td>  
		<td>'."'".$row["trip_no"].'</td>  
		<td>'.$row["actual_wt"].'</td>  
		<td>'.$row["charge_wt"].'</td>  
		<td>'.$row["bill_rate"].'</td>  
		<td>'.$row["bill_amount"].'</td>  
		<td>'.$row["t_type"].'</td>  
		<td>'."'".$row["po_no"].'</td>  
		<td>'."'".$row["do_no"].'</td>  
		<td>'."'".$row["inv_no"].'</td>  
		<td>'.$row["boe_date"].'</td>  
		<td>'.$row["item"].'</td>  
		<td>'."'".$row["hsn_code"].'</td>  
		<td>'.$row["articles"].'</td>  
		<td>'.$row["goods_desc"].'</td>  
		<td>'.$row["goods_value"].'</td>  
		<td>'.$row["igst_rate"].'</td>  
		<td>'.$row["igst_amount"].'</td>  
		<td>'.$row["total_value"].'</td>  
		<td><a href="https://rrpl.online/coal/'.$row["loading_slip"].'">View</a></td>  
		<td>'."'".$row["eway_bill_no"].'</td>  
		<td>'.$row["gen_on"].'</td>  
		<td>'.$row["unload_date"].'</td>  
		<td>'.$row["rcvd_weight"].'</td>  
		<td><a href="https://rrpl.online/coal/'.$row["unloading_slip"].'">View</a></td>  
		<td><a href="https://rrpl.online/coal/'.$row["pod_copy"].'">View POD</a></td>  
		<td>'.$row["pod_rcv_branch"].'</td>  
		<td>'.$row["pod_rcv_date"].'</td>  
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename='.$location_name.'_DISPATCH_REPORT.xls');
  echo $output;
 }
 else
 {
	 echo "<script>
		alert('No result found..');
		window.location.href='./';
		</script>";
 }

closeConnection($conn);

}
else if($report=='STATEMENT')
{
$col_name = escapeString($conn,$_POST['what_to_search']);
$party_id = escapeString($conn,$_POST['party_id']);

if($col_name=='' || $party_id=='')
{
	echo "<script>
		alert('Invalid data.');
		window.location.href='./report.php';
	</script>";
	exit();
}

$query = "SELECT f.fm_no,f.tno,f.date,f.weight,f.rate,f.freight,f.lrnos,f.pod_date,a.adv_party,a.party_name,a.party_pan,a.loading,a.other
,a.tds,a.total_freight,a.total_freight,a.total_adv,a.cash,a.cheque,a.diesel,a.rtgs,a.branch as adv_branch,a.date as adv_date,
b.party_name as bal_party,b.party_pan as bal_party_pan,b.unloading,b.other as bal_other,b.tds as bal_tds,b.claim,b.late_pod,b.total_bal,
b.cash as bal_cash,b.cheque as bal_cheque,b.diesel as bal_diesel,b.rtgs as bal_rtgs,b.branch as bal_branch,b.date as bal_date,p.rcvd_weight,
p.unload_date,p.branch 
FROM freight_memo as f 
LEFT OUTER JOIN freight_memo_adv as a ON a.fm_no=f.fm_no
LEFT OUTER JOIN freight_memo_bal as b ON b.fm_no=f.fm_no
LEFT OUTER JOIN rcv_pod as p ON p.fm_no=f.fm_no
WHERE `$col_name`='$party_id' AND f.branch='$branch'";

$result = Qry($conn,$query);

if(!$result)
{
	echo mysqli_error($conn);
	exit();
}

	if(numRows($result) > 0)
	{
	$output .= '
   <table border="1">  
                    <tr>  
						 <th>Ship Name</th>  
                         <th>LR_No</th>  
                         <th>Truck_No</th>  	
                         <th>LR_Type</th>  
                         <th>LR_Date</th>  
                         <th>Branch</th>  
                         <th>Company</th>  
                         <th>From_Loc</th>  
                         <th>From<br>PinCode</th>  
                         <th>To_Loc</th>  
						 <th>To<br>PinCode</th>  
						 <th>Distance</th>  
						 <th>Cosignor</th>  
						 <th>Cosignee</th>  
						 <th>Allow<br>Slip_No</th>  
						 <th>Trip_No</th>  
						 <th>Actual<br>Weight</th>  
						 <th>Charge<br>Weight</th>  
						 <th>Billing<br>Rate</th>  
						 <th>Billing<br>Amount</th>  
						 <th>Truck<br>Type</th>  
						 <th>PO<br>Number</th>  
						 <th>DO/BOE<br>Number</th>  
						 <th>Invoice<br>No</th>  
						 <th>BOE<br>Date</th>  
						 <th>Item</th>  
						 <th>HSN Code</th>  
						 <th>Articles</th>  
						 <th>Goods<br>Description</th>  
						 <th>Goods<br>Value</th>  
						 <th>Igst<br>Rate</th>  
						 <th>Igst<br>Value</th>  
						 <th>Total<br>Value</th>  
						 <th>Loading<br>Slip</th>  
						 <th>Ewb<br>Number</th>  
						 <th>Generated<br>On</th>  
						 <th>Unloading<br>Date</th>  
						 <th>Unloading<br>Weight</th>  
						 <th>Unloading<br>Slip</th>  
						 <th>POD<br>Copy</th>  
						 <th>POD<br>Branch</th>  
						 <th>Rcvd<br>On</th>  
                    </tr>
  ';
  while($row = fetchArray($result))
  {
	$output .= '
    <tr>  
		<td>'.$row["ship_name"].'('.$row["voyage_no"].')'.'</td>  
		<td>'.$row["lrno"].'</td>  
		<td>'.$row["tno"].'</td>  
		<td>'.$row["lr_type"].'</td>  
		<td>'.$row["lr_date"].'</td>  
		<td>'.$row["lr_branch"].'</td>  
		<td>'.$row["company"].'</td>  
		<td>'.$row["from_loc"].'</td>  
		<td>'.$row["from_pincode"].'</td>  
		<td>'.$row["to_loc"].'</td>  
		<td>'.$row["to_pincode"].'</td>  
		<td>'.$row["approx_distance"].'</td>  
		<td>'.$row["consignor"].'</td>  
		<td>'.$row["consignee"].'</td>  
		<td>'."'".$row["allow_slip_no"].'</td>  
		<td>'."'".$row["trip_no"].'</td>  
		<td>'.$row["actual_wt"].'</td>  
		<td>'.$row["charge_wt"].'</td>  
		<td>'.$row["bill_rate"].'</td>  
		<td>'.$row["bill_amount"].'</td>  
		<td>'.$row["t_type"].'</td>  
		<td>'."'".$row["po_no"].'</td>  
		<td>'."'".$row["do_no"].'</td>  
		<td>'."'".$row["inv_no"].'</td>  
		<td>'.$row["boe_date"].'</td>  
		<td>'.$row["item"].'</td>  
		<td>'."'".$row["hsn_code"].'</td>  
		<td>'.$row["articles"].'</td>  
		<td>'.$row["goods_desc"].'</td>  
		<td>'.$row["goods_value"].'</td>  
		<td>'.$row["igst_rate"].'</td>  
		<td>'.$row["igst_amount"].'</td>  
		<td>'.$row["total_value"].'</td>  
		<td><a href="https://rrpl.online/coal/'.$row["loading_slip"].'">View</a></td>  
		<td>'."'".$row["eway_bill_no"].'</td>  
		<td>'.$row["gen_on"].'</td>  
		<td>'.$row["unload_date"].'</td>  
		<td>'.$row["rcvd_weight"].'</td>  
		<td><a href="https://rrpl.online/coal/'.$row["unloading_slip"].'">View</a></td>  
		<td><a href="https://rrpl.online/coal/'.$row["pod_copy"].'">View POD</a></td>  
		<td>'.$row["pod_rcv_branch"].'</td>  
		<td>'.$row["pod_rcv_date"].'</td>  
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=DISPATCH_REPORT.xls');
  echo $output;
 }
 else
 {
	 echo "<script>
		alert('No result found..');
		window.location.href='./';
		</script>";
 }

	closeConnection($conn);
}
else if($report=="BROKER_A" || $report=="BROKER_B" || $report=="BROKER_FM_DATE")
{

$bid=escapeString($conn,$_POST['bid']);

if($report=="BROKER_A")
{
	$col_name="a.date";
}
else if($report=='BROKER_B')
{
	$col_name="b.date";
}
else
{
	$col_name="f.date";
}

$query = "SELECT f.fm_no,f.tno,f.company,f.date as fm_date,f.weight,f.rate,f.freight,f.lrnos,f.branch as fm_branch,f.pod as pod_rcvd,
a.party_name as adv_party,
a.party_pan as adv_pan,a.loading,a.dsl_inc,a.other as adv_other,a.tds as adv_tds,a.gps_deposit,a.total_freight,a.total_adv,a.cash as adv_cash,
a.cheque as adv_chq,a.chqno as adv_chq_no,a.diesel as adv_diesel,a.rtgs as adv_rtgs,r.crn as adv_crn,r.bank as adv_utr,
r.utr_date as adv_utr_date,a.branch as adv_branch,a.date as adv_date,a.narration as adv_nrr,
b.party_name as bal_party,b.party_pan as bal_pan,b.unloading,b.other as bal_other,b.tds as bal_tds,b.gps_charges,
b.gps_deposit_return,b.claim,b.late_pod,b.total_bal,
b.cash as bal_cash,b.cheque as bal_chq,b.diesel as bal_diesel,b.rtgs as bal_rtgs,r2.crn as bal_crn,r2.bank as bal_utr,
r2.utr_date as bal_utr_date,b.branch as bal_branch,b.date as bal_date,b.narration as bal_nrr,p.lr_weight,p.rcvd_weight,p.unload_date 
FROM `freight_memo` as f 
LEFT OUTER JOIN freight_memo_adv as a ON a.fm_no=f.fm_no 
LEFT OUTER JOIN freight_memo_bal as b ON b.fm_no=f.fm_no 
LEFT OUTER JOIN rrpl_database.rtgs_fm as r ON r.fno=f.fm_no and r.type='ADVANCE' 
LEFT OUTER JOIN rrpl_database.rtgs_fm as r2 ON r2.fno=b.fm_no and r2.type='BALANCE' 
LEFT OUTER JOIN rcv_pod as p ON p.fm_no=a.fm_no 
WHERE $col_name BETWEEN '$from_date' AND '$to_date' AND f.broker='$bid'";

$result = Qry($conn,$query);

if(!$result)
{
	echo mysqli_error($conn);
	exit();
}

if(numRows($result) > 0)
 {
	 $output .= '
		<table border="1">  
                    <tr>  
						 <th>FM No</th>  
                         <th>Truck No</th>  
                         <th>Company</th>  	
                         <th>FM Date</th>  
                         <th>LR<br>Weight</th>  
                         <th>Unloading<br>Weight</th>  
                         <th>Unloading<br>Date</th>  
                         <th>Rate</th>  
                         <th>Freight</th>  
                         <th>LR No</th>  
                         <th>FM<br>Branch</th>  
                         <th>Advance<br>Party</th>  
                         <th>Advance<br>Party PAN</th>  
						 <th>Loading(+)</th>  
						 <th>Diesel<br>Inc(+)</th>  
						 <th>Other_A(-)</th>  
						 <th>TDS_A(-)</th>  
						  <th>GPS_Deposit(-)</th>  
						 <th>Total<br>Freight</th>  
						 <th>Total<br>Advance</th>  
						 <th>Cash<br>Adv</th>  
						 <th>Cheque<br>Adv</th>  
						 <th>Diesel<br>Adv</th>  
						 <th>Rtgs<br>Adv</th>  
						 <th>CRN<br>Number</th>  
						 <th>UTR<br>Number</th>  
						 <th>UTR<br>Date</th>  
						 <th>Advance<br>Branch</th>  
						 <th>Advance<br>Date</th>  
						 <th>Advance<br>Narration</th>  
						 <th>Balance<br>Amount</th>  
						 <th>Balance<br>Party</th>  
						 <th>Balance<br>Party PAN</th>  
						 <th>Unloading(+)</th>  
						  <th>GPS Deposit_Return(+)</th>  
						 <th>GPS Charge(-)</th>  
						 <th>Other_B(-)</th>  
						 <th>TDS_B(-)</th>  
						 <th>Claim(-)</th>  
						 <th>Late POD(-)</th>  
						 <th>Total<br>Balance</th>  
						 <th>Cash<br>Bal</th>  
						 <th>Cheque<br>Bal</th>  
						 <th>Diesel<br>Bal</th>  
						 <th>Rtgs<br>Bal</th>  
						 <th>CRN<br>Number.</th>  
						 <th>UTR<br>Number.</th>  
						 <th>UTR<br>Date.</th>  
						 <th>Balance<br>Branch</th>  
						 <th>Balance<br>Date</th>  
						 <th>Balance<br>Narration</th>  
                    </tr>
			';
			
			
  while($row = fetchArray($result))
  {
	
	if($row['pod_rcvd']=='1')
	{
		if($row['lr_weight']<$row['rcvd_weight'])
		{
			$new_freight=round($row['lr_weight']*$row['rate']);
		}
		else
		{
			$new_freight=round($row['rcvd_weight']*$row['rate']);
		}
		
		$unld_date=$row['unload_date'];
		$unld_wt=$row['rcvd_weight'];
	}
	else
	{
		$unld_date="<font color='red'><b>POD PENDING</b></font>";
		$unld_wt="<font color='red'><b>POD PENDING</b></font>";
		$new_freight=$row['freight'];
	}
	
	$balance_amt=round((($new_freight+$row['loading'])-($row['adv_other']+$row['adv_tds']+$row['gps_deposit']))-$row['total_adv']);	
	
	$output .= '
    <tr>  
		<td>'.$row["fm_no"].'</td>  
		<td>'.$row["tno"].'</td>  
		<td>'.$row["company"].'</td>  
		<td>'.$row["fm_date"].'</td>  
		<td>'.$row["weight"].'</td>  
		<td>'.$unld_wt.'</td>  
		<td>'.$unld_date.'</td>  
		<td>'.$row["rate"].'</td>  
		<td>'.$row["freight"].'</td>  
		<td>'.$row["lrnos"].'</td>  
		<td>'.$row["fm_branch"].'</td>  
		<td>'.$row["adv_party"].'</td>  
		<td>'.$row["adv_pan"].'</td>  
		<td>'.$row["loading"].'</td>  
		<td>'.$row["dsl_inc"].'</td>  
		<td>'.$row["adv_other"].'</td>  
		<td>'.$row["adv_tds"].'</td>  
		<td>'.$row["gps_deposit"].'</td>  
		<td>'.$row["total_freight"].'</td>  
		<td>'.$row["total_adv"].'</td>  
		<td>'.$row["adv_cash"].'</td>  
		<td>'.$row["adv_chq"].'</td>  
		<td>'.$row["adv_diesel"].'</td>  
		<td>'.$row["adv_rtgs"].'</td>  
		<td>'.$row["adv_crn"].'</td>  
		<td>'."'".$row["adv_utr"].'</td>  
		<td>'.$row["adv_utr_date"].'</td>  
		<td>'.$row["adv_branch"].'</td>  
		<td>'.$row["adv_date"].'</td>  
		<td>'.$row["adv_nrr"].'</td>  
		<td>'.$balance_amt.'</td>  
		<td>'.$row["bal_party"].'</td>  
		<td>'.$row["bal_pan"].'</td>  
		<td>'.$row["unloading"].'</td>  
		<td>'.$row["gps_deposit_return"].'</td>  
		<td>'.$row["gps_charges"].'</td>  
		<td>'.$row["bal_other"].'</td>  
		<td>'.$row["bal_tds"].'</td>  
		<td>'.$row["claim"].'</td>  
		<td>'.$row["late_pod"].'</td>  
		<td>'.$row["total_bal"].'</td>  
		<td>'.$row["bal_cash"].'</td>  
		<td>'.$row["bal_chq"].'</td>  
		<td>'.$row["bal_diesel"].'</td>  
		<td>'.$row["bal_rtgs"].'</td>  
		<td>'.$row["bal_crn"].'</td>  
		<td>'."'".$row["bal_utr"].'</td>  
		<td>'.$row["bal_utr_date"].'</td>  
		<td>'.$row["bal_branch"].'</td>  
		<td>'.$row["bal_date"].'</td>  
		<td>'.$row["bal_nrr"].'</td>  
	</tr>
   ';
  }
	
 $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Freight_MEMO_Report.xls');
  echo $output;
 }
 else
 {
	 echo "<script>
		alert('No result found..');
		window.location.href='./';
		</script>";
 }

closeConnection($conn);
}
else if($report=="OWNER_A" || $report=="OWNER_B" || $report=="OWNER_FM_DATE")
{

$oid=escapeString($conn,$_POST['oid']);

if($report=="OWNER_A")
{
	$col_name="a.date";
}
else if($report=='OWNER_B')
{
	$col_name="b.date";
}
else
{
	$col_name="f.date";
}

$query = "SELECT f.fm_no,f.tno,f.company,f.date as fm_date,f.weight,f.rate,f.freight,f.lrnos,f.branch as fm_branch,f.pod as pod_rcvd,
a.party_name as adv_party,
a.party_pan as adv_pan,a.loading,a.dsl_inc,a.other as adv_other,a.tds as adv_tds,a.gps_deposit,a.total_freight,a.total_adv,a.cash as adv_cash,
a.cheque as adv_chq,a.chqno as adv_chq_no,a.diesel as adv_diesel,a.rtgs as adv_rtgs,r.crn as adv_crn,r.bank as adv_utr,
r.utr_date as adv_utr_date,a.branch as adv_branch,a.date as adv_date,a.narration as adv_nrr,
b.party_name as bal_party,b.party_pan as bal_pan,b.unloading,b.other as bal_other,b.tds as bal_tds,b.claim,b.gps_charges,
b.gps_deposit_return,b.late_pod,b.total_bal,
b.cash as bal_cash,b.cheque as bal_chq,b.diesel as bal_diesel,b.rtgs as bal_rtgs,r2.crn as bal_crn,r2.bank as bal_utr,
r2.utr_date as bal_utr_date,b.branch as bal_branch,b.date as bal_date,b.narration as bal_nrr,p.lr_weight,p.rcvd_weight,p.unload_date 
FROM `freight_memo` as f 
LEFT OUTER JOIN freight_memo_adv as a ON a.fm_no=f.fm_no 
LEFT OUTER JOIN freight_memo_bal as b ON b.fm_no=f.fm_no 
LEFT OUTER JOIN rrpl_database.rtgs_fm as r ON r.fno=f.fm_no and r.type='ADVANCE' 
LEFT OUTER JOIN rrpl_database.rtgs_fm as r2 ON r2.fno=b.fm_no and r2.type='BALANCE' 
LEFT OUTER JOIN rcv_pod as p ON p.fm_no=a.fm_no 
WHERE $col_name BETWEEN '$from_date' AND '$to_date' AND f.owner='$oid'";

$result = Qry($conn,$query);

if(!$result)
{
	echo mysqli_error($conn);
	exit();
}

if(numRows($result) > 0)
 {
	 $output .= '
		<table border="1">  
                    <tr>  
						 <th>FM No</th>  
                         <th>Truck No</th>  
                         <th>Company</th>  	
                         <th>FM Date</th>  
                         <th>LR<br>Weight</th>  
                         <th>Unloading<br>Weight</th>  
                         <th>Unloading<br>Date</th>  
                         <th>Rate</th>  
                         <th>Freight</th>  
                         <th>LR No</th>  
                         <th>FM<br>Branch</th>  
                         <th>Advance<br>Party</th>  
                         <th>Advance<br>Party PAN</th>  
						 <th>Loading(+)</th>  
						 <th>Diesel<br>Inc(+)</th>  
						 <th>Other_A(-)</th>  
						 <th>TDS_A(-)</th>  
						 <th>GPS_Deposit(-)</th>  
						 <th>Total<br>Freight</th>  
						 <th>Total<br>Advance</th>  
						 <th>Cash<br>Adv</th>  
						 <th>Cheque<br>Adv</th>  
						 <th>Diesel<br>Adv</th>  
						 <th>Rtgs<br>Adv</th>  
						 <th>CRN<br>Number</th>  
						 <th>UTR<br>Number</th>  
						 <th>UTR<br>Date</th>  
						 <th>Advance<br>Branch</th>  
						 <th>Advance<br>Date</th>  
						 <th>Advance<br>Narration</th>  
						 <th>Balance<br>Amount</th>  
						 <th>Balance<br>Party</th>  
						 <th>Balance<br>Party PAN</th>  
						 <th>Unloading(+)</th>  
						 <th>GPS Deposit_Return(+)</th>  
						 <th>GPS Charge(-)</th>  
						 <th>Other_B(-)</th>  
						 <th>TDS_B(-)</th>  
						 <th>Claim(-)</th>  
						 <th>Late POD(-)</th>  
						 <th>Total<br>Balance</th>  
						 <th>Cash<br>Bal</th>  
						 <th>Cheque<br>Bal</th>  
						 <th>Diesel<br>Bal</th>  
						 <th>Rtgs<br>Bal</th>  
						 <th>CRN<br>Number.</th>  
						 <th>UTR<br>Number.</th>  
						 <th>UTR<br>Date.</th>  
						 <th>Balance<br>Branch</th>  
						 <th>Balance<br>Date</th>  
						 <th>Balance<br>Narration</th>  
                    </tr>
			';
			
			
  while($row = fetchArray($result))
  {
	
	if($row['pod_rcvd']=='1')
	{
		if($row['lr_weight']<$row['rcvd_weight'])
		{
			$new_freight=round($row['lr_weight']*$row['rate']);
		}
		else
		{
			$new_freight=round($row['rcvd_weight']*$row['rate']);
		}
		
		$unld_date=$row['unload_date'];
		$unld_wt=$row['rcvd_weight'];
	}
	else
	{
		$unld_date="<font color='red'><b>POD PENDING</b></font>";
		$unld_wt="<font color='red'><b>POD PENDING</b></font>";
		$new_freight=$row['freight'];
	}
	
	$balance_amt=round((($new_freight+$row['loading'])-($row['adv_other']+$row['adv_tds']+$row['gps_deposit']))-$row['total_adv']);	
	
	$output .= '
    <tr>  
		<td>'.$row["fm_no"].'</td>  
		<td>'.$row["tno"].'</td>  
		<td>'.$row["company"].'</td>  
		<td>'.$row["fm_date"].'</td>  
		<td>'.$row["weight"].'</td>  
		<td>'.$unld_wt.'</td>  
		<td>'.$unld_date.'</td>  
		<td>'.$row["rate"].'</td>  
		<td>'.$row["freight"].'</td>  
		<td>'.$row["lrnos"].'</td>  
		<td>'.$row["fm_branch"].'</td>  
		<td>'.$row["adv_party"].'</td>  
		<td>'.$row["adv_pan"].'</td>  
		<td>'.$row["loading"].'</td>  
		<td>'.$row["dsl_inc"].'</td>  
		<td>'.$row["adv_other"].'</td>  
		<td>'.$row["adv_tds"].'</td>  
		<td>'.$row["gps_deposit"].'</td>  
		<td>'.$row["total_freight"].'</td>  
		<td>'.$row["total_adv"].'</td>  
		<td>'.$row["adv_cash"].'</td>  
		<td>'.$row["adv_chq"].'</td>  
		<td>'.$row["adv_diesel"].'</td>  
		<td>'.$row["adv_rtgs"].'</td>  
		<td>'.$row["adv_crn"].'</td>  
		<td>'."'".$row["adv_utr"].'</td>  
		<td>'.$row["adv_utr_date"].'</td>  
		<td>'.$row["adv_branch"].'</td>  
		<td>'.$row["adv_date"].'</td>  
		<td>'.$row["adv_nrr"].'</td>  
		<td>'.$balance_amt.'</td>  
		<td>'.$row["bal_party"].'</td>  
		<td>'.$row["bal_pan"].'</td>  
		<td>'.$row["unloading"].'</td>  
		<td>'.$row["gps_deposit_return"].'</td>  
		<td>'.$row["gps_charges"].'</td>  
		<td>'.$row["bal_other"].'</td>  
		<td>'.$row["bal_tds"].'</td>  
		<td>'.$row["claim"].'</td>  
		<td>'.$row["late_pod"].'</td>  
		<td>'.$row["total_bal"].'</td>  
		<td>'.$row["bal_cash"].'</td>  
		<td>'.$row["bal_chq"].'</td>  
		<td>'.$row["bal_diesel"].'</td>  
		<td>'.$row["bal_rtgs"].'</td>  
		<td>'.$row["bal_crn"].'</td>  
		<td>'."'".$row["bal_utr"].'</td>  
		<td>'.$row["bal_utr_date"].'</td>  
		<td>'.$row["bal_branch"].'</td>  
		<td>'.$row["bal_date"].'</td>  
		<td>'.$row["bal_nrr"].'</td>  
	</tr>
   ';
  }
	
 $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Freight_MEMO_Report.xls');
  echo $output;
 }
 else
 {
	 echo "<script>
		alert('No result found..');
		window.location.href='./';
		</script>";
 }

closeConnection($conn);
}
?>