<?php
include "header.php";

$today_date = date("Y-m-d");

$get_pod_details = Qry($conn,"SELECT 
(SELECT COUNT(id) FROM rcv_pod WHERE date(timestamp)='$today_date' AND pod_type='MARKET') as 'mkt_pod',
(SELECT COUNT(id) FROM rcv_pod WHERE date(timestamp)='$today_date' AND pod_type='OWN') as 'own_pod',
(SELECT coalesce(SUM(weight_diff),0) FROM rcv_pod WHERE date(timestamp)='$today_date' AND weight_diff>0) as 'weight_diff'");

if(!$get_pod_details){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	exit();
}

$row_details = fetchArray($get_pod_details);
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Dashboard
        <small></small>
      </h4>
    </section>

<section class="content">
		
	<div class="row" style="font-size:13px;">
		  
		<div class="col-lg-3 col-xs-6">
			  <div class="small-box bg-green">
				<div class="inner">
				  <h4><?php echo $row_details['own_pod']; ?></h4>
				  <p>Own truck POD</p>
				</div>
				<a href="#0" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
			  </div>
		 </div> 
	 
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4><?php echo $row_details['mkt_pod']; ?></h4>
			 <p>Market truck POD</p>
            </div>
            <a href="#0" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4><?php echo $row_details['weight_diff']; ?></h4>

              <p>Weight Diff.</p>
            </div>
            <a href="#0" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4><?php echo "0"; ?></h4>

              <p>POD Pending</p>
            </div>
            <a href="#0" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		
	</div>
	
</section>
 </div>

<?php
include "footer.php";
?>