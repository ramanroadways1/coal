<?php 
require_once("connect.php");

$fm_no=escapeString($conn,strtoupper($_POST['fm_no']));
$type=escapeString($conn,($_POST['type']));
$timestamp = date("Y-m-d H:i:s");

if($type=='')
{
	echo "<script type='text/javascript'>
		window.location.href='./rcv_pod.php';
	</script>";
	exit();
}

if($fm_no!='')
{
	if($type=='MARKET')
	{
		$qry = Qry($conn,"SELECT f.id,f.date,f.branch as fm_branch,l.branch as lr_branch,
		l.id as lr_eId,l.charge_wt as lr_weight,l.pod_rcv_date,l.is_claim_updated,f.freight,f.pod,f.gps_install_type,b.name as broker_name,
				c1.name as con1,c2.name as con2,s1.name as from1,s2.name as to1,f.tno,l.lrno,
				l.main_lr_id as lr_id,l.lr_date,l.con1 as con1_id,l2.billparty,l2.billingbranch 
				FROM freight_memo as f 
				LEFT OUTER JOIN lr_entry as l ON FIND_IN_SET(l.id,lr_ids)
				LEFT OUTER JOIN rrpl_database.lr_sample as l2 ON l2.id=l.main_lr_id
				LEFT OUTER JOIN rrpl_database.mk_broker as b ON b.id=f.broker
				LEFT OUTER JOIN rrpl_database.consignor as c1 ON c1.id=l.con1
				LEFT OUTER JOIN rrpl_database.consignee as c2 ON c2.id=l.con2
				LEFT OUTER JOIN rrpl_database.station as s1 ON s1.id=l.from_loc
				LEFT OUTER JOIN rrpl_database.station as s2 ON s2.id=l.to_loc
				WHERE f.fm_no='$fm_no' AND f.advance='1' GROUP by l.lrno");
	}
	else
	{
		$qry=Qry($conn,"SELECT f.id,f.date,f.branch as fm_branch,l.branch as lr_branch,
		l.id as lr_eId,l.charge_wt as lr_weight,l.pod_rcv_date,l.is_claim_updated,'0' as freight,'' as gps_install_type,f.pod,c1.name as con1,
		c2.name as con2,s1.name as from1,s2.name as to1,f.truck_no as tno,l.lrno,l.main_lr_id as lr_id,
		l.lr_date,l.con1 as con1_id,l2.billparty,l2.billingbranch 
		FROM own_truck_form as f 
		LEFT OUTER JOIN lr_entry as l ON FIND_IN_SET(l.id,lr_ids)
		LEFT OUTER JOIN rrpl_database.lr_sample as l2 ON l2.id=l.main_lr_id
		LEFT OUTER JOIN rrpl_database.consignor as c1 ON c1.id=l.con1
		LEFT OUTER JOIN rrpl_database.consignee as c2 ON c2.id=l.con2
		LEFT OUTER JOIN rrpl_database.station as s1 ON s1.id=l.from_loc
		LEFT OUTER JOIN rrpl_database.station as s2 ON s2.id=l.to_loc
		WHERE f.fm_no='$fm_no' GROUP by l.lrno");
	}
	
	if(!$qry){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./rcv_pod.php");
		exit();
	}
	
	if(numRows($qry)>0)
	{
		if($type=='MARKET')
		{
			$trip_id="0";
			$trip_no="0";
		}
		else
		{
			$fetch_trip_id=Qry($conn,"SELECT id,trip_no FROM dairy.trip WHERE FIND_IN_SET('$fm_no',lr_type)");
			if(!$qry){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./rcv_pod.php");
				exit();
			}
			
			if(numRows($fetch_trip_id)==0)
			{
				echo "<font color='red'>Trip not created in e-Diary.</font>
				<script type='text/javascript'>
					$('#loadicon').hide();
				</script>";
				exit();
			}
			else if(numRows($fetch_trip_id)>1)
			{
				echo "<font color='red'>More than 1 Trip Found with Same LR. e-Diary.</font>";
				echo "<script type='text/javascript'>
						$('#loadicon').hide();
					</script>";
				exit();
			}
			else
			{
				$row_trip_id=fetchArray($fetch_trip_id);
				$trip_id=$row_trip_id['id'];
				$trip_no=$row_trip_id['trip_no'];
			}
		}
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<table class="table table-bordered table-striped" style="font-size:12px">								   
<tr style="background:#3c8dbc;color:#FFF;font-size:14px">
	<td colspan="12">Vehicle Number : <span id="veh_no1"></span>, Vou_No : <?php echo $fm_no; ?></td>
</tr>		
	<tr>
		<th>#</th>
		<th>Branch</th>
		<th>LR_No</th>
		<th>LR_Weight</th>
		<th>LR_Date</th>
		<th>Consignor</th>
		<th>Locations</th> 
		<th>GPS Charge</th>
		<th>Claim & POD</th>
	</tr>
<?php
$sn=1;

while($row=fetchArray($qry))
{
if($row['gps_install_type']=='' AND $type=='MARKET')
{
	$btn_disabled="disabled";
	$btn_html="GPS not found";
}
else
{
	$btn_disabled="";
	$btn_html="Upload";
}

if($row['is_claim_updated']=="0")
{
	$btn_disabled="disabled";
	$btn_html="Update Claim First";
}
	
$con1_id_DB = $row['con1_id'];
$veh_no = $row['tno'];
$LR_date = date('d/m/y', strtotime($row['lr_date']));

	echo "
	<tr>
		<td>$sn</td>
		<td>$row[fm_branch]</td>
		<td>$row[lrno]</td>
		<td>$row[lr_weight]</td>
		<td>$LR_date</td>
		<td>$row[con1]</td>
		<td>From: $row[from1]<br>To: $row[to1]</td>
		<td id='gps_charge_$sn'>0</td>";
		
		if($row['is_claim_updated']=="0")
		{
		echo " 
		<input type='hidden' id='claim_branch_lrno_$row[lr_id]' value='$row[lrno]'>
		<input type='hidden' id='claim_branch_vou_no_$row[lr_id]' value='$fm_no'>
		
		<td>
		<div class='form-inline'>
			<label style='font-size:12px'>Claim in this LR? <font color='red'><sup>*</sup></font></label>
			<br />
			<select style='width:130px !important;font-size:12px;height:30px;' onchange=ClaimSelection(this.value,'$row[lr_id]') class='form-control' id='claim_branch_$row[lr_id]'>
				<option value=''>--select--</option>
				<option value='YES'>YES</option>
				<option value='NO'>NO</option>
			</select>
		</div>
		</td>";
		
			// $pod_td_show_hide="style='display:none'";
		}
		else
		{
			if($row['is_claim_updated']=="1"){
				$claim_branch="YES";
			}else{
				$claim_branch="NO";
			}
			// echo "<td>$claim_branch</td>";
			
			// $pod_td_show_hide="";
		
		if($row['pod_rcv_date']=='' || $row['pod_rcv_date']==0)
		{
		echo '
		<script type="text/javascript">
		$(document).ready(function (e) {
				$("#PodFormUpload'.$sn.'").on("submit",(function(e) {
				e.preventDefault();
				$("#loadicon").show();
				$("#rcv_button'.$sn.'").attr("disabled", true);
				$.ajax({
						url: "./upload_pod.php",
						type: "POST",
						data:  new FormData(this),
						contentType: false,
						cache: false,
						processData:false,
						success: function(data)
						{
							$("#func_result").html(data);
						},
						error: function() 
						{
						} 	        
				   });
				}));
			});
		</script>
		<td>
		<font color="red">Claim: '.$claim_branch.'</font><br><br>
		<form id="PodFormUpload'.$sn.'" method="POST">
		<div class="row">
			<div class="form-group col-md-6">
				<label style="font-size:11px;">Unload. Date <font color="red"><sup>*</sup></font></label>
				<input max="'.date("Y-m-d").'" class="form-control" required="required" 
				pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" id="unload_date_id_'.$sn.'" type="date" style="font-size:12px;height:28px;" name="unload_date">
			</div>

			<div class="form-group col-md-6">
				<label style="font-size:11px;">Unload. Weight <font color="red"><sup>*</sup></font></label>
				<input placeholder="Unload Weight" step="any" min="1" class="form-control" required="required" 
				type="number"  id="unload_wt_id_'.$sn.'" style="font-size:12px;height:28px;" name="unload_weight">
			</div>	

			<div class="form-group col-md-6">
				<label style="font-size:11px;">Unload. Slip <font color="red"><sup>*</sup></font></label>
				<input  id="unload_slip_id_'.$sn.'" required="required" type="file" multiple="multiple" class="form-control" 
				accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" style="font-size:9px;height:28px;" 
				name="unload_slip[]">
			</div>		

			<div class="form-group col-md-6">
				<label style="font-size:11px;">POD Copy <font color="red"><sup>*</sup></font></label>
				<input  id="pod_copy_id_'.$sn.'" required="required" type="file" multiple="multiple" class="form-control" 
				accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" style="font-size:9px;height:28px;" 
				name="upload_file[]">
			</div>	
			
			<div class="form-group col-md-6">
				<label style="font-size:11px;">Other Deduction <font color="red"><sup>*</sup></font></label>
				<input placeholder="Deduction in FM" min="0" class="form-control" required="required" 
				type="number"  id="other_deduction_id_'.$sn.'" style="font-size:12px;height:28px;" name="other_deduction">
			</div>	
		';	
	
		echo "
		<input type='hidden' name='lr_id' value='$row[lr_id]'>			
		<input type='hidden' name='lr_entry_id' value='$row[lr_eId]'>			
		<input type='hidden' name='lr_weight' value='$row[lr_weight]'>			
		<input type='hidden' name='vou_no' value='$fm_no'>			
		<input type='hidden' name='lrno' value='$row[lrno]'>			
		<input type='hidden' name='truck_no' value='$row[tno]'>			
		<input type='hidden' name='lr_branch' value='$row[lr_branch]'>			
		<input type='hidden' name='lr_date' value='$row[lr_date]'>			
		<input type='hidden' name='consignor' value='$row[con1]'>			
		<input type='hidden' name='con1_id' value='$row[con1_id]'>			
		<input type='hidden' name='fm_id' value='$row[id]'>			
		<input type='hidden' name='freight_amount' value='$row[freight]'>			
		<input type='hidden' name='fm_branch' value='$row[fm_branch]'>
		<input type='hidden' name='billparty_id' value='$row[billparty]'>
		<input type='hidden' name='billing_branch' value='$row[billingbranch]'>
		<input type='hidden' name='unq_id' value='$sn'>
		<input type='hidden' name='truck_type' value='$type'>
		<input type='hidden' name='trip_id' value='$trip_id'>
		<input type='hidden' name='trip_no' value='$trip_no'>
		";
		
		if($row['gps_install_type']=='TRIP' AND $type=='MARKET')
		{
			echo '<div class="form-group col-md-6">
				<label style="font-size:11px;">GPS Received ? <font color="red"><sup>*</sup></font></label>
				<select id="gps_sel_id_'.$sn.'"  name="gps_selection" style="font-size:12px;height:30px;" onchange="GPSDeviceToggle'.$sn.'(this.value,'.$sn.')" class="form-control" required="required">
					<option style="font-size:12px;" value="">--select--</option>
					<option style="font-size:12px;" value="YES">YES</option>
					<option style="font-size:12px;" value="NO">NO</option>
				</select>
			</div>';
			
			echo "<script>
				function GPSDeviceToggle$sn(elem,id)
				{
					if(elem=='NO')
					{
						$('#gps_charge_$sn').html('3000');
					}
					else
					{
						$('#gps_charge_$sn').html('0');
					}
				}
			</script>";
		}
		else
		{
			echo "<script>
				$('#gps_charge_$sn').html('0');
			</script>";
		}
		
		echo "<div class='form-group col-md-12'>";
		
		// echo "<label></label><br>";
		echo "<button $btn_disabled type='submit' style='font-size:11px' id='rcv_button$sn' class='btn btn-sm btn-primary'>
				<span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span> $btn_html
			</button>
		</div>	
		</div>
		</td>
		</form>";
		}
		else		
		{
			echo "<td id='pod_upload_td'><font color='red'>Claim: $claim_branch<br>POD Received</font></td>";
		}
		}
		
		echo "</tr>";
$sn++;	
}
echo "</table>

";

echo "
<script>
$('#veh_no1').html('$veh_no');
$('#loadicon').hide();
</script>";
}
else
{
	echo "<font color='red'>No records found..</font>
	<script type='text/javascript'>
		$('#loadicon').hide();
	</script>";
	exit();
}	
}
?>