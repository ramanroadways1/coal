<?php
require_once("connect.php");

$vou_no = escapeString($conn,$_POST['vou_no']);
$type = escapeString($conn,$_POST['type']);

?>

<script>
	function RemoveDieselEntry(id)
	{
			$('#loadicon').show();
			jQuery.ajax({
			url: "delete_diesel.php",
			data: 'id=' + id,
			type: "POST",
			success: function(data) {
				$("#result_dlt").html(data);
			},
			error: function() {}
			});
	}
</script>

<div id="result_dlt"></div>

<?php

$fetch=Qry($conn,"SELECT id,amount,card_pump,phy_card,mobile_no FROM diesel_cache WHERE fm_no='$vou_no' AND type='$type'");
if(!$fetch){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($fetch)>0)
{
	echo "
	<div class='col-md-12'>
	
	<div class='row'>
		<div class='col-md-12'>
			<h5 style='padding:4px;' class='bg-primary'><b>Diesel Entries : </b>
			<button class='btn btn-default btn-xs' id='DieselModalButton' type='button' data-toggle='modal' data-target='#DieselModal'> 
			Add diesel</button></h5>
		</div>
	</div>
	<table style='font-family:verdana;font-size:12px' class='table table-bordered'>
		<tr>
			<th>Card/Pump</th>
			<th>Card/Mobile No</th>
			<th>Amount</th>
			<th>Action</th>
		</tr>";
	
	while($row=fetchArray($fetch))
	{
		if($row['card_pump']=='OTP'){
			$card_no=$row['mobile_no'];
		}else { $card_no=$row['phy_card']; }
		
		echo "<tr>
			<td>$row[card_pump]</td>
			<td>$card_no</td>
			<td>$row[amount]</td>
			<td><button onclick='RemoveDieselEntry($row[id])' type='button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove-circle'></span> Remove</button></td>
		</tr>";
		
	}	
	
	echo "</tr>	
		</table>
		</div>
		";
	echo "<script>$('#loadicon').hide();</script>";	
}
else
{
	echo "<div class='col-md-12'>
			<font color='red'><b>NO DIESEL RECORD FOUND.</b></font>
			<button class='btn btn-sm btn-success' id='DieselModalButton' type='button' data-toggle='modal' data-target='#DieselModal'>
			Add diesel</button>
			
		</div>";
	echo "<script>$('#loadicon').hide();</script>";	
}
?>