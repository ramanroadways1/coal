<?php
require_once("./connect.php");

$lr_no=escapeString($conn,strtoupper($_POST['lr_no']));
$company=escapeString($conn,strtoupper($_POST['company']));
$lr_type=escapeString($conn,strtoupper($_POST['lr_type']));
$branch=escapeString($conn,strtoupper($_SESSION['rrpl_ship']));
$timestamp=date("Y-m-d H:i:s");

if($lr_type=='MARKET')
{
	$tno = escapeString($conn,strtoupper($_POST['market_tno']));
	$wheeler=escapeString($conn,strtoupper($_POST['market_wheeler']));
}
else
{
	$tno = escapeString($conn,strtoupper($_POST['own_tno']));
	$wheeler=escapeString($conn,strtoupper($_POST['own_wheeler']));
}

$owner_mobile_no="";
$driver_mobile_no="";

if($branch=='MAGDALLA')
{
	$ewb_no=escapeString($conn,strtoupper($_POST['ewb_no']));
	$gen_way_bill="1";
	
	if(strlen($ewb_no) != 12)
	{
		echo "<script>
			alert('Eway bill number is not valid !');
			$('#loadicon').hide();
			$('#create_button').attr('disabled',false);
		</script>";
		exit();
	}
	
	if(!isset($_SESSION['coal_ewb_date']))
	{
		echo "<script>
			alert('Eway bill not verified !');
			$('#loadicon').hide();
			$('#create_button').attr('disabled',false);
		</script>";
		exit();
	}
}
else
{
	$gen_way_bill=escapeString($conn,($_POST['gen_way_bill']));
}
	
if(strpos($lr_no, '0') === 0)
{
	echo "<script>
		alert('Error: Hey you have entered invalid LR No.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}

$chk_lr_no=Qry($conn,"SELECT id FROM lr_entry WHERE lrno='$lr_no'");
if(!$chk_lr_no){
	ScriptError($conn,$page_name,__LINE__);
	exit();	
}

if(numRows($chk_lr_no)>0)
{
	echo "<script>
		alert('Error: Duplicate LR Number found.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}

$sel_bilty_id=Qry($conn,"SELECT id,stock_left FROM rrpl_database.lr_bank WHERE $lr_no>=from_range AND $lr_no<=to_range 
AND branch='$branch'");

if(!$sel_bilty_id){
	ScriptError($conn,$page_name,__LINE__);
	exit();	
}

if(numRows($sel_bilty_id)==0)
{
	echo "<script>
		alert('Error: LR Stock not found.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}
	
$row_stock=fetchArray($sel_bilty_id);

$bilty_id=$row_stock['id'];

if($row_stock['stock_left']<=0)
{
	echo "<script>
		alert('Error: LR not in Stock.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}
	
$lr_date=escapeString($conn,strtoupper($_POST['lr_date']));

$from_loc=escapeString($conn,strtoupper($_POST['from_loc']));
$to_loc=escapeString($conn,strtoupper($_POST['to_loc']));
$con1=escapeString($conn,strtoupper($_POST['con1']));
$con2=escapeString($conn,strtoupper($_POST['con2']));
$actual_wt=escapeString($conn,strtoupper($_POST['actual_wt']));
$charge_wt=escapeString($conn,strtoupper($_POST['charge_wt']));
$do_no=escapeString($conn,strtoupper($_POST['boe_no']));
$inv_no=escapeString($conn,strtoupper($_POST['inv_no']));
$ship_no=escapeString($conn,strtoupper($_POST['ship_no']));
$t_type=escapeString($conn,strtoupper($_POST['t_type']));
$items=escapeString($conn,strtoupper($_POST['items']));
$acticles="";
$goods_value=escapeString($conn,strtoupper($_POST['goods_value']));
$bill_rate=escapeString($conn,strtoupper($_POST['bill_rate']));
$bill_amount=escapeString($conn,strtoupper($_POST['bill_amount']));
$goods_desc=escapeString($conn,strtoupper($_POST['goods_desc']));
$shipment_id=escapeString($conn,strtoupper($_POST['shipment_id']));
$allow_slip_no=escapeString($conn,strtoupper($_POST['allow_slip_no']));
$trip_no=escapeString($conn,strtoupper($_POST['trip_no']));
$item_id=escapeString($conn,strtoupper($_POST['item_id']));

$from_pincode=escapeString($conn,($_POST['from_pincode']));
$to_pin_code=escapeString($conn,($_POST['to_pin_code']));
$po_no=escapeString($conn,($_POST['po_no']));
$boe_date=escapeString($conn,($_POST['boe_date']));
$hsn_code=escapeString($conn,($_POST['hsn_code']));
$igst_rate=escapeString($conn,($_POST['igst_rate']));
$igst_value=escapeString($conn,($_POST['igst_value']));
$total_inv_value=escapeString($conn,($_POST['total_inv_value']));
$approx_distance=escapeString($conn,($_POST['approx_distance']));
$con1_gst=escapeString($conn,($_POST['con1_gst']));
$con2_gst=escapeString($conn,($_POST['con2_gst']));
$do_no_id=escapeString($conn,($_POST['do_no_id']));

if($boe_date==0)
{
	echo "<script>
		alert('Error : BOE date not found.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}

if($do_no_id==0)
{
	echo "<script>
		alert('Error : BOE number not found.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}


if(strtotime($lr_date)<strtotime($boe_date))
{
	echo "<script>
		alert('Error : LR Date is less than Bill of entry Date.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}

$array1=explode("_",$shipment_id);

$shipment_id=$array1[0];
$shipment_no=$array1[1];

if($shipment_id=='' || $shipment_no=='')
{
	echo "<script>
		alert('Unable to fetch Shipment Details.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}

$from_id = escapeString($conn,($_POST['from_loc_id']));
$to_id = escapeString($conn,($_POST['to_loc_id']));
$con1_id = escapeString($conn,($_POST['con1_id']));
$con2_id = escapeString($conn,($_POST['con2_id']));

if($from_id=='' || $to_id=='' || $con1_id=='' || $con2_id=='')
{
	echo "<script>
		alert('Something went wrong unable to fetch Data.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}

if(count($_FILES['loading_slip']['name'])>0)
{
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	foreach($_FILES['loading_slip']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			echo "<script>
				alert('Error : Only Image Upload Allowed.');
				$('#loadicon').hide();
				$('#create_button').attr('disabled',false);
			</script>";
			exit();
		}
    }
	
	for($i2=0; $i2<count($_FILES['loading_slip']['name']);$i2++) 
	{
		$UploadedSize = $_FILES['loading_slip']['size'][$i2];
		
		if(($UploadedSize/1024/1024)>2)
		{
			echo "<script>
				alert('Error : MAX Upload size allowed is : 2MB');
				$('#loadicon').hide();
				$('#create_button').attr('disabled',false);
			</script>";
			exit();
		}
	}
	
		for($i=0; $i<count($_FILES['loading_slip']['name']);$i++) 
		{
			$sourcePath = $_FILES['loading_slip']['tmp_name'][$i];
			
			if($sourcePath!="")
			{
				$fix_name=date('dmYHis').mt_rand();
				$targetPath = "loading_slip/".$fix_name.".".pathinfo($_FILES['loading_slip']['name'][$i],PATHINFO_EXTENSION);
				
				if(pathinfo($_FILES['loading_slip']['name'][$i],PATHINFO_EXTENSION)!='pdf')
				{
					ImageUpload(700,700,$sourcePath);
				}

				if(move_uploaded_file($sourcePath, $targetPath)) 
				{
					$files[] = $targetPath;
				}
				else
				{
					echo "<script>
						alert('Error : Unable to upload Loading SLIP.');
						$('#loadicon').hide();
						$('#create_button').attr('disabled',false);
					</script>";
					exit();
				}
			}
			else
			{
				echo "<script>
					alert('Error : No attachment found.');
					$('#loadicon').hide();
					$('#create_button').attr('disabled',false);
				</script>";
				exit();
			}
        }
		$file_name=implode(',',$files);
}
else
{
	echo "<script>
			alert('Error : Please Upload Loading SLIP.');
			$('#loadicon').hide();
			$('#create_button').attr('disabled',false);
		</script>";
	exit();
}

StartCommit($conn);
$flag = true;	

$insert_lr_sample=Qry($conn,"INSERT INTO rrpl_database.lr_sample(company,branch,branch_user,lrno,lr_type,wheeler,date,create_date,truck_no,
fstation,tstation,consignor,consignee,from_id,to_id,con1_id,con2_id,wt12,weight,weight2,bill_rate,bill_amt,t_type,do_no,shipno,invno,
item,item_id,articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,crossing) VALUES ('$company','$branch','$branch_sub_user',
'$lr_no','$lr_type','$wheeler','$lr_date','".date('Y-m-d')."','$tno','$from_loc','$to_loc','$con1','$con2','$from_id','$to_id',
'$con1_id','$con2_id','$actual_wt','$charge_wt','$charge_wt','$bill_rate','$bill_amount','$t_type','$do_no','$ship_no','$inv_no',
'$items','$item_id','$acticles','$goods_desc','$goods_value','$con1_gst','$con2_gst','$hsn_code','$timestamp','NO')");

if(!$insert_lr_sample){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$lr_id = getInsertID($conn);

$insert_lr = Qry($conn,"INSERT INTO lr_entry(main_lr_id,shipment_id,lrno,tno,lr_type,lr_date,branch,branch_user,company,from_loc,from_pincode,
to_loc,to_pincode,approx_distance,con1,con2,allow_slip_no,trip_no,actual_wt,charge_wt,bill_rate,bill_amount,t_type,po_no,do_no,boe_id,
boe_date,inv_no,ship_no,item,hsn_code,articles,goods_desc,goods_value,igst_rate,igst_amount,total_value,loading_slip,eway_bill,
timestamp) VALUES ('$lr_id','$shipment_id','$lr_no','$tno','$lr_type','$lr_date','$branch','$branch_sub_user','$company','$from_id',
'$from_pincode','$to_id','$to_pin_code','$approx_distance','$con1_id','$con2_id','$allow_slip_no','$trip_no','$actual_wt','$charge_wt',
'$bill_rate','$bill_amount','$t_type','$po_no','$do_no','$do_no_id','$boe_date','$inv_no','$ship_no','$items','$hsn_code','$acticles','$goods_desc',
'$goods_value','$igst_rate','$igst_value','$total_inv_value','$file_name','$gen_way_bill','$timestamp')");

if(!$insert_lr){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_lr_sample_pending=Qry($conn,"INSERT INTO rrpl_database.lr_sample_pending(lr_id,company,branch,branch_user,lrno,lr_type,wheeler,date,create_date,truck_no,
fstation,tstation,consignor,consignee,from_id,to_id,con1_id,con2_id,wt12,weight,weight2,bill_rate,bill_amt,t_type,do_no,shipno,invno,
item,item_id,articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,crossing) VALUES ('$lr_id','$company','$branch','$branch_sub_user',
'$lr_no','$lr_type','$wheeler','$lr_date','".date('Y-m-d')."','$tno','$from_loc','$to_loc','$con1','$con2','$from_id','$to_id',
'$con1_id','$con2_id','$actual_wt','$charge_wt','$charge_wt','$bill_rate','$bill_amount','$t_type','$do_no','$ship_no','$inv_no',
'$items','$item_id','$acticles','$goods_desc','$goods_value','$con1_gst','$con2_gst','$hsn_code','$timestamp','NO')");

if(!$insert_lr_sample_pending){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_lr_check=Qry($conn,"INSERT INTO rrpl_database.lr_check(branch,lrno,bilty_id,timestamp) VALUES ('$branch','$lr_no','$bilty_id','$timestamp')");

if(!$insert_lr_check){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_stock=Qry($conn,"UPDATE rrpl_database.lr_bank SET stock_left=stock_left-1 WHERE id='$bilty_id'");

if(!$update_stock){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($branch=='MAGDALLA')
{
	$ewb_date_db = $_SESSION['coal_ewb_date'];
	$docNo = $_SESSION['coal_ewb_doc_no'];
	$totInvValue = $_SESSION['coal_ewb_total_inv_value'];
	$totalValue = $_SESSION['coal_ewb_total_value'];
	$ewb_exp_db = $_SESSION['coal_ewb_exp_date'];
	// $ewb_date_db = $_SESSION['coal_ewb_extd_time'];
	$docDate = $_SESSION['coal_ewb_doc_date'];
	
	$insert_ewb_data = Qry($conn,"INSERT INTO rrpl_database._eway_bill_lr_wise(lrno,lr_id,branch,ewb_no,ewb_flag_desc,by_pass,ewb_copy,
	ewayBillDate,ewb_date,ewb_expiry,ewb_extended,docNo,docDate,fromPincode,totInvValue,totalValue,toPincode,timestamp) VALUES ('$lr_no',
	'$lr_id','$branch','$ewb_no','','','','$ewb_date_db','$ewb_date_db','$ewb_exp_db','','$docNo','$docDate','$from_pincode',
	'$totInvValue','$totalValue','$to_pin_code','$timestamp')");

	if(!$insert_ewb_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$ewb_id = getInsertID($conn);

	$insert_ewb_data2 = Qry($conn,"INSERT INTO rrpl_database._eway_bill_validity(owner_mobile,driver_mobile,lrno,truck_no,lr_date,from_loc,
	to_loc,consignor,consignee,lr_id,branch,ewb_no,ewb_flag_desc,ewb_id,ewb_date,ewb_expiry,timestamp) VALUES 
	('$owner_mobile_no','$driver_mobile_no','$lr_no','$tno','$lr_date','$from_loc','$to_loc',
	'$con1','$con2','$lr_id','$branch','$ewb_no','','$ewb_id','$ewb_date_db','$ewb_exp_db','$timestamp')");

	if(!$insert_ewb_data2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	if($branch=='MAGDALLA')
	{
		unset($_SESSION['coal_ewb_date']);
		unset($_SESSION['coal_ewb_doc_no']);
		unset($_SESSION['coal_ewb_total_inv_value']);
		unset($_SESSION['coal_ewb_total_value']);
		unset($_SESSION['coal_ewb_exp_date']);
		unset($_SESSION['coal_ewb_extd_time']);
		unset($_SESSION['coal_ewb_veh_type']);
		unset($_SESSION['coal_ewb_doc_date']);
	}
	
	echo "<script>
			alert('LR : $lr_no Created Successfully !');
			document.getElementById('Form1').reset();
			$('#loadicon').hide();
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',true);
	</script>";
	exit();
}
	
exit();

if($gen_way_bill==1)
{

$supplyType="I"; 
$subSupplyType="2";
$subSupplyDesc=" ";
$docType="BOE";
$docNo=$do_no;
$docDate=date("d/m/Y",strtotime($boe_date));
$TransType="1";
$fromGstin="URP";
$fromTrdName="GLENCORE / HZL C/O RRPL";
$fromAddr1="";
$fromAddr2="";
$fromPlace="KANDALA PORT";
$fromPincode="370210";
$actFromStateCode="24";
$fromStateCode="99";
$dispatchFromGSTIN="URP";
$dispatchFromTradeName="GLENCORE";
$toGstin="08AAACH7354K1ZB";
$toTrdName="HINDUSTHAN ZINC LIMITED";
$toAddr1=$to_loc;
$toAddr2=$to_loc;
$toCity=$to_loc;
$toPincode=$to_pin_code;
$actToStateCode="08";
$toStateCode="08";
$shipToGSTIN="08AAACH7354K1ZB";
$shipToTradeName="HINDUSTHAN ZINC LIMITED";
$Others="0";
$totalValue="";
$Cgst="";
$Sgst="";
$Igst="";
$cessValue="";
$cessNonAdvolValue="";
$TotalInvValue=$total_inv_value;
$transporterId="24AAGCR0742P1Z5";
$transporterName="RAMAN ROADWAYS PRIVATE LIMITED";
$transDocNo=$lr_no;
$transDocDate=date("d/m/Y",strtotime($lr_date));
$transMode="1";
$transDistance=$approx_distance;
$vehicleNo=$tno;
$vehicleType="R";
$productName=$items;
$productDesc=$goods_desc;
$hsnCode=$hsn_code;
$quantity=$charge_wt;
$qtyUnit="MTS";
$cgstRate=0;
$sgstRate=0;
$igstRate=$igst_rate;
$cessRate="0";
$cessNonAdvol="0";
$taxableAmount=$goods_value;

$data = array(
"supplyType"=>"$supplyType",
"subSupplyType"=>"$subSupplyType",
"subSupplyDesc"=>"$subSupplyDesc",
"docType"=>"$docType",
"docNo"=>"$docNo",
"docDate"=>"$docDate",
"fromGstin"=>"$fromGstin",
"fromTrdName"=>"$fromTrdName",
"fromAddr1"=>"$fromAddr1",
"fromAddr2"=>"$fromAddr2",
"fromPlace"=>"$fromPlace",
"fromPincode"=>$fromPincode,
"actFromStateCode"=>$actFromStateCode,
"fromStateCode"=>$fromStateCode,
"toGstin"=>"$toGstin",
"toTrdName"=>"$toTrdName",
"toAddr1"=>"$toAddr1",
"toAddr2"=>"$toAddr2",
"toPlace"=>"$toCity",
"toPincode"=>$toPincode,
"actToStateCode"=>$actToStateCode,
"toStateCode"=>$toStateCode,
"transactionType"=>$TransType,
"dispatchFromGSTIN"=>"$dispatchFromGSTIN",
"dispatchFromTradeName"=>"$dispatchFromTradeName",
"shipToGSTIN"=>"$shipToGSTIN",
"shipToTradeName"=>"$shipToTradeName",
"otherValue"=>$Others,
"totalValue"=>$totalValue,
"cgstValue"=>$Cgst,
"sgstValue"=>$Sgst,
"igstValue"=>$Igst,
"cessValue"=>$cessValue,
"cessNonAdvolValue"=>$cessNonAdvolValue,
"totInvValue"=>$TotalInvValue,
"transporterId"=>"$transporterId",
"transporterName"=>"$transporterName",
"transDocNo"=>"$transDocNo",
"transMode"=>"$transMode",
"transDistance"=>"$transDistance",
"transDocDate"=>"$transDocDate",
"vehicleNo"=>"$vehicleNo",
"vehicleType"=>"$vehicleType",
"itemList"=>array([
'productName'=>$productName,
'productDesc'=>$productDesc,
'hsnCode'=>$hsnCode,
'quantity'=>$quantity,
'qtyUnit'=>$qtyUnit,
'cgstRate'=>$cgstRate,
'sgstRate'=>$sgstRate,
'igstRate'=>$igstRate,
'cessRate'=>$cessRate,
'cessNonAdvol'=>$cessNonAdvol,
'taxableAmount'=>$taxableAmount,
])
);
$payload = json_encode($data);

	$asp_id="1613151119";
	$asp_Passwd="RRPL@123";
	$myGSTIn="24AAGCR0742P1Z5";
	$UserName="RRPL@AHD_API_111";
	$PassWord="RamanRoadways12";
	$url="https://api.taxprogsp.co.in/v1.03/dec";
	$authToken="xL6Royjmv88X4bvha4yeon3nY";
	
	$ch = curl_init('https://api.taxprogsp.co.in/v1.03/dec/ewayapi?action=GENEWAYBILL&aspid='.$asp_id.'&password='.$asp_Passwd.'&gstin='.$myGSTIn.'&username='.$UserName.'&authtoken='.$authToken.'');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLINFO_HEADER_OUT, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($payload)));
		
		$result = curl_exec($ch);
		curl_close($ch);
		$EwayArray = json_decode($result, true);
		
		$EWayBillNo=$EwayArray['ewayBillNo'];
		$EWayDate=$EwayArray['ewayBillDate'];
		$EWayValidity=$EwayArray['validUpto'];
		
	$InsertLog=Qry($conn,"INSERT INTO eway_bill_log(eway_bill_no,lrno,gen_on,exp_on,timestamp) VALUES 
	('$EWayBillNo','$lr_no','$EWayDate','$EWayValidity','$timestamp')");
		
	 $curl = curl_init();
	 curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.taxprogsp.co.in/v1.03/dec/ewayapi?action=GetEwayBill&aspid=$asp_id&password=$asp_Passwd&gstin=24AAGCR0742P1Z5&username=RRPL@AHD_API_111&authtoken=$authToken&ewbNo=$EWayBillNo",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
		"Accept: */*",
		"Accept-Encoding: gzip, deflate",
		"Cache-Control: no-cache",
		"Connection: keep-alive",
		"Host: api.taxprogsp.co.in",
		"Postman-Token: fbf12a45-9e77-4846-a07e-aa599957d3ed,cc3bfe12-9401-47f6-a573-e12a12761576",
		"User-Agent: PostmanRuntime/7.15.2",
		"cache-control: no-cache"
	  ),
	));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if($err)
		{ echo "cURL Error #:" . $err; }
	
		$response = json_decode($response, true);
		
		$genMode=$response['genMode'];
		$userGstin=$response['userGstin'];
		$supplyType=$response['supplyType'];
		$subSupplyType=$response['subSupplyType'];
		$docType=$response['docType'];
		$docNo=$response['docNo'];
		$docDate=$response['docDate'];
		$fromGstin=$response['fromGstin'];
		$fromTrdName=$response['fromTrdName'];
		$fromAddr1=$response['fromAddr1'];
		$fromAddr2=$response['fromAddr2'];
		$fromPlace=$response['fromPlace'];
		$fromPincode=$response['fromPincode'];
		$fromStateCode=$response['fromStateCode'];
		$toGstin=$response['toGstin'];
		$toTrdName=$response['toTrdName'];
		$toAddr1=$response['toAddr1'];
		$toAddr2=$response['toAddr2'];
		$toPlace=$response['toPlace'];
		$toPincode=$response['toPincode'];
		$toStateCode=$response['toStateCode'];
		$totalValue=$response['totalValue'];
		$totInvValue=$response['totInvValue'];
		$cgstValue=$response['cgstValue'];
		$sgstValue=$response['sgstValue'];
		$igstValue=$response['igstValue'];
		$cessValue=$response['cessValue'];
		$transporterId=$response['transporterId'];
		$transporterName=$response['transporterName'];
		$status=$response['status'];
		$actualDist=$response['actualDist'];
		$noValidDays=$response['noValidDays'];
		$validUpto=$response['validUpto'];
		$extendedTimes=$response['extendedTimes'];
		$rejectStatus=$response['rejectStatus'];
		$vehicleType=$response['vehicleType'];
		$actFromStateCode=$response['actFromStateCode'];
		$actToStateCode=$response['actToStateCode'];
		$transactionType=$response['transactionType'];
		$otherValue=$response['otherValue'];
		$cessNonAdvolValue=$response['cessNonAdvolValue'];
		
		foreach ($response['itemList'] as $One_data)
		{
			$itemNo=$One_data['itemNo'];
			$productId=$One_data['productId'];
			$productName=$One_data['productName'];
			$productDesc=$One_data['productDesc'];
			$hsnCode=$One_data['hsnCode'];
			$quantity=$One_data['quantity'];
			$qtyUnit=$One_data['qtyUnit'];
			$cgstRate=$One_data['cgstRate'];
			$sgstRate=$One_data['sgstRate'];
			$igstRate=$One_data['igstRate'];
			$cessRate=$One_data['cessRate'];
			$cessNonAdvol=$One_data['cessNonAdvol'];
			$taxableAmount=$One_data['taxableAmount'];
		}
		
		foreach ($response['VehiclListDetails'] as $Two_Data)
		{
			$updMode=$Two_Data['updMode'];
			$vehicleNo=$Two_Data['vehicleNo'];
			$fromPlace_v=$Two_Data['fromPlace'];
			$fromState=$Two_Data['fromState'];
			$tripshtNo=$Two_Data['tripshtNo'];
			$userGSTINTransin=$Two_Data['userGSTINTransin'];
			$enteredDate=$Two_Data['enteredDate'];
			$transMode=$Two_Data['transMode'];
			$transDocNo=$Two_Data['transDocNo'];
			$transDocDate=$Two_Data['transDocDate'];
			$groupNo=$Two_Data['groupNo'];
		}
		
		$insert_WayBill=Qry($conn,"INSERT INTO eway_bill(ewbNo,ewayBillDate,genMode,userGstin,supplyType,subSupplyType,
		docType,docNo,docDate,fromGstin,fromTrdName,fromAddr1,fromAddr2,fromPlace,fromPincode,fromStateCode,toGstin,toTrdName,
		toAddr1,toAddr2,toPlace,toPincode,toStateCode,totalValue,totInvValue,cgstValue,sgstValue,igstValue,cessValue,
		transporterId,transporterName,status,actualDist,noValidDays,validUpto,extendedTimes,rejectStatus,vehicleType,
		actFromStateCode,actToStateCode,transactionType,otherValue,cessNonAdvolValue,itemNo,productId,productName,productDesc,
		hsnCode,quantity,qtyUnit,cgstRate,sgstRate,igstRate,cessRate,cessNonAdvol,taxableAmount,updMode,vehicleNo,fromPlace_v,
		fromState,tripshtNo,userGSTINTransin,enteredDate,transMode,transDocNo,transDocDate,groupNo,timestamp) VALUES 
		('$EWayBillNo','$EWayDate','$genMode','$userGstin','$supplyType','$subSupplyType','$docType','$docNo','$docDate',
		'$fromGstin','$fromTrdName','$fromAddr1','$fromAddr2','$fromPlace','$fromPincode','$fromStateCode','$toGstin',
		'$toTrdName','$toAddr1','$toAddr2','$toPlace','$toPincode','$toStateCode','$totalValue','$totInvValue','$cgstValue',
		'$sgstValue','$igstValue','$cessValue','$transporterId','$transporterName','$status','$actualDist','$noValidDays',
		'$validUpto','$extendedTimes','$rejectStatus','$vehicleType','$actFromStateCode','$actToStateCode','$transactionType',
		'$otherValue','$cessNonAdvolValue','$itemNo','$productId','$productName','$productDesc','$hsnCode','$quantity',
		'$qtyUnit','$cgstRate','$sgstRate','$igstRate','$cessRate','$cessNonAdvol','$taxableAmount','$updMode','$vehicleNo',
		'$fromPlace_v','$fromState','$tripshtNo','$userGSTINTransin','$enteredDate','$transMode','$transDocNo','$transDocDate',
		'$groupNo','$timestamp')");
		
		if(!$insert_WayBill)
		{
			echo mysqli_error($conn);exit();
		}
}
?>