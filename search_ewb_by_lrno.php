<?php
require_once("connect.php");

$lrno = escapeString($conn,$_POST['lrno']);
?>
	   
	   <table id="myTable" class="table table-bordered table-striped" style="font-family:Verdana;font-size:12px;">
          <tr>
				<th>#</th>
				<th>EWB<br>No</th>
				<th>LR<br>Number</th>
				<th>TruckNo</th>
				<th>LR<br>Date</th>
				<th>PO<br>No</th>
				<th>BOE<br>No</th>
				<th>BOE<br>Date</th>
				<th>Taxable<br>Value</th>
				<th>Total<br>Value</th>
				<th>#</th>
				<th>#</th>
			</tr>
          
            <?php
              $sql = Qry($conn,"SELECT lr_entry.id,lr_entry.lrno,tno,lr_date,po_no,do_no,boe_date,goods_value,
			  total_value,eway_bill,eway_bill_log.eway_bill_no,eway_bill_log.download_link FROM 
			  lr_entry,eway_bill_log WHERE eway_bill_log.lrno='$lrno' AND eway_bill=1 AND lr_entry.lrno=eway_bill_log.lrno");
              
			 if(!$sql){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			}
			  
			 if(numRows($sql)==0)
			 {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			 }
			 else
			{
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 if($row['download_link']=='')
				 {
					 $button_d="";
					 $button_v="disabled";
					 $button_d_name="Download";
				 }	
				else
				{
					// $button_d="";
					$button_d="disabled";
				   $button_v="";
				   $button_d_name="Download<br>Again";
				}
				  
				echo 
                "<tr>
				  <td>$sn</td>
				  <td>$row[eway_bill_no]</td>
				  <td>$row[lrno]</td>
				  <td>$row[tno]</td>
				  <td>".date("d/m/y",strtotime($row['lr_date']))."</td>				 
				 <td>$row[po_no]</td>
				 <td>$row[do_no]</td>
				 <td>".date("d/m/y",strtotime($row['boe_date']))."</td>		
				<td>$row[goods_value]</td>				 
				<td>$row[total_value]</td>				 
				<input type='hidden' id='ShipNameBox$row[id]' value='$row[id]'>
				<td>
					<button $button_d type='button' id='Download$row[id]' onclick=Download('$row[id]','$row[eway_bill_no]') class='btn btn-xs btn-danger'>$button_d_name</button>
				</td>
				<td>
					<a href='./EWayFolder/$row[eway_bill_no].pdf' target='_blank'>
						<button type='button' $button_v id='View$row[id]' class='btn btn-xs btn-primary'>View</button>
					</a>	
				</td>
				</tr>
				";
			$sn++;		
              }
			}
            ?>
        </table>

<script>
$("#loadicon").hide();
</script>