<?php
require_once("./connect.php");

$timestamp = date("Y-m-d H:i:s");

$ewbNo= escapeString($conn,$_POST['ewb_no']);
$vessel_name= escapeString($conn,$_POST['vessel_name']);
$vehicle_type= escapeString($conn,$_POST['vehicle_type']);
$market_tno= escapeString($conn,$_POST['market_tno']);
$own_tno= escapeString($conn,$_POST['own_tno']);
$lr_date= escapeString($conn,$_POST['lr_date']);
$from_pincode= escapeString($conn,$_POST['from_pincode']);
$to_loc_sel= escapeString($conn,$_POST['to_loc_sel']);
$to_pin_code= escapeString($conn,$_POST['to_pin_code']);
$con1= escapeString($conn,$_POST['con1']);
$con1_gst= escapeString($conn,$_POST['con1_gst']);
$con2= escapeString($conn,$_POST['con2']);
$con2_gst= escapeString($conn,$_POST['con2_gst']);
$company = 'RRPL';

if($vessel_name=='')
{
	echo "<script>
		alert('Vessel name not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($vehicle_type=='')
{
	echo "<script>
		alert('Vehicle type not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($vehicle_type=='MARKET')
{
	$vehicle_no_ip = $market_tno;
}
else
{
	$vehicle_no_ip = $own_tno;
}

if($vehicle_no_ip=='')
{
	echo "<script>
		alert('Vehicle number not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($lr_date=='' || $lr_date==0)
{
	echo "<script>
		alert('LR date not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($from_pincode=='')
{
	echo "<script>
		alert('Source pincode not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($to_loc_sel=='')
{
	echo "<script>
		alert('Destination not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($to_pin_code=='')
{
	echo "<script>
		alert('Destination pincode not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($con1=='')
{
	echo "<script>
		alert('Consignor not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($con2=='')
{
	echo "<script>
		alert('Consignee not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($con1_gst=='')
{
	echo "<script>
		alert('Consignor GST not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($con2_gst=='')
{
	echo "<script>
		alert('Consignee GST not found !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(strlen($ewbNo)!=12)
{
	echo "<script>
		alert('Please check eway bill number !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$get_token=Qry($conn,"SELECT token FROM ship.api_token WHERE company='$company' ORDER BY id DESC LIMIT 1");
if(!$get_token){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_token=fetchArray($get_token);
$authToken=$row_token['token'];

if($company=='RRPL')
{
	$gst_no_company=$rrpl_gst_no;
	$gst_username=$ewb_rrpl_username;
	$gst_password=$ewb_rrpl_password;
}
else
{
	$gst_no_company=$rr_gst_no;
	$gst_username=$ewb_rr_username;
	$gst_password=$ewb_rr_password;
}

		$Curl_EwbFetch = curl_init();
		curl_setopt_array($Curl_EwbFetch, array(
		  CURLOPT_URL => "$tax_pro_url/ewayapi?action=GetEwayBill&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$gst_no_company&username=$gst_username&authtoken=$authToken&ewbNo=$ewbNo",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 300,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"Accept: */*",
			"Accept-Encoding: gzip, deflate",
			"Cache-Control: no-cache",
			"Connection: keep-alive",
			"Host: api.taxprogsp.co.in",
			"Postman-Token: fbf12a45-9e77-4846-a07e-aa599957d3ed,cc3bfe12-9401-47f6-a573-e12a12761576",
			"User-Agent: PostmanRuntime/7.15.2",
			"cache-control: no-cache"
		  ),
		));

		$response_gen = curl_exec($Curl_EwbFetch);
		$err = curl_error($Curl_EwbFetch);
		curl_close($Curl_EwbFetch);
		
		if($err)
		{
			$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(company,error_desc,lrno,branch,timestamp) VALUES 
			('$company','cURL Error : $err','RRPL.ONLINE','$branch','$timestamp')");
			
			if(!$insert_error){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
				
			echo "<script>
				alert('Error: While Fetching Eway-Bill.');
				$('#ewb_no').val('');
				$('#loadicon').fadeOut('slow');
			</script>";
			exit();
		}
		
		$response_decoded = json_decode($response_gen, true);
		
		if(@$response_decoded['error'])
		{
			if($response_decoded['error']['error_cd']=="GSP102")
			{
				  $curlToken = curl_init();
				  curl_setopt_array($curlToken, array(
				  CURLOPT_URL => "$tax_pro_url/auth?action=ACCESSTOKEN&aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&gstin=$gst_no_company&username=$gst_username&ewbpwd=$gst_password",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 300,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "GET",
				  CURLOPT_HTTPHEADER => array(
					"Accept: */*",
					"Accept-Encoding: gzip, deflate",
					"Cache-Control: no-cache",
					"Connection: keep-alive",
					"Host: api.taxprogsp.co.in",
					"Postman-Token: fbf12a45-9e77-4846-a07e-aa599957d3ed,cc3bfe12-9401-47f6-a573-e12a12761576",
					"User-Agent: PostmanRuntime/7.15.2",
					"cache-control: no-cache"
				  ),
				));
				$response_Token = curl_exec($curlToken);
				$errToken = curl_error($curlToken);
				curl_close($curlToken);
				
				if($errToken)
				{
					$insert_error2 = Qry($conn,"INSERT INTO ship.eway_bill_error(company,error_desc,lrno,branch,timestamp) VALUES 
					('$company','cURL Error : $errToken','RRPL.ONLINE','$branch','$timestamp')");
					if(!$insert_error2){
							errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
							Redirect("Error while processing Request","./");
							exit();
					}
						
					echo "<script>
						alert('Error: While Fetching Eway-Bill Token.');
						$('#loadicon').fadeOut('slow');
					</script>";
					exit();
				}
				
				$response_decodedToken1 = json_decode($response_Token, true);
				$authToken_new = $response_decodedToken1['authtoken'];
				
				if($authToken_new=='')
				{
					echo "<script>
						alert('Error : Empty token. Please try once again..');
						$('#ewb_no').val('');
						$('#loadicon').fadeOut('slow');
					</script>";
					exit();
				}
				
				$insert_token=Qry($conn,"INSERT INTO ship.api_token (token,company,user,branch_user,timestamp) VALUES 
				('$authToken_new','$company','$branch','$branch_sub_user','$timestamp')");
				if(!$insert_token){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","./");
					exit();
				}
				
				echo "<script>
						alert('Please try again !!');
						$('#ewb_no').val('');
						$('#loadicon').fadeOut('slow');
					</script>";
				exit();
				
			}
			else if($response_decoded['error']['error_cd']=="325")
			{
				echo "<script>
						alert('Error: Invalid Eway-Bill Number : $ewbNo.');
						$('#ewb_no').val('');
						$('#loadicon').fadeOut('slow');
					</script>";
				exit();
			}
			else
			{
				// echo $response_gen."<br>";
				$errorMsg = $response_decoded['error']['message'];
				$errorMsgCode = $response_decoded['error']['error_cd'];
				
				$insert_error=Qry($conn,"INSERT INTO ship.eway_bill_error(company,error_desc,lrno,msg,branch,timestamp) VALUES 
				('$company','$response_gen','RRPL.ONLINE','$errorMsg','$branch','$timestamp')");
				
				if(!$insert_error){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","./");
					exit();
				}
				
				echo "<script>
						alert('Error: While Fetching Eway-Bill. Code : $errorMsgCode.');
						$('#loadicon').fadeOut('slow');
					</script>";
				exit();
			}
		}
	
	$i_veh_no = 0;
	
	if(empty($response_decoded['VehiclListDetails']))
	{
		echo "<script>
				alert('Error: While Fetching Eway-Bill.');
				$('#ewb_no').val('');
				$('#loadicon').fadeOut('slow');
			</script>";
		exit();
	}	
	
	foreach($response_decoded['VehiclListDetails'] as $Data1){
		if($i_veh_no == 0) {
			$Veh_no=$Data1['vehicleNo'];
		}
	$i_veh_no++;
		// $Veh_no=$Data1['vehicleNo'];
	}
		
	$billDate = $response_decoded['ewayBillDate'];	
	$Con1Gst = $response_decoded['userGstin'];	
	$Doc_No = $response_decoded['docNo'];	
	$Doc_date = $response_decoded['docDate'];	 // LR Date
	$fromGstin = $response_decoded['fromGstin'];	
	$fromTrdName = escapeString($conn,$response_decoded['fromTrdName']);	
	$fromPincode = $response_decoded['fromPincode'];	
	$toGstin = $response_decoded['toGstin'];	
	$toTrdName = escapeString($conn,$response_decoded['toTrdName']);	
	$totInvValue = $response_decoded['totInvValue'];	
	$totalValue = $response_decoded['totalValue'];	
	// $Veh_no = $response_decoded['VehiclListDetails']['vehicleNo'];	
	$FromLoc = escapeString($conn,$response_decoded['fromPlace']);	
	$ToLoc = escapeString($conn,$response_decoded['toPlace']);	
	$fromPincode = $response_decoded['fromPincode'];	
	$toPincode = $response_decoded['toPincode'];	
	$validUpto = $response_decoded['validUpto'];	
	
	$extendedTimes = $response_decoded['extendedTimes'];	
	$validUpto = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$validUpto)));
	$billDate2 = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$response_decoded['ewayBillDate'])));
	$ewb_lr_date = date("Y-m-d",strtotime(str_replace('/','-',$response_decoded['docDate'])));
	$expiry_date = date("Y-m-d",strtotime($validUpto));
	
if(strtotime(date("Y-m-d"))>strtotime($expiry_date))
{
	$chk_allow_expired_ewb = Qry($conn,"SELECT id FROM _allow_lr_ewb_expired WHERE ewb_no='$ewbNo' AND branch='$branch'");
				
	if(!$chk_allow_expired_ewb){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
		
	if(numRows($chk_allow_expired_ewb)>0)
	{		
		echo "<script>
			alert('Eway bill: $ewbNo expired on $expiry_date !!');
			$('#ewb_no').val('');
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}

if($fromGstin != $con1_gst)
{
	echo "<script>
		alert('Consignor GST not matchinig with eway bill !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($ewb_lr_date != $lr_date)
{
	echo "<script>
		alert('LR date not matchinig with eway bill $ewb_lr_date !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

// if($fromPincode != $from_pincode)
// {
	// echo "<script>
		// alert('Source pincode not matchinig with eway bill !');
		// $('#ewb_no').val('');
		// $('#loadicon').fadeOut('slow');
	// </script>";
	// exit();
// }

if($toGstin != $con2_gst)
{
	echo "<script>
		alert('Consignee GST not matchinig with eway bill !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

// if($toPincode != $to_pin_code)
// {
	// echo "<script>
		// alert('Destination pincode not matchinig with eway bill !');
		// $('#ewb_no').val('');
		// $('#loadicon').fadeOut('slow');
	// </script>";
	// exit();
// }

if($Veh_no != $vehicle_no_ip)
{
	echo "<script>
		alert('Vehicle number not matchinig with eway bill !');
		$('#ewb_no').val('');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

echo "<script>
	$('.veh_type_sel_opt').attr('disabled',true);
	$('#veh_type_sel_opt_$vehicle_type').attr('disabled',false);
	$('#market_tno').attr('readonly',true);
	$('#own_tno').attr('readonly',true);
	$('#lr_date').attr('readonly',true);
	$('#from_pincode').attr('readonly',true);
	$('.dest_loc_name_opt').attr('disabled',true);
	$('#dest_loc_name_opt_$to_loc_sel').attr('disabled',false);
	$('#to_pin_code').attr('readonly',true);
	$('#con1').attr('readonly',true);
	$('#con1_gst').attr('readonly',true);
	$('#con2').attr('readonly',true);
	$('#con2_gst').attr('readonly',true);
</script>";

$_SESSION['coal_ewb_date'] = $billDate2;
$_SESSION['coal_ewb_doc_no'] = $Doc_No;
$_SESSION['coal_ewb_total_inv_value'] = $totInvValue;
$_SESSION['coal_ewb_total_value'] = $totalValue;
$_SESSION['coal_ewb_exp_date'] = $validUpto;
$_SESSION['coal_ewb_extd_time'] = $extendedTimes;
$_SESSION['coal_ewb_veh_type'] = $vehicle_type;
$_SESSION['coal_ewb_doc_date'] = $Doc_date;
 
	echo "<script>
		$('#ewb_no').attr('readonly',true);
		$('#ewb_verify_btn').attr('disabled',true);
		$('#ewb_verify_btn').hide();
		$('#create_button').attr('disabled',false);
		$('#create_button').show();
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
?>	