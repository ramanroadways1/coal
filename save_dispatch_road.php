<?php
require_once("./connect.php");

$truck_type=mysqli_real_escape_string($conn,($_POST['truck_type'])); // Mkt/Own
$vou_no2=mysqli_real_escape_string($conn,($_POST['vou_no2'])); // Vou No select
$vou_no=mysqli_real_escape_string($conn,($_POST['vou_no'])); // FM No
$vou_id=mysqli_real_escape_string($conn,($_POST['vou_id'])); // FM TABLE id
$lr_type=mysqli_real_escape_string($conn,($_POST['lr_type'])); // MARKET / OWN
$ship_id=mysqli_real_escape_string($conn,($_POST['ship_id'])); // Shipment Id
$tno=mysqli_real_escape_string($conn,strtoupper($_POST['truck_no'])); 
$vou_date=mysqli_real_escape_string($conn,($_POST['vou_date2'])); // FM Date
$fm_weight=mysqli_real_escape_string($conn,($_POST['fm_weight'])); // Fm total Weight in LRs
$balance_weight=mysqli_real_escape_string($conn,($_POST['bal_weight'])); // Balance Weight of Shipment
$shipment_no=mysqli_real_escape_string($conn,($_POST['shipment_no'])); // Balance Weight of Shipment
$lrnos=mysqli_real_escape_string($conn,($_POST['lrnos'])); // Balance Weight of Shipment

$timestamp=date("Y-m-d H:i:s");

$dataFrom_Db=explode("_",$vou_no2);

$VouIdDb=$dataFrom_Db[0];
$weightDb=$dataFrom_Db[1];
$LRTypeDb=$dataFrom_Db[2];
$TruckNoDb=$dataFrom_Db[3];
$VouNoDb=$dataFrom_Db[4];
$VouDateDb=$dataFrom_Db[5];

if($vou_id!=$VouIdDb || $fm_weight!=$weightDb || $lr_type!=$LRTypeDb || $tno!=$TruckNoDb || $vou_no!=$VouNoDb || $vou_date!=$VouDateDb)
{
	echo "<script type='text/javascript'> alert('Verification Failed.'); </script>";exit();
}

if($vou_date=='' || $vou_id=='' || $vou_no=='' || $lr_type=='' || $tno=='' || $fm_weight=='' || $fm_weight==0)
{
	if($vou_date=='')
	{$error_name="VOU DATE";
	}else if($vou_id==''){
		$error_name="VOU ID";}
	else if($vou_no==''){
		$error_name="VOU NO";}
	else if($lr_type==''){
		$error_name="LR TYPE";}
	else if($tno==''){
		$error_name="TRUCK Number";}
	else if($fm_weight==''){
		$error_name="FM WEIGHT EMPTY";}
	else if($fm_weight==0){
		$error_name="FM WEIGHT ZERO";}
	else{
		$error_name="OTHERS";}
	
	echo "<script type='text/javascript'> alert('Unable to fetch Required Inputs : $error_name.'); </script>";exit();
}

if($ship_id=='' || $shipment_no=='')
{
	echo "<script type='text/javascript'> alert('Unable to fetch Shipment Id or Number.'); </script>";exit();
}

if($fm_weight>$balance_weight)
{
	echo "<script>alert('Error : LR total Weight is Grater than Balance Weight. Remaining Weight is : $balance_weight');</script>";exit();
}

$bal_weight_new=sprintf("%.02f",($balance_weight-$fm_weight));

$insert_dispatch = mysqli_query($conn,"INSERT INTO dispatch_road(shipment_id,vou_id,vou_no,lrnos,tno,vou_date,weight,branch,timestamp) 
VALUES ('$ship_id','$vou_id','$vou_no','$lrnos','$tno','$vou_date','$fm_weight','$branch','$timestamp')");

if(!$insert_dispatch)
{
	echo mysqli_error($conn);
	exit();
}

$update_shipment_table=mysqli_query($conn,"UPDATE shipment SET bal_weight='$bal_weight_new',by_road=by_road+'$fm_weight' WHERE 
id='$ship_id'");

if(!$update_shipment_table)
{
	echo mysqli_error($conn);
	exit();
}

if($lr_type=='MARKET')
{
	$table="freight_memo";
}
else
{
	$table="own_truck_form";
}

$update_vou_table=mysqli_query($conn,"UPDATE `$table` SET done='1',shipment_id='$ship_id' WHERE id='$vou_id'");

if(!$update_vou_table)
{
	echo mysqli_error($conn);
	exit();
}

	echo "<script>
			alert('Dispatch Via Road Success !');
			window.location.href='./view_rcvd.php';
		</script>";
	exit();
?>