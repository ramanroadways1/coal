<?php
require_once './connect.php';

$mobile = escapeString($conn,strtoupper($_POST['username']));
$timestamp = date("Y-m-d H:i:s");

if($mobile==''){
	echo "<script>
		alert('Mobile number is not valid !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$verify_login = Qry($conn,"SELECT name,mobile_no,whatsapp_enabled,code FROM rrpl_database.emp_attendance where 
code = (SELECT emp_code FROM rrpl_database.manager WHERE branch='$branch')");

if(!$verify_login){
	errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	echo "<script>
		alert('ERROR : While processing Request !');	
		window.location.href='./';
	</script>";
	exit();
}
		
		if(numRows($verify_login)==0)
        {
			echo "<script>
				alert('ERROR : Manager not found !');	
				window.location.href='./';
			</script>";
			exit();
		}
		
		$row_login = fetchArray($verify_login);
		
		
// $code = $row_login['code'];

// if($branch_sub_user!=$code)
// {
	// echo "<script>
		// alert('Warning : You're not manager of $branch Branch !');	
		// window.location.href='./';
	// </script>";
	// exit();
// }

$password = GenPassword(8);

if($row_login['whatsapp_enabled']=="0")
{
	MsgSendNewPasswordManager($row_login['mobile_no'],$branch,$password);
	
	$updatePass = Qry($conn,"UPDATE rrpl_database.manager SET pass='".md5($password)."',last_pass='$timestamp' WHERE branch='$branch'");
	
	if(!$updatePass){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	echo "<script>
			alert('Password reset success!');
			window.location.href='./';
			$('#loadicon').hide();
	</script>";
	exit();
}
else
{	
	$msg_template="Your new password for manager($branch) login is: $password.\nRamanRoadways.";
	$wa_msg_func = SendWAMsg($conn,$row_login['mobile_no'],$msg_template);
	
	if($wa_msg_func=="1")
	{
		$updatePass = Qry($conn,"UPDATE rrpl_database.manager SET pass='".md5($password)."',last_pass='$timestamp' WHERE branch='$branch'");
		
		if(!$updatePass){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}

		echo "<script>
				alert('Password reset success!');
				window.location.href='./';
				$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		echo "<script>
				alert('WhatsApp offline ! Unable to send msg.');
				window.location.href='./';
				$('#loadicon').hide();
			</script>";
		exit();
	}
}

?>