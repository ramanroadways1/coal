<?php
require_once("../connect.php");

if(isset($_POST['search'])){
 $search = $_POST['search'];

 $sql = Qry($conn, "SELECT id,name,state,pincode FROM rrpl_database.station WHERE name LIKE '%$search%' ORDER BY name ASC LIMIT 5");
 $response = array();
 while($row = fetchArray($sql) ){
   $response[] = array("value"=>$row['name'],"label"=>$row['name']." ($row[state]) - $row[pincode]","id"=>$row['id'],"pincode"=>$row['pincode']);
 }

 echo json_encode($response);
}
?>