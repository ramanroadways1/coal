<?php
require_once("../connect.php");

if(isset($_POST['search'])){
 $search = $_POST['search'];

 $sql = Qry($conn, "SELECT id,tno,wheeler,name,mo1,pan FROM rrpl_database.mk_truck WHERE tno LIKE '$search%' AND blocked='0' ORDER BY tno ASC LIMIT 8");
 $response = array();
 while($row = fetchArray($sql) ){
   $response[] = array("value"=>$row['tno'],"label"=>$row['tno'],"id"=>$row['id'],"wheeler"=>$row['wheeler'],"owner_name"=>$row['name'],"owner_mobile"=>$row['mo1'],"owner_pan"=>$row['pan']);
 }

 echo json_encode($response);
 closeConnection($conn);
}
?>