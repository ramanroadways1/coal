<?php
require_once("../connect.php");

if(isset($_GET['term']))
{
  $return_arr = array();

  try {
      $conn_autofill->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
      $stmt = $conn_autofill->prepare('SELECT name FROM station WHERE name LIKE :term ORDER BY name ASC LIMIT 10');
      $stmt->execute(array('term' => '%'.$_GET['term'].'%'));
      $total_row = $stmt->rowCount();
      if($total_row > 0){
      while($row = $stmt->fetch()) {
          $return_arr[] =  $row['name'];
      }
    }

  } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
  }
	echo json_encode($return_arr);
}
?>