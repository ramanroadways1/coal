<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Add More BL/BE :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	 </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
			
<script type="text/javascript">
$(document).ready(function (e) {
$("#AddForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_add_more_bl_be.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>			

<div id="result_form"></div> 		
			
<div class="row">
 
	<div class="form-group col-md-12 table-responsive">
	
        <table id="myTable" class="table table-bordered table-striped" style="font-size:13px;">
          <tr>
				<th>#</th>
				<th>Shipment_No</th>
				<th>ShipName</th>
				<th>Voyage_No</th>
				<th>Total_BL</th>
				<th>BL_Weight</th>
				<th>Shipment Date</th>
				<th>Receive</th>
			</tr>
          
            <?php
              $sql = Qry($conn,"SELECT shipment.id,unq_id,voyage_no,total_bl,bl_weight,date,name FROM shipment,vessel_name where 
			  rcv=1 AND vessel_name.id=vessel_name AND shipment.branch='$branch' ORDER BY shipment.id ASC");
             if(!$sql){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
					<td colspan='8'><b>NO RESULT FOUND..</b></td>
				</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
                echo 
                "<tr>
				  <td>$sn</td>
				  <td>$row[unq_id]</td>
				  <td>$row[name]</td>
				  <td>$row[voyage_no]</td>
				  <td>$row[total_bl]</td>
				  <td>$row[bl_weight]</td>
				  <td>".date("d-m-y",strtotime($row['date']))."</td>
				  <td>
				  <input type='hidden' value='$row[name]' id='shipname$row[id]'>
				  <button type='button' id='RcvButton$row[id]' onclick=RcvModal('$row[id]','$row[total_bl]','$row[unq_id]') 
				  class='btn btn-xs bg-green'>ADD BL/BE</button></td>
				 </tr>
				";
				 $sn++;
              }
			}
            ?>
        </table>
      </div>

  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="bg-primary modal-header">
        <h5 class="modal-title">Add BL/BE : <span id="ship_name"></span></h5>
      </div>
	<form id="AddForm" autocomplete="off">
      <div class="modal-body">
		 <div class="row">
		 
			<div class="form-group col-md-4">
				<label>BL Number <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,]/,'')" type="text" name="bl_no" class="form-control" placeholder="BL Number" required>
			</div>
			
			<div class="form-group col-md-4">
				<label>BL Date <font color="red">*</font></label>
				<input type="date" name="bl_date" class="form-control" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
			</div>
			
			<div class="form-group col-md-4">
				<label>BL Weight (in MTS) <font color="red">*</font></label>
				<input type="number" step="any" min="0.01" name="bl_weight" class="form-control" required>
			</div>
			
			<div class="form-group col-md-4">
				<label>BE Number <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9-,]/,'')" type="text" name="be_no" class="form-control" required>
			</div>
			
			<div class="form-group col-md-4">
				<label>BE Date <font color="red">*</font></label>
				<input type="date" name="be_date" class="form-control" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
			</div>
			
			<div class="form-group col-md-4">
				<label>BE Copy <font color="red">*</font></label>
				<!--<input type="file" accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" class="form-control" -->
				<input type="file" accept="application/pdf" class="form-control" name="be_copy" required />
			</div>
			
		</div>
			<input type="hidden" id="ship_id" name="ship_id">	
			<input type="hidden" id="total_no_of_bl" name="total_bl">	
			<input type="hidden" id="unq_id" name="unq_id">	
	  </div>

      <div class="modal-footer">
        <button type="submit" id="submit_button" class="btn btn-primary">ADD</button>
        <button type="button" id="close_modal_button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	
	
<script>	
function RcvModal(id,no_of_bl,unq_id)
{
	var name = $('#shipname'+id).val();
	$('#ship_id').val(id);
	$('#total_no_of_bl').val(no_of_bl);
	$('#unq_id').val(unq_id);
	$('#ship_name').html(name);
	$('#ModalButton')[0].click();
}
</script>
	
<?php
include "footer.php";
?>