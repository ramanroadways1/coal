<?php
require_once("connect.php");

$shipment_no = escapeString($conn,$_POST['shipment_no']);
$id = escapeString($conn,$_POST['id']);

$qry = Qry($conn,"SELECT location FROM dest_location WHERE ship_id='$shipment_no'");

if(!$qry){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

echo '<option class="dest_loc_name_opt" value="">--select destination--</option>';

if(numRows($qry)>0)
{
	while($row = fetchArray($qry))
	{
		echo "<option class='dest_loc_name_opt' id='dest_loc_name_opt_$row[location]' value='$row[location]'>$row[location]</option>";
	}
	
	echo "<script>
		$('.vessel_name_sel_box_opt').attr('disabled',true);
		$('#vessel_name_sel_box_opt_$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
		</script>";
}
else
{
	echo "<script>$('#loadicon').fadeOut('slow');</script>";
}

?>