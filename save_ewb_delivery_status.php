<?php
require_once 'connect.php';

$id=escapeString($conn,($_POST['ewb_id']));
$ewb_status=escapeString($conn,($_POST['ewb_status']));
$del_date=escapeString($conn,($_POST['del_date']));
$narration=escapeString($conn,($_POST['narration']));
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$update = Qry($conn,"UPDATE rrpl_database._eway_bill_validity SET branch_narration='$narration',branch_timestamp='$timestamp',
update_by_user='$branch_sub_user',del_date='$del_date' WHERE id='$id'");

if(!$update){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	echo "<script>
		alert('Ok : Status updated !!');
		$('#status_btn_$id').attr('disabled',true);
		$('#status_btn_$id').html('Updated');
		$('#btn_form').attr('disabled',false);
		$('#modal_close_btn')[0].click();
		$('#loadicon').hide();
	</script>";
	exit();
?>
