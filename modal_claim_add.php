<div id="ClaimModal" class="modal fade" style="background:#eee;overflow:hidden" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Add Claim Amount
      </div>
      <div class="modal-body">
        <div class="row">
		<form id="ClaimForm" action="#" style="font-size:13px" method="POST">
			
			<span style="color:red">&nbsp; &nbsp;  जिस LR का पैसा काटना चाहते है </span>
			<br />
			<br />
			
			<div class="form-group col-md-3">
				<label>Enter LR No. <font color="red"><sup>*</sup></font></label>
				<input type="text" onblur="GetClaimVouNo(this.value)" id="claim_lrno" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" name="lrno" class="form-control" required="required">
			</div>
			
			<script>
			function GetClaimVouNo(lrno)
			{
				if(lrno!='')
				{
					$("#loadicon").show();
					jQuery.ajax({
						url: 'check_claim_by_lrno.php',
						data: 'lrno=' + lrno,
						type: "POST",
						success: function(data){
							$("#claim_modal_vou_no").html(data);
						},
						error: function() {}
					});
				}
			}
			</script>
			
			<div class="form-group col-md-4">
				<label>Select Voucher. <font color="red"><sup>*</sup></font></label>
				<select name="claim_vou_no" style="font-size:13px" id="claim_modal_vou_no" class="form-control" required="required">
					<option value="" style="font-size:13px">--select voucher--</option>
				</select>
			</div>
			
			<div class="form-group col-md-3">
				<label>Claim Amount <font color="red"><sup>*</sup></font></label>
				<input type="number" min="1" name="claim_amount" id="claim_amt" class="form-control" required="required">
			</div>
			
			<input type="hidden" id="modal_claim_vou_no" value="" name="vou_no">
			<input type="hidden" value="BAL" name="vou_type">
				
			<div class="form-group col-md-2">
				<label>&nbsp;</label>
				<br>
				<button type="submit" id="claim_add_btn_one" class="btn btn-success">Add claim</button>
			</div>
		</form>	
			
		<div class="form-group col-md-12 table-responsive" id="claim_cache_result">
		</div>
	
		</div>
      </div>
	  <div id="claimFormResult"></div>
      <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#ClaimForm").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
		$.ajax({
        	url: 'add_claim_temp.php',
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#claimFormResult").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function LoadClaimLR(vou_no)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: 'get_claim_temp.php',
		data: 'vou_no=' + vou_no + '&type=' + 'BAL',
		type: "POST",
		success: function(data){
			$("#claim_cache_result").html(data);
		},
		error: function() {}
	});
}

function DeleteClaim(id)
{
	$("#loadicon").show();
	jQuery.ajax({
		url: 'delete_claim_temp.php',
		data: 'id=' + id + '&vou_no=' + 'BAL',
		type: "POST",
		success: function(data){
			$("#claimFormResult").html(data);
		},
		error: function() {}
	});
}
</script>