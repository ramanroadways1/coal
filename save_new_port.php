<?php
require_once("connect.php");

$timestamp=date("Y-m-d H:i:s");

$port_name=escapeString($conn,strtoupper($_POST['name']));

$chk=Qry($conn,"SELECT id FROM port_name WHERE name='$port_name'");
if(!$chk){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk)>0)
{
	echo "<script>
		alert('Duplicate Entry Found');
		$('#loadicon').hide();
		$('#add_port_button').attr('disabled',false);
	</script>";
	exit();	
}

$qry=Qry($conn,"INSERT INTO port_name(name,added_by,branch_user,timestamp) VALUES ('$port_name','$branch','$branch_sub_user','$timestamp')");

if(!$qry){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

echo "<script>
		alert('PORT Successfully Added !!');
		$('#AddPort')[0].reset();
		$('#loadicon').hide();
		$('#add_port_button').attr('disabled',false);
	</script>";
	exit();	
?>