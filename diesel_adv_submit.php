<?php
require_once("connect.php");

$card_pump=escapeString($conn,strtoupper($_POST['card_pump']));
$vou_id_for_diesel=escapeString($conn,strtoupper($_POST['vou_id_for_diesel']));
$vou_type=escapeString($conn,strtoupper($_POST['vou_type']));

$timestamp = date("Y-m-d H:i:s");
$rilpre = escapeString($conn,$_POST['rilpre']);

if($card_pump=='CARD' || $card_pump=='OTP')
{
	if($card_pump=='OTP')
	{
		$mobile_no = escapeString($conn,strtoupper($_POST['mobile_no']));
		$card_no = $mobile_no;
		$phy_card_no=$mobile_no;
	}
	else 
	{
		$card_no = escapeString($conn,strtoupper($_POST['card_no']));
		$mobile_no = "";
		$phy_card_no=escapeString($conn,strtoupper($_POST['phy_card_no']));
	}
	
	$cardno_id=escapeString($conn,strtoupper($_POST['cardno_id']));
	$fuel_company=escapeString($conn,strtoupper($_POST['fuel_company_man']));
}
else
{
	$card_no=escapeString($conn,strtoupper($_POST['pump_name']));
	$card_no=strtok($card_no,'-');
	$phy_card_no=$card_no;
	$cardno_id="0";
	$fuel_company=escapeString($conn,strtoupper($_POST['fuel_company_auto']));
	$mobile_no = "";
}

$diesel_amount=escapeString($conn,strtoupper($_POST['diesel_amount']));
$bill_no=escapeString($conn,strtoupper($_POST['bill_no']));

if($vou_type=="" || $vou_id_for_diesel=="" || $fuel_company=='')
{
	echo "<script>
		alert('Error : Unable to fetch required data.');
		window.location.href='./';
	</script>";
	exit();
}

if($rilpre=="1")
{
	$check_stock = Qry($conn,"SELECT stockid,balance FROM diesel_api.dsl_ril_stock WHERE cardno='$phy_card_no' ORDER BY id DESC LIMIT 1");
	
	if(!$check_stock){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($check_stock)==0){
		errorLog("Ril Stock not found. Card_no : $phy_card_no",$conn,$page_name,__LINE__);
		echo "<script>
			alert('Stock not found !');
			$('#card_no').val('');
			$('#card_no2').val('');
			$('#phy_card_no').val('');
			$('#rilpre').val('');
			$('#submit_diesel').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_ril_stock = fetchArray($check_stock);
	
	$ril_stockid = $row_ril_stock['stockid'];
	
	if($row_ril_stock['balance']<$diesel_amount)
	{
		echo "<script>
			alert('Card not in stock. Available Balance is : $row_ril_stock[balance].');
			$('#card_no').val('');
			$('#card_no2').val('');
			$('#phy_card_no').val('');
			$('#rilpre').val('');
			$('#submit_diesel').attr('disabled', false);
			$('#loadicon').hide();
		</script>";
		exit();
	}		
}
else
{
	$ril_stockid="";
}

	$insert = Qry($conn,"INSERT INTO diesel_cache (fm_no,type,amount,card_pump,bill_no,phy_card,card_id,card_no,fuel_company,narration,
	mobile_no,timestamp,ril_card,stockid) VALUES ('$vou_id_for_diesel','$vou_type','$diesel_amount','$card_pump','$bill_no','$phy_card_no','$cardno_id',
	'$card_no','$fuel_company','$fuel_company-$phy_card_no Rs: $diesel_amount/-','$mobile_no','$timestamp','$rilpre','$ril_stockid')");
		
	if(!$insert){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	echo "<script>
		alert('OK : SUCCESS');
		LoadDiesel();
		$('#submit_diesel').attr('disabled',false);
		document.getElementById('close_diesel_modal').click(); 
		document.getElementById('DieselAdvForm').reset();
		$('#loadicon').hide();
	</script>";		
?>