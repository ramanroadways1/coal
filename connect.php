<?php
session_start();

include($_SERVER['DOCUMENT_ROOT']."/_connect.php"); // for localhost
	
$timestamp=date("Y-m-d H:i:s");

if(!isset($_SESSION['rrpl_ship']))
{
	session_destroy();
	echo "<script>
		window.location.href='/';
	</script>";
	exit();
}

if(!isset($_SESSION['user_code']))
{
	session_destroy();
	echo "<script>
		window.location.href='/';
	</script>";
	exit();
}

if(isset($_SESSION['user']))
{
	session_destroy();
	echo "<script>
		//alert('$_SESSION[user]');
		window.location.href='/';
	</script>";
	exit();
}

$branch=$_SESSION['rrpl_ship'];
$branch_sub_user = $_SESSION['user_code'];

$conn=mysqli_connect($host,$username,$password,"ship");

if(!$conn){
    echo "<span style='font-family:Verdana'>Database Connection failed: <b><font color='red'>" .mysqli_connect_error()."</b></font></span>";
	exit();
}

$fromTime2 = DateTime::createFromFormat('H:i a', "18:00 pm");
$toTime2 = DateTime::createFromFormat('H:i a', "23:30 pm");

$mypage__name = basename($_SERVER['PHP_SELF']);

if(DateTime::createFromFormat('H:i a', date("H:i a")) > $fromTime2 && DateTime::createFromFormat('H:i a', date("H:i a")) < $toTime2)		
{		
	$chk_ewb_todays_1 = Qry($conn,"SELECT id FROM rrpl_database._eway_bill_validity WHERE branch_timestamp IS NULL AND 
	date(ewb_expiry)<='".date("Y-m-d")."' AND ewb_expiry!=0 AND branch='$branch'");
			
	if(!$chk_ewb_todays_1){
		errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
		exit();
	}

	if(numRows($chk_ewb_todays_1)>0 AND $mypage__name!='ewb_summary_2.php' AND $mypage__name!='get_ewb_summary_2.php' AND $mypage__name!='save_ewb_delivery_status.php')
	{
		$no_of_bills =  numRows($chk_ewb_todays_1);
				
		echo '<script>
			alert("********** It\'s time to update eway-bill status **********\nPending bills till today : '.$no_of_bills.'");
			window.location.href="https://rrpl.online/coal/ewb_summary_2.php";
			</script>';
		exit();
	}
}
?>