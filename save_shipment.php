<?php
require_once("./connect.php");

if(!isset($_POST['shipment_party'])){
	echo "<script>
		window.location.href='./add_shipment.php';
	</script>";
	exit();
}

$shipment_party=escapeString($conn,strtoupper($_POST['shipment_party']));
$shipper=escapeString($conn,strtoupper($_POST['shipper']));
$vessel_name=escapeString($conn,strtoupper($_POST['vessel_name']));
$loading_port=escapeString($conn,strtoupper($_POST['loading_port']));
$discharge_port=escapeString($conn,strtoupper($_POST['discharge_port']));
$hsn_code=escapeString($conn,($_POST['hsn_code']));

$ship_party_id=escapeString($conn,strtoupper($_POST['shipment_party_id']));
$shipper_id=escapeString($conn,strtoupper($_POST['shipper_name_id']));
$vessel_id=escapeString($conn,strtoupper($_POST['vessel_name_id']));
$loading_port_id=escapeString($conn,strtoupper($_POST['loading_port_id']));
$discharge_port_id=escapeString($conn,strtoupper($_POST['discharge_port_id']));

if($ship_party_id=='' || $shipper_id=='' || $vessel_id=='' || $loading_port_id=='' || $discharge_port_id=='')
{
	echo "<script>
		alert('Something went wrong unable to fetch Data.');
		$('#loadicon').hide();
	</script>";
	exit();
}

$voy_no=escapeString($conn,strtoupper($_POST['voy_no']));
$total_bl_weight=escapeString($conn,($_POST['total_bl_weight']));
$goods_desc=escapeString($conn,strtoupper($_POST['goods_desc']));

$chk_voyage=Qry($conn,"SELECT id FROM vessel_records WHERE vessel_id='$vessel_id' AND voyage_no='$voy_no'");
if(!$chk_voyage){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_voyage)>0)
{
	echo "<script>
		alert('Duplicate record found for vessel name: $vessel_name and voyage : $voy_no.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}

$timestamp=date("Y-m-d H:i:s");

$ramanc=Qry($conn,"SELECT unq_id FROM shipment where unq_id like '".date("dmy")."%' ORDER BY id DESC LIMIT 1");
if(!$ramanc){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}
		$row_count=fetchArray($ramanc);
		if(numRows($ramanc)>0)
		{
			$count=$row_count['unq_id'];
			$newstring = substr($count,6);
			$unq_id = ++$newstring;
			$unq_id = date("dmy").$unq_id;
		}
		else
		{
			$unq_id=date("dmy")."1";
		}
	
$total_bl=count($_POST['bl_no']);

if($total_bl<=0)
{
	echo "<script type='text/javascript'>
		alert('BL NOT ADDED YET.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}
	
if($total_bl_weight!=array_sum($_POST['bl_weight']))
{
	echo "<script type='text/javascript'>
		alert('Total BL weight and entered BL weight not verified.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;	

$insert_shipment = Qry($conn,"INSERT INTO shipment(unq_id,shipment_party,hsn_code,shipper,vessel_name,voyage_no,pol,pod,goods_desc,
total_bl,bl_weight,date,branch,branch_user,timestamp) VALUES ('$unq_id','$ship_party_id','$hsn_code','$shipper_id','$vessel_id','$voy_no','$loading_port_id',
'$discharge_port_id','$goods_desc','$total_bl','$total_bl_weight','".date("Y-m-d")."','$branch','$branch_sub_user','$timestamp')");

if(!$insert_shipment){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_vessel=Qry($conn,"INSERT INTO vessel_records(vessel_id,voyage_no,branch,branch_user) 
VALUES('$vessel_id','$voy_no','$branch','$branch_sub_user')");

if(!$update_vessel){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}


for($i=0; $i<$total_bl;$i++) 
{
    $insert_bl=Qry($conn,"INSERT INTO bl_data(unq_id,bl_no,bl_date,bl_weight,branch,branch_user,timestamp) VALUES 
    ('".$unq_id."','".escapeString($conn,strtoupper($_POST['bl_no'][$i]))."','".$_POST['bl_date'][$i]."',
	'".$_POST['bl_weight'][$i]."','$branch','$branch_sub_user','".$timestamp."')");
		  
	if(!$insert_bl){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('Shipment: $unq_id Created Successfully !');
			document.getElementById('Form1').reset();
			$('#loadicon').hide();
			$('#create_button').attr('disabled',false);
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',true);
	</script>";
	exit();
}
?>