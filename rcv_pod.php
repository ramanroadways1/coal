<?php
exit();
include_once "header.php";

if(isset($_POST['fm_no']))
{
	$fm_no=mysqli_real_escape_string($conn,$_POST['fm_no']);
	echo "<script>
	function ButtonClick()
	{
		document.getElementById('get_button').click();
	}
	</script>";
}
else
{
	$fm_no="";
	echo "<script>
	function ButtonClick()
	{
		
	}
	</script>";
}
?>
<div class="content-wrapper">
    <section class="content-header">
       <h4 style="font-family:Verdana">
		Receive Proof of Delivery (POD) :
      </h4>
	  
	  <style>
	  label{font-family:Verdana;font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#upload_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_rcv_pod.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_form").html(data);
	$("#loadicon").hide();
	$("#upload_button").attr("disabled", false);
	},
	error: function() 
	{} });}));});
</script>			

<div id="result_form"></div> 
   
<script type="text/javascript">
function FetchLRData()
{
	$('#lrnos').val('');
	$('#fm_no2').val('');
	
	var fm_no=$('#fm_no').val();
	
	if(fm_no!='')
	{
			$("#loadicon").show();
			jQuery.ajax({
			url: "fetch_lr_for_pod_rcv.php",
			data: 'fm_no=' + fm_no,
			type: "POST",
			success: function(data) {
			$("#result_form").html(data);
			$("#loadicon").hide();
			},
			error: function() {}
		});
	}
	else
	{
		alert('Enter Freight Memo Number First.');
		$('#result_table').hide();
	}
}
</script>  
   
			<div class="row">
			
			 <div class="form-group col-md-12">
			 <div class="form-group col-md-12">
		
				<div class="form-group col-md-12">
				 </div>
				 
                 <div class="form-group col-md-3">
                       <label>Freight Memo Number <font color="red">*</font></label>
                        <input type="text" value="<?php echo $fm_no; ?>" class="form-control" id="fm_no" name="fm_no" required />
                 </div>
				 
				 <div class="form-group col-md-1">
                       <label>&nbsp;</label>
                       <button type="button" onclick="FetchLRData()" id="get_button" class="btn btn-danger">Get Details</button>
                 </div>
			
			<form id="Form1" autocomplete="off">			
			
				<div class="row" id="result_table" style="display:none">
				
					<div class="form-group col-md-12">
						
						<input type="hidden" id="lrnos" name="lrno">
						<input type="hidden" id="fm_no2" name="fm_no">
						
						<div class="form-group col-md-3">
							 <label>Truck Number <font color="red">*</font></label>
							 <input type="text" class="form-control" id="tno" readonly required />
						 </div>
						 
						 <div class="form-group col-md-3">
							 <label>Freight Memo Date <font color="red">*</font></label>
							 <input type="text" class="form-control" id="fm_date" readonly required />
						 </div>
						 
						 <div class="form-group col-md-3">
							 <label>Weight <font color="red">*</font></label>
							 <input type="text" class="form-control" id="lr_weight" readonly required />
						 </div>
						 
						 <div class="form-group col-md-3">
							 <label>Consignor <font color="red">*</font></label>
							 <input type="text" class="form-control" id="con1" readonly required />
						 </div>
						 
						 <div class="form-group col-md-3">
							 <label>Consignee <font color="red">*</font></label>
							 <input type="text" class="form-control" id="con2" readonly required />
						 </div>
						 
						 <div class="form-group col-md-3">
							 <label>From <font color="red">*</font></label>
							 <input type="text" class="form-control" id="from_loc" readonly required />
						 </div>
						 
						 <div class="form-group col-md-3">
							 <label>To <font color="red">*</font></label>
							 <input type="text" class="form-control" id="to_loc" readonly required />
						 </div>
						 
						 <div class="form-group col-md-3">
							 <label>Un Loading Slip <font color="red">*</font></label>
							 <input type="file" class="form-control" name="unloading_slip[]" multiple="multiple" required />
						 </div>
						
						<div class="form-group col-md-3">
							<label>Unloading Date <font color="red">*</font></label>
							<input type="date" id="unload_date" class="form-control" name="unload_date" required max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
						 </div>
				 
						<div class="form-group col-md-3">
							 <label>POD Copy <font color="red">*</font></label>
							 <input type="file" class="form-control" name="pod_copy[]" multiple="multiple" required />
						 </div>
						 
						 <div class="form-group col-md-12">
						 <br />
							 <button type="submit" id="upload_button" class="btn btn-success btn-lg">Upload POD</button>
						 </div>
					</form>	
					 </div>
				 
				 </div>
		</div>
          </div>
          </div>
         
      </form>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		</div>
			</div> 
          </div>
        </div>       
    </section>
<script>	
ButtonClick();
</script>	
<?php
include "footer.php";
?>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />     
<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>	