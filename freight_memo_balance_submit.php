<?php
require_once("./connect.php");
$date=date("Y-m-d");
$timestamp=date("Y-m-d H:i:s");

$balance=escapeString($conn,strtoupper($_POST['balance']));
$bal_to=escapeString($conn,strtoupper($_POST['bal_to']));
$unloading=escapeString($conn,strtoupper($_POST['unloading']));
$others=escapeString($conn,strtoupper($_POST['others']));
$gps_charge=escapeString($conn,strtoupper($_POST['gps_device_charge']));
$gps_deposit_return_amt=escapeString($conn,strtoupper($_POST['gps_deposit_return_amt']));
$tds_amount=escapeString($conn,strtoupper($_POST['tds_amount']));
$total_balance=escapeString($conn,strtoupper($_POST['total_balance']));

$cash_bal=escapeString($conn,strtoupper($_POST['cash_bal']));
$diesel_bal=escapeString($conn,strtoupper($_POST['diesel_bal']));
$rtgs_bal=escapeString($conn,strtoupper($_POST['rtgs_bal']));

$bal_remains=escapeString($conn,strtoupper($_POST['bal_remains']));

$fm_id=escapeString($conn,strtoupper($_POST['fm_id']));
$fm_number=escapeString($conn,strtoupper($_POST['fm_number']));
$owner=escapeString($conn,strtoupper($_POST['owner']));
$owner_pan=escapeString($conn,strtoupper($_POST['owner_pan']));
$broker=escapeString($conn,strtoupper($_POST['broker']));
$broker_pan=escapeString($conn,strtoupper($_POST['broker_pan']));
$owner_id=escapeString($conn,strtoupper($_POST['owner_id']));
$broker_id=escapeString($conn,strtoupper($_POST['broker_id']));
$company=escapeString($conn,strtoupper($_POST['company']));
$truck_no=escapeString($conn,strtoupper($_POST['truck_no']));
$tds_deduct=escapeString($conn,strtoupper($_POST['tds_deduct']));
$pod_date=escapeString($conn,strtoupper($_POST['pod_date']));
$vou_date=escapeString($conn,strtoupper($_POST['vou_date']));
$claim=escapeString($conn,strtoupper($_POST['claim']));
$late_pod=escapeString($conn,strtoupper($_POST['late_pod']));
$pod_deduction=escapeString($conn,strtoupper($_POST['deduction'])); // amt entered at pod rcv

$datediff = strtotime($pod_date) - strtotime($vou_date);
$diff_value=round($datediff / (60 * 60 * 24));	

$chk_pod_status=Qry($conn,"SELECT gps_charge FROM rcv_pod WHERE fm_no='$fm_number'");

if(!$chk_pod_status){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
if(numRows($chk_pod_status)==0)
{
	echo "<script>
			alert('ERROR : POD not received.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}
else
{
	$row_chk_pod_status = fetchArray($chk_pod_status);
	$gps_charge_db = $row_chk_pod_status['gps_charge'];
}
	
	if($gps_charge_db!=$gps_charge)
	{
		echo "<script>
			alert('ERROR : GPS Charges not verified.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
// check gps deposit return
	
$chk_deposit_return=Qry($conn,"SELECT amount,status FROM gps_deposit_return WHERE fm_no='$fm_number'");

if(!$chk_deposit_return){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}
	
if(numRows($chk_deposit_return)==0)
{
	$gps_deposit_db = 0;
}
else
{
	$row_gps_deposit = fetchArray($chk_deposit_return);
	
	if($row_gps_deposit['status']=="0")
	{
		$gps_deposit_db = 0;
	}
	else
	{
		$gps_deposit_db = $row_gps_deposit['amount'];
	}
}	
	
	if($gps_deposit_db!=$gps_deposit_return_amt)
	{
		echo "<script>
			alert('ERROR : GPS Deposit not verified.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
if($bal_to=='OWNER')
{
	$party_name=$owner;
	$party_pan=$owner_pan;
	$party_type=2;
}
else if($bal_to=='BROKER')
{
	$party_name=$broker;
	$party_pan=$broker_pan;
	$party_type=1;
}
else
{
	if(strlen($owner_pan!=10))
	{
		if(strlen($broker_pan!=10))
		{
			echo "<script>
				alert('ERROR : ATLEAST ONE PARTY PAN CARD REQUIRED.');
				$('#submit_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
		else
		{
			$party_name=$broker;
			$party_pan=$broker_pan;
			$party_type=1;
		}
	}
	else
	{
		$party_name=$owner;
		$party_pan=$owner_pan;
		$party_type=2;
	}
}

if($tds_deduct>0)
{
	$pan_latter = substr($party_pan,3,1);
	
	$get_tds_value=Qry($conn,"SELECT tds_value FROM rrpl_database.tds_value WHERE pan_letter='$pan_latter'");

	if(!$get_tds_value){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($get_tds_value)==0)
	{
		$tds_value1 = 0;
	}
	else
	{
		$row_tds_value = fetchArray($get_tds_value);
		$tds_value1 = $row_tds_value['tds_value'];
	}
	
	if($tds_amount!=round($unloading*$tds_value1/100))
	{
		echo "<script>
			alert('ERROR : TDS Amount is Invalid.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

if($total_balance!=$balance+$unloading+$gps_deposit_return_amt-$others-$gps_charge-$tds_amount-$claim-$late_pod)
{
	echo "<script>
			alert('ERROR : BALANCE AMOUNT NOT MATCHING.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if($bal_remains!=0)
{
	echo "<script>
			alert('ERROR : BALANCE REMAINS SHOULD BE ZERO.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if($bal_remains<0)
{
	echo "<script>
			alert('ERROR : Balance Remains is Negative. Please CHECK !!');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if($diff_value>30)
{
	$qry_pod_charges=Qry($conn,"SELECT chrg FROM rrpl_database.pod_late_chrg WHERE $diff_value>=`from1` AND $diff_value<=`to1` AND 
	$balance>=`amt_frm` AND $balance<=`amt_to`");
	
	if(!$qry_pod_charges){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	$row_pod_charges=fetchArray($qry_pod_charges);
	$late_pod_db=$row_pod_charges['chrg'];
}
else
{
	$late_pod_db=0;
}

if($late_pod!=$late_pod_db)
{
	echo "<script>
			alert('ERROR : LATE POD Charge is not Valid.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
	</script>";
	exit();
}

if($cash_bal>0)
{
	$fetch_bal=Qry($conn,"SELECT balance,balance2 FROM rrpl_database.user WHERE username='$branch'");
	if(!$fetch_bal){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	$row_balance=fetchArray($fetch_bal);
	
	$rrpl_cash=$row_balance['balance'];
	$rr_cash=$row_balance['balance2'];
	
	if($company=='RRPL' AND $row_balance['balance']>=$cash_bal)
	{
		$dr_col="debit";
		$balance_col="balance";
		$new_bal=$rrpl_cash-$cash_bal;
	}
	else if($company=='RAMAN_ROADWAYS' AND $row_balance['balance2']>=$cash_bal)
	{
		$dr_col="debit2";
		$balance_col="balance2";
		$new_bal=$rr_cash-$cash_bal;
	}
	else
	{
		echo "<script>
			alert('BALANCE ERROR : INSUFFICIENT BALANCE IN : $company.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

if($diesel_bal>0)
{
	$chk_diesel = Qry($conn,"SELECT SUM(amount) as total_dsl FROM diesel_cache WHERE fm_no='$fm_number' AND type='BAL'");
	if(!$chk_diesel){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_diesel)==0)
	{
		echo "<script>
			alert('Error : Diesel entry not found.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_sum_diesel = fetchArray($chk_diesel);
	
	if($row_sum_diesel['total_dsl']!=$diesel_bal)
	{
		echo "<script>
			alert('Error : Please check diesel amount.');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$chk_ril_stock = Qry($conn,"SELECT phy_card,amount FROM diesel_cache WHERE fm_no='$fm_number' AND ril_card='1' AND type='BAL'");
	
	if(!$chk_ril_stock){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	if(numRows($chk_ril_stock)>0)
	{
		while($row_ril = fetchArray($chk_ril_stock))
		{
			$get_balance = Qry($conn,"SELECT balance FROM diesel_api.dsl_ril_stock WHERE cardno='$row_ril[phy_card]' ORDER BY id DESC LIMIT 1");
			if(!$get_balance){
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				Redirect("Error while processing Request","./");
				exit();
			}
			
			$row_balance = fetchArray($get_balance);
			
			if($row_balance['balance']<$row_ril['amount'])
			{
				echo "<script>
					alert('Card not in stock : $row_ril[phy_card]. Available balance is : $row_balance[balance] !');
					$('#loadicon').hide();
					$('#submit_button').attr('disabled', false);
				</script>";
				exit();	
			}
		}
	}
}

if($rtgs_bal>0)
{
	if($bal_to=='BROKER')
	{
		$qry_ac = Qry($conn,"SELECT acname,accno as acno,bank,ifsc FROM rrpl_database.mk_broker WHERE id='$broker_id'");
		if(!$qry_ac){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($qry_ac)==0)
		{
			echo "<script>
				alert('Error : Broker account details not found.');
				$('#submit_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
	
	}
	else if($bal_to=='OWNER')
	{
		$qry_ac = Qry($conn,"SELECT acname,accno as acno,bank,ifsc FROM rrpl_database.mk_truck WHERE id='$owner_id'");
		if(!$qry_ac){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}
		
		if(numRows($qry_ac)==0)
		{
			echo "<script>
				alert('Error : Owner account details not found.');
				$('#submit_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	
	$row_ac=fetchArray($qry_ac);
	
	$acname=$row_ac['acname'];
	$acno=$row_ac['acno'];
	$bank=$row_ac['bank'];
	$ifsc=$row_ac['ifsc'];
	
	if($acname=='')
	{
		$acname=escapeString($conn,strtoupper($_POST['ac_holder']));
		$acno=escapeString($conn,strtoupper($_POST['ac_number']));
		$bank=escapeString($conn,strtoupper($_POST['bank_name']));
		$ifsc=escapeString($conn,strtoupper($_POST['ifsc_code']));
		
		if($acname=='')
		{
			echo "<script>
				alert('ERROR : UNABLE TO FETCH ACCOUNT DETAILS.');
				$('#submit_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		if($bal_to=='BROKER')
		{
			$update_id=$broker_id;
			$tab_name="mk_broker";
		}
		else if($bal_to=='OWNER')
		{
			$update_id=$owner_id;
			$tab_name="mk_truck";
		}
			
		$update_rtgs_ac="YES";
	}
	else
	{
		$update_rtgs_ac="NO";
	}
	
	$rtgs_bal_set="0";
}
else
{
	$update_rtgs_ac="NO";
	$rtgs_bal_set="1";
}

// echo "<script>
			// alert('OK');
			// $('#submit_button').attr('disabled',false);
			// $('#loadicon').hide();
		// </script>";
		// exit();
		
StartCommit($conn);
$flag = true;
$rtgs_flag = false;	

$insert_balance_fm=Qry($conn,"INSERT INTO freight_memo_bal(fm_no,bal_party,party_name,party_pan,unloading,other,pod_deduction,
tds,claim,gps_charges,gps_deposit_return,late_pod,total_bal,cash,diesel,rtgs,branch,branch_user,date,timestamp,rtgs_bal) 
VALUES ('$fm_number','$party_type','$party_name','$party_pan','$unloading','$others','$pod_deduction','$tds_amount','$claim','$gps_charge',
'$gps_deposit_return_amt','$late_pod','$total_balance','$cash_bal','$diesel_bal','$rtgs_bal','$branch','$branch_sub_user',
'$date','$timestamp','$rtgs_bal_set')");

if(!$insert_balance_fm){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($gps_deposit_return_amt>0)
{
	$update_gps_deposit_status = Qry($conn,"UPDATE gps_deposit_return SET status='0' WHERE fm_no='$fm_number'");

	if(!$update_gps_deposit_status){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

// gps_deposit_return

$update_freight_memo=Qry($conn,"UPDATE freight_memo SET balance='1' WHERE id='$fm_id'");
if(!$update_freight_memo){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Freight memo table not updated for balance flag.",$conn,$page_name,__LINE__);
}

if($cash_bal>0)
{
	$insert_cash=Qry($conn,"INSERT INTO cashbook(branch,branch_user,company,vou_date,date,vou_no,vou_type,narration,`$dr_col`,
	`$balance_col`,timestamp) VALUES ('$branch','$branch_sub_user','$company','$vou_date','$date','$fm_number','Freight_Memo',
	'BALANCE_CASH','$cash_bal','$new_bal','$timestamp')");
	
	if(!$insert_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	$update_new_bal=Qry($conn,"UPDATE rrpl_database.user SET `$balance_col`=`$balance_col`-'$cash_bal' WHERE username='$branch'");
	
	if(!$update_new_bal){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($diesel_bal>0)
{
	$insert_diesel=Qry($conn,"INSERT INTO diesel(vou_no,vou_type,card_pump,card_no,fuel_company,date,branch,branch_user,
	timestamp) SELECT '$fm_number','BALANCE',card_pump,phy_card,fuel_company,'$date','$branch','$branch_sub_user','$timestamp' 
	FROM diesel_cache WHERE fm_no='$fm_number' AND type='BAL'");
	
	if(!$insert_diesel){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_diesel_main_Db=Qry($conn,"INSERT INTO rrpl_database.diesel_fm(branch,branch_user,fno,com,qty,rate,disamt,tno,type,
	dsl_by,bill_no,dcard,veh_no,dcom,dsl_nrr,pay_date,fm_date,ril_card,dsl_mobileno,timestamp,stockid) SELECT '$branch',
	'$branch_sub_user','$fm_number','$company',qty,rate,amount,'$truck_no','BALANCE',card_pump,bill_no,card_no,phy_card,fuel_company,
	narration,'$date','$vou_date',ril_card,mobile_no,'$timestamp',stockid FROM diesel_cache WHERE fm_no='$fm_number' AND type='BAL'");
	
	if(!$insert_diesel_main_Db){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$get_ril_amount = Qry($conn,"SELECT phy_card,amount FROM diesel_cache WHERE fm_no='$fm_number' AND ril_card='1' AND type='BAL'");
	
	if(!$get_ril_amount){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(numRows($get_ril_amount)>0)
	{
		while($row_ril2 = fetchArray($get_ril_amount))
		{
			$update_ril_stock = Qry($conn,"UPDATE diesel_api.dsl_ril_stock SET balance=balance-'$row_ril2[amount]' WHERE 
			cardno='$row_ril2[phy_card]' ORDER BY id DESC LIMIT 1");
			
			if(!$update_ril_stock){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
	
	$sel_data_for_log = Qry($conn,"SELECT id,branch,disamt,tno,dsl_by,dcard,veh_no,dcom,timestamp,dsl_mobileno FROM 
	rrpl_database.diesel_fm WHERE timestamp='$timestamp' AND fno='$fm_number' AND type='BALANCE'");
	
	if(!$sel_data_for_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
		
	while($row_log=fetchArray($sel_data_for_log))
	{
		if($row_log['dsl_by']=='CARD')
		{
			$card_live_bal=Qry($conn,"select balance from diesel_api.dsl_cards where cardno='$row_log[dcard]'");
					
			if(!$card_live_bal){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		
			$row_card_bal=fetchArray($card_live_bal); 
			$live_bal = $row_card_bal['balance']; 

			$qry_log=Qry($conn,"insert into diesel_api.dsl_logs (user,status,content,timestamp,trucktype,truckno,
			dslcomp,dsltype,cardno,vehno,cardbal,amount,mobileno,reqid) VALUES 
			('$row_log[branch]','SUCCESS','New Request Added','$row_log[timestamp]','MARKET','$row_log[tno]','$row_log[dcom]',
			'CARD','$row_log[dcard]','$row_log[veh_no]','$live_bal','$row_log[disamt]','$row_log[dsl_mobileno]','$row_log[id]')");
					
			if(!$qry_log){
				$flag = false;
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
		}
	}
	
	$dlt_cache = Qry($conn,"DELETE FROM diesel_cache WHERE fm_no='$fm_number' AND type='BAL'");
	
	if(!$dlt_cache){
		$flag = false;	
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($rtgs_bal>0)
{
	if(date('m')>3){ $year = date('y'); } else { $year = (date('y')-1); }
												
		if($company=='RRPL')
		{
			$crn="RRPL".$year;
			
			$ramanc=Qry($conn,"SELECT crn FROM rrpl_database.rtgs_fm where crn like '$crn%' ORDER BY id DESC LIMIT 1");
			if(!$ramanc){
				$flag = false;	
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($ramanc)!=0)
			{
				$rowcrn=fetchArray($ramanc);
				$count=$rowcrn['crn'];	
				$newstring = substr($count,6,13);
				$newstr = ++$newstring;
			}
			else
			{
				$newstr=1;
			}	
		}
		else if($company=='RAMAN_ROADWAYS')
		{	
			$crn="RR".$year;
			
			$ramanc = Qry($conn,"SELECT DISTINCT crn FROM rrpl_database.rtgs_fm where crn like '$crn%' ORDER BY id DESC LIMIT 1");
			if(!$ramanc){
				$flag = false;	
				errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			}
			
			if(numRows($ramanc)!=0)
			{
				$rowcrn = fetchArray($ramanc);
				$count = $rowcrn['crn'];	
				$newstring = substr($count,4);
				$newstr = ++$newstring;
			}
			else
			{
				$newstr = 1;
			}	
		}
		
		$crnnew = $crn.sprintf("%'.06d",$newstr);
		
	$insert_diesel=Qry($conn,"INSERT INTO rtgs(vou_no,vou_type,branch,amount,acname,acno,bank,ifsc,pan,tno,crn,date,timestamp) 
	VALUES ('$fm_number','BALANCE','$branch','$rtgs_bal','$acname','$acno','$bank','$ifsc','$party_pan','$truck_no','$crnnew',
	'$date','$timestamp')");
	
	if(!$insert_diesel){
		$flag = false;	
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
			
	$insert_rtgs_main_Db=Qry($conn,"INSERT INTO rrpl_database.rtgs_fm (fno,branch,com,totalamt,amount,acname,acno,bank_name,
	ifsc,pan,pay_date,fm_date,tno,type,crn,timestamp) VALUES ('$fm_number','$branch','$company','$total_balance','$rtgs_bal',
	'$acname','$acno','$bank','$ifsc','$party_pan','$date','$vou_date','$truck_no','BALANCE','$crnnew','$timestamp')");
	
	if(!$insert_rtgs_main_Db){
		$flag = false;	
		$rtgs_flag = true;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($update_rtgs_ac=='YES')
{
	if($bal_to=='OWNER')
	{
		$update_ac=Qry($conn,"UPDATE rrpl_database.mk_truck SET acname='$acname',accno='$acno',bank='$bank',ifsc='$ifsc' WHERE 
		id='$update_id'");
		if(!$update_ac){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$update_ac=Qry($conn,"UPDATE rrpl_database.mk_broker SET acname='$acname',accno='$acno',bank='$bank',ifsc='$ifsc' WHERE 
		id='$update_id'");
		if(!$update_ac){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
			
	$update_ac_log=Qry($conn,"INSERT INTO rrpl_database.ac_update(o_b,ac_for,at_the_time_of,acname,acno,bank,ifsc,branch,
	branch_user,timestamp) VALUES ('$bal_to','$update_id','BALANCE','$acname','$acno','$bank','$ifsc','$branch',
	'$branch_sub_user','$timestamp')");
		
	if(!$update_ac_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('SUCCESS : BALANCE PAID.');
		$('#BalForm')[0].reset();
		$('#BalanceId$fm_id').attr('disabled',true);
		$('#BalanceId$fm_id').html('ADVANCE PAID');
		$('#BalanceId$fm_id').attr('class','btn btn-xs bg-green');
		$('#loadicon').hide();
		document.getElementById('close_modal_button').click();
		// window.location.href='./freight_memo_adv_pending.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	if($rtgs_flag)
	{
		echo "<script type='text/javascript'>
			alert('Please try again after 5-10 seconds..');
			$('#submit_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	else
	{
		echo "<script type='text/javascript'>
			alert('Error While Processing Request.');
			$('#submit_button').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
?>