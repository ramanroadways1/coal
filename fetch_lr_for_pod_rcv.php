<?php 
require_once("connect.php");

$fm_no=mysqli_real_escape_string($conn,strtoupper($_POST['fm_no']));

if($fm_no!='')
{
	$qry=mysqli_query($conn,"SELECT freight_memo.id,date,weight,freight,pod,
			  GROUP_CONCAT(lrno SEPARATOR ', ') as lrno,broker.name as broker_name,consignor.name as con1,consignee.name as con2,
			  station.name as from1,station.name as to1,tno FROM freight_memo,lr_entry,broker,consignor,consignee,station 
			  WHERE freight_memo.fm_no='$fm_no' AND freight_memo.advance='1' AND 
			  FIND_IN_SET(lr_entry.id,lr_ids) AND freight_memo.broker=broker.id AND lr_entry.from_loc=station.id AND 
			  lr_entry.to_loc=station.id AND lr_entry.con1=consignor.id AND lr_entry.con2=station.id GROUP by fm_no");
	
	if(mysqli_num_rows($qry)>0)
	{
		$row=mysqli_fetch_array($qry);
		
		if($row['pod']=='1')
		{
			echo "<script type='text/javascript'>
				alert('POD Already Received for This FM : $fm_no.');
				$('#result_table').hide();
				$('#fm_no2').val('');
				$('#lrnos').val('');
				$('#get_button').attr('disabled',false);
				$('#fm_no').attr('readonly',false);
			</script>";
			exit();
		}
		else
		{
		echo "<script type='text/javascript'>
				$('#fm_no').attr('readonly',true);
				$('#lrnos').val('$row[lrno]');
				$('#fm_no2').val('$fm_no');
				$('#tno').val('$row[tno]');
				$('#fm_date').val('$row[date]');
				$('#lr_weight').val('$row[weight]');
				$('#con1').val('$row[con1]');
				$('#con2').val('$row[con2]');
				$('#from_loc').val('$row[from1]');
				$('#to_loc').val('$row[to1]');
				$('#get_button').attr('disabled',true);
				$('#result_table').show();
			</script>";
			exit();
		}	
	}
	else
	{
		echo "<script type='text/javascript'>
				alert('Invalid Freight Memo Number Entered.');
				$('#result_table').hide();
				$('#lrnos').val('');
				$('#fm_no2').val('');
				$('#get_button').attr('disabled',false);
				$('#fm_no').attr('readonly',false);
			</script>";
			exit();
	}	
}
?>