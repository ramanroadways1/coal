<?php
require_once("./connect.php");
 
$EwbNo = escapeString($conn,$_POST['EwbNo']);
$id2 = escapeString($conn,$_POST['id']);

$Fetch=Qry($conn,"SELECT ewbNo,ewayBillDate,genMode,userGstin,supplyType,subSupplyType,
		docType,docNo,docDate,fromGstin,fromTrdName,fromAddr1,fromAddr2,fromPlace,fromPincode,fromStateCode,toGstin,toTrdName,
		toAddr1,toAddr2,toPlace,toPincode,toStateCode,totalValue,totInvValue,cgstValue,sgstValue,igstValue,cessValue,
		transporterId,transporterName,status,actualDist,noValidDays,validUpto,extendedTimes,rejectStatus,vehicleType,
		actFromStateCode,actToStateCode,transactionType,otherValue,cessNonAdvolValue,itemNo,productId,productName,productDesc,
		hsnCode,quantity,qtyUnit,cgstRate,sgstRate,igstRate,cessRate,cessNonAdvol,taxableAmount,updMode,vehicleNo,fromPlace_v,
		fromState,tripshtNo,userGSTINTransin,enteredDate,transMode,transDocNo,transDocDate,groupNo,timestamp 
		FROM eway_bill WHERE ewbNo='$EwbNo'");
		
if(!$Fetch){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($Fetch)==0)
{
	echo "<script>
		alert('Error: Invalid EWB Number.');
		$('#loadicon').hide();
		$('#Download$id2').attr('disabled',false);
	</script>";
	exit();
}	

$row1=fetchArray($Fetch);

$ewbNo=$row1['ewbNo'];
$ewayBillDate=$row1['ewayBillDate'];
$genMode=$row1['genMode'];
$userGstin=$row1['userGstin'];
$supplyType=$row1['supplyType'];
$subSupplyType=$row1['subSupplyType'];
$docType=$row1['docType'];
$docNo=$row1['docNo'];
$docDate=$row1['docDate'];
$fromGstin=$row1['fromGstin'];
$fromTrdName=$row1['fromTrdName'];
$fromAddr1=$row1['fromAddr1'];
$fromAddr2=$row1['fromAddr2'];
$fromPlace=$row1['fromPlace'];
$fromPincode=$row1['fromPincode'];
$fromStateCode=$row1['fromStateCode'];
$toGstin=$row1['toGstin'];
$toTrdName=$row1['toTrdName'];
$toAddr1=$row1['toAddr1'];
$toAddr2=$row1['toAddr2'];
$toPlace=$row1['toPlace'];
$toPincode=$row1['toPincode'];
$toStateCode=$row1['toStateCode'];
$totalValue=$row1['totalValue'];
$totInvValue=$row1['totInvValue'];
$cgstValue=$row1['cgstValue'];
$sgstValue=$row1['sgstValue'];
$igstValue=$row1['igstValue'];
$cessValue=$row1['cessValue'];
$transporterId=$row1['transporterId'];
$transporterName=$row1['transporterName'];
$status=$row1['status'];
$actualDist=$row1['actualDist'];
$noValidDays=$row1['noValidDays'];
$validUpto=$row1['validUpto'];
$extendedTimes=$row1['extendedTimes'];
$rejectStatus=$row1['rejectStatus'];
$vehicleType=$row1['vehicleType'];
$actFromStateCode=$row1['actFromStateCode'];
$actToStateCode=$row1['actToStateCode'];
$transactionType=$row1['transactionType'];
$otherValue=$row1['otherValue'];
$cessNonAdvolValue=$row1['cessNonAdvolValue'];

// ITEM DETAILS

$itemNo=$row1['itemNo'];
$productId=$row1['productId'];
$productName=$row1['productName'];
$productDesc=$row1['productDesc'];
$hsnCode=$row1['hsnCode'];
$quantity=$row1['quantity'];
$qtyUnit=$row1['qtyUnit'];
$cgstRate=$row1['cgstRate'];
$sgstRate=$row1['sgstRate'];
$igstRate=$row1['igstRate'];
$cessRate=$row1['cessRate'];
$cessNonAdvol=$row1['cessNonAdvol'];
$taxableAmount=$row1['taxableAmount'];

// VEHICLE DETAILS

$updMode=$row1['updMode'];
$vehicleNo=$row1['vehicleNo'];
$fromPlace_v=$row1['fromPlace_v'];
$fromState=$row1['fromState'];
$tripshtNo=$row1['tripshtNo'];
$userGSTINTransin=$row1['userGSTINTransin'];
$enteredDate=$row1['enteredDate'];
$transMode=$row1['transMode'];
$transDocNo=$row1['transDocNo'];
$transDocDate=$row1['transDocDate'];
$groupNo=$row1['groupNo'];

$data = array(
"ewbNo"=>"$ewbNo",
"ewayBillDate"=>"$ewayBillDate",
"genMode"=>"$genMode",
"userGstin"=>"$userGstin",
"supplyType"=>"$supplyType",
"subSupplyType"=>"$subSupplyType",
"docType"=>"$docType",
"docNo"=>"$docNo",
"docDate"=>"$docDate",
"fromGstin"=>"$fromGstin",
"fromTrdName"=>"$fromTrdName",
"fromAddr1"=>"$fromAddr1",
"fromAddr2"=>"$fromAddr2",
"fromPlace"=>"$fromPlace",
"fromPincode"=>"$fromPincode",
"fromStateCode"=>"$fromStateCode",
"toGstin"=>"$toGstin",
"toTrdName"=>"$toTrdName",
"toAddr1"=>"$toAddr1",
"toAddr2"=>"$toAddr2",
"toPlace"=>"$toPlace",
"toPincode"=>"$toPincode",
"toStateCode"=>"$toStateCode",
"totalValue"=>"$totalValue",
"totInvValue"=>"$totInvValue",
"cgstValue"=>"$cgstValue",
"sgstValue"=>"$sgstValue",
"igstValue"=>"$igstValue",
"cessValue"=>"$cessValue",
"transporterId"=>"$transporterId",
"transporterName"=>"$transporterName",
"status"=>"$status",
"actualDist"=>"$actualDist",
"noValidDays"=>"$noValidDays",
"validUpto"=>"$validUpto",
"extendedTimes"=>"$extendedTimes",
"rejectStatus"=>"$rejectStatus",
"vehicleType"=>"$vehicleType",
"actFromStateCode"=>"$actFromStateCode",
"actToStateCode"=>"$actToStateCode",
"transactionType"=>"$transactionType",
"otherValue"=>"$otherValue",
"cessNonAdvolValue"=>"$cessNonAdvolValue",
"itemList"=>array([
'itemNo'=>$itemNo,
'productId'=>$productId,
'productName'=>$productName,
'productDesc'=>$productDesc,
'hsnCode'=>$hsnCode,
'quantity'=>$quantity,
'qtyUnit'=>$qtyUnit,
'cgstRate'=>$cgstRate,
'sgstRate'=>$sgstRate,
'igstRate'=>$igstRate,
'cessRate'=>$cessRate,
'cessNonAdvol'=>$cessNonAdvol,
'taxableAmount'=>$taxableAmount,
]),
"VehiclListDetails"=>array([
'updMode'=>$updMode,
'vehicleNo'=>$vehicleNo,
'fromPlace'=>$fromPlace_v,
'fromState'=>$fromState,
'tripshtNo'=>$tripshtNo,
'userGSTINTransin'=>$userGSTINTransin,
'enteredDate'=>$enteredDate,
'transMode'=>$transMode,
'transDocNo'=>$transDocNo,
'transDocDate'=>$transDocDate,
'groupNo'=>$groupNo,
])
);
$postData = json_encode($data);

$asp_id = $tax_pro_asp_id;
$asp_Passwd = $tax_pro_asp_password;
$myGSTIn = $rrpl_gst_no;
$UserName = $ewb_rrpl_username;
$PassWord = $ewb_rrpl_password;
 
$ch = curl_init("$tax_pro_url/aspapi/v1.0/printdetailewb?aspid=$asp_id&password=$asp_Passwd&gstin=$myGSTIn&username=$UserName");
curl_setopt_array($ch, 
  array( CURLOPT_POST => TRUE, 
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 300,
  CURLOPT_RETURNTRANSFER => TRUE,
  CURLOPT_BINARYTRANSFER => TRUE, 
  CURLOPT_HTTPHEADER => array('Content-type:application/pdf'), 
  CURLOPT_POSTFIELDS => $postData)
);

$response = curl_exec($ch);

// echo $response;

// echo "<script>
	// $('#View$id2').attr('disabled',true);
	// $('#Download$id2').attr('disabled',false);
	// $('#loadicon').hide();
// </script>";
// exit();
file_put_contents("EWayFolder/$EwbNo.pdf", $response);

$update=Qry($conn,"UPDATE eway_bill_log SET download_link='EWayFolder/$EwbNo.pdf' WHERE eway_bill_no='$EwbNo'");

if(!$update){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

echo "<script>
	alert('Download Success. Click on View Button');
	$('#View$id2').attr('disabled',false);
	$('#Download$id2').attr('disabled',true);
	$('#loadicon').hide();
</script>";
?>