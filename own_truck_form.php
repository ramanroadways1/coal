<?php
include_once "header.php";
?>
<script type="text/javascript">
   	$(function() {
		$("#truck_no").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/get_own_vehicle.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
               // $('#wheeler').val(ui.item.wheeler);	
				$("#chk_lr_button").attr('disabled',false);				   			   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle does not exists.');
			$("#truck_no").val('');
			$("#truck_no").focus();
			// $("#wheeler").focus();
			$("#chk_lr_button").attr('disabled',true);				   
		}}, 
		focus: function (event, ui){
			return false;}
		});});	
</script>	

<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Create Own Truck Form :
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#create_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_own_truck_form.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>			

	<form id="Form1" autocomplete="off">
	<div class="row">
			<div class="form-group col-md-12">
				<div class="form-group col-md-3">
                       <label>Vehicle Number <font color="red">*</font></label>
                        <input type="text" class="form-control" id="truck_no" name="truck_no" required />
                 </div>
				 
				<div class="form-group col-md-2">
                     <label>Total LR Weight <font color="red">*</font></label>
                     <input type="number" value="0" min="1" step="any" class="form-control" id="weight" name="weight" readonly required />
                </div>
				
				<div class="form-group col-md-2">
					 <label>&nbsp;</label>
					 <br>
					<button type="button" id="chk_lr_button" class="btn btn-primary" onclick="FetchLR();">ADD LR</button>
                </div>
				 
				 <script>
				 function FetchLR()
				 {
					 var tno = $('#truck_no').val();
					 
					 if(tno=='')
					 {
						 alert('Enter Truck Number First !');
						 $('#lr_result').html('');
					 }
					 else
					 {
						 $('#chk_lr_button').attr('disabled',true);
						 
						 $("#loadicon").show();
							jQuery.ajax({
							url: "fetch_lr_for_fm.php",
							data: 'tno=' + tno + '&own_vehicle=' + 'YES',
							type: "POST",
							success: function(data) {
								$("#lr_result").html(data);
							},
							error: function() {}
						});
					 }	
				 }
				 
				 function AddLR(shipment_id,id,lrno,weight)
				 {
					var ext_weight = Number($('#weight').val());
					 
					 var ext_shiment_id = $('#shipment_id').val();
					 
					 if(ext_shiment_id!='' && ext_shiment_id!=shipment_id)
					 {
						 alert('LR not belongs to another shipment.');
					 }
					 else
					 {
						$('#shipment_id').val(shipment_id);
						 $('#AddBtn'+id).hide();
						 $('#RemoveBtn'+id).show();
						 $('#weight').val(Number(ext_weight+Number(weight)));
						 $('#lrid'+id).val(id);
						 $('#rightImg'+id).show();
					 }	 
				 }
				 
				 function RemoveLR(shipment_id,id,lrno,weight)
				 {
					var ext_weight = Number($('#weight').val());
					 
					 $('#AddBtn'+id).show();
					 $('#RemoveBtn'+id).hide();
					 $('#weight').val(Number(ext_weight-Number(weight)));
					 
					 if(Number(ext_weight-Number(weight))<=0)
					 {
						 $('#shipment_id').val('');
					 }
					 
					 $('#lrid'+id).val('');
					 $('#rightImg'+id).hide();
				 }
				 </script>
				 
				 <input id="shipment_id" type="hidden" name="shipment_id">
				 
				 <div class="form-group col-md-12" id="lr_result">
                 </div>
				
				<div class="form-group col-md-3">   
					<button style="display:none" type="submit" disabled id="create_button" class="btn btn-success">Create Own Truck Form</button>
                </div> 
				
				<div class="form-group col-md-6" id="result_form"></div> 
				 
          </div>
          </div>
         
      </form>

</div>

		</div>
			</div> 
          </div>
        </div>       
    </section>
	
		<script>
		// function Func1()
		// {
			// var sum = 0;
			// $(".bl_weight").each(function(){
				// sum += +$(this).val();
			// });
			// $(".total_bl_weight").val(sum);
		// }
		</script>
<?php
include "footer.php";
?>