<?php
include_once "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Reports :
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
 
<script type="text/javascript">
   	$(function() {
		$("#truck_no").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/get_market_vehicle.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
               $('#truck_no_id').val(ui.item.id);     
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle does not exists.');
			$("#truck_no").val('');
			$("#truck_no").focus();
			$("#truck_no_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});	
  
     	$(function() {
		$("#broker_name").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/broker.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#broker_name').val(ui.item.value);   
               $('#broker_id').val(ui.item.id);     
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Broker does not exists.');
			$("#broker_name").val('');
			$("#broker_name").focus();
			$("#broker_id").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script>		
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<div id="result_form"></div> 
   
        <form action="report2.php" method="POST" autocomplete="off">

         <div class="row">
			
			 <div class="form-group col-md-12">
			 <div class="form-group col-md-12">
		
				<div class="form-group col-md-12">
				 </div>
				 
				 <script>
				 function MyFunc1(elem)
				 {
					 $('#from_date').val('');
					 $('#to_date').val('');
					 
					 if(elem=='DISPATCH')
					 {
						 $('#from_div').show();
						 $('#to_div').show();
						 $('#broker_name_div').hide();
						 $('#broker_name').attr('required',false);
						 $('#truck_no_div').hide();
						 $('#truck_no').attr('required',false);
						 
						 $('#from_date_html').html('LR From Date');
						 $('#to_date_html').html('LR To Date');
					 }
					 else if(elem=='BROKER_A' || elem=='BROKER_B' || elem=='BROKER_FM_DATE')
					 {
						 $('#from_div').show();
						 $('#to_div').show();
						 $('#broker_name_div').show();
						 $('#broker_name').attr('required',true);
						 
						 $('#truck_no_div').hide();
						 $('#truck_no').attr('required',false);
						 
						 if(elem=='BROKER_A')
						 {
							 $('#from_date_html').html('Advance From Date');
							$('#to_date_html').html('Advance To Date');
						 }
						 else if(elem=='BROKER_B')
						 {
							 $('#from_date_html').html('Balance From Date');
							$('#to_date_html').html('Balance To Date');
						 }
						 else
						 {
							$('#from_date_html').html('FM From Date');
							$('#to_date_html').html('FM To Date');
						 }
					}
					else if(elem=='OWNER_A' || elem=='OWNER_B' || elem=='OWNER_FM_DATE')
					 {
						 $('#from_div').show();
						 $('#to_div').show();
						 $('#broker_name_div').hide();
						 $('#broker_name').attr('required',false);
						 
						 $('#truck_no_div').show();
						 $('#truck_no').attr('required',true);
						 
						 if(elem=='OWNER_A')
						 {
							 $('#from_date_html').html('Advance From Date');
							$('#to_date_html').html('Advance To Date');
						 }
						 else if(elem=='OWNER_B')
						 {
							 $('#from_date_html').html('Balance From Date');
							$('#to_date_html').html('Balance To Date');
						 }
						 else
						 {
							$('#from_date_html').html('FM From Date');
							$('#to_date_html').html('FM To Date');
						 }
					}
					 else
					 {
						$('#from_div').hide();
					    $('#to_div').hide(); 
						 $('#truck_no_div').hide();
						 $('#truck_no').attr('required',true);
					    $('#broker_name_div').hide(); 
						 $('#broker_name').attr('required',true);
					 }	
				 }
				 </script>
				 
				<div class="form-group col-md-3">
                    <label>Select your report <font color="red">*</font></label>
                    <select onchange="MyFunc1(this.value)" name="report" class="form-control" required>
						<option value="">---SELECT---</option>
						<option value="DISPATCH">Full Dispatch Report</option>
						<option value="BROKER_A">Broker's Wise FM (Adv Date)</option>
						<option value="BROKER_B">Broker's Wise FM (Bal Date)</option>
						<option value="BROKER_FM_DATE">Broker's Wise FM (FM Date)</option>
						<option value="OWNER_A">Truck Wise FM (Adv Date)</option>
						<option value="OWNER_B">Truck Wise FM (Bal Date)</option>
						<option value="OWNER_FM_DATE">Truck Wise FM (FM Date)</option>
					</select>
                 </div>
				 
				 <div class="form-group col-md-3" id="broker_name_div" style="display:none">
                    <label>Broker Name <font color="red">*</font></label>
                    <input type="text" id="broker_name" name="broker_name" class="form-control" required />
                 </div>
				 
				 <div class="form-group col-md-3" id="truck_no_div" style="display:none">
                    <label>Vehicle Number <font color="red">*</font></label>
                   <input type="text" id="truck_no" class="form-control" required />
                 </div>
				 
				 <input type="hidden" id="broker_id" name="bid">
				 <input type="hidden" id="truck_no_id" name="oid">
				 
				 <div style="display:none" id="from_div" class="form-group col-md-3">
                       <label><span id="from_date_html">From Date</span> <font color="red">*</font></label>
                       <input type="date" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" name="from_date" required class="form-control">
                 </div>
				 
				 <div style="display:none" id="to_div" class="form-group col-md-3">
                       <label><span id="to_date_html">To Date</span> <font color="red">*</font></label>
                       <input type="date" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" name="to_date" required class="form-control">
                 </div>
				
				<div class="form-group col-md-12">   
					<button type="submit" id="create_button" class="btn btn-success">Show Report</button>
                </div> 
		</div>
          </div>
          </div>
         
      </form>

</div>
</div>
			</div> 
          </div>
        </div>       
    </section>
<?php
include "footer.php";
?>