<?php
require_once("connect.php");

$location = escapeString($conn,$_POST['loc']);
$branch = escapeString($conn,$_POST['branch']);
$shipment_no = escapeString($conn,$_POST['shipment_no']);

$qry = Qry($conn,"SELECT loc_id,pincode,po_no,distance FROM dest_location WHERE branch='$branch' AND 
location='$location' AND ship_id='$shipment_no'");

if(!$qry){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($qry)>0)
{
	$row = fetchArray($qry);
	echo "<script>
		$('#to_pin_code').val('$row[pincode]'); 
		$('#po_no').val('$row[po_no]'); 
		$('#approx_distance').val('$row[distance]');
		$('#to_loc_id').val('$row[loc_id]');
		$('#loadicon').hide();
	</script>";
}
else
{
	echo "<script>
		$('#to_pin_code').val(''); 
		$('#po_no').val('');
		$('#approx_distance').val('');
		$('#to_loc_id').val('');
		$('#loadicon').hide();
	</script>";
}
?>