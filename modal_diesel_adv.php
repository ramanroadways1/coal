<script type="text/javascript">
$(document).ready(function (e) {
$("#DieselAdvForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_diesel").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./diesel_adv_submit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#diesel_adv_res").html(data);
	},
	error: function() 
	{} });}));});
</script>

<div class="modal" id="DieselModal" style="background:#DDD">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
	
    <div class="bg-primary modal-header">
        <h4 class="modal-title"><b>Add Diesel Advance : </h4></b>
	</div>

	<form id="DieselAdvForm" autocomplete="off">
	
      <div class="modal-body">
	  <div id="diesel_adv_res"></div>	
		 <div class="row">
		 
			<div class="form-group col-md-4">
				<label>Card/Pump <font color="red">*</font></label>
				<select name="card_pump" id="card_pump" onchange="DieselSel(this.value);ResetFields2();$('#fuel_company_man').val('')" class="form-control" required>
					<option value="">--SELECT--</option>
					<option value="CARD">Card</option>
					<option value="OTP">OTP</option>
					<option value="PUMP">PUMP</option>
				</select>
			</div>
			
			<div class="form-group col-md-4">
				<label>Amount <font color="red">*</font></label>
				<input type="number" min="1" class="form-control" name="diesel_amount" required />
			</div>
			
			<script>
			function DieselSel(elem)
			{
				$('#card_no').val('');
				$('#mobile_no').val('');
				
				if(elem=='CARD')
				{
					$('#card_no_div').show();
					$('#phy_card_no_div').show();
					$('#fuel_company_man_div').show();
					$('#fuel_company_man').attr('required',true);
					$('#card_no').attr('required',true);
					$('#mobile_no_div').hide();
					$('#mobile_no').attr('required',false);
					
					$('#pump_name_div').hide();
					$('#fuel_company_auto_div').hide();
					$('#fuel_company_auto').attr('required',false);
					$('#pump_name').attr('required',false);
					
					$('#bill_no_div').hide();	
			        $('#bill_no').attr('required',false);	
					
				}
				else if(elem=='OTP')
				{
					$('#phy_card_no_div').hide();
					$('#card_no_div').hide();
					$('#fuel_company_man_div').show();
					$('#fuel_company_man').attr('required',true);
					$('#card_no').attr('required',false);
					$('#mobile_no_div').show();
					$('#mobile_no').attr('required',true);
					
					$('#pump_name_div').hide();
					$('#fuel_company_auto_div').hide();
					$('#fuel_company_auto').attr('required',false);
					$('#pump_name').attr('required',false);
					
					$('#bill_no_div').hide();	
			        $('#bill_no').attr('required',false);	
					
				}
				else if(elem=='PUMP')
				{
					$('#phy_card_no_div').hide();
					$('#card_no_div').hide();
					$('#fuel_company_man_div').hide();
					$('#fuel_company_man').attr('required',false);
					$('#card_no').attr('required',false);
					$('#mobile_no_div').hide();
					$('#mobile_no').attr('required',false);
					
					$('#pump_name_div').show();
					$('#fuel_company_auto_div').show();
					$('#fuel_company_auto').attr('required',true);
					$('#pump_name').attr('required',true);
					
					$('#bill_no_div').show();	
			        $('#bill_no').attr('required',true);
				}
				else
				{
					$('#phy_card_no_div').show();
					$('#card_no_div').show();
					$('#fuel_company_man_div').show();
					$('#fuel_company_man').attr('required',true);
					$('#card_no').attr('required',true);
					$('#mobile_no_div').hide();
					$('#mobile_no').attr('required',false);
					
					$('#pump_name_div').hide();
					$('#fuel_company_auto_div').hide();
					$('#fuel_company_auto').attr('required',false);
					$('#pump_name').attr('required',false);
					
					$('#bill_no_div').hide();	
			        $('#bill_no').attr('required',false);	
				}
			}
			</script>
			
			<div class="form-group col-md-4" style="display:none" id="pump_name_div">
				<label>PUMP Name <font color="red">*</font></label>
				<select onchange="SetPumpCode(this.value)" name="pump_name" id="pump_name" class="form-control" required>
					<option value="">--SELECT--</option>
					<?php
					$sel_pump=Qry($conn,"SELECT name,code,comp FROM rrpl_database.diesel_pump WHERE branch='$branch'");
					if(!$sel_pump){
						errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
						Redirect("Error while processing Request","./");
						exit();	
					}
					
					if(numRows($sel_pump)>0)
					{
						while($row_pump=fetchArray($sel_pump))
						{
							echo "<option value='$row_pump[code]-$row_pump[comp]'>$row_pump[name]</option>";
						}	
					}
					?>
				</select>
			</div>
			
			<script>
			function SetPumpCode(code)
			{
				var com = code.split("-").pop();
				$('#fuel_company_auto').val(com);
			}
			
			function ResetFields2()
			{
				$('#card_no').val('');
				$('#mobile_no').val('');
				$('#phy_card_no').val('');
				$('#cardno_id').val('');
			}
			</script>
			
			<div class="form-group col-md-4" id="fuel_company_man_div">
				<label>Fuel Company <font color="red">*</font></label>
				<select name="fuel_company_man" id="fuel_company_man" onchange="ResetFields2();" class="form-control" required>
					<option value="">--SELECT--</option>
					<option value="IOCL">IOCL</option>
					<option value="BPCL">BPCL</option>
					<option disabled value="HPCL">HPCL</option>
					<option value="RELIANCE">RELIANCE</option>
					<option value="RIL">RIL</option>
				</select>
			</div>
			
			<div class="form-group col-md-4" id="card_no_div">
				<label>Card Number <font color="red">*</font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');ChkCompany();" id="card_no" 
				name="card_no" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4" id="mobile_no_div" style="display:none">
				<label>Mobile Number <font color="red">*</font></label>
				<input type="text" oninput="this.value=this.value.replace(/[^0-9]/,'');ChkCompany();" id="mobile_no" 
				name="mobile_no" maxlength="10" minlength="10" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4" id="phy_card_no_div">
				<label>Virtual Id <font color="red">*</font></label>
				<input type="text" name="phy_card_no" id="phy_card_no" readonly class="form-control" required />
			</div>
			
			<input type="hidden" id="cardno_id" name="cardno_id">
			<input type="hidden" id="rilpre" name="rilpre">
			
			<input type="hidden" id="vou_id_for_diesel" name="vou_id_for_diesel">
			<input type="hidden" id="vou_type_diesel2" name="vou_type">
			
			<script>
			function ChkCompany()
			{
				if($('#fuel_company_man').val()=='')
				{
					$('#card_no').val("");
					$('#mobile_no').val("");
					$('#fuel_company_man').focus();
				}
			}
			</script>
			
			<div style="display:none" id="fuel_company_auto_div" class="form-group col-md-4">
				<label>Fuel Company <font color="red">*</font></label>
				<input type="text" id="fuel_company_auto" name="fuel_company_auto" class="form-control" readonly required />
			</div>
			
			<div style="display:none" id="bill_no_div" class="col-md-6">
                   <div class="form-group">
                       <label class="control-label mb-1">Bill/Slip No.</label>
					   <input type="text" maxlength="10" minlength="10" oninput="this.value=this.value.replace(/[^A-Za-z 0-9,-]/,'')" name="bill_no" id="bill_no" class="form-control" />
				  </div>
            </div>
			
	</div>
</div>
	
	<div class="modal-footer">
        <button type="submit" id="submit_diesel" class="btn btn-primary">Submit</button>
        <button type="button" id="close_diesel_modal" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>
	
	</form>
    </div>
  </div>
</div>	