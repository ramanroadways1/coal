<?php
require_once("./connect.php");

$ship_id=escapeString($conn,($_POST['ship_id']));
$rr_no=escapeString($conn,strtoupper($_POST['rr_no']));
$frn_no=escapeString($conn,strtoupper($_POST['frn_no']));
$rr_date=escapeString($conn,($_POST['rr_date']));
$rr_weight=escapeString($conn,($_POST['rr_weight']));
$no_of_wgn=escapeString($conn,($_POST['no_of_wgn']));
$bl_weight=escapeString($conn,($_POST['weight']));
$balance_weight=escapeString($conn,($_POST['bal_weight']));

$timestamp=date("Y-m-d H:i:s");

if($frn_no=='' || $rr_no=='' || $rr_date=='' || $rr_date==0)
{
	echo "<script type='text/javascript'>
		alert('All fields are required.');
		$('#loadicon').hide();
		$('#submit_rail').attr('disabled',false);
	</script>";
	exit();
}

if($ship_id=='')
{
	echo "<script type='text/javascript'>
		alert('Unable to fetch Shipment Id.');
		$('#loadicon').hide();
		$('#submit_rail').attr('disabled',false);
	</script>";
	exit();
}

if($rr_weight>$balance_weight)
{
	echo "<script>
		alert('Error : RR Weight is Grater than Balance Weight. Remaining Weight is : $balance_weight');
		$('#loadicon').hide();
		$('#submit_rail').attr('disabled',false);
	</script>";
	exit();
}

$chk_frno_no=Qry($conn,"SELECT id FROM dispatch_rail WHERE frn_no='$frn_no'");
if(!$chk_frno_no){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_frno_no)>0)
{
	echo "<script type='text/javascript'>
		alert('Duplicate FRN Number Found.');
		$('#loadicon').hide();
		$('#submit_rail').attr('disabled',false);
	</script>";
	exit();
}

$chk_rr_no=Qry($conn,"SELECT id FROM dispatch_rail WHERE rr_no='$rr_no'");
if(!$chk_rr_no){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_rr_no)>0)
{
	echo "<script type='text/javascript'>
		alert('Duplicate RR Number Found.');
		$('#loadicon').hide();
		$('#submit_rail').attr('disabled',false);
	</script>";
	exit();
}

$bal_weight_new=sprintf("%.02f",($balance_weight-$rr_weight));

if(count($_FILES['upload']['name'])>0)
{
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	foreach($_FILES['upload']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			echo "<script>
				alert('Error : Only Image Upload Allowed.');
				$('#loadicon').hide();
				$('#submit_rail').attr('disabled',false);
			</script>";
			exit();
		}
    }
	
	for($i2=0; $i2<count($_FILES['upload']['name']);$i2++) 
	{
		$UploadedSize = $_FILES['upload']['size'][$i2];
		
		if(($UploadedSize/1024/1024)>2)
		{
			echo "<script>
				alert('Error : MAX Upload size allowed is : 2MB');
				$('#loadicon').hide();
				$('#submit_rail').attr('disabled',false);
			</script>";
			exit();	
		}
	}
	
		for($i=0; $i<count($_FILES['upload']['name']);$i++) 
		{
			$sourcePath = $_FILES['upload']['tmp_name'][$i];
			
			if($sourcePath!="")
			{
				$fix_name=date('dmYHis').mt_rand();
				$targetPath = "weagon_photo/".$fix_name.".".pathinfo($_FILES['upload']['name'][$i],PATHINFO_EXTENSION);
				
				ImageUpload(700,700,$sourcePath);

				if(move_uploaded_file($sourcePath, $targetPath)) 
				{
					$files[] = $targetPath;
				}
				else
				{
					echo "<script>
						alert('No attachment found.');
						$('#loadicon').hide();
						$('#submit_rail').attr('disabled',false);
					</script>";
					exit();
				}
			}
			else
			{
				echo "<script>
					alert('No attachment found.');
					$('#loadicon').hide();
					$('#submit_rail').attr('disabled',false);
				</script>";
				exit();
			}
        }
		
		$file_name=implode(',',$files);
}
else
{
	echo "<script>
		alert('No attachment found.');
		$('#loadicon').hide();
		$('#submit_rail').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;	

$insert_dispatch = Qry($conn,"INSERT INTO dispatch_rail(shipment_id,frn_no,rr_no,rr_date,rr_weight,no_of_wgn,upload,branch,
branch_user,timestamp) VALUES ('$ship_id','$frn_no','$rr_no','$rr_date','$rr_weight','$no_of_wgn','$file_name','$branch',
'$branch_sub_user','$timestamp')");

if(!$insert_dispatch){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_shipment_table=Qry($conn,"UPDATE shipment SET bal_weight='$bal_weight_new',by_rail=by_rail+'$rr_weight' WHERE 
id='$ship_id'");

if(!$update_shipment_table){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('Dispatch Via Railway Success !');
			window.location.href='./view_rcvd.php';
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#submit_rail').attr('disabled',false);
	</script>";
	exit();
}
?>