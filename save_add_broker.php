<?php
require_once './connect.php'; 

$date=date('Y-m-d');
$timestamp=date("Y-m-d H:i:s");

$name= trim(escapeString($conn,strtoupper($_POST['name'])));
$mobile=escapeString($conn,strtoupper($_POST['mobile']));
$pan_no=escapeString($conn,strtoupper($_POST['pan_no']));
$addr=trim(escapeString($conn,strtoupper($_POST['addr'])));

if(strpos($name, 'RECT') !== false)
{
   echo "<script>
			alert('Invald Broker Name');
			$('#loadicon').hide();
			$('#add_broker_button').attr('disabled',false);
		</script>";
	exit();
}

if(strlen($mobile)!=10)
{
	 echo "<script>
			alert('Invald Mobile Number');
			$('#loadicon').hide();
			$('#add_broker_button').attr('disabled',false);
		</script>";
	exit();
}

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_broker_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(!preg_match("/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/", $pan_no))
{
	echo "<script>
			alert('Invalid PAN No.');
			$('#loadicon').hide();
			$('#add_broker_button').attr('disabled',false);
		</script>";
	exit();
}

$chk_pan = Qry($conn,"SELECT name FROM rrpl_database.mk_broker WHERE pan='$pan_no' AND pan!='NA' AND pan!=''");
if(!$chk_pan){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_pan)>0)
{
	$row_pan=fetchArray($chk_pan);
	
	echo "<script type='text/javascript'>
			alert('PAN No $pan_no already exists with Broker $row_pan[name].'); 
			$('#loadicon').hide();
			$('#add_broker_button').attr('disabled',false);
		</script>";
	exit();	
}


$chk_name=Qry($conn,"SELECT pan FROM rrpl_database.mk_broker WHERE name='$name'");
if(!$chk_name){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_name)>0)
{
	$row_name=fetchArray($chk_name);
	
	echo "<script type='text/javascript'>
			alert('Broker Name : $name already exists with PAN No : $row_name[pan].'); 
			$('#loadicon').hide();
			$('#add_broker_button').attr('disabled',false);
		</script>";
	exit();	
}

	
$sourcePath = $_FILES['pan_copy']['tmp_name']; // UPLOAD FILE

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");

if(!in_array($_FILES['pan_copy']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image and PDF Upload Allowed.');
		$('#add_broker_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$fix_name = mt_rand().date('dmYHis');

$targetPath="broker_pan/".$fix_name.".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);
$targetPath2="../b5aY6EZzK52NA8F/broker_pan/".$fix_name.".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);

ImageUpload(1000,1000,$sourcePath);

if(!move_uploaded_file($sourcePath,$targetPath2))
{
	echo "<script>
		alert('Error : Unable to Upload PAN Card Copy.');
		$('#add_broker_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
	
$insert = Qry($conn,"INSERT INTO rrpl_database.mk_broker(name,mo1,pan,full,up5,branch,branch_user,timestamp) 
VALUES ('$name','$mobile','$pan_no','$addr','$targetPath','$branch','$branch_sub_user','$timestamp')");

	
	if(!$insert)
	{
		unlink($targetPath2);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	echo "
	<script type='text/javascript'> 
		alert('Broker : $name Added Successfully..');
		$('#AddBroker')[0].reset();
		$('#loadicon').hide();
		$('#add_broker_button').attr('disabled',false);
	</script>";
	exit();
?>