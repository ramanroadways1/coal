<?php
require_once("../_connect.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/png" href="favicon.png"/>
	<title>Raman Sea Trans : Raman Group</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="../coal/google_font.css" rel="stylesheet">

</head>

<style>
td {
  white-space: normal !important; 
  word-wrap: break-word;  
}
table {
  table-layout: fixed;
}
</style>

<style type="text/css" media="print">
@media print {
body {
   zoom:60%;
 }
}
</style>

<style type="text/css">
@media print
{
body * { visibility: hidden; }
.container-fluid * { visibility: visible}
.container-fluid { position: absolute; top: 0; left: 0; }
}

label{
	font-size:13px;
}
</style>

<body style="font-family: 'Open Sans', sans-serif !important">

<button onclick="window.close()" type="button" style="margin:10px" class="btn btn-sm btn-danger">Close window</button>
<button onclick="window.print()" type="button" style="margin:10px" class="btn btn-sm btn-primary">Print Voucher</button>

<div class="container-fluid">
	<div class="col-md-12">
<?php
if(isset($_POST['key']))
{
	$lrno11=escapeString($conn,($_POST['lrno']));
	
	$fm_search = Qry($conn,"SELECT lr_type FROM ship.lr_entry WHERE lrno='$lrno11'");
	if(!$fm_search){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}

	if(numRows($fm_search)==0)
	{
		echo "<script>
			alert('LR not found !');
			window.close();
		</script>";
		exit();
	}
	
	$row11=fetchArray($fm_search);
	
	if($row11['lr_type']=='MARKET')
	{
		$fm_search1 = Qry($conn,"SELECT fm_no FROM ship.freight_memo WHERE lrnos='$lrno11'");
		$type='F';
	}
	else
	{
		$fm_search1 = Qry($conn,"SELECT fm_no FROM ship.own_truck_form WHERE lrnos='$lrno11'");
		$type='O';
	}
	
	if(!$fm_search1){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($fm_search)==0)
	{
		echo "<script>
			alert('Voucher not found. Contact System ADMIN !');
			window.close();
		</script>";
		exit();
	}
	
	$row22 = fetchArray($fm_search1);
	
	$vou_no=escapeString($conn,($row22['fm_no']));
}
else
{
	$vou_no=escapeString($conn,($_POST['vou_no']));
	$type=substr($vou_no,3,1);
}

if($type=='O')
{
	$qry=Qry($conn,"SELECT fm_no,truck_no,company,date,weight,lr_ids FROM ship.own_truck_form WHERE fm_no='$vou_no'");
	if(!$qry){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}
	
	if(numRows($qry)==0)
	{
		echo "<br><br>
		<center><h3>NO RESULT FOUND : OWN TRUCK FORM.</h3></center>";
		exit();
	}
	
	$row=fetchArray($qry);
	
	echo "
	<h4>OWN TRUCK FORM : $vou_no </h4>
	<br>
	<table class='table table-bordered table-striped' style='font-size:13px;'>
		<tr>
			<th>Vou Number</th>
			<th>Truck Number</th>
			<th>Company</th>
			<th>Vou Date</th>
			<th>Weight</th>
		</tr>	
		
		<tr>
			<td>$row[fm_no]</td>
			<td>$row[truck_no]</td>
			<td>$row[company]</td>
			<td>$row[date]</td>
			<td>$row[weight]</td>
		</tr>	
		
	</table>
	<h5>LR Details : </h5>
	<table class='table table-bordered table-striped' style='font-size:13px;'>
		<tr>
			<th>LR No</th>
			<th>From</th>
			<th>To</th>
			<th>Consignor</th>
			<th>Consignee</th>
			<th>ActWt</th>
			<th>ChrgWt</th>
		</tr>	
	";
	// echo $row['lr_ids']
		$qry2=Qry($conn,"SELECT l.lrno,l.actual_wt,l.charge_wt,loc1.name as from1,
		loc2.name as to1,c1.name as con1,c2.name as con2
		FROM ship.lr_entry as l 
		LEFT OUTER JOIN rrpl_database.station as loc1 on loc1.id=l.from_loc
		LEFT OUTER JOIN rrpl_database.station as loc2 on loc2.id=l.to_loc
		LEFT OUTER JOIN rrpl_database.consignor as c1 on c1.id=l.con1
		LEFT OUTER JOIN rrpl_database.consignee as c2 on c2.id=l.con2
		WHERE l.id IN($row[lr_ids])");
		
		if(!$qry2){
			ScriptError($conn,$page_name,__LINE__);
			exit();
		}
		
		if(numRows($qry2)>0)
		{
			while($row2=fetchArray($qry2))
			{
				echo "<tr>
					<td>$row2[lrno]</td>
					<td>$row2[from1]</td>
					<td>$row2[to1]</td>
					<td>$row2[con1]</td>
					<td>$row2[con2]</td>
					<td>$row2[actual_wt]</td>
					<td>$row2[charge_wt]</td>
				</tr>";
			}
		}
		else
		{
			echo "<tr>
				<td colspan='7'>NO LR FOUND.</td>
			</tr>";
		}
	echo "	
	</table>
	";
}
else if($type=='F')
{
	$qry=Qry($conn,"SELECT f.fm_no,f.tno,f.company,f.date as fm_date,f.weight,f.rate_fix,f.rate,f.freight,f.lr_ids,f.advance,f.balance,
	f.pod,f.pod_date,b.name as broker_name,b.pan as broker_pan,o.name as owner_name,o.pan as owner_pan,d.name as driver_name,d.pan as 
	driver_lic_no,a.adv_party as adv_to,a.party_name as adv_party,a.party_pan as adv_pan,a.loading,a.other as adv_other,a.tds as adv_tds,
	a.gps_deposit,a.total_freight,a.total_adv,a.cash as adv_cash,a.cheque as adv_cheque,a.diesel as adv_diesel,a.rtgs as adv_rtgs,a.date as adv_date,
	bal.bal_party as bal_to,bal.party_name as bal_party,
	bal.party_pan as bal_pan,bal.unloading,bal.other as bal_other,bal.tds as bal_tds,bal.claim,bal.gps_charges,
	bal.gps_deposit_return,bal.late_pod,bal.total_bal,
	bal.cash as bal_cash,bal.cheque bal_cheque,
	bal.diesel as bal_diesel,bal.rtgs as bal_rtgs,bal.branch as bal_branch,bal.date as bal_date,d1.card_pump as adv_card,d1.card_no as adv_card_no,
	d1.fuel_company as adv_fuel_comp,d2.card_pump as bal_card,d2.card_no as bal_card_no,d2.fuel_company as bal_fuel_comp
	FROM ship.freight_memo as f
	LEFT OUTER JOIN rrpl_database.mk_broker as b ON b.id=f.broker
	LEFT OUTER JOIN rrpl_database.mk_truck as o ON o.id=f.owner
	LEFT OUTER JOIN rrpl_database.mk_driver as d ON d.id=f.driver
	LEFT OUTER JOIN ship.freight_memo_adv as a ON a.fm_no=f.fm_no
	LEFT OUTER JOIN ship.freight_memo_bal as bal ON bal.fm_no=f.fm_no
	LEFT OUTER JOIN ship.diesel as d1 ON d1.vou_no=f.fm_no AND d1.vou_type='ADVANCE'
	LEFT OUTER JOIN ship.diesel as d2 ON d2.vou_no=f.fm_no AND d2.vou_type='BALANCE'
	WHERE f.fm_no='$vou_no'");
	
	if(!$qry){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}
		
	if(numRows($qry)==0)
	{
		echo "<br><br>
		<center><h3>NO RESULT FOUND : FREIGHT MEMO.</h3></center>
		";
		exit();
	}
	
	$row=fetchArray($qry);
	
	echo "
	<h4>FREIGHT MEMO : $vou_no </h4>
	<br>
	<table class='table table-bordered' style='font-size:12px;'>
		<tr>
			<th>TruckNo :</th> <td>$row[tno]</td>
			<th>Company :</th> <td>$row[company]</td>
			<th>Date :</th> <td>".date("d-m-y",strtotime($row['fm_date']))."</td>
			<th>Weight :</th> <td>$row[weight]</td>
			<th>Rate :</th> <td>$row[rate]</td>
			<th>Freight :</th> <td>$row[freight]</td>
		</tr>	
		
		<tr>
			<th>Owner :</th> <td>$row[owner_name]</td>
			<th>PAN :</th> <td>$row[owner_pan]</td>
			<th>Broker :</th> <td>$row[broker_name]</td>
			<th>PAN :</th> <td>$row[broker_pan]</td>
			<th>Driver :</th> <td>$row[driver_name]</td>
			<th>LicNo :</th> <td>$row[driver_lic_no]</td>
		</tr>
		
	</table>
	";
	if($row['advance']==1)
	{
	 if($row['adv_to']==1)
	 { $adv_to="BROKER"; } else { $adv_to="OWNER"; }	
	 
	echo "	
	<h4>Advance : </h4>
	<table class='table table-bordered table-striped bg-success' style='font-size:12px;'>
		<tr>
			<th>PartyName</th>
			<th>PartyPAN</th>
			<th>Loading(+)</th>
			<th>Other(-)</th>
			<th>TDS(-)</th>
			<th>GPS Deposit(-)</th>
			<th>TotalFreight</th>
		</tr>	
		
		<tr>
			<td>$row[adv_party]</td>
			<td>$row[adv_pan]</td>
			<td>$row[loading]</td>
			<td>$row[adv_other]</td>
			<td>$row[adv_tds]</td>
			<td>$row[gps_deposit]</td>
			<td>$row[total_freight]</td>
		</tr>	
		
		<tr>
			<th>TotalAdv</th>
			<th>AdvDate</th>
			<th>Cash</th>
			<th>Cheque</th>
			<th colspan='2'>Diesel</th>
			<th>Rtgs</th>
		</tr>	
		
		<tr>
			<td>$row[total_adv]</td>
			<td>".date("d-m-y",strtotime($row['adv_date']))."</td>
			<td>$row[adv_cash]</td>
			<td>$row[adv_cheque]</td>
			<td colspan='2'>
				$row[adv_diesel]
			";
				if($row['adv_diesel']>0)
				{
					echo "<br>".$row['adv_card']." : ".$row['adv_fuel_comp']."-".$row['adv_card_no'];
				}
			echo "
			</td>
			<td>$row[adv_rtgs]</td>
		</tr>	
	</table>	
	";
	}
	
	if($row['balance']==1)
	{
	 if($row['bal_to']==1)
	 { $bal_to="BROKER"; } else { $bal_to="OWNER"; }	
	 
	 $balance_amount=$row['total_freight']-$row['total_adv'];
	 
	echo "	
	<h4>Balance : </h4>
	<table class='table table-bordered table-striped bg-danger' style='font-size:12px;'>
		<tr>
			<th>BalanceBy</th>
			<th colspan='2'>PartyName</th>
			<th>PartyPAN</th>
			<th>Balance</th>
			<th>UnLoading(+)</th>
			<th>Other(-)</th>
			<th>TDS(-)</th>
			<th>Claim(-)</th>
			<th>GPS Charges(-)</th>
		</tr>	
		
		<tr>
			<td>$row[bal_branch]</td>
			<td colspan='2'>$row[bal_party]</td>
			<td>$row[bal_pan]</td>
			<td>$balance_amount</td>
			<td>$row[unloading]</td>
			<td>$row[bal_other]</td>
			<td>$row[bal_tds]</td>
			<td>$row[claim]</td>
			<td>$row[gps_charges]</td>
		</tr>	
		
		<tr>
			<th>Deposit Rev.(+)</th>
			<th>LatePOD(-)</th>
			<th>TotalBal</th>
			<th>BalDate</th>
			<th>Cash</th>
			<th>Cheque</th>
			<th colspan='3'>Diesel</th>
			<th>Rtgs</th>
		</tr>	
		
		<tr>
			<td>$row[gps_deposit_return]</td>
			<td>$row[late_pod]</td>
			<td>$row[total_bal]</td>
			<td>".date("d-m-y",strtotime($row['bal_date']))."</td>
			<td>$row[bal_cash]</td>
			<td>$row[bal_cheque]</td>
			<td colspan='3'>$row[bal_diesel]
			";
				if($row['bal_diesel']>0)
				{
					echo "<br>".$row['bal_card']." : ".$row['bal_fuel_comp']."-".$row['bal_card_no'];
				}
			echo "
			</td>
			<td>$row[bal_rtgs]</td>
		</tr>	
	</table>	
	";
	}
	
	echo "
	<h4>LR Details : </h4>
	<table class='table table-bordered table-striped' style='font-size:12px;'>
		<tr>
			<th>LR No</th>
			<th>From</th>
			<th>To</th>
			<th>Consignor</th>
			<th>Consignee</th>
			<th>ActWt</th>
			<th>ChrgWt</th>
		</tr>	
	";
	
		$qry2=Qry($conn,"SELECT l.lrno,l.actual_wt,l.charge_wt,loc1.name as from1,
		loc2.name as to1,c1.name as con1,c2.name as con2
		FROM ship.lr_entry as l 
		LEFT OUTER JOIN rrpl_database.station as loc1 on loc1.id=l.from_loc
		LEFT OUTER JOIN rrpl_database.station as loc2 on loc2.id=l.to_loc
		LEFT OUTER JOIN rrpl_database.consignor as c1 on c1.id=l.con1
		LEFT OUTER JOIN rrpl_database.consignee as c2 on c2.id=l.con2
		WHERE l.id IN($row[lr_ids])");
		
		if(!$qry2){
			ScriptError($conn,$page_name,__LINE__);
			exit();
		}
		
		if(numRows($qry2)>0)
		{
			while($row2=fetchArray($qry2))
			{
				echo "<tr>
					<td>$row2[lrno]</td>
					<td>$row2[from1]</td>
					<td>$row2[to1]</td>
					<td>$row2[con1]</td>
					<td>$row2[con2]</td>
					<td>$row2[actual_wt]</td>
					<td>$row2[charge_wt]</td>
				</tr>";
			}
		}
		else
		{
			echo "<tr>
				<td colspan='7'>NO LR FOUND.</td>
			</tr>";
		}
	echo "	
	</table>
	";
}
else
{
	echo "<br><br>
	<center><h3>NO RESULT FOUND.</h3></center>
	";
	exit();
}
?>
</div>
</div>
</body>
</html>