<?php
require_once("connect.php");

$type=mysqli_real_escape_string($conn,strtoupper($_POST['truck_type']));

if($type=='MARKET')
{
	$qry=mysqli_query($conn,"SELECT id,fm_no,date,tno,lrnos,weight FROM freight_memo WHERE done!='1' AND branch='$branch'");
	if(mysqli_num_rows($qry)>0)
	{
		echo "<option value=''>---SELECT---</option>";
		
		while($row=mysqli_fetch_array($qry))
		{
			echo "<option value='$row[id]_$row[weight]_MARKET_$row[tno]_$row[fm_no]_$row[date]_$row[lrnos]'>$row[tno]-> LRNo: $row[lrnos] ($row[weight] TN)</option>";
		}
	}
	else
	{
		echo "<option value=''>--NO LR FOUND--</option>";
	}
}
else if($type=='OWN')
{
	$qry=mysqli_query($conn,"SELECT id,fm_no,date,truck_no,weight FROM own_truck_form WHERE done!='1' AND branch='$branch'");
	if(mysqli_num_rows($qry)>0)
	{
		echo "<option value=''>---SELECT---</option>";
		
		while($row=mysqli_fetch_array($qry))
		{
			echo "<option value='$row[id]_$row[weight]_OWN_$row[truck_no]_$row[fm_no]_$row[date]_$row[lrnos]'>$row[truck_no]-> LRNo: $row[lrnos] ($row[weight] TN)</option>";
		}
	}
	else
	{
		echo "<option value=''>--NO LR FOUND--</option>";
	}
}
else
{
	echo "<option value=''>---SELECT---</option>";
}
?>