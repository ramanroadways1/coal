<?php
require_once("./connect.php");
$timestamp=date("Y-m-d H:i:s");

if(!isset($_POST['be_id'])){
	echo "<script>
		window.location.href='./upload_pending_be.php';
	</script>";
	exit();
}

$be_id = escapeString($conn,$_POST['be_id']);
$sourcePath = $_FILES['be_copy']['tmp_name'];

$valid_types = array("application/pdf");
	
	if(!in_array($_FILES['be_copy']['type'], $valid_types))
	{
			echo "<script>
				alert('Error : Only PDF Upload Allowed.');
				$('#loadicon').hide();
				$('#submit_button').attr('disabled',false);
			</script>";
			exit();
	}
    
		$UploadedSize = $_FILES['be_copy']['size'];
		
		if(($UploadedSize/1024/1024)>4)
		{
			echo "<script>
				alert('Error : MAX Upload size allowed is : 4MB');
				$('#loadicon').hide();
				$('#submit_button').attr('disabled',false);
			</script>";
			exit();
		}
	
	
StartCommit($conn);
$flag = true;	

$fix_name = date('dmYHis').mt_rand();
$targetPath="be_copy/".$fix_name.".".pathinfo($_FILES['be_copy']['name'],PATHINFO_EXTENSION);

if(pathinfo($_FILES['be_copy']['name'][$i],PATHINFO_EXTENSION)!='pdf')
{
	ImageUpload(700,900,$sourcePath);
}
	
// ImageUpload(700,900,$sourcePath);

if(!move_uploaded_file($sourcePath,$targetPath))
{
	$flag = false;
	errorLog("File not uploaded.",$conn,$page_name,__LINE__);
}

$update_be=Qry($conn,"UPDATE be_data SET be_copy='$targetPath' WHERE id='$be_id'");
		  
if(!$update_be){
	unlink($targetPath);
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('BE Uploaded Successfully !');
			window.location.href='upload_pending_be.php';
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',true);
	</script>";
	exit();
}
?>