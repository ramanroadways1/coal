<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Receive Shipment :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	 </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
			
<script type="text/javascript">
$(document).ready(function (e) {
$("#RcvForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_rcv_shipment.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>			

<div id="result_form"></div> 		
			
<div class="row">
 
	<div class="form-group col-md-12 table-responsive">
	
        <table id="myTable" class="table table-bordered table-striped" style="font-size:13px;">
          <tr>
				<th>#</th>
				<th>Shipment_No</th>
				<th>ShipName</th>
				<th>Voyage_No</th>
				<th>Total_BL</th>
				<th>BL_Weight</th>
				<th>Shipment Date</th>
				<th>Receive</th>
			</tr>
          
            <?php
              $sql = Qry($conn,"SELECT shipment.id,unq_id,voyage_no,total_bl,bl_weight,date,name FROM shipment,vessel_name where 
			  rcv=0 AND vessel_name.id=vessel_name AND shipment.branch='$branch' ORDER BY shipment.id ASC");
             if(!$sql){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
					<td colspan='8'><b>NO RESULT FOUND..</b></td>
				</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
                echo 
                "<tr>
				  <td>$sn</td>
				  <td>$row[unq_id]</td>
				  <td>$row[name]</td>
				  <td>$row[voyage_no]</td>
				  <td>$row[total_bl]</td>
				  <td>$row[bl_weight]</td>
				  <td>".date("d-m-y",strtotime($row['date']))."</td>
				  <td>
				  <input type='hidden' value='$row[name]' id='shipname$row[id]'>
				  <button type='button' id='RcvButton$row[id]' onclick=RcvModal('$row[id]','$row[total_bl]','$row[unq_id]') 
				  class='btn btn-xs bg-green'>Mark as <br>RCV</button></td>
				 </tr>
				";
              }
			}
            ?>
        </table>
      </div>

  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="bg-primary modal-header">
        <h5 class="modal-title">Receive Shipment : <span id="ship_name"></span></h5>
      </div>
	<form id="RcvForm" autocomplete="off">
      <div class="modal-body">
		 <div class="row">
		 
			<div class="form-group col-md-4">
				<label>Draft Weight (in MTS) <font color="red">*</font></label>
				<input type="number" min="1" name="draft_weight" class="form-control" required />
			</div>
			
			<div class="form-group col-md-4">
				<label>Date of Arrival <font color="red">*</font> </label>
				<input type="date" name="date_of_arrival" onchange="$('.be_dates1').val($('#date_of_arrival').val())" id="date_of_arrival" class="form-control" max="<?php echo date("Y-m-d"); ?>" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
			</div>
			
			<div class="form-group col-md-4">
				<label>Time of Arrival <font color="red">*</font></label>
				<select name="time_of_arrival" class="form-control" required>
					<option value="">---SELECT TIME---</option>
					<?php 
					$start = "01:00";
					$end = "24:00";

					$tStart = strtotime($start);
					$tEnd = strtotime($end);
					$tNow = $tStart;

					while($tNow <= $tEnd)
					{
					  echo "<option value='".date("H:i",$tNow)."'>".date("h:i A",$tNow)."</option>";
					  $tNow = strtotime('+15 minutes',$tNow);
					}
					?>
				</select>
			</div>
			
			<div class="form-group col-md-12">
				<hr>
			</div>
		
		<script>
		function LoadBL(shipment_id)
		{
			$("#loadicon").show();
				jQuery.ajax({
				url: "fetch_total_bl.php",
				data: 'shipment_id=' + shipment_id,
				type: "POST",
				success: function(data) {
					$("#result_bl").html(data);
				},
				error: function() {}
			});
		}
		</script>
		
		 <div id="result_bl">
				
		 </div>
		 
	</div>
			<input type="hidden" id="ship_id" name="ship_id">	
			<input type="hidden" id="total_no_of_bl" name="total_bl">	
			<input type="hidden" id="unq_id" name="unq_id">	
	  </div>

      <div class="modal-footer">
        <button type="submit" disabled id="submit_button" class="btn btn-primary">Mark as RCV</button>
        <button type="button" id="close_modal_button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	
	
<script>	
function RcvModal(id,no_of_bl,unq_id)
{
	var name = $('#shipname'+id).val();
	$('#ship_id').val(id);
	$('#total_no_of_bl').val(no_of_bl);
	$('#unq_id').val(unq_id);
	$('#ship_name').html(name);
	LoadBL(unq_id);
}
</script>
	
<?php
include "footer.php";
?>