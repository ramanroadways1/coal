<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		EWay Bill Summary :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
	
<div id="result_form"></div> 		
			
<div class="row">
 <div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
          <thead>
		  <tr>
				<th>#</th>
				<th>LR number</th>
				<th>Vehicle no.</th>
				<th>LR date</th>
				<th>Parties</th>
				<th>PO number</th>
				<th>BOE no.</th>
				<th>BOE date</th>
				<th>Taxable value</th>
				<th>Total value</th>
				<th>#</th>
			</tr>
          </thead>
          <tbody>
            <?php
           $sql = Qry($conn,"SELECT l.id,l.lrno,l.tno,l.lr_date,l.po_no,l.do_no,l.boe_date,l.goods_value,l.total_value,
		   l.eway_bill,con1.name as consignor,con2.name as consignee 
		   FROM lr_entry as l 
			LEFT OUTER JOIN rrpl_database.consignor AS con1 ON con1.id=l.con1	
			LEFT OUTER JOIN rrpl_database.consignee AS con2 ON con2.id=l.con2
			WHERE date(l.timestamp)>='2020-10-09' AND l.cancel!='1' AND l.eway_bill=0 AND l.branch='$branch'");
              
			if(!$sql){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			{
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				echo 
                "<tr>
				  <td>$sn</td>
				  <td>$row[lrno]</td>
				  <td>$row[tno]</td>
				  <td>".date("d/m/y",strtotime($row['lr_date']))."</td>				 
				 <td>$row[consignor]<br>$row[consignee]</td>
				 <td>$row[po_no]</td>
				 <td>$row[do_no]</td>
				 <td>".date("d/m/y",strtotime($row['boe_date']))."</td>		
				<td>$row[goods_value]</td>				 
				<td>$row[total_value]</td>				 
				<input type='hidden' id='ShipNameBox$row[id]' value='$row[id]'>
				<td>
					<button type='button' id='GenBtn$row[id]' onclick=GenBill('$row[id]') 
					class='btn btn-sm btn-success'>Generate</button>
				</td>
				</tr>
				";
			$sn++;		
              }
			}
            ?>
		</tbody>
        </table>
      </div>

  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
	
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
	
function GenBill(id)
{
		$('#GenBtn'+id).attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
		url: "gen_eway_bill.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
		$("#tab_result").html(data);
		
		},
		error: function() {}
	});
}
</script>

<?php
	include "footer.php";
?>