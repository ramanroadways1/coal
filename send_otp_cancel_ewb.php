<?php
require_once("./connect.php");

$ewb_no = escapeString($conn,$_POST['ewb_no']);
$rsn_code = escapeString($conn,$_POST['rsn_code']);
$remark = escapeString($conn,$_POST['remark']);
$timestamp=date("Y-m-d H:i:s");

if($ewb_no=='' || $rsn_code=='' || $remark=='')
{
	echo "<font color='red'>Fill out all fields first !</font>";
	echo "<script>$('#loadicon').fadeOut('slow');$('#otp_sent_btn').attr('disabled',false);</script>";
	exit();
}

$branch = $_SESSION['rrpl_ship'];
$branch_sub_user = $_SESSION['user_code'];

$get_username = Qry($conn,"SELECT name FROM rrpl_database.emp_attendance WHERE code='$branch_sub_user'");
	
if(!$get_username){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}	

if(numRows($get_username)==0)
{
	echo "<font color='red'>Error : Username not found !</font>";
	echo "<script>$('#loadicon').fadeOut('slow');$('#otp_sent_btn').attr('disabled',false);</script>";
	exit();
}

$row_username = fetchArray($get_username);

$chk_ewb = Qry($conn,"SELECT id,transDocNo FROM eway_bill WHERE ewbNo='$ewb_no'");
	
if(!$chk_ewb){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}	

if(numRows($chk_ewb)==0)
{
	echo "<font color='red'>Error : Invalid ewb number !</font>";
	echo "<script>$('#loadicon').fadeOut('slow');$('#otp_sent_btn').attr('disabled',false);</script>";
	exit();
}

$row_ewb = fetchArray($chk_ewb);

$ewb_id = $row_ewb['id'];
$lrno = $row_ewb['transDocNo'];

$chk_lr = Qry($conn,"SELECT done FROM lr_entry WHERE lrno='$lrno'");
	
if(!$chk_lr){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}	

if(numRows($chk_lr)==0)
{
	echo "<font color='red'>Error : LR not found !</font>";
	echo "<script>$('#loadicon').fadeOut('slow');$('#otp_sent_btn').attr('disabled',false);</script>";
	exit();
}

$row_lr = fetchArray($chk_lr);

if($row_lr['done']=="1")
{
	echo "<font color='red'>Error : Voucher created. Delete it first !</font>";
	echo "<script>$('#loadicon').fadeOut('slow');$('#otp_sent_btn').attr('disabled',false);</script>";
	exit();
}

$otp = rand(100000,999999);

$_SESSION['ewb_cancel_otp'] = $otp;
$_SESSION['ewb_cancel_ewb_no'] = $ewb_no;
$_SESSION['ewb_cancel_rsn_code'] = $rsn_code;
$_SESSION['ewb_cancel_remark'] = $remark;

$msg_template = "Branch: $branch wants to cancel an eway-bill.\nEwb No: $ewb_no.\nUsername: $row_username[name].\nLR No: $lrno.\nRemark: $remark.\nOTP is: $otp.";

SendWAMsg($conn,"9024281599,8600997434",$msg_template);
	
echo "<font color='green'>OTP has been sent to 9024281599 and 8600997434 !</font>";
echo "<script>
	$('#loadicon').fadeOut('slow');
	$('#otp_sent_btn').attr('disabled',true);
	$('#remark').attr('disabled',true);
	$('#button1').attr('disabled',false);
	$('#button1').show();
	$('.ewb_cancel_rsn_class').attr('disabled',true);
</script>";
exit();
?>