<?php
require_once("connect.php");

$id = escapeString($conn,$_POST['id']);
$shipment_name = escapeString($conn,$_POST['shipment_name']);

if(empty($id))
{
	echo "<script>
		alert('Shipment not found !!');
		$('#loadicon').hide();
	</script>";
	exit();
}

$get_shipment = Qry($conn,"SELECT vou_id,vou_no,lrnos,tno,vou_date,weight FROM dispatch_road WHERE shipment_id='$id'");

if(!$get_shipment){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_shipment)==0)
{
	echo "<script>
		alert('no record found from this shipment !!');
		$('#loadicon').hide();
	</script>";
	exit();
}
?>				
<div class="modal" id="ShipmentModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
	
    <div class="bg-primary modal-header">
        <h4 class="modal-title">Shipment summary: <?php echo $shipment_name; ?> </h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="form-group col-md-12 table-responsive">
				<table id="example" class="table table-bordered table-striped" style="font-size:12px">
				<thead>
				<tr>
					<th>Vou_No</th>
					<th>LR_No</th>
					<th>Truck No</th>
					<th>Vou_Date</th>
					<th>LR_Weight (in MTS)</th>
				</tr>
				</thead>	
				<tbody>	
				<tr>
					<td colspan="4"><b>Total Dispatch by Road : </b></td>
					<td><b><span id="total_weight_span"></span> MTS</b></td>
				</tr>
				
			<?php	
			$total_weight = 0;
				while($row2=fetchArray($get_shipment))
				{
					$total_weight = $total_weight+$row2['weight'];
					
					echo "<tr>
						<td>
							<form action='view_vou.php' target='_blank' method='POST'>
							<input type='hidden' value='$row2[vou_no]' name='vou_no'>	
							<button type='submit'>$row2[vou_no]</button>
							</form>	
						</td>
						<td>$row2[lrnos]</td>
						<td>$row2[tno]</td>
						<td>".date('d-M-y',strtotime($row2['vou_date']))."</td>
						<td>$row2[weight]</td>
					</tr>";
				}
				?>	
</tbody>				
				</table>
			</div>
		</div>
	</div>
	
	<div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>
	</div>
  </div>
</div>	

<button style="display:none" id="modal_btn" data-toggle="modal" data-target="#ShipmentModal"></button>

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
				
$('#total_weight_span').html('<?php echo $total_weight; ?>');
$('#modal_btn').click();
$('#loadicon').hide();
</script>				