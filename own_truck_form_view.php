<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		View Own Truck Form :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
	
<div id="result_form"></div> 		
			
<div class="row">
 <div class="form-group col-md-12 table-responsive">
	   <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
		<thead>	
		 <tr>
				<th>Id</th>
				<th>VesselName</th>
				<th>Vou_No</th>
				<th>Company</th>
				<th>TruckNo</th>
				<th>Date</th>
				<th>LRNo</th>
				<th>Weight</th>
			</tr>
        </thead> 
        <tbody> 
            <?php
              $sql = Qry($conn,"SELECT o.fm_no,o.truck_no,o.company,o.date,o.weight,o.lrnos,s.voyage_no,v.name 
			  FROM own_truck_form as o 
			  LEFT OUTER JOIN shipment AS s ON s.id=o.shipment_id
			  LEFT OUTER JOIN vessel_name AS v ON v.id=vessel_name
			  WHERE o.branch='$branch'");
             
			 if(!$sql){
				 ScriptError($conn,$page_name,__LINE__);
				 exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='10'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  while($row = fetchArray($sql))
			  {
				 echo "
				<tr>
				  <td>$sn</td>
				  <td>$row[name]-$row[voyage_no]</td>
				  <td>
							<form action='view_vou.php' target='_blank' method='POST'>
							<input type='hidden' value='$row[fm_no]' name='vou_no'>	
							<button type='submit'>$row[fm_no]</button>
							</form>	
					</td>
				  <td>$row[company]</td>
				  <td>$row[truck_no]</td>
				  <td>".date("d-m-y",strtotime($row['date']))."</td>
				  <td>$row[lrnos]</td>
				  <td>$row[weight]</td>
				</tr>
				";
				$sn++;	
              }
			}
            ?>
		</tbody>	
        </table>
      </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
	
	<script>	
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<?php
include "footer.php";
?>