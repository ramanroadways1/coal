<?php
require_once("./connect.php");

$id=escapeString($conn,strtoupper($_POST['id']));
$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$getLRData = Qry($conn,"SELECT lr_id,lrno,lr_type,branch,wheeler,date,create_date,truck_no,fstation,tstation,dest_zone,consignee,con2_addr,
from_id,to_id,con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,weight,bill_rate,bill_amt,t_type,do_no,shipno,
invno,item,item_id,plant,articles,goods_desc,goods_value,con2_gst,crossing,cancel,break,diesel_req,download 
FROM rrpl_database.lr_sample_pending WHERE id='$id'");

if(!$getLRData){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($getLRData)==0){
	echo "<script>
		alert('LR not found.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',true);
	</script>";
	exit();
}

$row = fetchArray($getLRData);

$lr_type_db = $row['lr_type'];

$datediff = strtotime($timestamp) - strtotime($row['create_date']);
$diff_value=round($datediff / (60 * 60 * 24));	

if($diff_value>3)
{
	echo "<script>
		alert('LR older than 3 days. You can\'t delete this LR.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',true);
	</script>";
	exit();
}

$lr_id = $row['lr_id'];
$lrno = $row['lrno'];

if($row['download']=="1")
{
	echo "<script>
		alert('You can\'t delete this LR. Contact Head-Office.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',true);
	</script>";
	exit();
}

if($row['branch']!=$branch)
{
	echo "<script>
		alert('LR belongs to $row[branch] Branch.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}

// if($row['crossing']!="")
// {
	// echo "<script>
		// alert('You Can\'t edit this LR. LR has been Closed.');
		// $('#loadicon').hide();
		// $('#delete_button_$id').attr('disabled',false);
	// </script>";
	// exit();
// }

if($row['cancel']==1)
{
	echo "<script>
		alert('LR Marked as Canceled.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row['break']>0)
{
	echo "<script>
		alert('This is Breaking LR. You Can\'t edit.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}

if($row['diesel_req']>0)
{
	echo "<script>
		alert('Diesel Request From This LR. You Can\'t edit.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}

$chkEwayBill = Qry($conn,"SELECT eway_bill,done FROM lr_entry WHERE lrno='$lrno'");
if(!$chkEwayBill){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chkEwayBill)==0)
{
	echo "<script>
		alert('Invalid LR No');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}
	
$rowChk=fetchArray($chkEwayBill);
	
if($rowChk['done']==1)
{
	echo "<script>
		alert('LR Attached with Voucher.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}
	
if($rowChk['eway_bill']=='1' AND $branch!='MAGDALLA')
{
	echo "<script>
		alert('Eway Bill Generated for this LR. Cancel it First');
		window.location.href='./lr_delete.php';
	</script>";
	exit();
}
	
$fetch_bilty_id = Qry($conn,"SELECT id,bilty_id FROM rrpl_database.lr_check WHERE lrno='$lrno'");
if(!$fetch_bilty_id){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}
	
if(numRows($fetch_bilty_id)==0){
	echo "<script>
		alert('Bilty id not found.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',false);
	</script>";
	exit();
}
	
$row_bilty_id = fetchArray($fetch_bilty_id);
$bilty_id = $row_bilty_id['bilty_id'];
$lr_check_id = $row_bilty_id['id'];

StartCommit($conn);
$flag = true;
	
$update_stock = Qry($conn,"UPDATE rrpl_database.lr_bank SET `stock_left`=`stock_left`+1 WHERE id='$bilty_id'");
	
	if(!$update_stock){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	if(AffectedRows($conn)==0){
		$flag = false;
		errorLog("Unable to update Bilty Stock Id: $bilty_id, LRNo: $lrno.",$conn,$page_name,__LINE__);
	}

$copy_lr_data = Qry($conn,"INSERT INTO rrpl_database.lr_sample_deleted(deleted_branch,deleted_branch_user,lr_id,company,branch,branch_user,lrno,lr_type,
wheeler,date,create_date,truck_no,fstation,tstation,dest_zone,consignor,consignor_before,consignee,con2_addr,from_id,to_id,con1_id,con2_id,
zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,bill_rate,bill_amt,t_type,do_no,shipno,invno,item,item_id,plant,
articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,crossing,cancel,break,diesel_req,nrr,downloadid,downtime,billrate,
billparty,partyname,pod_rcv_date,download,billfreight,lrstatus) SELECT '$branch','$branch_sub_user',lr_id,company,branch,branch_user,
lrno,lr_type,wheeler,date,create_date,truck_no,fstation,tstation,dest_zone,consignor,consignor_before,consignee,con2_addr,from_id,to_id,
con1_id,con2_id,zone_id,po_no,bank,freight_type,sold_to_pay,lr_name,wt12,gross_wt,weight,bill_rate,bill_amt,t_type,do_no,shipno,invno,
item,item_id,plant,articles,goods_desc,goods_value,con1_gst,con2_gst,hsn_code,timestamp,crossing,cancel,break,diesel_req,nrr,downloadid,
downtime,billrate,billparty,partyname,pod_rcv_date,download,billfreight,lrstatus FROM rrpl_database.lr_sample_pending WHERE id='$id'");

if(!$copy_lr_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$dlt_insert_id = getInsertID($conn);

$query_dlt = Qry($conn,"DELETE FROM rrpl_database.lr_sample_pending WHERE id='$id' AND branch='$branch' AND diesel_req=0 
AND download=0");
if(!$query_dlt){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Unable to delete LR lr_sample_pending. LRNo: $lrno.",$conn,$page_name,__LINE__);
}
	
$query_dlt_main = Qry($conn,"DELETE FROM rrpl_database.lr_sample WHERE id='$lr_id' AND branch='$branch' AND diesel_req=0 
AND download=0");

if(!$query_dlt_main){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Unable to delete LR lr_sample. LRNo: $lrno.",$conn,$page_name,__LINE__);
}
	
$query_dlt2=Qry($conn,"DELETE FROM rrpl_database.lr_check WHERE id='$lr_check_id'");
if(!$query_dlt2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$dlt_lr_entry = Qry($conn,"DELETE FROM lr_entry WHERE lrno='$lrno' AND done!='1'");
if(!$dlt_lr_entry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Unable to delete LR from lr_entry. LRNo: $lrno.",$conn,$page_name,__LINE__);
}

if($lr_type_db=='MARKET')
{
	$update_lr_count = Qry($conn,"UPDATE rrpl_database._pending_lr SET market_lr=market_lr-1 WHERE branch='$branch'");
}
else
{
	$update_lr_count = Qry($conn,"UPDATE rrpl_database._pending_lr SET own_lr=own_lr-1 WHERE branch='$branch'");
}

if(!$update_lr_count){
	$flag = false; 
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(AffectedRows($conn)==0)
{
	$flag = false;
	errorLog("Unable to update LR count. Branch : $branch. LRNo: $lrno.",$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO rrpl_database.edit_log(vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp,branch_user) VALUES 
('$lrno','$lr_id','LR_DELETE','$dlt_insert_id','$branch','BRANCH','$timestamp','$branch_sub_user')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($branch=='MAGDALLA')
{
	$dlt_ewb_data1 = Qry($conn,"DELETE FROM rrpl_database._eway_bill_lr_wise WHERE lrno = '$lrno'");

	if(!$dlt_ewb_data1){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$dlt_ewb_data2 = Qry($conn,"DELETE FROM rrpl_database._eway_bill_validity WHERE lrno = '$lrno'");

	if(!$dlt_ewb_data2){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('LR Successfully Deleted.');
		window.location.href='./lr_delete.php';
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#delete_button_$id').attr('disabled',true);
	</script>";
	exit();
}
?>