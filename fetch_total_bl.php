<?php
require_once("connect.php");

$shipment_id = escapeString($conn,$_POST['shipment_id']);

if($shipment_id=='')
{
	echo "<script>
		alert('Shipment id not found..');
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',true);
	</script>";
	exit();
}

$qry=Qry($conn,"SELECT id,bl_no FROM bl_data WHERE unq_id='$shipment_id'");
if(!$qry){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($qry)==0)
{
	echo "<script>
		alert('BL record not found..');
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',true);
	</script>";
	exit();
}

while($row=fetchArray($qry))
{
	?>
	<div class="form-group col-md-4">
		<label>BL Number <font color="red">*</font></label>
		<input readonly type="text" name="bl_no[]" class="form-control" value="<?php echo $row['bl_no']; ?>" required />
	</div>
	
	<div class="form-group col-md-4">
		<label>BE Number <font color="red">*</font></label>
		<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9-,]/,'')" type="text" name="be_no[]" class="form-control" required>
	</div>
	
	<input type="hidden" name="bl_id[]" value="<?php echo $row['id']; ?>">
	
	<div class="form-group col-md-4">
		<label>BE Copy <font color="red">*</font></label>
        <input type="file" accept="application/pdf" class="form-control" name="be_copy[]" required />
	</div>
	
	<input type="hidden" readonly name="be_date[]" class="be_dates1" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
	<?php
}

echo "<script>
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',false);
		document.getElementById('ModalButton').click();
	</script>";
	exit();
?>