<?php
require_once("./connect.php");

if(!isset($_SESSION['ewb_cancel_otp']))
{
	echo "<script>
		alert('Error : OTP not found !');
		$('#loadicon').fadeOut('slow');
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

// $ewb_no = escapeString($conn,$_POST['ewb_no']);
// $rsn_code = escapeString($conn,$_POST['rsn_code']);
// $remark = escapeString($conn,$_POST['remark']);

$otp = escapeString($conn,$_POST['otp']);
$ewb_no = $_SESSION['ewb_cancel_otp'];
$rsn_code = $_SESSION['ewb_cancel_rsn_code'];
$remark = $_SESSION['ewb_cancel_remark'];
$timestamp=date("Y-m-d H:i:s");

if($otp=='')
{
	echo "<script>
		alert('Error : Enter OTP first !');
		$('#loadicon').fadeOut('slow');
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

if($otp != $_SESSION['ewb_cancel_otp'])
{
	echo "<script>
		alert('Error : Invalid OTP !');
		$('#loadicon').fadeOut('slow');
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

if($ewb_no=='' || $rsn_code=='' || $remark=='')
{
	echo "<script>
		alert('Error : All fields are required !');
		$('#loadicon').fadeOut('slow');
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

$data = array(
"ewbNo"=>$ewb_no,
"cancelRsnCode"=>$rsn_code,
"cancelRmrk"=>$remark,
);

$payload = json_encode($data);

// {
// "ewbNo": 111000609282,
 // "cancelRsnCode": 2, // cancelRsnCode  // 1-Duplicate // 2- Order Cancelled // 3- Data Entry mistake // cancelRmrk
// "cancelRmrk": "Cancelled the order"
// }

$asp_id = $tax_pro_asp_id;
$asp_Passwd = $tax_pro_asp_password;
$myGSTIn = $rrpl_gst_no;
$UserName = $ewb_rrpl_username;
$PassWord = $ewb_rrpl_password;
	
$chk_ewb = Qry($conn,"SELECT id,transDocNo FROM eway_bill WHERE ewbNo='$ewb_no'");
	
if(!$chk_ewb){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}	

if(numRows($chk_ewb)==0)
{
	echo "<script>
		alert('Error : Invalid ewb number !');
		$('#loadicon').fadeOut('slow');
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

$row_ewb = fetchArray($chk_ewb);

$ewb_id = $row_ewb['id'];
$lrno = $row_ewb['transDocNo'];

$chk_lr = Qry($conn,"SELECT done FROM lr_entry WHERE lrno='$lrno'");
	
if(!$chk_lr){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}	

if(numRows($chk_lr)==0)
{
	echo "<script>
		alert('Error : LR not found !');
		$('#loadicon').fadeOut('slow');
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}

$row_lr = fetchArray($chk_lr);

if($row_lr['done']=="1")
{
	echo "<script>
		alert('Error : Voucher created. Delete it first !');
		$('#loadicon').fadeOut('slow');
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}
	
	$get_token=Qry($conn,"SELECT token FROM api_token WHERE company='RRPL' ORDER BY id DESC LIMIT 1");
	
	if(!$get_token){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}

	$row_token=fetchArray($get_token);
	$authToken=$row_token['token'];

	$ch = curl_init("$tax_pro_url/v1.03/dec/ewayapi?action=CANEWB&aspid=$asp_id&password=$asp_Passwd&gstin=$myGSTIn&username=$UserName&authtoken=$authToken");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLINFO_HEADER_OUT, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Content-Length: ' . strlen($payload)));
	
	$result = curl_exec($ch);
	$err1 = curl_error($ch);
	curl_close($ch);
	
	$result1 = json_decode($result, true);
	
	if($err1)
	{
		$insert_error=Qry($conn,"INSERT INTO eway_bill_error(company,error_desc,lrno,branch,timestamp) VALUES 
		('RRPL','cURL Error : $err1','COAL','$branch','$timestamp')");
		
		if(!$insert_error){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","../");
			exit();
		}
			
		echo "<font color='red'>Error : $err1.</font>";
			
		echo "<script>
			// alert('Error : $err1');
			$('#loadicon').fadeOut('slow');
			$('#button1').attr('disabled',false);
		</script>";
		exit();
	}
	
	if(@$result1['error'])
	{
		$ErrorMsg = $result1['error']['message'];
		
		if($result1['error']['error_cd']=="GSP102")
		{
			echo "<font color='red'>Error : $ErrorMsg.</font>";
			
			echo "<script>
				alert('Error : Token expired !');
				$('#loadicon').fadeOut('slow');
				$('#button1').attr('disabled',false);
			</script>";
			exit();
		}
		
		echo "<font color='red'>Error : $ErrorMsg.</font>";
		echo "<script>
			$('#loadicon').fadeOut('slow');
			$('#button1').attr('disabled',false);
		</script>";
		exit();
	}
	
	$ewb_no_cancelled = $result1['ewayBillNo'];
	$cancelDate = $result1['cancelDate'];
	
StartCommit($conn);
$flag = true;

	$update_cancelled = Qry($conn,"UPDATE eway_bill SET cancel_rsn='$rsn_code',cancel_remark='$remark',cancel_user='$branch_sub_user',
	cancel_timestamp='$timestamp' WHERE id='$ewb_id'");
	
	if(!$update_cancelled){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__); 
	}
	
	$update_lr = Qry($conn,"UPDATE lr_entry SET eway_bill='0' WHERE lrno='$lrno'");
		
	if(!$update_lr){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$deleted_ewb_lr_wise = Qry($conn,"DELETE FROM rrpl_database._eway_bill_lr_wise WHERE ewb_no='$ewb_no'");
	
	if(!$deleted_ewb_lr_wise){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__); 
	}
	
	$deleted_ewb_validity = Qry($conn,"DELETE FROM rrpl_database._eway_bill_validity WHERE ewb_no='$ewb_no'");
	
	if(!$deleted_ewb_validity){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__); 
	}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	unset($_SESSION['ewb_cancel_otp']);
	unset($_SESSION['ewb_cancel_ewb_no']);
	unset($_SESSION['ewb_cancel_rsn_code']);
	unset($_SESSION['ewb_cancel_remark']);
	
	echo "<font color='green'>SUCCESS : Eway bill: $ewb_no_cancelled cancelled on : $cancelDate !</font>
	<script>
			alert('SUCCESS : Eway bill: $ewb_no_cancelled cancelled on : $cancelDate !');
			$('#loadicon').fadeOut('slow');
			$('#ewb_no').val('');
			$('#rsn_code').val('');
			$('#remark').val('');
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').fadeOut('slow');
		$('#button1').attr('disabled',false);
	</script>";
	exit();
}	