<?php
require_once("./connect.php");
$timestamp=date("Y-m-d H:i:s");

if(!isset($_POST['unq_id'])){
	echo "<script>
		window.location.href='./add_more_bl.php';
	</script>";
	exit();
}

$ship_id = escapeString($conn,strtoupper($_POST['ship_id']));
$shipment_no = escapeString($conn,strtoupper($_POST['unq_id']));
$bl_weight = escapeString($conn,($_POST['bl_weight']));
$bl_no = escapeString($conn,strtoupper($_POST['bl_no']));
$bl_date = escapeString($conn,($_POST['bl_date']));
$be_no = escapeString($conn,strtoupper($_POST['be_no']));
$be_date = escapeString($conn,($_POST['be_date']));

$chk_be_no = Qry($conn,"SELECT id FROM be_data WHERE unq_id='$shipment_no' AND be_no='$be_no'");
		  
	if(!$chk_be_no){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		echo "<script type='text/javascript'>
			alert('Error..');
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
		exit();
	}
	
	if(numRows($chk_be_no)>0)
	{
		echo "<script type='text/javascript'>
			alert('Duplicate BE number : $be_no in shipment number : $shipment_no.');
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
		exit();
	}
	
$sourcePath = $_FILES['be_copy']['tmp_name'];

	// $valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png","application/pdf");
	$valid_types = array("application/pdf");
	
	if(!in_array($_FILES['be_copy']['type'], $valid_types))
	{
			echo "<script>
				alert('Error : Only PDF Upload Allowed.');
				$('#loadicon').hide();
				$('#submit_button').attr('disabled',false);
			</script>";
			exit();
	}
    
		$UploadedSize = $_FILES['be_copy']['size'];
		
		if(($UploadedSize/1024/1024)>4)
		{
			echo "<script>
				alert('Error : MAX Upload size allowed is : 4MB');
				$('#loadicon').hide();
				$('#submit_button').attr('disabled',false);
			</script>";
			exit();
		}
	
	
StartCommit($conn);
$flag = true;	

$update_shipment = Qry($conn,"UPDATE shipment SET total_bl=total_bl+1,bl_weight=bl_weight+'$bl_weight',
draft_weight=draft_weight+'$bl_weight',bal_weight=bal_weight+'$bl_weight' WHERE id='$ship_id'");

if(!$update_shipment){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_bl=Qry($conn,"INSERT INTO bl_data(unq_id,bl_no,bl_date,bl_weight,branch,branch_user,timestamp) VALUES 
    ('".$shipment_no."','".$bl_no."','".$bl_date."',
	'".$bl_weight."','$branch','$branch_sub_user','".$timestamp."')");
		  
if(!$insert_bl){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$bl_id = getInsertID($conn);

$fix_name = date('dmYHis').mt_rand();

$targetPath="be_copy/".$fix_name.".".pathinfo($_FILES['be_copy']['name'],PATHINFO_EXTENSION);

if(pathinfo($_FILES['be_copy']['name'],PATHINFO_EXTENSION)!='pdf')
{
	ImageUpload(700,900,$sourcePath);
}
	
// ImageUpload(700,900,$sourcePath);

if(!move_uploaded_file($sourcePath,$targetPath))
{
	$flag = false;
	errorLog("File not uploaded.",$conn,$page_name,__LINE__);
}

 $insert_be=Qry($conn,"INSERT INTO be_data(unq_id,bl_id,be_no,be_copy,be_date,branch,branch_user,timestamp) VALUES 
	('".$shipment_no."','".$bl_id."','".$be_no."','".$targetPath."','".$be_date."','$branch','$branch_sub_user','".$timestamp."')");
		  
	if(!$insert_be){
		unlink($targetPath);
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('Shipment Updated Successfully !');
			window.location.href='add_more_bl.php';
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',true);
	</script>";
	exit();
}
?>