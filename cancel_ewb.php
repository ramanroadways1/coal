<?php
include_once "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Cancel Eway-bill :
      </h4>
	  
	  <style>
		label{font-size:13px;}
		.form-control{text-transform:uppercase;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<div id="result_form"></div> 
   
        <div class="row">
			
			 <div class="form-group col-md-12">


				<div class="form-group col-md-12">
				 </div>
				  
                 <div class="form-group col-md-3">
                     <label>EWB Number <font color="red">*</font></label>
                     <input style="font-size:12px" oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="12" type="text" class="form-control" id="ewb_no" required />
                 </div>
				 
				  <div class="form-group col-md-3">
                     <label>Cancel Reason <font color="red">*</font></label>
                     <select style="font-size:12px" class="form-control" id="rsn_code" required>
						<option class="ewb_cancel_rsn_class" style="font-size:12px" value="">--select reason--</option>
						<option class="ewb_cancel_rsn_class" id="ewb_cancel_rsn_1" style="font-size:12px" value="1">Duplicate</option>
						<option class="ewb_cancel_rsn_class" id="ewb_cancel_rsn_2" style="font-size:12px" value="2">Order Cancelled</option>
						<option class="ewb_cancel_rsn_class" id="ewb_cancel_rsn_3" style="font-size:12px" value="3">Data Entry mistake</option>
					 </select>
                 </div>
				 
				  <div class="form-group col-md-3">
                     <label>Cancel Remark <font color="red">*</font></label>
                     <input style="font-size:12px" maxlength="50" oninput="this.value=this.value.replace(/[^A-Z a-z0-9,./]/,'')" type="text" class="form-control" id="remark" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Enter OTP <font color="red">*</font> <button type="button" id="otp_sent_btn" class="btn btn-danger btn-xs" style="font-size:10px;" onclick="SendOtp()">Send OTP</button></label>
                     <input style="font-size:12px" maxlength="6" oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" class="form-control" id="otp" required />
                 </div>
				 
				<div class="form-group col-md-12">   
					<button type="button" onclick="CancelEwb()" disabled style="display:none" id="button1" class="btn btn-sm btn-success">Confirm Cancel</button>
                 </div> 
				 
		 </div>
		  
			<div class="form-group col-md-12">
			
				<div class="form-group col-md-12 table-responsive" id="tab_result">
				 </div>
				 
			</div>
			
         </div>
     
</div>

		</div>
			</div> 
          </div>
        </div>       
    </section>
	
<script>
function SendOtp()
{
	var ewb_no = $('#ewb_no').val();
	var rsn_code = $('#rsn_code').val();
	var remark = $('#remark').val();
	
	
	if(ewb_no=='' || rsn_code=='' || remark=='')
	{
		alert('Fill all fields first !');
	}
	else
	{
		$('#otp_sent_btn').attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
		url: "send_otp_cancel_ewb.php",
		data: 'ewb_no=' + ewb_no + '&rsn_code=' + rsn_code + '&remark=' + remark,
		type: "POST",
		success: function(data) {
			$("#tab_result").html(data);
		},
		error: function() {}
		});
	}
}

		function CancelEwb()
		{
			var ewb_no = $('#ewb_no').val();
			var rsn_code = $('#rsn_code').val();
			var remark = $('#remark').val();
			var otp = $('#otp').val();
			
			if(ewb_no=='' || rsn_code=='' || remark=='' || otp=='')
			{
				alert('All fields are required !');
			}
			else
			{
				$('#button1').attr('disabled',true);
				$("#loadicon").show();
					jQuery.ajax({
					url: "cancel_ewb_save.php",
					data: 'ewb_no=' + ewb_no + '&rsn_code=' + rsn_code + '&remark=' + remark + '&otp=' + otp,
					type: "POST",
					success: function(data) {
					$("#tab_result").html(data);
					},
					error: function() {}
				});
			}
		}</script>
<?php
include "footer.php";
?>
