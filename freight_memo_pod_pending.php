<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Freight MEMO POD Pending :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div id="tab_result"></div>
	
<div id="result_form"></div> 		
			
<div class="row">
 <div class="form-group col-md-12 table-responsive">
	<table id="example" class="table table-bordered table-striped" style="font-size:12px;">
		 <thead>	
		 <tr>
				<th>Id</th>
				<th>FM_No</th>
				<th>TruckNo</th>
				<th>FM_Date</th>
				<th>LRNo</th>
				<th>Weight</th>
				<th>Freight</th>
				<th>Broker</th>
			</tr>
          </thead> 
          <tbody> 
            <?php
              $sql = Qry($conn,"SELECT f.id,f.fm_no,f.company,f.date,f.owner,f.broker,f.weight,f.freight,f.lrnos,
			  b.name as broker_name,b.pan as broker_pan,o.tno,o.name as owner_name,o.pan as owner_pan,up6 as declaration 
			  FROM freight_memo as f 
			  LEFT OUTER JOIN rrpl_database.mk_broker as b ON b.id=f.broker 
			  LEFT OUTER JOIN rrpl_database.mk_truck as o ON o.id=f.owner 
			  WHERE f.advance='1' AND f.pod='0' AND f.branch='$branch'");
             
			if(!$sql){
				 ScriptError($conn,$page_name,__LINE__);
				 exit();
			  }
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='10'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				echo "
				<input type='hidden' value='$row[lrnos]' id='lrno_text$row[id]'>
				<tr>
				  <td>$sn</td>
				  <td>$row[fm_no]</td>
				  <td>$row[tno]</td>
				   <td>".date("d-m-y",strtotime($row['date']))."</td>
				  <td>$row[lrnos]</td>
				  <td>$row[weight]</td>
				  <td>$row[freight]</td>
				  <td>$row[broker_name]</td>
				</tr>";
			$sn++;	
			 }
			}
            ?>
		</tbody>	
        </table>
      </div>
    
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
	
<script>	
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
	
<?php
include "footer.php";
?>