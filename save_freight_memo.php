<?php
require_once("./connect.php");

$timestamp=date("Y-m-d H:i:s");
$date=date("Y-m-d");

$truck_id=escapeString($conn,($_POST['truck_id']));
$broker_id=escapeString($conn,($_POST['broker_id']));
$driver_id=escapeString($conn,($_POST['driver_id']));
$weight=escapeString($conn,($_POST['weight']));
$rate=escapeString($conn,($_POST['rate']));
$freight=escapeString($conn,($_POST['freight']));
$rate_or_fix=escapeString($conn,($_POST['rate_or_fix']));
$company=escapeString($conn,($_POST['company']));
$truck_no=escapeString($conn,($_POST['truck_no']));
$shipment_id=escapeString($conn,($_POST['shipment_id']));
$gps_type=escapeString($conn,($_POST['gps_type']));

if($truck_id=='' || $broker_id=='' || $driver_id=='' || $weight=='' || $rate=='' || $freight=='' || $company=='' || $shipment_id=='')
{
	echo "<script type='text/javascript'>
		alert('Unable to fetch Required Inputs.');
		$('#create_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_gps = Qry($conn,"SELECT id FROM gps_device_record WHERE uninstall_date=0 AND tno='$truck_no' AND install_type='FIX'");

if(!$chk_gps){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk_gps)==0)
{
	$gps_device_status=$gps_type;
	
	if($gps_type=='')
	{
		echo "<script type='text/javascript'>
			alert('Select GPS type first !!');
			$('#create_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}
else
{
	$gps_device_status="FIX";
	
	if($gps_type!='')
	{
		echo "<script type='text/javascript'>
			alert('GPS value is invalid !!');
			$('#create_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}


if(count(array_filter($_POST['lrid']))>0)
{
		foreach(array_filter($_POST['lrid']) as $lrids=>$value) 
		{
			if($_POST['ship_ids'][$lrids]!=$shipment_id)
			{
				echo "<script>
					alert('Multiple Shipment Found in LR.');
					$('#create_button').attr('disabled',false);
					$('#loadicon').hide();
				</script>";
				exit();
			}
			
			$lr_ids[]=$_POST['lrid'][$lrids];
			$lrnos[]=$_POST['lrnos'][$lrids];
			$lrnos_search[]=$_POST['lrnos'][$lrids];
			$DbWeight[]=$_POST['chrg_wt'][$lrids];
		}
		
		$lrIds=implode(',',array_filter($lr_ids));
		$lrnos=implode(',',array_filter($lrnos));
		$lrnos_search = implode(',',array_filter($lrnos_search));
		
		$DbWeight=array_sum($DbWeight);
		
		if($weight!=$DbWeight)
		{
			echo "<script>
				alert('Weight not matching.');
				$('#create_button').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
}
else
{
	echo "<script>
		alert('No LR found.');
		$('#create_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$get_lr_ids = Qry($conn,"SELECT id FROM rrpl_database.lr_sample WHERE FIND_IN_SET(lrno,'$lrnos')");
if(!$get_lr_ids){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_lr_ids)!=count(array_filter($_POST['lrid'])))
{
	echo "<script>
		alert('LR record not found.');
		$('#create_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

	$lrno_ids= array();
			
	while($row_lr_ids=fetchArray($get_lr_ids))
	{
		$lrno_ids[] = "'".$row_lr_ids['id']."'";
	}
	
	$lrno_ids=implode(',', $lrno_ids);

$fetch_bal_weight=Qry($conn,"SELECT bal_weight FROM shipment WHERE id='$shipment_id'");

if(!$fetch_bal_weight){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($fetch_bal_weight)==0)
{
	echo "<script>
		alert('Shipment not found.');
		$('#create_button').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_bal_weight=fetchArray($fetch_bal_weight);

$balance_weight=sprintf("%.2f",($row_bal_weight['bal_weight']-$DbWeight));

if($balance_weight<0)
{
	echo "<script>
			alert('Shipment Balance Weight ERROR : Bal Weight Aailable is : $row_bal_weight[bal_weight].');
			$('#create_button').attr('disabled',true);
			$('#loadicon').hide();
		</script>";
	exit();
}

$get_branch_code = Qry($conn,"SELECT branch_code FROM rrpl_database.user WHERE username='$branch'");

if(!$get_branch_code){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_branch_code) == 0)
{
	errorLog("Branch not found. $branch.",$conn,$page_name,__LINE__);
	exit();
}

$row_branch_code = fetchArray($get_branch_code);

if(strlen($row_branch_code['branch_code']) != 3)
{
	errorLog("Branch code not found. $branch.",$conn,$page_name,__LINE__);
	exit();
}

		$branch2 = strtoupper($row_branch_code['branch_code'])."F".date("dmy");
		
        $ramanc=Qry($conn,"SELECT fm_no FROM freight_memo where fm_no like '$branch2%' ORDER BY id DESC LIMIT 1");
		if(!$ramanc){
			ScriptError($conn,$page_name,__LINE__);
			exit();
		}
		
		$row=fetchArray($ramanc);
		if(numRows($ramanc)>0)
		{
			$count=$row['fm_no'];
			$newstring = substr($count,10);
			$newstr = ++$newstring;
			$memoid=$branch2.$newstr;
		}
		else
		{
			$memoid=$branch2."1";
		}

StartCommit($conn);
$flag = true;	
		
$insert_freight_memo = Qry($conn,"INSERT INTO freight_memo(fm_no,tno,company,date,owner,broker,driver,weight,rate_fix,rate,
freight,lr_ids,lrnos,done,shipment_id,gps_install_type,branch,branch_user,timestamp) VALUES ('$memoid','$truck_no','$company',
'$date','$truck_id','$broker_id','$driver_id','$weight','$rate_or_fix','$rate','$freight','$lrIds','$lrnos','1',
'$shipment_id','$gps_device_status','$branch','$branch_sub_user','$timestamp')");	

if(!$insert_freight_memo){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$vou_id=getInsertID($conn);

$update_lr_done=Qry($conn,"UPDATE lr_entry SET done='1' WHERE id IN($lrIds)");

if(!$update_lr_done){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_lr_done2=Qry($conn,"UPDATE rrpl_database.lr_sample SET crossing='NO' WHERE id IN($lrno_ids)");

if(!$update_lr_done2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$dlr_lr_temp = Qry($conn,"DELETE FROM rrpl_database.lr_sample_pending WHERE lr_id IN($lrno_ids)");

if(!$dlr_lr_temp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_dispatch = Qry($conn,"INSERT INTO dispatch_road(shipment_id,vou_id,vou_no,lrnos,tno,vou_date,weight,branch,branch_user,
timestamp) VALUES ('$shipment_id','$vou_id','$memoid','$lrnos','$truck_no','$date','$weight','$branch','$branch_sub_user','$timestamp')");

if(!$insert_dispatch){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_shipment_table=Qry($conn,"UPDATE shipment SET bal_weight='$balance_weight',by_road=by_road+'$DbWeight' WHERE 
id='$shipment_id'");

if(!$update_shipment_table){ 
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($gps_type!='')
{
	$gps_insert = Qry($conn,"INSERT INTO gps_device_record(frno,tno,install_date,device_status,install_type,first_install,timestamp) VALUES 
	('$memoid','$truck_no','$date','1','$gps_type','1','$timestamp')");

	if(!$gps_insert){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('Freight Memo : $memoid. Created Successfully !');
			window.location.href='./freight_memo.php';
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#create_button').attr('disabled',true);
	</script>";
	exit();
}
?>