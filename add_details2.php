<?php
include_once "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Add Truck / Broker / Driver :
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<script type="text/javascript">
$(document).ready(function (e) {
$("#AddTruck").on('submit',(function(e) {
$("#loadicon").show();
$("#add_truck_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_add_truck.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
	
$(document).ready(function (e) {
$("#AddBroker").on('submit',(function(e) {
$("#loadicon").show();
$("#add_broker_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_add_broker.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});

$(document).ready(function (e) {
$("#AddDriver").on('submit',(function(e) {
$("#loadicon").show();
$("#add_driver_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_add_driver.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});	
</script>			

<div id="result_form"></div> 
   
       <div class="row">
			
		
		 <div class="form-group col-md-6">
	
		<form id="AddTruck" autocomplete="off">
		
				<div class="form-group col-md-12">
					<h4 style="background:gray;padding:6px;color:#FFF">Add New Truck</h4>	
				</div>
				
				 <div class="form-group col-md-6">
                      <label>Truck Number <font color="red">*</font></label>
                      <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" name="tno" required />
                 </div>
				 
                <div class="form-group col-md-6">
                      <label>Owner Name <font color="red">*</font></label>
                      <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z ]/,'')" class="form-control" name="name" required />
                 </div>
				 
				 <div class="form-group col-md-6">
                      <label>Mobile Number <font color="red">*</font></label>
                      <input type="text" maxlength="10" minlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" name="mobile" required />
                 </div>
				 
				 <div class="form-group col-md-6">
                      <label>PAN Card Number <font color="red">*</font></label>
                      <input type="text" maxlength="10" id="truck_pan" minlength="10" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePan(this.value,this.id,'add_truck_button')" class="form-control" name="pan_no" required />
                 </div>
				 
				 <div class="form-group col-md-6">
                      <label>RC Copy (front side) <font color="red">*</font></label>
                      <input type="file" class="form-control" name="rc_front" required />
                 </div>
				 
				 <div class="form-group col-md-6">
                      <label>RC Copy (rear side) <font color="red">*</font></label>
                      <input type="file" class="form-control" name="rc_rear" required />
                 </div>
				 
				 <div class="form-group col-md-6">
                      <label>PAN Card Copy <font color="red">*</font></label>
                      <input type="file" class="form-control" name="pan_copy" required />
                 </div>
				 
				  <div class="form-group col-md-6">
                      <label>Wheeler <font color="red">*</font></label>
                      <select name="wheeler" required class="form-control" />
						<option value="">Select an option</option>
						<option value="4">4</option>
						<option value="6">6</option>
						<option value="10">10</option>
						<option value="12">12</option>
						<option value="14">14</option>
						<option value="16">16</option>
						<option value="18">18</option>
						<option value="22">22</option>
					</select>
                 </div>
				 
				 <div class="form-group col-md-6">
                      <label>Declaration Copy </label>
                      <input type="file" class="form-control" name="dec_copy" />
                 </div>
				 
				 <div class="form-group col-md-6">            
                      <label>Address <font color="red">*</font></label>
                      <textarea rows="2" oninput="this.value=this.value.replace(/[^a-zA-Z0-9 ,-]/,'')" required class="form-control" name="addr"></textarea>
                 </div> 
				 
				 <div class="form-group col-md-12">            
                      <button type="submit" id="add_truck_button" class="btn btn-block btn-success">ADD NEW TRUCK</button>
                 </div> 
		</form>	 
           </div>
		  
		  <div class="form-group col-md-6">
			<form id="AddBroker" autocomplete="off">
				
				<div class="form-group col-md-12">
					<h4 style="background:gray;padding:6px;color:#FFF">Add New Broker</h4>	
				</div>
				
                <div class="form-group col-md-6">
                      <label>Broker Name <font color="red">*</font></label>
                      <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z ]/,'')" class="form-control" name="name" required />
                 </div>
				 
				 <div class="form-group col-md-6">
                      <label>Mobile Number <font color="red">*</font></label>
                      <input type="text" maxlength="10" minlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" name="mobile" required />
                 </div> 
				 
				 <div class="form-group col-md-6">
                      <label>PAN Card Number <font color="red">*</font></label>
                      <input id="broker_pan" type="text" maxlength="10" minlength="10" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidatePan(this.value,this.id,'add_driver_button')" class="form-control" name="pan_no" required />
                 </div> 
				 
				 <div class="form-group col-md-6">
                      <label>PAN Card Copy <font color="red">*</font></label>
                      <input type="file" class="form-control" name="pan_copy" required />
                 </div> 
				 
				 <div class="form-group col-md-12">            
                        <label>Address <font color="red">*</font></label>
                        <textarea oninput="this.value=this.value.replace(/[^a-zA-Z0-9 ,-]/,'')" required rows="2" class="form-control" name="addr"></textarea>
                 </div> 
				 
				 <div class="form-group col-md-12">            
                       <button type="submit" id="add_driver_button" class="btn btn-block btn-success">ADD NEW BROKER</button>
                 </div> 
            </form>
          </div>
		  
		  <div class="form-group col-md-12">
				<hr>
		  </div>
		  
		  <div class="form-group col-md-6">
			<form id="AddDriver" autocomplete="off">
				
				<div class="form-group col-md-12">
					<h4 style="background:gray;padding:6px;color:#FFF">Add New Driver</h4>	
				</div>
				
                <div class="form-group col-md-6">
                      <label>Driver Name <font color="red">*</font></label>
                      <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z ]/,'')" class="form-control" name="name" required />
                 </div>
				 
				 <div class="form-group col-md-6">
                      <label>Mobile Number <font color="red">*</font></label>
                      <input type="text" maxlength="10" minlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" name="mobile" required />
                 </div> 
				 
				 <div class="form-group col-md-6">
                      <label>License Number <font color="red">*</font></label>
                      <input type="text" oninput="this.value=this.value.replace(/[^a-zA-Z0-9-]/,'')" class="form-control" name="lic_no" required />
                 </div> 
				 
				 <div class="form-group col-md-6">
                      <label>License Copy <font color="red">*</font></label>
                      <input type="file" class="form-control" name="lic_copy" required />
                 </div> 
				 
				  <div class="form-group col-md-6">
                      <label>Mobile Number-2 <font color="red">*</font></label>
                      <input type="text" maxlength="10" minlength="10" oninput="this.value=this.value.replace(/[^0-9]/,'')" 
					  class="form-control" name="mobile2" required />
                 </div> 
				 
				 <div class="form-group col-md-6">            
                        <label>Address <font color="red">*</font></label>
                        <textarea oninput="this.value=this.value.replace(/[^a-zA-Z0-9 ,-]/,'')" required rows="2" class="form-control" name="addr"></textarea>
                 </div> 
				 
				 <div class="form-group col-md-12">            
                       <button type="submit" id="add_driver_button" class="btn btn-block btn-success">ADD NEW DRIVER</button>
                 </div> 
            </form>
          </div>
		  
		</div>
     
</div>

<script>
function ValidatePan(elem,id,button)
{ 
  var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
  var PanNo = document.getElementById(id).value;
  
  if(PanNo!="")
  {
	  if (PanNo.search(panPat) == -1)
	  {
         document.getElementById(id).setAttribute("style","background-color:red;color:#fff;");
         document.getElementById(button).setAttribute("disabled",true);
        return false;
      }
      else
      {
         document.getElementById(id).setAttribute("style","background-color:green;color:#fff;");
		 document.getElementById(button).removeAttribute("disabled");
      }
  }
  else
  {
	document.getElementById(id).setAttribute("style","background-color:white;color:#fff");
	document.getElementById(button).removeAttribute("disabled");
  }
} 			
</script>

			</div>
			</div> 
          </div>
        </div>       
    </section>
<?php
include "footer.php";
?>