<?php
require_once './connect.php'; 

$date=date('Y-m-d');
$timestamp=date('Y-m-d H:i:s');

$name=escapeString($conn,strtoupper($_POST['name']));
$mobile=escapeString($conn,strtoupper($_POST['mobile']));
$mobile2=escapeString($conn,strtoupper($_POST['mobile2']));
$lic_no=escapeString($conn,strtoupper($_POST['lic_no']));
$addr=escapeString($conn,strtoupper($_POST['addr']));

if(strlen($mobile)!=10)
{
	 echo "<script>
			alert('Invald Mobile Number');
			$('#add_driver_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}

if(strlen($lic_no)<6)
{
	echo "<script>
		alert('Lic number must be atleast 6 digit.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$crop_lic=substr($lic_no,-5);
$name=$name." ".$crop_lic;

$chk_lic_no = Qry($conn,"SELECT name FROM rrpl_database.mk_driver WHERE pan='$lic_no' AND pan!='NA' AND pan!=''");

if(!$chk_lic_no)
{
	ScriptError($conn,$page_name,__LINE__);
	exit();	
}

if(numRows($chk_lic_no)>0)
{
	$row_name = fetchArray($chk_lic_no);
	
	echo "<script type='text/javascript'>]
		alert('License No : $lic_no. Already exists with Driver $row_name[name].'); 
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}

$chk_name = Qry($conn,"SELECT pan FROM rrpl_database.mk_driver WHERE name='$name'");

if(!$chk_name)
{
	ScriptError($conn,$page_name,__LINE__);
	exit();	
}

if(numRows($chk_name)>0)
{
	$row_pan = fetchArray($chk_name);
	
		echo "<script type='text/javascript'>
			alert('Driver : $name already exists with License No $row_pan[pan].'); 
			$('#add_driver_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();	
}

$sourcePath = $_FILES['lic_copy']['tmp_name'];

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");

if(!in_array($_FILES['lic_copy']['type'], $valid_types))
{
	echo "<script>
		alert('Error : Only Image Upload Allowed.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$fix_name = mt_rand().date('dmYHis');

$targetPath="driver_pan/".$fix_name.".".pathinfo($_FILES['lic_copy']['name'],PATHINFO_EXTENSION);
$targetPath2="../b5aY6EZzK52NA8F/driver_pan/".$fix_name.".".pathinfo($_FILES['lic_copy']['name'],PATHINFO_EXTENSION);

ImageUpload(1000,1000,$sourcePath);

if(!move_uploaded_file($sourcePath,$targetPath2))
{
	echo "<script>
		alert('Error : Unable to Upload License Copy.');
		$('#add_driver_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	$insert = Qry($conn,"INSERT INTO rrpl_database.mk_driver(name,mo1,mo2,pan,full,up5,branch,branch_user,timestamp) VALUES 
	('$name','$mobile','$mobile2','$lic_no','$addr','$targetPath','$branch','$branch_sub_user','$timestamp')");
	
	if(!$insert)
	{
		unlink($targetPath2);
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	echo "<script type='text/javascript'> 
		alert('Driver : $name. Added Successfully..');
		$('#loadicon').hide();
		$('#add_driver_button').attr('disabled',false);
		$('#AddDriver')[0].reset();
	</script>";
	exit();
}
?>