<?php
require_once 'connect.php';

$by=escapeString($conn,$_POST['by']);
$p_time=date("d-m-Y h:i A");
?>
<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/png" href="favicon.png"/>
	<title>Raman Sea Trans : Raman Group</title>
	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="./google_font.css" rel="stylesheet">

<style type="text/css" media="print">
@media print {

  body {
   zoom:70%;
 }
}
</style>

<style type="text/css">
@media print
{
#button_div * { visibility: hidden; }
.new1 { page-break-after: always }
.new2 { page-break-after: always }
.new3 { page-break-after: always }
.new4 { page-break-after: always }
}
</style>

<style> 
.table-bordered > tbody > tr > th {
     border:solid #000 !important;
    border-width:1px !important;
}

.table-bordered > tbody > tr > td {
      border:solid #000 !important;
    border-width:1px !important;
}
</style>
</head>

<body style="font-family: 'Open Sans', sans-serif !important">

<?php
if($by=='LR')
{
$lrno=escapeString($conn,strtoupper($_POST['lrno']));
// $lrno='141';
$qry = Qry($conn,"SELECT l.*,l2.boe_id,be.be_copy,be.unq_id 
	FROM rrpl_database.lr_sample AS l 
LEFT OUTER JOIN lr_entry AS l2 ON l2.lrno=l.lrno 
LEFT OUTER JOIN be_data AS be ON be.id=l2.boe_id 
WHERE l.lrno='$lrno'");
	if($qry)
	{
		if(numRows($qry)==0)
		{
			echo "<script>
				alert('LR Not Found..');
				window.location.href='./e_lr.php';
			</script>";
			closeConnection($conn);
			exit();
	}
	
	$row = fetchArray($qry);
	$date = date('d-M-y', strtotime($row['date']));
	
	$fetch_po_no = Qry($conn,"SELECT po_no FROM dest_location WHERE branch='$row[branch]' 
	AND location='$row[tstation]' AND ship_id='$row[unq_id]'");
	$row_po = fetchArray($fetch_po_no);
	$po_no = $row_po['po_no'];
	
	$ship_nameQry = Qry($conn,"SELECT l.shipment_id,s.vessel_name,v.name 
	FROM lr_entry as l 
LEFT OUTER JOIN shipment AS s ON s.id=l.shipment_id 
LEFT OUTER JOIN vessel_name AS v ON v.id=s.vessel_name 
	WHERE l.lrno='$lrno'");
	
	$row_ship_name = fetchArray($ship_nameQry);
	$ship_name = $row_ship_name['name'];
	
	$company = $row['company'];
	$branch1 = $row['branch'];
	$tno=$row['truck_no'];
	$from = $row['fstation'];
	$to = $row['tstation'];
	$con1 = $row['consignor'];
	$con2 = $row['consignee'];
	$act_wt = $row['wt12'];
	$chrg_wt = $row['weight'];
	$do_no = $row['do_no'];
	$invno = $row['invno'];
	$shipno = $row['shipno'];
	$item = $row['item'];
	$con1_gst = $row['con1_gst'];
	$con2_gst = $row['con2_gst'];
	$hsn_code = $row['hsn_code'];
	$wheeler = $row['wheeler'];
	
	$art = $row['articles'];
	
	$desc = $row['goods_desc'];
	$mt = $row['weight'];
	$frt = $row['bill_amt'];
	$b_value = $row['goods_value'];
	
	$gross_wt = $row['gross_wt'];
	$bank = $row['bank'];
	$lr_name = $row['lr_name'];
	$sold_to_pay = $row['sold_to_pay'];
	$freight_type = $row['freight_type'];
	$be_copy_path = $row['be_copy'];
	
	if($company=="RRPL")
	{
		$path1 = "../b5aY6EZzK52NA8F/rrpl.png";
		$gst = "24AAGCR0742P1Z5";
		$iba_code="ADR-1931";
		$iba_value="IBA Code No :";
	}
	else
	{
		$path1 = "../b5aY6EZzK52NA8F/rr.png";
		$gst = "24AAEHK7360R1ZO";
		$iba_code="";
		$iba_value="";
	}
	
$table="<table class='table table-bordered' style='font-size:13px;'>
<tr>
	<td colspan='2'>
		<img style='width:600px;height:100px' src='$path1' />
	</td>	
	<td colspan='2'>
		<b>GST No - $gst</b>
		<br>
		<br>
		<font size='3'><b>LR No: </b>$lrno</b>
		<br>
		<b>Truck No: </b>$tno, <b>Wheeler: </b>$wheeler
		<br>
		<b>LR Date :</b> $date</font>
	</td>	
</tr>

	<tr>
		<td style='width:50%' colspan='2'><font size='3'><b>Consignor :</b> $con1</font></td>
		<td style='width:50%' colspan='2'><font size='3'><b>Consignee :</b> $con2</font></td>
	</tr>
	
	<tr>
		<td colspan='4'><font size='3'><b>Sold To PARTY :</b> $sold_to_pay</font></td>
	</tr>
	
	<tr>
		<td colspan='4'><font size='3'><b>Vessel Name :</b> $ship_name</font></td>
	</tr>


	<tr>
		<td style='font-size:14px;padding-top:10px;padding-bottom:10px;' colspan='2'><b>From Station :</b> $from</td>
		<td style='font-size:14px;padding-top:10px;padding-bottom:10px;' colspan='2'><b>To Station :</b> $to</td>
	</tr>	

	<tr>
		<td colspan=''><b>Consignor GST :</b> $con1_gst</td>
		<td colspan=''><b>Consignee GST :</b> $con2_gst</td>
		<td colspan='2'><b>$iba_value</b> $iba_code</td>
	</tr>

	
	<tr>
		<td><b>Act Weight :</b> $act_wt</td>
		<td><b>Charge Weight :</b> $chrg_wt</td>
		<td><b>Gross Weight :</b> $gross_wt</td>
		<td><b>LR Name :</b> $lr_name</td>
	</tr>

	<tr>
		<td><b>No. of Articals :</b></td>
		<td><b>Description of Goods :</b></td>
		<td><b>M.T. :</b></td>
		<td><b>Freight :</b> ($freight_type)</td>
	</tr>

	<tr>
		<td style='font-size:15px;padding-top:40px;padding-bottom:40px'>$art</td>
		<td style='font-size:15px;padding-top:40px;padding-bottom:40px'>$desc</td>
		<td style='font-size:15px;padding-top:40px;padding-bottom:40px'>$mt</td>
		<td style='font-size:15px;padding-top:40px;padding-bottom:40px'>$frt</td>
	</tr>	


	<tr>
		<td colspan='1'><b>DO No. :</b> $do_no</td>
		<td colspan='1'><b>PO No. :</b> $po_no</td>
		<td colspan='2'><b>Shipment No. :</b> $shipno</td>
	</tr>
	
	<tr>	
		<td colspan='3'><b>Bank Name :</b> $bank</td>
		<td colspan='1'><b>HSN Code. :</b> $hsn_code</td>
	</tr>
	
	<tr>	
		<td colspan='1'><b>Invoice No. :</b> $invno</td>
		<td colspan='3'><b>Item :</b> $item</td>
	</tr>

	<tr>	
		<td colspan='4'><b>Note :</b> The Firm does not take any responsibility for accidents, earthquake, theft, leakage, breaking, shortage, soliage by sun, 
		rain water or weather. Sender is responsible for proper packing and breaking Goods are Carried at Owner's Risk.</td>
	</tr>

	<tr>	
		<td colspan='1'><b>Value :</b> $b_value</td>
		<td colspan='2' style='padding-top:40px;padding-bottom:40px'><b>Singature of Receiver with Rubber Stamp :</b></td>
		<td colspan='1' style='padding-top:30px;padding-bottom:30px'><b>Singature :</b></td>
	</tr>	
	
	</table>
	
	<br />
	<br />
<table class='table table-bordered' style='font-size:13px;'>
<tr>
	<td colspan='2'>
		<center><b>ACKNOWLEDGE BY THE CUSTOMER ON RECIEPT OF GOOD'S</b></center>
	</td>	
</tr>

<tr>
	
	<td style='padding-top:50px;padding-bottom:50px'>
		
	<b>Name & Signature</b> 
	<br />
	<b>With Rubber Stamp</b> 
	<br />
	<b>of Customer/Receiver</b> 
	
	</td>
	
	<td style='padding-top:50px;padding-bottom:50px'>
	<b>Received On</b> 
	<br />
	<br />
	<b>Remarks</b> 
	</td>
		
</tr>

	</table>";	
	
echo '
<div id="button_div">
<a href="./e_lr.php">
	<button style="margin-top:10px;margin-left:10px;font-weight:bold;color:#FFF;" class="btn btn-sm btn-primary">Go back</button></a>
	<button style="margin-top:10px;margin-left:10px;font-weight:bold;color:#FFF;" 
	class="btn btn-sm btn-danger" onclick="print();">Print e-LR</button></div>';
	
	echo "
	
<div class='container new1' style=''>

<div class='row' style=''>
	 <div class='col-md-6'>
		$p_time
	 </div>
	 <div class='col-md-6'>
		<span class='pull-right' style='color:#AAA;letter-spacing:1px'>Consignor Copy</span>
	 </div>
</div>

<div class='col-md-12'>
<br />
".$table."
</div>
</div>

<div class='container new2' style=''>

<div class='row' style=''>
	<div class='col-md-6'>
		$p_time
	 </div>
	 <div class='col-md-6'>
		<span class='pull-right' style='color:#AAA;letter-spacing:1px'>Consignee Copy</span>
	 </div>
</div>

<div class='col-md-12'>
<br />
".$table."
</div>
</div>


<div class='container new3' style=''>

<div class='row' style=''>
	 <div class='col-md-6'>
		$p_time
	 </div>
	 <div class='col-md-6'>
		<span class='pull-right' style='color:#AAA;letter-spacing:1px'>Driver Copy</span>
	 </div>
</div>

<div class='col-md-12'>
<br />
".$table."
	</div>
</div>

<div class='container new4 gstcopy' style=''>

<div class='row' style=''>
	 <div class='col-md-6'>
		$p_time
	 </div>
	 <div class='col-md-6'>
		<span class='pull-right' style='color:#AAA;letter-spacing:1px'>GST Copy</span>
	 </div>
</div>

<div class='col-md-12'>
<br />
".$table."
	</div>
</div>



<div class='container new5 filecopy' style=''>

<div class='row' style=''>
	 <div class='col-md-6'>
		$p_time
	 </div>
	 <div class='col-md-6'>
		<span class='pull-right' style='color:#AAA;letter-spacing:1px'>File Copy</span>
	 </div>
</div>

<div class='col-md-12'>
<br />
".$table."
	</div>
</div>
";	
}
else
{
echo mysqli_error($conn);
}
}
else
{
$frno=escapeString($conn,strtoupper($_POST['frno']));
// $lrno='141';

$frno_chk=Qry($conn,"SELECT lrno FROM freight_form_lr WHERE frno='$frno'");
if(!$frno_chk)
{
	echo mysqli_error($conn);
	exit();
}

if(numRows($frno_chk)==0)
{
	echo "<script>
	alert('No record found.');
	window.location.href='./e_lr.php';
	</script>";
	closeConnection($conn);
	exit();
}

if(numRows($frno_chk)>1)
{
echo '<style type="text/css">
@media print
{
.new5 { page-break-after: always }
}
</style>';
}

echo '
<div id="button_div">
<a href="./e_lr.php">
	<button style="margin-top:10px;margin-left:10px;font-weight:bold;letter-spacing:2px;color:#FFF;font-family:Verdana" class="btn btn-primary">Go back</button></a>
	<button style="margin-top:10px;margin-left:10px;font-weight:bold;letter-spacing:2px;color:#FFF;font-family:Verdana" 
	class="btn btn-danger" onclick="print();">Print e-LR</button></div>';

while($row_fm=fetchArray($frno_chk))
{
	$lrno=$row_fm['lrno'];
	$qry=Qry($conn,"SELECT * FROM lr_sample WHERE lrno='$lrno'");
	if($qry)
	{
	$row = fetchArray($qry);
	$date = date('d-M-y', strtotime($row['date']));
	$company = $row['company'];
	$branch1 = $row['branch'];
	$tno=$row['truck_no'];
	$from = $row['fstation'];
	$to = $row['tstation'];
	$con1 = $row['consignor'];
	$con2 = $row['consignee'];
	$act_wt = $row['wt12'];
	$chrg_wt = $row['weight'];
	$do_no = $row['do_no'];
	$invno = $row['invno'];
	$shipno = $row['shipno'];
	$item = $row['item'];
	$con1_gst = $row['con1_gst'];
	$con2_gst = $row['con2_gst'];
	$hsn_code = $row['hsn_code'];
	
	$art = $row['articles'];
	$desc = $row['goods_desc'];
	$mt = $row['weight'];
	$frt = $row['bill_amt'];
	$b_value = $row['goods_value'];
	
	$gross_wt = $row['gross_wt'];
	$bank = $row['bank'];
	$lr_name = $row['lr_name'];
	$sold_to_pay = $row['sold_to_pay'];
	$freight_type = $row['freight_type'];
	
	if($company=="RRPL")
	{
		$path1 = "rrpl.PNG";
		$gst = "24AAGCR0742P1Z5";
	}
	else
	{
		$path1 = "rr.PNG";
		$gst = "24AAEHK7360R1ZO";
	}
	
	$table="<table class='table table-bordered' style='font-size:13px;'>
<tr>
	<td colspan='2'>
		<img style='width:600px;height:100px' src='$path1' />
	</td>	
	<td colspan='2'>
		<b>GST No - $gst</b>
		<br>
		<br>
		<font size='3'><b>LR No: </b>$lrno</b>
		<br>
		<b>Truck No: </b>$tno, <b>Wheeler: </b>$wheeler
		<br>
		<b>LR Date :</b> $date</font>
	</td>	
</tr>

	<tr>
		<td style='width:50%' colspan='2'><font size='3'><b>Consignor :</b> $con1</font></td>
		<td style='width:50%' colspan='2'><font size='3'><b>Consignee :</b> $con2</font></td>
	</tr>
	
	<tr>
		<td colspan='4'><font size='3'><b>Sold To PARTY :</b> $sold_to_pay</font></td>
	</tr>

	<tr>
		<td style='font-size:14px;padding-top:10px;padding-bottom:10px;' colspan='2'><b>From Station :</b> $from</td>
		<td style='font-size:14px;padding-top:10px;padding-bottom:10px;' colspan='2'><b>To Station :</b> $to</td>
	</tr>	

	<tr>
		<td colspan=''><b>Consignor GST :</b> $con1_gst</td>
		<td colspan=''><b>Consignee GST :</b> $con2_gst</td>
		<td colspan='2'><b>$iba_value</b> $iba_code</td>
	</tr>

	
	<tr>
		<td><b>Act Weight :</b> $act_wt</td>
		<td><b>Charge Weight :</b> $chrg_wt</td>
		<td><b>Gross Weight :</b> $gross_wt</td>
		<td><b>LR Name :</b> $lr_name</td>
	</tr>

	<tr>
		<td><b>No. of Articals :</b></td>
		<td><b>Description of Goods :</b></td>
		<td><b>M.T. :</b></td>
		<td><b>Freight :</b> ($freight_type)</td>
	</tr>

	<tr>
		<td style='font-size:15px;padding-top:40px;padding-bottom:40px'>$art</td>
		<td style='font-size:15px;padding-top:40px;padding-bottom:40px'>$desc</td>
		<td style='font-size:15px;padding-top:40px;padding-bottom:40px'>$mt</td>
		<td style='font-size:15px;padding-top:40px;padding-bottom:40px'>$frt</td>
	</tr>	


	<tr>
		<td colspan='1'><b>DO No. :</b> $do_no</td>
		<td colspan='3'><b>Shipment No. :</b> $shipno</td>
	</tr>
	
	<tr>	
		<td colspan='3'><b>Bank Name :</b> $bank</td>
		<td colspan='1'><b>HSN Code. :</b> $hsn_code</td>
	</tr>
	
	<tr>	
		<td colspan='1'><b>Invoice No. :</b> $invno</td>
		<td colspan='3'><b>Item :</b> $item</td>
	</tr>

	<tr>	
		<td colspan='4'><b>Note :</b> The Firm does not take any responsibility for accidents, earthquake, theft, leakage, breaking, shortage, soliage by sun, 
		rain water or weather. Sender is responsible for proper packing and breaking Goods are Carried at Owner's Risk.</td>
	</tr>

	<tr>	
		<td colspan='1'><b>Value :</b> $b_value</td>
		<td colspan='2' style='padding-top:40px;padding-bottom:40px'><b>Singature of Receiver with Rubber Stamp :</b></td>
		<td colspan='1' style='padding-top:30px;padding-bottom:30px'><b>Singature :</b></td>
	</tr>	
	
	</table>
	
	<br />
	<br />
<table class='table table-bordered' style='font-size:13px;'>
<tr>
	<td colspan='2'>
		<center><b>ACKNOWLEDGE BY THE CUSTOMER ON RECIEPT OF GOOD'S</b></center>
	</td>	
</tr>

<tr>
	
	<td style='padding-top:50px;padding-bottom:50px'>
		
	<b>Name & Signature</b> 
	<br />
	<b>With Rubber Stamp</b> 
	<br />
	<b>of Customer/Receiver</b> 
	
	</td>
	
	<td style='padding-top:50px;padding-bottom:50px'>
	<b>Received On</b> 
	<br />
	<br />
	<b>Remarks</b> 
	</td>
		
</tr>

	</table>";	

echo "
	
<div class='container new1' style='font-family:Verdana'>

<div class='row' style='font-family:Verdana'>
	 <div class='col-md-6'>
		$p_time
	 </div>
	 <div class='col-md-6'>
		<span class='pull-right' style='color:#AAA;letter-spacing:1px'>Consignor Copy</span>
	 </div>
</div>

<div class='col-md-12'>
<br />
".$table."
	</div>
</div>

<div class='container new2' style='font-family:Verdana'>

<div class='row' style='font-family:Verdana'>
	 <div class='col-md-6'>
		$p_time
	 </div>
	 <div class='col-md-6'>
		<span class='pull-right' style='color:#AAA;letter-spacing:1px'>Consignee Copy</span>
	 </div>
</div>

<div class='col-md-12'>
<br />
".$table."
	</div>
</div>


<div class='container new3' style='font-family:Verdana'>

<div class='row' style='font-family:Verdana'>
	 <div class='col-md-6'>
		$p_time
	 </div>
	 <div class='col-md-6'>
		<span class='pull-right' style='color:#AAA;letter-spacing:1px'>Driver Copy</span>
	 </div>
</div>

<div class='col-md-12'>
<br />
".$table."
	</div>
</div>

<div class='container new4 gstcopy' style='font-family:Verdana'>

<div class='row' style='font-family:Verdana'>
	 <div class='col-md-6'>
		$p_time
	 </div>
	 <div class='col-md-6'>
		<span class='pull-right' style='color:#AAA;letter-spacing:1px'>GST Copy</span>
	 </div>
</div>

<div class='col-md-12'>
<br />
".$table."
	</div>
</div>



<div class='container new5 filecopy' style='font-family:Verdana'>

<div class='row' style='font-family:Verdana'>
	 <div class='col-md-6'>
		$p_time
	 </div>
	 <div class='col-md-6'>
		<span class='pull-right' style='color:#AAA;letter-spacing:1px'>File Copy</span>
	 </div>
</div>

<div class='col-md-12'>
<br />
".$table."
	</div>
</div>

";	
}
else
{
echo mysqli_error($conn);
}
}
}
?>