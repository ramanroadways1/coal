<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Freight Memo Balance Pending :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  </style>
	  
<style>
.modal {
  overflow-y:auto;
}

/*td {
  white-space: normal !important; 
  word-wrap: break-word;  
}
table {
  table-layout: fixed;
}*/
</style>

<style>
 .ui-autocomplete { z-index:2147483647; }
 
 .ui-front {
    z-index: 9999999 !important;
}
</style>



<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
	
<script type="text/javascript">
$(document).ready(function (e) {
$("#BalForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./freight_memo_balance_submit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>			

<div id="result_form"></div> 		
			
<div class="row">
 <div class="form-group col-md-12 table-responsive">
	
		<table id="example" class="table table-bordered table-striped" style="font-size:12px;">
		 <thead>		
		 <tr>
				<th>Id</th>
				<th>Vou No.</th>
				<th>Vou Date</th>
				<th>LR_Weight</th>
				<th>LRNo</th>
				<th>Unload Date</th>
				<th>Unloading Weight</th>
				<th>Unloading Slip</th>
				<th>Pod Copy</th>
				<th>Freight</th>
				<th>Adv Paid</th>
				<th>Balance</th>
				<th>Broker</th>
				<th>#</th>
			</tr>
           </thead>
           <tbody>
            <?php
              $sql = Qry($conn,"SELECT f.id,f.fm_no,f.company,f.date,f.owner,f.broker,
			  f.weight,f.pod,f.pod_date,f.rate,f.freight,f.lrnos,b.name as broker_name,b.pan as broker_pan,a.loading,a.dsl_inc,
			  a.other,a.commission_amount,a.tds,a.gps_deposit,a.total_freight,a.total_adv,pod.lr_weight,pod.rcvd_weight,pod.weight_release,
			  pod.weight_diff,pod.unloading_slip,pod.gps_charge,pod.pod_copy,pod.unload_date,pod.deduction,o.tno,o.name as owner_name,
			  o.pan as owner_pan,o.up6 as declaration,gps_deposit.amount as gps_deposit_return_amt,gps_deposit.status as gps_deposit_return_status
			  FROM freight_memo as f
			  LEFT OUTER JOIN rrpl_database.mk_broker as b ON b.id=f.broker
			  LEFT OUTER JOIN rrpl_database.mk_truck as o ON o.id=f.owner
			  LEFT OUTER JOIN freight_memo_adv as a ON a.fm_no=f.fm_no
			  LEFT OUTER JOIN rcv_pod as pod ON pod.fm_no=f.fm_no
			  LEFT OUTER JOIN gps_deposit_return as gps_deposit ON gps_deposit.fm_no=f.fm_no
			  WHERE f.balance='0' AND f.advance='1' AND f.branch='$branch'");
             
			 if(!$sql){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			 }
			  
			 if(numRows($sql)==0)
			 {
				echo "<tr>
					<td colspan='12'><b>NO RESULT FOUND..</b></td>
				</tr>";  
			 }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 $wt_diff_set=0.5;
				  
				 if($row['gps_deposit_return_amt']=='' || $row['gps_deposit_return_amt']==0)
				 {
					$gps_deposit_return_amt=0;
				 }
					else{
						$gps_deposit_return_amt=$row['gps_deposit_return_amt'];
					}				 
									  
				 if($row['pod']==1)
				 {
					if($row['weight_diff']>$wt_diff_set AND $row['weight_release']==0)
					{
						$lr_weight=$row['lr_weight'];
						$btn_set=""; 
						$btn_name="Weight <br>Diff"; 
						$btn_class="btn-warning"; 
						$other_amountDb=round(((($row['weight_diff']-$wt_diff_set)*$lr_weight)/100)*5500);
						
						$chk_diff_in_kg = round(($row['lr_weight']-$row['rcvd_weight'])*1000);
					
						if($chk_diff_in_kg>400)
						{
							$btn_set="disabled"; 
							$btn_name="Balance Blocked<br>Weight Diff."; 
							$btn_class="btn-danger"; 
						}
					}
					else
					{
						$btn_set=""; 
						$btn_name="PAY <br>Balance"; 
						$btn_class="bg-primary"; 
						$other_amountDb=0;
					}
					
					if($row['lr_weight']<$row['rcvd_weight'])
					{
						$new_freight=round($row['lr_weight']*$row['rate']);
					}
					else
					{
						$new_freight=round($row['rcvd_weight']*$row['rate']);
					}
					
					$unld_date=date("d/m/y",strtotime($row['unload_date']));
					$unld_wt=$row['rcvd_weight'];
				 }
				 else
				 {
					$btn_set="disabled"; 
					$btn_name="POD<br>Pending"; 
					$btn_class="btn-danger"; 
					$other_amountDb=0;
					
					$new_freight=$row['freight'];
					$unld_date="NULL";
					$unld_wt="--";
				}
				
				$deduction=$row['deduction'];
				$owner_name=$row['owner_name'];
				$owner_pan=$row['owner_pan'];
				$declaration=$row['declaration'];
				$gps_deposit=$row['gps_deposit'];
				
				$other_amountDb = $other_amountDb+$deduction;

				$balance_amt=round((($new_freight+$row['loading']+$row['dsl_inc'])-($row['gps_deposit']+$row['other']+$row['commission_amount']+$row['tds']))-$row['total_adv']);	
			
                echo "
				<tr>
				  <td>$sn</td>
				  <td>$row[fm_no]<br>
				  $row[tno]</td>
				   <td>".date("d/m/y",strtotime($row['date']))."</td>
				   <td>$row[weight]</td>
				  <td>$row[lrnos]</td>
				  <td>".$unld_date."</td>
				  <td>$unld_wt</td>
				  <td><a target='_blank' href='./$row[unloading_slip]'>View</a></td>
				  <td><a target='_blank' href='./$row[pod_copy]'>View</a></td>
				  <td>$row[freight]</td>
				  <td>$row[total_adv]</td>
				  <td>$balance_amt</td>
				  <td>$row[broker_name]</td>
				
				<input type='hidden' value='$row[broker_name]' id='broker_name_$row[id]' />
				<input type='hidden' value='$owner_name' id='OwnerName$row[id]' />
				<input type='hidden' value='$owner_pan' id='OwnerPan$row[id]' />
				<input type='hidden' value='$declaration' id='DecCopy$row[id]' />
				<input type='hidden' value='$row[lrnos]' id='lr_all$row[id]' />
				<input type='hidden' value='$row[broker_pan]' id='broker_pan_$row[id]' />
				<input type='hidden' value='$row[gps_charge]' id='gps_rcv_charge_$row[id]' />
				
				  <td><button $btn_set type='button' id='BalanceId$row[id]' 
					onclick=BalanceModal('$row[id]','$row[fm_no]','$row[company]','$row[tno]','$row[freight]','$row[owner]','$row[broker]','$row[loading]','$row[dsl_inc]','$row[other]','$row[commission_amount]','$gps_deposit','$row[tds]','$balance_amt','$row[pod_date]','$row[date]','$other_amountDb','$gps_deposit_return_amt','$deduction') 
				  class='btn btn-xs $btn_class'>$btn_name</button></td>
				 </tr>";
				 
				$sn++;	
              }
			}
            ?>
		 </tbody>	
        </table>
      </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
	
<style>
@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
</style>
	
<script>
  $(function() {  
      $("#card_no").autocomplete({
			source: function(request, response) { 
			if(document.getElementById('fuel_company_man').value!=''){
                 $.ajax({
                  url: "../b5aY6EZzK52NA8F/autofill/get_diesel_cards.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   company: document.getElementById('fuel_company_man').value
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              } else {
                alert('Please select company first !');
				$("#card_no").val('');
				$("#cardno_id").val('');
				$("#phy_card_no").val('');
              }

              },
              select: function (event, ui) { 
               $('#card_no').val(ui.item.value);   
               $('#cardno_id').val(ui.item.dbid);     
               $('#phy_card_no').val(ui.item.phy_card_no);     

               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
				$("#cardno_id").val('');
				$("#phy_card_no").val('');
                $(event.target).focus();
                alert('Card No. does not exist !'); 
              } 
              },
			}); 
      }); 
	  
 </script> 
	
<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
	
      <div class="bg-primary modal-header">
        <h4 class="modal-title"><b>Pay BALANCE : <span style="color:" id="fm_no_html"></span>
		&nbsp; (<span id="lrnos_html"></span>)</h4></b>
		<h5>
			<span><b>OWNER :</b> <span id="owner_name_html"></span> &nbsp; (<span id="truck_html"></span>) </span>
			 <span class="pull-right"><b>BROKER :</b> <span id="broker_name_html"></span></span>
		</h5>
		
		<h5>
			<span><b>Loading(+) :</b>  &nbsp; <span id="loading_html"></span> </span> &nbsp;
			<span><b>Dsl Incentive(+) :</b>  &nbsp; <span id="dsl_inc_html"></span> </span> &nbsp;
			<span><b>Gps Device Charge(-) :</b>  &nbsp; <span id="gps_charge_html"></span> </span> &nbsp;
			<span><b>Other(-) :</b>  &nbsp; <span id="other_html"></span> </span> &nbsp;
			<span><b>Commission(-) :</b>  &nbsp; <span id="commission_amount_html"></span> </span> &nbsp;
			<span><b>TDS(-) :</b> &nbsp;  <span id="tds_html"></span> </span>
		</h5>
      </div>

<div id="get_tds_div"></div>

	<form id="BalForm" autocomplete="off">
      <div class="modal-body">
		 <div class="row">
		 
			<div class="form-group col-md-2">
				<label>Balance <font color="red">*</font></label>
				<input type="number" min="1" id="balance_pay" name="balance" class="form-control" readonly required />
			</div>
			
			<script>
			function BalTo(elem)
			{
				$('#cash_bal').val('');
				$('#diesel_bal').val('');
				$('#rtgs_bal').val('');
				$('#unloading').val('');
				$('#others').val($('#other_min_value').val());
				$('#tds_amount').val('0');
				
				$('#cash_bal').attr('readonly',false);
				$('#diesel_bal').attr('readonly',false);
				$('#rtgs_bal').attr('readonly',false);
				
				$('#diesel_div').hide();
				$('#rtgs_div').hide();
				
				var broker_pan=$('#broker_pan').val();
				var owner_pan=$('#owner_pan').val();
				var unloading=$('#unloading').val();
					
				if(elem=='BROKER')
				{
					if($('#tds_deduct').val()=="1")
					{
						var pan_letter = broker_pan.slice(3, 4);
						
						if(pan_letter=='P' || pan_letter=='F' || pan_letter=='C' || pan_letter=='H' || pan_letter=='E')
						{
							$('#party_name').val($('#broker').val());
							$('#party_pan').val($('#broker_pan').val());
						}
						else
						{
							alert('Broker PAN Card is not Valid. Select another Option');
							$('#bal_to').val('');
							$('#tds_amount').val('0');
							$('#party_pan').val('');
							$('#party_name').val('');
						}
					}
					else
					{
						$('#party_name').val($('#broker').val());
						$('#party_pan').val($('#broker_pan').val());
					}
					
					$('#ac_holder').val($('#db_broker_acname').val());
					$('#ac_number').val($('#db_broker_acno').val());
					$('#bank_name').val($('#db_broker_bank').val());
					$('#ifsc_code').val($('#db_broker_ifsc').val());
				}
				else if(elem=='OWNER')
				{
					if($('#tds_deduct').val()=="1")
					{
						var pan_letter = owner_pan.slice(3, 4);
						
						if(pan_letter=='P' || pan_letter=='F' || pan_letter=='C' || pan_letter=='H' || pan_letter=='E')
						{
							$('#party_name').val($('#owner').val());
							$('#party_pan').val($('#owner_pan').val());
						}
						else
						{
							alert('Owner PAN Card is not Valid. Select another Option');
							$('#bal_to').val('');
							$('#tds_amount').val('0');
							$('#party_pan').val('');
							$('#party_name').val('');
						}
					}
					else
					{
						$('#party_name').val($('#owner').val());
						$('#party_pan').val($('#owner_pan').val());
					}
					
					$('#ac_holder').val($('#db_owner_acname').val());
					$('#ac_number').val($('#db_owner_acno').val());
					$('#bank_name').val($('#db_owner_bank').val());
					$('#ifsc_code').val($('#db_owner_ifsc').val());
				}
				else if(elem=='NULL')
				{
					$('#party_name').val('');
					$('#party_pan').val('');
					
					$('#cash_bal').val('0');
					$('#diesel_bal').val('0');
					$('#rtgs_bal').val('0');
					
					$('#cash_bal').attr('readonly',true);
					$('#diesel_bal').attr('readonly',true);
					$('#rtgs_bal').attr('readonly',true);
					DieselBal('0');
					RtgsBal('0');
					
					$('#ac_holder').val('');
					$('#ac_number').val('');
					$('#bank_name').val('');
					$('#ifsc_code').val('');
					
					$('#tds_amount').val('0');
				}
				else
				{
					$('#party_name').val('');
					$('#party_pan').val('');
					
					$('#ac_holder').val('');
					$('#ac_number').val('');
					$('#bank_name').val('');
					$('#ifsc_code').val('');
					
					$('#tds_amount').val('0');
				}
			}
			</script>
			
			<div class="form-group col-md-3">
				<label>PAY Balance To <font color="red">*</font></label>
				<select name="bal_to" id="bal_to" onchange="BalTo(this.value)" class="form-control" required>
					<option value="">--SELECT--</option>
					<option id="broker_sel" value="BROKER">BROKER</option>
					<option id="owner_sel" value="OWNER">OWNER</option>
				</select>
			</div>
			
			<div class="form-group col-md-4">
				<label>Party Name <font color="red">*</font></label>
				<input type="text" id="party_name" name="" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Party PAN <font color="red">*</font></label>
				<input type="text" id="party_pan" name="" class="form-control" readonly required />
			</div>
			
			<script>
			function TdsDeduct(amount)
			{
				if($('#bal_to').val()=='')
				{
					$('#unloading').val('');
					$('#bal_to').focus();
				}
				else
				{
					if($('#tds_deduct').val()==1)
					{
						$('#loadicon').show();
						jQuery.ajax({
							url: "get_tds_value.php",
							data: 'pan_letter=' + $('#party_pan').val().slice(3,4) + '&freight=' + amount + '&adv_bal=' + 'BAL' + '&pay_to=' + '',
							type: "POST",
							success: function(data) {
							$("#get_tds_div").html(data);
							},
							error: function() {}
						});
						
						// var pan_letter = $('#party_pan').val().slice(3, 4);
						
						// if(pan_letter=='P' || pan_letter=='H')
						// {
							// $('#tds_amount').val(Math.round(amount*1/100));
							// $('#tds_amount').val(Math.round(amount*0.75/100));
						// }
						// else if(pan_letter=='C' || pan_letter=='F' || pan_letter=='E')
						// {
							// $('#tds_amount').val(Math.round(amount*2/100));
							// $('#tds_amount').val(Math.round(amount*1.50/100));
						// }
						// else
						// {
							// alert('Invalid PAN Number '+ pan_letter);
							// $('#unloading').val('');
						// }
					}
					else
					{
						$('#tds_amount').val('0');
					}
				}
			}
			</script>
			
			<div class="form-group col-md-3">
				<label>UnLoading(+) <font color="red">*</font></label>
				<input type="number" min="0" oninput="TdsDeduct(this.value)" id="unloading" name="unloading" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>GPS Return(+) <font color="red">*</font></label>
				<input type="number" min="0" id="gps_deposit_return_amt" name="gps_deposit_return_amt" readonly class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Others(-) <font color="red">*</font> <sup id="other_min_value_span" style="color:red"></sup></label>
				<input type="number" min="0" id="others" name="others" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>GPS Charge(-) <font color="red">*</font></label>
				<input type="number" min="0" id="gps_device_charge" name="gps_device_charge" readonly class="form-control" required />
			</div>
			
			<input type="hidden" id="other_min_value">
			<input type="hidden" id="pod_deduction" name="deduction">
			
			<div class="form-group col-md-3">
				<label>TDS(-) <font color="red">*</font></label>
				<input type="number" min="0" value="0" readonly id="tds_amount" name="tds_amount" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<!-- <label>Claim(-) <font color="red">*</font> <a href="#" data-toggle="modal" data-target="#ClaimModal">ADD Claim</a></label>-->
				<label>Claim(-) <font color="red">*</font></label>
				<input type="number" min="0" value="0" id="claim" name="claim" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>LatePOD(-) <font color="red">*</font></label>
				<input type="number" min="0" value="0" readonly id="late_pod" name="late_pod" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Total Balance <font color="red">*</font></label>
				<input type="number" min="0" id="total_balance" name="total_balance" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Cash Bal. <font color="red">*</font></label>
				<input type="number" min="0" value="0" max="0" id="cash_bal" name="cash_bal" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Diesel Bal. <font color="red">*</font></label>
				<input type="number" min="0" oninput="DieselBal(this.value)" value="" id="diesel_bal" name="diesel_bal" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>RTGS Bal. <font color="red">*</font></label>
				<input type="number" min="0" oninput="RtgsBal(this.value)" value="" id="rtgs_bal" name="rtgs_bal" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Remain Bal. <font color="red">*</font></label>
				<input type="number" min="0" id="bal_remains" name="bal_remains" class="form-control" readonly required />
			</div>	
			
			<div id="ac_result"></div>
			
			<script>
			function FetchAc(owner_id,broker_id)
			{
				var dec = $('#declaration').val();
				var vou_date = $('#vou_date').val();
				var pod_date = $('#pod_date').val();
				var balance = $('#balance').val();
				
				if(dec=='')
				{
					$('#tds_deduct').val('1');
				}
				else
				{
					$('#tds_deduct').val('0');
				}
				
				jQuery.ajax({
					url: "fetch_ac_details.php",
					data: 'owner_id=' + owner_id + '&broker_id=' + broker_id + '&pod_date=' + pod_date + '&vou_date=' + vou_date + '&balance=' + balance + '&type=' + 'BAL',
					type: "POST",
					success: function(data) {
					$("#ac_result").html(data);
					},
					error: function() {}
				});
			}
			</script>
			
			<input type="hidden" id="tds_deduct" name="tds_deduct">
			
			
			<input type="hidden" id="db_owner_acname">
			<input type="hidden" id="db_owner_acno">
			<input type="hidden" id="db_owner_bank">
			<input type="hidden" id="db_owner_ifsc">
			
			<input type="hidden" id="db_broker_acname">
			<input type="hidden" id="db_broker_acno">
			<input type="hidden" id="db_broker_bank">
			<input type="hidden" id="db_broker_ifsc">
			
		<script>	
		function DieselBal(amount)
		{
			if($('#bal_to').val()==''){
				alert('Select Balance to option first.');
				$('#diesel_bal').val('');
			}
			else{	
				if(amount>0){
					$('#diesel_div').show();
				}
				else{
					$('#diesel_div').hide();
				}
			}
		}
		</script>	
		
		<script>	
		function RtgsBal(amount)
		{
			if($('#bal_to').val()=='')
			{
				alert('Select Balance to option first.');
				$('#rtgs_bal').val('');
			}
			else
			{	
			if(amount>0)
			{
				$('#ac_holder').attr('required',true);
				$('#ac_number').attr('required',true);
				$('#bank_name').attr('required',true);
				$('#ifsc_code').attr('required',true);
				
				if($('#ac_holder').val()=='')
				{
					$('#ac_holder').attr('readonly',false);
					$('#ac_number').attr('readonly',false);
					$('#bank_name').attr('readonly',false);
					$('#ifsc_code').attr('readonly',false);
				}
				else
				{
					$('#ac_holder').attr('readonly',true);
					$('#ac_number').attr('readonly',true);
					$('#bank_name').attr('readonly',true);
					$('#ifsc_code').attr('readonly',true);
				}
				
				$('#rtgs_div').show();
			}
			else
			{
				$('#ac_holder').attr('required',false);
				$('#ac_number').attr('required',false);
				$('#bank_name').attr('required',false);
				$('#ifsc_code').attr('required',false);
				
				$('#rtgs_div').hide();
			}
			}
		}
		</script>	
		
		<script>
		function LoadDiesel()
		{
			$('#loadicon').show();
			jQuery.ajax({
			url: "fetch_diesel_cache.php",
			data: 'vou_no=' + $('#VouId_for_diesel').val() + '&type=' + "BAL",
			type: "POST",
			success: function(data) {
			$("#diesel_div").html(data);
			},
			error: function() {}
			});
		}
		</script>
		
		<input type="hidden" id="VouId_for_diesel">
		
		<div id="diesel_div" style="display:none">
		
		</div>
		
		<div id="rtgs_div" style="display:none">
		
			<div class="form-group col-md-3">
				<label>Ac Holder <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" type="text" id="ac_holder" name="ac_holder" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Ac Number <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" id="ac_number" name="ac_number" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Bank Name <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" type="text" id="bank_name" name="bank_name" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>IFSC Code <font color="red">*</font></label>
				<input maxlength="11" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateIFSC(this.value)" type="text" id="ifsc_code" name="ifsc_code" class="form-control" readonly required />
			</div>
		</div>
	</div>
	
<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("ifsc_code");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
                //alert("Invalid Pan No");
				document.getElementById("ifsc_code").setAttribute(
   "style","background-color:red;text-transform:uppercase;color:#fff;");
   document.getElementById("submit_button").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
              //alert("Correct Pan No");
			  document.getElementById("ifsc_code").setAttribute(
   "style","background-color:green;color:#fff;text-transform:uppercase;");
			document.getElementById("submit_button").removeAttribute("disabled");
              }
        }
		else
		{
			document.getElementById("ifsc_code").setAttribute(
   "style","background-color:white;color:#fff;text-transform:uppercase;");
	document.getElementById("submit_button").removeAttribute("disabled");
		//	$("#pan_div").val("1");
		}
  } 			
</script>
			<input type="hidden" id="fm_id" name="fm_id">	
			<input type="hidden" id="fm_number" name="fm_number">	
			<input type="hidden" id="freight" name="freight">	
			<input type="hidden" id="balance" name="balance">	
			<input type="hidden" id="owner" name="owner">	
			<input type="hidden" id="owner_pan" name="owner_pan">	
			<input type="hidden" id="broker" name="broker">	
			<input type="hidden" id="broker_pan" name="broker_pan">	
			<input type="hidden" id="company" name="company">	
			
			<input type="hidden" id="owner_id" name="owner_id">	
			<input type="hidden" id="broker_id" name="broker_id">	
			<input type="hidden" id="truck_no" name="truck_no">	
			<input type="hidden" id="pod_date" name="pod_date">	
			<input type="hidden" id="vou_date" name="vou_date">	
			
			<input type="hidden" id="declaration" name="declaration">	
      </div>
	  
	 <script> 
$(function()
{
$("#balance,#unloading,#others,#gps_device_charge,#gps_deposit_return_amt,#tds_amount,#claim,#late_pod,#cash_bal,#diesel_bal,#rtgs_bal").on("keydown keyup blur change",sum);
function sum() {
	$("#total_balance").val((Number($("#balance").val()) + Number($("#unloading").val()) + Number($("#gps_deposit_return_amt").val()) - Number($("#gps_device_charge").val()) - Number($("#others").val()) - Number($("#tds_amount").val()) - Number($("#claim").val()) - Number($("#late_pod").val())).toFixed(2));
	// $("#total_adv").val((Number($("#cash_adv").val()) + Number($("#diesel_adv").val()) + Number($("#rtgs_adv").val())).toFixed(2));
	$("#bal_remains").val((Number($("#total_balance").val()) - (Number($("#cash_bal").val()) + Number($("#diesel_bal").val()) + Number($("#rtgs_bal").val()))).toFixed(2));
}});
	 </script> 

      <div class="modal-footer">
        <button type="submit" id="submit_button" class="btn btn-primary">Submit</button>
        <button type="button" id="close_modal_button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<?php
include("modal_diesel_adv.php");
include("modal_claim_add.php");
?>
		
	
<script>	
function BalanceModal(id,fm_no,company,truck_no,freight,owner_id,broker_id,load,dsl_inc,other,commission_amount,gps_deposit,tds,balance,pod_date,vou_date,otherAmtDb,gps_deposit_return_amt,deduction)
{
	var broker_name = $('#broker_name_'+id).val();
	var broker_pan = $('#broker_pan_'+id).val();
	var gps_device_charge = $('#gps_rcv_charge_'+id).val();
	var owner_name = $('#OwnerName'+id).val();
	var owner_pan = $('#OwnerPan'+id).val();
	var dec = $('#DecCopy'+id).val();
	
	$('#vou_type_diesel2').val("BAL");
	$('#vou_id_for_diesel').val(fm_no);
	$('#VouId_for_diesel').val(fm_no);
	$('#fm_id').val(id);
	$('#modal_claim_vou_no').val(fm_no);
	$('#fm_number').val(fm_no);
	$('#freight').val(freight);
	$('#freight_pay').val(freight);
	$('#owner').val(owner_name);
	$('#owner_pan').val(owner_pan);
	$('#declaration').val(dec);
	$('#broker').val(broker_name);
	$('#broker_pan').val(broker_pan);
	$('#owner_id').val(owner_id);
	$('#broker_id').val(broker_id);
	$('#company').val(company);
	$('#truck_no').val(truck_no);
	$('#truck_html').html(truck_no);
	$('#dsl_inc_html').html(dsl_inc);
	$('#gps_charge_html').html(gps_deposit);
	$('#lrnos_html').html($('#lr_all'+id).val());
	$('#loading').val(load);
	$('#other').val(other);
	$('#commission_amount').val(other);
	$('#tds').val(tds);
	$('#balance').val(balance);
	$('#balance_pay').val(balance);
	$('#pod_date').val(pod_date);
	$('#vou_date').val(vou_date);
	$('#gps_device_charge').val(gps_device_charge);
	$('#gps_deposit_return_amt').val(gps_deposit_return_amt);
	
	$('#owner_name_html').html(owner_name);
	$('#broker_name_html').html(broker_name);
	$('#fm_no_html').html(fm_no);
	
	$('#loading_html').html(load);
	$('#other_html').html(other);
	$('#commission_amount_html').html(commission_amount);
	$('#tds_html').html(tds);
	
	$('#others').attr('min',otherAmtDb);
	$('#others').val(otherAmtDb);
	$('#other_min_value').val(otherAmtDb);
	$('#pod_deduction').val(deduction);
	$('#other_min_value_span').html('Min Val: '+otherAmtDb);
	
	FetchAc(owner_id,broker_id);
	LoadDiesel();
	document.getElementById('ModalButton').click();
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
	
<?php
include "footer.php";
?>