<?php
include_once "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Add Vessel / ShipmentParty / Shipper Party / PORT :
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<script type="text/javascript">
$(document).ready(function (e) {
$("#AddShipmentParty").on('submit',(function(e) {
$("#loadicon").show();
$("#add_shipment_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_new_shipment.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
	
$(document).ready(function (e) {
$("#AddShipperParty").on('submit',(function(e) {
$("#loadicon").show();
$("#add_shipper_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_new_shipper.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});

$(document).ready(function (e) {
$("#AddPort").on('submit',(function(e) {
$("#loadicon").show();
$("#add_port_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_new_port.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});	
	
$(document).ready(function (e) {
$("#AddVessel").on('submit',(function(e) {
$("#loadicon").show();
$("#add_vessel_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_new_vessel.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});	
</script>			

<div id="result_form"></div> 
   
       <div class="row">
			
		
		 <div class="form-group col-md-4">
	
		<form id="AddShipmentParty" autocomplete="off">
		
				<div class="form-group col-md-12">
					<h4 style="background:gray;padding:6px;color:#FFF">Add Shipment Party</h4>	
				</div>
				
                <div class="form-group col-md-12">
                      <label>Party Name <font color="red">*</font></label>
                      <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9,.-/@#$_-]/,'');" type="text" class="form-control" name="name" required />
                 </div>
				 
				 <div class="form-group col-md-12">            
                     <label>Address <font color="red">*</font></label>
                     <textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9,.-/@#$_-]/,'');" rows="1" required class="form-control" name="addr"></textarea>
                 </div> 
				 
				 <div class="form-group col-md-12">            
                      <button type="submit" id="add_shipment_button" class="btn btn-block btn-success">Add Shipment Party</button>
                 </div> 
		</form>	 
           </div>
		  
		  <div class="form-group col-md-4">
			<form id="AddShipperParty" autocomplete="off">
				
				<div class="form-group col-md-12">
					<h4 style="background:gray;padding:6px;color:#FFF">Add Shipper Party</h4>	
				</div>
				
                <div class="form-group col-md-12">
                      <label>Party Name <font color="red">*</font></label>
                      <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9,.-/@#$_-]/,'');" type="text" class="form-control" name="name" required />
                 </div>
				 
				 <div class="form-group col-md-12">            
                        <label>Address <font color="red">*</font></label>
                        <textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9,.-/@#$_-]/,'');" required rows="1" class="form-control" name="addr"></textarea>
                 </div> 
				 
				 <div class="form-group col-md-12">            
                       <button type="submit" id="add_shipper_button" class="btn btn-block btn-success">Add Shipper Party</button>
                 </div> 
            </form>
          </div>
		  
		<div class="form-group col-md-4">
			<form id="AddPort" autocomplete="off">
				<div class="form-group col-md-12">
					<h4 style="background:gray;padding:6px;color:#FFF">Add PORT / Vessel</h4>	
				</div>
				
                <div class="form-group col-md-12">
                      <label>PORT Name <font color="red">*</font></label>
					<div class="form-inline">	
					 <input style="width:80%" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,.-/@#$_-]/,'');" type="text" class="form-control" name="name" required />
					<button type="submit" id="add_port_button" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
					</div>
                 </div>
			
			</form>	
		
			<form id="AddVessel" autocomplete="off">	
				
				<div class="form-group col-md-12">
                      <label>Vessel Name <font color="red">*</font></label>
					<div class="form-inline">		
					 <input style="width:80%" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,.-/@#$_-]/,'');" type="text" class="form-control" name="name" required />
						 <button type="submit" id="add_vessel_button" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
					</div>
				</div>
				 
				<div class="form-group col-md-12">            
                      
                </div> 
		 </form>
          </div>
		  
		 </div>
     
</div>

			</div>
			</div> 
          </div>
        </div>       
    </section>
<?php
include "footer.php";
?>