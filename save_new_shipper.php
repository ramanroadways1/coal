<?php
require_once("connect.php");

$timestamp=date("Y-m-d H:i:s");

$party_name=escapeString($conn,strtoupper($_POST['name']));
$addr=escapeString($conn,strtoupper($_POST['addr']));

$chk=Qry($conn,"SELECT id FROM shipper WHERE name='$party_name'");
if(!$chk){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk)>0)
{
	echo "<script>
		alert('Duplicate Party Found');
		$('#loadicon').hide();
		$('#add_shipper_button').attr('disabled',false);
	</script>";
	exit();	
}

$qry=Qry($conn,"INSERT INTO shipper(name,addr,branch,branch_user,timestamp) VALUES ('$party_name','$addr','$branch','$branch_sub_user','$timestamp')");

if(!$qry){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

echo "<script>
		alert('Party Successfully Added !!');
		$('#AddShipperParty')[0].reset();
		$('#loadicon').hide();
		$('#add_shipper_button').attr('disabled',false);
	</script>";
	exit();	
?>