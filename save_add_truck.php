<?php
require_once './connect.php'; 

$date=date('Y-m-d');
$timestamp=date("Y-m-d H:i:s");

$tno = escapeString($conn,strtoupper($_POST['tno'])); 
$name = trim(escapeString($conn,strtoupper($_POST['name'])));
$mobile = escapeString($conn,strtoupper($_POST['mobile']));
$pan_no = escapeString($conn,strtoupper($_POST['pan_no']));
$wheeler = escapeString($conn,strtoupper($_POST['wheeler']));
$addr = trim(escapeString($conn,strtoupper($_POST['addr'])));

if(strlen($mobile)!=10)
{
	 echo "<script>
			alert('Invald Mobile Number');
			$('#loadicon').hide();
			$('#add_truck_button').attr('disabled',false);
		</script>";
	exit();
}

if($mobile=="0000000000" || $mobile=="1234567890" || $mobile=="1234512345" || $mobile=="1234567899")
{
	echo "<script>
		alert('Invalid Mobile No.');
		$('#add_truck_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(!preg_match("/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/", $pan_no))
{
	echo "<script>
		alert('Invalid PAN No.');
		$('#loadicon').hide();
		$('#add_truck_button').attr('disabled',false);
	</script>";
	exit();
}

$chk_truck_no = Qry($conn,"SELECT tno FROM rrpl_database.mk_truck WHERE tno='$tno'");
if(!$chk_truck_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing Request');
		$('#loadicon').hide();
		$('#add_truck_button').attr('disabled',false);
	</script>";
	exit();
}

if(numRows($chk_truck_no)>0)
{
	echo "<script type='text/javascript'>
		alert('Vehicle Number : $tno already registered.'); 
		$('#add_truck_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();	
}

$sourcePath_Rc_Front = $_FILES['rc_front']['tmp_name']; // UPLOAD FILE
$sourcePath_Rc_Rear = $_FILES['rc_rear']['tmp_name']; // UPLOAD FILE
$sourcePath_Pan = $_FILES['pan_copy']['tmp_name']; // UPLOAD FILE
$sourcePath_Dec = $_FILES['dec_copy']['tmp_name']; // UPLOAD FILE

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png", "application/pdf");

if(!in_array($_FILES['pan_copy']['type'], $valid_types)){
	echo "<script>
		alert('Error : Only Image Upload Allowed. PAN Copy');
		$('#add_truck_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(!in_array($_FILES['rc_front']['type'], $valid_types)){
	echo "<script>
		alert('Error : Only Image Upload Allowed. PAN Copy');
		$('#add_truck_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if(!in_array($_FILES['rc_rear']['type'], $valid_types)){
	echo "<script>
		alert('Error : Only Image Upload Allowed. PAN Copy');
		$('#add_truck_button').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($sourcePath_Dec!='')
{
	if(!in_array($_FILES['dec_copy']['type'], $valid_types)){
		echo "<script>
			alert('Error : Only Image Upload Allowed. PAN Copy');
			$('#add_truck_button').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}

	$dec_time=$timestamp;
}
else
{
	$dec_time="";
}

$fix_name = mt_rand().date('dmYHis');

$targetPath_Pan="truck_pan/".mt_rand().date('dmYHis').".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);
$targetPath_Pan2="../b5aY6EZzK52NA8F/truck_pan/".mt_rand().date('dmYHis').".".pathinfo($_FILES['pan_copy']['name'],PATHINFO_EXTENSION);
$targetPath_Rc_front="truck_rc/Front_".mt_rand().date('dmYHis').".".pathinfo($_FILES['rc_front']['name'],PATHINFO_EXTENSION);
$targetPath_Rc_front2="../b5aY6EZzK52NA8F/truck_rc/Front_".mt_rand().date('dmYHis').".".pathinfo($_FILES['rc_front']['name'],PATHINFO_EXTENSION);
$targetPath_Rc_rear="truck_rc/Rear_".mt_rand().date('dmYHis').".".pathinfo($_FILES['rc_rear']['name'],PATHINFO_EXTENSION);
$targetPath_Rc_rear2="../b5aY6EZzK52NA8F/truck_rc/Rear_".mt_rand().date('dmYHis').".".pathinfo($_FILES['rc_rear']['name'],PATHINFO_EXTENSION);

if($sourcePath_Dec!='')
{	
	$targetPath_Dec="truck_dec/".mt_rand().date('dmYHis').".".pathinfo($_FILES['dec_copy']['name'],PATHINFO_EXTENSION);
	$targetPath_Dec2="../b5aY6EZzK52NA8F/truck_dec/".mt_rand().date('dmYHis').".".pathinfo($_FILES['dec_copy']['name'],PATHINFO_EXTENSION);
}
else
{
	$targetPath_Dec="";
}

ImageUpload(1000,1000,$sourcePath_Pan);

if(!move_uploaded_file($sourcePath_Pan,$targetPath_Pan2)){
	Redirect("Unable to Upload PAN Card Copy.","./");
	exit();
}

ImageUpload(1000,1000,$sourcePath_Rc_Front);

if(!move_uploaded_file($sourcePath_Rc_Front,$targetPath_Rc_front2)){
	unlink($targetPath_Pan2);
	Redirect("Unable to Upload RC Front Copy.","./");
	exit();
}

ImageUpload(1000,1000,$sourcePath_Rc_Rear);

if(!move_uploaded_file($sourcePath_Rc_Rear,$targetPath_Rc_rear2)){
	unlink($targetPath_Pan2);
	unlink($targetPath_Rc_front2);
	Redirect("Unable to Upload RC Rear Copy.","./");
	exit();
}

if($sourcePath_Dec!='')
{
	ImageUpload(1000,1000,$sourcePath_Dec);
	
	if(!move_uploaded_file($sourcePath_Dec,$targetPath_Dec2)){
		unlink($targetPath_Pan2);
		unlink($targetPath_Rc_front2);
		unlink($targetPath_Rc_rear2);
		Redirect("Unable to Upload Declaration Copy.","./");
		exit();
	}
}

$insert=Qry($conn,"INSERT INTO rrpl_database.mk_truck (tno,wheeler,name,mo1,pan,up1,rc_rear,up4,full,up6,dec_upload_time,branch,
branch_user,tds,timestamp) VALUES ('$tno','$wheeler','$name','$mobile','$pan_no','$targetPath_Rc_front','$targetPath_Rc_rear',
'$targetPath_Pan','$addr','$targetPath_Dec','$dec_time','$branch','$branch_sub_user','','$timestamp')");

if(!$insert){
	unlink($targetPath_Pan2);
	unlink($targetPath_Rc_front2);
	unlink($targetPath_Rc_rear2);
	unlink($targetPath_Dec2);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error while processing Request');
		$('#loadicon').hide();
		$('#add_truck_button').attr('disabled',false);
	</script>";
	exit();
}
	
	echo "<script type='text/javascript'> 
		alert('Vehicle : $tno. Added Successfully.');
		$('#AddTruck')[0].reset();
		$('#loadicon').hide();
		$('#add_truck_button').attr('disabled',false);
	</script>";
	exit();

?>