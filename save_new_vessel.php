<?php
require_once("connect.php");

$timestamp=date("Y-m-d H:i:s");

$vessel_name=escapeString($conn,strtoupper($_POST['name']));

$chk=Qry($conn,"SELECT id FROM vessel_name WHERE name='$vessel_name'");
if(!$chk){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($chk)>0)
{
	echo "<script>
		alert('Duplicate Entry Found');
		$('#loadicon').hide();
		$('#add_vessel_button').attr('disabled',false);
	</script>";
	exit();	
}

$qry=Qry($conn,"INSERT INTO vessel_name(name,added_by,branch_user,timestamp) VALUES 
('$vessel_name','$branch','$branch_sub_user','$timestamp')");

if(!$qry){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

echo "<script>
		alert('Vessel Successfully Added !!');
		$('#AddVessel')[0].reset();
		$('#loadicon').hide();
		$('#add_vessel_button').attr('disabled',false);
	</script>";
	exit();	
?>