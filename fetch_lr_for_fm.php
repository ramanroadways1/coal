<?php
require_once("connect.php");

$tno=escapeString($conn,strtoupper($_POST['tno']));

$qry_fetch_lrs=Qry($conn,"SELECT l.id,l.lr_type,l.shipment_id,l.lrno,l.lr_date,l.company,l.actual_wt,l.charge_wt,loc1.name as from1,
loc2.name as to1,from_loc,to_loc,con1 as con1_id,con2 as con2_id,c1.name as con1,c2.name as con2,s.voyage_no,v.name as ship_name
FROM lr_entry as l 
LEFT OUTER JOIN rrpl_database.station as loc1 on loc1.id=l.from_loc
LEFT OUTER JOIN rrpl_database.station as loc2 on loc2.id=l.to_loc
LEFT OUTER JOIN rrpl_database.consignor as c1 on c1.id=l.con1
LEFT OUTER JOIN rrpl_database.consignee as c2 on c2.id=l.con2
LEFT OUTER JOIN shipment as s on s.id=l.shipment_id
LEFT OUTER JOIN vessel_name as v on v.id=s.vessel_name
WHERE l.done='0' AND l.branch='$branch' AND l.tno='$tno'");

if(!$qry_fetch_lrs){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($qry_fetch_lrs)==0)
{
	echo "<script>
			alert('NO LR FOUND');
			$('#create_button').attr('disabled',true);
			$('#loadicon').hide();
			$('#create_button').hide();
		</script>";
	exit();
}

if(!isset($_POST['own_vehicle']))
{
	$chk_gps = Qry($conn,"SELECT id FROM gps_device_record WHERE uninstall_date=0 AND tno='$tno' AND install_type='FIX'");

	if(!$chk_gps){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}
}
?>
<br />

<style>
td {
  white-space: normal !important; 
  word-wrap: break-word;  
}
table {
  table-layout: fixed;
}
</style>

        <table id="myTable" class="table table-bordered table-striped" style="font-size:12px;">
          <tr>
				<th>#</th>
				<th>Ship<br>Name</th>
				<th>LRNo</th>
				<th>LRDate</th>
				<th>ActWt</th>
				<th>ChrgWt</th>
				<th>From</th>
				<th>To</th>
				<th>Consignor</th>
				<th>Consignee</th>
				<th>ADD</th>
			</tr>
          
            <?php
            $sn=1;
			  
			  while($row = fetchArray($qry_fetch_lrs))
			  {
                echo 
                "<tr>
				  <td>$sn <img id='rightImg$row[id]' src='right.png' style='display:none;width:15px;height:15px' /></td>
				  <td>$row[ship_name]-$row[voyage_no]</td>
				  <td>$row[lrno]</td>
				  <td>".date("d-m-y",strtotime($row['lr_date']))."</td>
				  <td>$row[actual_wt]</td>
				  <td>$row[charge_wt]</td>
				  <td>$row[from1]</td>
				  <td>$row[to1]</td>
				  <td>$row[con1]</td>
				  <td>$row[con2]</td>
				  <td>
					<input type='hidden' name='lrnos[]' value='$row[lrno]'>
					<input type='hidden' name='lrid[]' id='lrid$row[id]'>
					<input type='hidden' name='company' value='$row[company]'>
					<input type='hidden' name='ship_ids[]' value='$row[shipment_id]'>
					
					<input type='hidden' name='lr_date[]' value='$row[lr_date]'>
					<input type='hidden' name='act_wt[]' value='$row[actual_wt]'>
					<input type='hidden' name='chrg_wt[]' value='$row[charge_wt]'>
					<input type='hidden' name='from1[]' value='$row[from1]'>
					<input type='hidden' name='to1[]' value='$row[to1]'>
					<input type='hidden' name='con1[]' value='$row[con1]'>
					<input type='hidden' name='con2[]' value='$row[con2]'>
					<input type='hidden' name='from_id[]' value='$row[from_loc]'>
					<input type='hidden' name='to_id[]' value='$row[to_loc]'>
					<input type='hidden' name='con1_id[]' value='$row[con1_id]'>
					<input type='hidden' name='con2_id[]' value='$row[con2_id]'>
					
					<button type='button' id='AddBtn$row[id]' onclick=AddLR('$row[shipment_id]','$row[id]','$row[lrno]','$row[charge_wt]') class='btn btn-xs bg-green'>Add LR</button>
					<button type='button' style='display:none' id='RemoveBtn$row[id]' onclick=RemoveLR('$row[shipment_id]','$row[id]','$row[lrno]','$row[charge_wt]') class='btn btn-xs bg-red'>Remove LR</button>
				  </td>
				 </tr>
				";
		 	 $sn++;
             }
			?>
        </table>
		
<?php	
if(!isset($_POST['own_vehicle']))
{
	if(numRows($chk_gps)==0)
	{
		echo "<script>
			$('#gps_type_div').show();
			$('#gps_type').attr('required',true);
		</script>";
	}
	else
	{
		echo "<script>
			$('#gps_type_div').hide();
			$('#gps_type').attr('required',false);
		</script>";
	}
}
?>
<script>	
	$('#truck_no').attr('readonly',true);
	$('#create_button').attr('disabled',false);
	$('#loadicon').hide();
	$('#create_button').show();
</script>	
     