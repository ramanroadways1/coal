<?php 
require_once './connect.php';

$timestamp = date("Y-m-d H:i:s");

$user=escapeString($conn,strtoupper($_POST['username']));
$pass = escapeString($conn,$_POST['password']);

$FetchMobile = Qry($conn,"SELECT mobile_no,whatsapp_enabled FROM rrpl_database.emp_attendance WHERE 
code = (SELECT emp_code FROM rrpl_database.manager WHERE branch='$branch')");
if(!$FetchMobile){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($FetchMobile)==0)
{
	echo "<center><span style='color'>Manager not assigned !</span></center>";
	if(isset($_POST['mgr_flag'])) { echo "<script>$('#login_mgr_btn').attr('disabled', false)</script>"; }
	exit();
}
	
$row_mobile = fetchArray($FetchMobile);	

if($row_mobile['mobile_no']!=$user)
{
	echo "<center><span style='color:'>Invalid Username or Password !</span></center>";
	if(isset($_POST['mgr_flag'])) { echo "<script>$('#login_mgr_btn').attr('disabled', false)</script>"; }
	exit();
}
	
$verify_login = Qry($conn,"SELECT pass,last_pass FROM rrpl_database.manager WHERE branch='$branch' AND pass='".md5($pass)."'");
if(!$verify_login){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

		if(numRows($verify_login)==0)
        {
			echo "<center><span style='color'>Invalid Username or Password !</span></center>";
			if(isset($_POST['mgr_flag'])) { echo "<script>$('#login_mgr_btn').attr('disabled', false)</script>"; }
			exit();
		}
		
		$row_login = fetchArray($verify_login);
		
		$hours = CalcHours($row_login['last_pass'],$timestamp);
		
		$get_password_exp_hours = Qry($conn,"SELECT hours FROM rrpl_database.password_expiry_policy where role='3'");
		
		if(!$get_password_exp_hours)
		{
			errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
			echo "<script>
				alert('ERROR : While processing Request !');	
				window.location.href='./';
			</script>";
			exit();
		}
		
		if(numRows($get_password_exp_hours)==0)
        {
			echo "<center><span style='color'>User Type not Listed. Contact Head-Office !</span></center>";
			if(isset($_POST['mgr_flag'])) { echo "<script>$('#login_mgr_btn').attr('disabled', false)</script>"; }
			exit();
		}
		
		$row_exp_hours = fetchArray($get_password_exp_hours);
		
		if($hours>=$row_exp_hours['hours'])
		{
			$newPass = GenPassword(8);
			
			if($row_mobile['whatsapp_enabled']=="0")
			{
				MsgSendNewPasswordManager($row_mobile['mobile_no'],$branch,$newPass);
				
				$update_new_pass = Qry($conn,"UPDATE rrpl_database.manager SET pass='".md5($newPass)."',last_pass='$timestamp' WHERE 
				branch='$branch'");
				
				if(!$update_new_pass)
				{
					errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
					echo "<script>
						alert('ERROR : While processing Request !');	
						window.location.href='./';
					</script>";
					exit();
				}
				
				$insert_login_log = Qry($conn,"INSERT INTO rrpl_database.log_login(username,loc_lat,loc_long,loc_error,action,timestamp) VALUES 
				('$user','','','','AUTO_PASSWORD_RESET','$timestamp')");
				
				if(!$insert_login_log)
				{
					// echo getMySQLError($conn);
					errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
					echo "<script>
						alert('ERROR : While processing Request !');	
						window.location.href='./';
					</script>";
					exit();
				}
				
				echo "<script>
					alert('$user : Your password has been expired. System Will Send You the new Password Shortly on Your Registered Mobile Number.');	
					window.location.href='./';
				</script>";
				exit();
			}
			else
			{	
				$msg_template="Your new password for manager($branch) login is: $newPass.\nRamanRoadways.";
				$wa_msg_func = SendWAMsg($conn,$row_mobile['mobile_no'],$msg_template);
			
				if($wa_msg_func=="1")
				{
					$update_new_pass = Qry($conn,"UPDATE rrpl_database.manager SET pass='".md5($newPass)."',last_pass='$timestamp' WHERE 
					branch='$branch'");
					
					if(!$update_new_pass)
					{
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						echo "<script>
							alert('ERROR : While processing Request !');	
							window.location.href='./';
						</script>";
						exit();
					}
					
					$insert_login_log = Qry($conn,"INSERT INTO rrpl_database.log_login(username,loc_lat,loc_long,loc_error,action,timestamp) VALUES 
					('$user','','','','AUTO_PASSWORD_RESET','$timestamp')");
					
					if(!$insert_login_log)
					{
						// echo getMySQLError($conn);
						errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
						echo "<script>
							alert('ERROR : While processing Request !');	
							window.location.href='./';
						</script>";
						exit();
					}
					
					echo "<script>
						alert('$user : Your password has been expired. System Will Send You the new Password Shortly on Your Registered Mobile Number.');	
						window.location.href='./';
					</script>";
					exit();
				}
			}
		}
		
		$insert_login_log = Qry($conn,"INSERT INTO rrpl_database.log_login(username,loc_lat,loc_long,loc_error,action,timestamp) VALUES 
			('$user','','','','LOGIN','$timestamp')");
			
			if(!$insert_login_log)
			{
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
				echo "<script>
					alert('ERROR : While processing Request !');	
					window.location.href='./';
				</script>";
				exit();
			}
			
		$_SESSION['rrpl_ship_manager']=$branch."_MANAGER";		
		
		echo "<script>
				window.location.href='./manager_approval_payment_list.php';
			</script>";
		exit();
	
?>