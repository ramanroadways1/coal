<?php
include_once "header_manager.php";

if(isset($_SESSION['rrpl_ship_manager']))
{
	echo "<script>
		window.location.href='./manager_approval_payment_list.php';
	</script>";
	exit();
}
?>

<script type="text/javascript">
$(document).ready(function (e) {
	$("#managerForm").on('submit',(function(e) {
	$("#loadicon").show();
		e.preventDefault();
		$.ajax({
        	url: "./login_validate_manager.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#result").html(data);
				$("#loadicon").hide();
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

function ShowPassFunc(){
	$('#password1').attr('type','text');
	$('#ShowBtn').hide();
	$('#HideBtn').show();
}
function HidePassFunc(){
	$('#password1').attr('type','password');
	$('#HideBtn').hide();
	$('#ShowBtn').show();
}
</script>

<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Manager Approval Log in :
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
	<form id="managerForm" method="POST" autocomplete="off">
		<div class="row">
		
		<div class="form-group col-md-12">
		</div>
				
				<div class="form-group col-md-4 col-md-offset-3">
				
				<div class="form-group col-md-12">
                       <label>Username <font color="red">*</font></label>
                        <input oninput="this.value=this.value.replace(/[^0-9]/,'')" maxlength="10" type="text" class="form-control" name="username" required />
                 </div>
				 
				 <div class="form-group col-md-12">
                       <label>Password <font color="red">*</font> 
					   &nbsp; &nbsp; <span id="ShowBtn" onclick="ShowPassFunc()" style="">show Password</span>
						<span id="HideBtn" style="display:none" onclick="HidePassFunc()">hide Password</span></label>
                       <input id="password1" type="password" class="form-control" name="password" required />
                 </div>
					
					<div class="form-group col-md-12">
					<label><a id="password_link" style="color:" href="#" onclick="ResetPassword()">Forgot Password ?</a></label>		
			</div>
					
				<div class="form-group col-md-12" style="color:red" id="result">
				</div>
	
				<div class="form-group col-md-12">   
					<button type="submit" name="login2" class="btn btn-lg btn-primary btn-block btn-lg">Log in</button>
                 </div> 
                
				</div> 
		</div>
      </form>

</div>

		</div>
			</div> 
          </div>
        </div>       
    </section>
	
<script type="text/javascript">
function ResetPassword()
{
	var username = $('#username1').val();
	
	if(username=='')
	{
		alert('Enter username first !');
	}
	else
	{
		$("#loadicon").show();
		$('#password_link').attr('disabled',true);
			jQuery.ajax({
				url: "./reset_password.php",
				data: 'username=' + username,
				type: "POST",
				success: function(data) {
					$("#result").html(data);
				},
			error: function() {}
		});
	}	
}
</script>
	
<?php
include "footer.php";
?>
