<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Upload pending BE :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	 </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
			
<script type="text/javascript">
$(document).ready(function (e) {
$("#AddForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_pending_be.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>			

<div id="result_form"></div> 		
			
<div class="row">
 
	<div class="form-group col-md-12 table-responsive">
	
        <table id="myTable" class="table table-bordered table-striped" style="font-size:13px;">
          <tr>
				<th>#</th>
				<th>Shipment_No</th>
				<th>ShipName</th>
				<th>Voyage_No</th>
				<th>BE_Number</th>
				<th>BE_Date</th>
				<th>Upload</th>
			</tr>
          
            <?php
              $sql = Qry($conn,"SELECT b.id,b.be_no,b.be_date,b.unq_id,v.name as vessel_name,s.voyage_no 
			  FROM be_data AS b 
			  LEFT JOIN shipment AS s ON s.unq_id=b.unq_id 
			  LEFT JOIN vessel_name AS v ON v.id=s.vessel_name 
			  WHERE b.branch='$branch' AND b.be_copy='' ORDER BY b.id ASC");

             if(!$sql){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
					<td colspan='7'><b>NO RESULT FOUND..</b></td>
				</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
                echo 
                "<tr>
				  <td>$sn</td>
				  <td>$row[unq_id]</td>
				  <td>$row[vessel_name]</td>
				  <td>$row[voyage_no]</td>
				  <td>$row[be_no]</td>
				  <td>".date("d-m-y",strtotime($row['be_date']))."</td>
				  <td>
				  <input type='hidden' value='$row[vessel_name]' id='shipname$row[id]'>
				  <input type='hidden' value='$row[be_no]' id='be_no_$row[id]'>
				  <input type='hidden' value='$row[be_date]' id='be_date_$row[id]'>
				  <button type='button' id='RcvButton$row[id]' onclick=RcvModal('$row[id]','$row[unq_id]') 
				  class='btn btn-sm bg-green'>Upload</button></td>
				 </tr>
				";
              }
			}
            ?>
        </table>
      </div>

  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="bg-primary modal-header">
        <h5 class="modal-title">Upload BE Copy : <span id="ship_name"></span></h5>
      </div>
	<form id="AddForm" autocomplete="off">
      <div class="modal-body">
		 <div class="row">
		 
			<div class="form-group col-md-4">
				<label>BE Number <font color="red">*</font></label>
				<input type="text" name="be_no" id="be_no_modal" class="form-control" readonly required>
			</div>
			
			<div class="form-group col-md-4">
				<label>BE Date <font color="red">*</font></label>
				<input type="text" name="be_no" id="be_date_modal" class="form-control" readonly required>
			</div>
			
			<div class="form-group col-md-4">
				<label>BE Copy <font color="red">*</font></label>
				<input type="file" accept="application/pdf" class="form-control" name="be_copy" required />
			</div>
			
		</div>
			<input type="hidden" id="be_id" name="be_id">	
			<input type="hidden" id="unq_id" name="unq_id">	
	  </div>

      <div class="modal-footer">
        <button type="submit" id="submit_button" class="btn btn-primary">Upload</button>
        <button type="button" id="close_modal_button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	
	
<script>	
function RcvModal(id,unq_id)
{
	var name = $('#shipname'+id).val();
	var be_no = $('#be_no_'+id).val();
	var be_date = $('#be_date_'+id).val();
	$('#be_id').val(id);
	$('#unq_id').val(unq_id);
	$('#be_no_modal').val(be_no);
	$('#be_date_modal').val(be_date);
	$('#ship_name').html(name+" : "+be_no);
	$('#ModalButton')[0].click();
}
</script>
	
<?php
include "footer.php";
?>