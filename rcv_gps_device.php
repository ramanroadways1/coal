<?php
require_once("connect.php");

$id=escapeString($conn,($_POST['id']));
$timestamp = date("Y-m-d H:i:s");

echo "<script>
	$('#gps_btn_$id').attr('disabled',true);
</script>";

$chk_gps = Qry($conn,"SELECT frno,tno FROM gps_device_record WHERE id='$id'");
	
	if(!$chk_gps){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_gps)==0){
		echo "<script>
			alert('Device record not found !');
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
$row = fetchArray($chk_gps);
	
$chk_gps_entry = Qry($conn,"SELECT id from gps_deposit_return WHERE fm_no='$row[frno]'");
	
	if(!$chk_gps_entry){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($chk_gps_entry)>0){
		echo "<script>
			alert('Duplicate record found !');
			$('#loadicon').hide();
		</script>";
		exit();
	}


$insert = Qry($conn,"INSERT INTO gps_deposit_return (fm_no,amount,status,branch,branch_user,timestamp) VALUES ('$row[frno]','3000',
'1','$branch','$branch_sub_user','$timestamp')");
		
	if(!$insert){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	echo "<script>
		alert('OK : SUCCESS');
		$('#gps_btn_$id').attr('disabled',true);
		$('#gps_btn_$id').html('Received');
		$('#loadicon').hide();
	</script>";		
?>