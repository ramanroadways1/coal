<?php
require_once("./connect.php");

$lrid = escapeString($conn,$_POST['id']);

$GetLr=Qry($conn,"SELECT l.id,l.lrno,l.main_lr_id,l.tno,l.lr_type,l.lr_date,l.from_loc,l.from_pincode,l.to_loc,l.to_pincode,l.approx_distance,l.charge_wt,
l.do_no,l.boe_date,l.item,l.hsn_code,l.goods_desc,l.goods_value,l.igst_rate,l.total_value,l.eway_bill,loc1.name as from_station,
loc2.name as to_station,con1.name as consignor,con1.gst as con1_gst,con2.name as consignee,con2.gst as con2_gst,mt.mo1 as owner_mobile,
ot.driver_code as ot_driver_code,fm.driver,mt_driver.mo1 as market_veh_driver_mobile
FROM lr_entry AS l 
LEFT OUTER JOIN rrpl_database.station as loc1 ON loc1.id = l.from_loc
LEFT OUTER JOIN rrpl_database.station as loc2 ON loc2.id = l.to_loc 
LEFT OUTER JOIN rrpl_database.consignor as con1 ON con1.id = l.con1 
LEFT OUTER JOIN rrpl_database.consignee as con2 ON con2.id = l.con2 
LEFT OUTER JOIN rrpl_database.mk_truck as mt ON mt.tno = l.tno 
LEFT OUTER JOIN freight_memo as fm ON fm.lr_ids = l.id 
LEFT OUTER JOIN rrpl_database.mk_driver as mt_driver ON mt_driver.id = fm.driver 
LEFT OUTER JOIN dairy.own_truck as ot ON ot.tno = l.tno 
WHERE l.id='$lrid'");

if(!$GetLr){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($GetLr)==0)
{
	echo "<script>
		alert('Error: Invalid LR Number.');
		$('#loadicon').hide();
	</script>";
	exit();
}

$rowLR=fetchArray($GetLr);

$owner_mobile_no="";
$driver_mobile_no="";

if($rowLR['lr_type']=='MARKET')
{
	$owner_mobile_no = $rowLR['owner_mobile'];
	$driver_mobile_no = $rowLR['market_veh_driver_mobile'];
}
else
{ 
	$get_active_driver_mobile = Qry($conn,"SELECT mobile FROM dairy.driver WHERE code='$rowLR[ot_driver_code]'");

	if(!$get_active_driver_mobile){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}
	
	$row_ot_driver = fetchArray($get_active_driver_mobile);
	
	$driver_mobile_no = $row_ot_driver['mobile'];
}

$IgstAmount = round($rowLR['total_value']-$rowLR['goods_value']);

if($IgstAmount<=0)
{
	echo "<script>
		alert('Error: IGST Amount is Invalid.');
		$('#loadicon').hide();
	</script>";
	exit();
}

$supplyType="I"; 
$subSupplyType="2";
$subSupplyDesc=" ";
$docType="BOE";
$docNo=$rowLR['do_no'];
$docDate=date("d/m/Y",strtotime($rowLR['boe_date']));
$TransType="1";
$fromGstin="URP";
$fromTrdName=$rowLR['consignor'];
$fromAddr1="";
$fromAddr2="";
$fromPlace=$rowLR['from_station'];
$fromPincode=$rowLR['from_pincode'];
$actFromStateCode="24";
$fromStateCode="99";
$dispatchFromGSTIN="URP";
$dispatchFromTradeName=$rowLR['consignor'];
$toGstin=$rowLR['con2_gst'];
$toTrdName=$rowLR['consignee'];
$toAddr1=$rowLR['to_station'];
$toAddr2=$rowLR['to_station'];
$toCity=$rowLR['to_station'];
$toPincode=$rowLR['to_pincode'];
$actToStateCode="08";
$toStateCode="08";
$shipToGSTIN=$rowLR['con2_gst'];
$shipToTradeName=$rowLR['consignee'];
$Others="0";
$totalValue="";
$Cgst="";
$Sgst="";
$Igst=$IgstAmount;
$cessValue="";
$cessNonAdvolValue="";
$TotalInvValue=$rowLR['total_value'];
$transporterId="24AAGCR0742P1Z5";
$transporterName="RAMAN ROADWAYS PRIVATE LIMITED";
$transDocNo=$rowLR['lrno'];
$transDocDate=date("d/m/Y",strtotime($rowLR['lr_date']));
$transMode="1";
$transDistance=$rowLR['approx_distance'];
$vehicleNo=$rowLR['tno'];
$vehicleType="R";
$productName=$rowLR['item'];
$productDesc=$rowLR['goods_desc'];
$hsnCode=$rowLR['hsn_code'];
$quantity=$rowLR['charge_wt'];
$qtyUnit="MTS";
$cgstRate=0;
$sgstRate=0;
$igstRate=$rowLR['igst_rate'];
$cessRate="0";
$cessNonAdvol="0";
$taxableAmount=$rowLR['goods_value'];

$data = array(
"supplyType"=>"$supplyType",
"subSupplyType"=>"$subSupplyType",
"subSupplyDesc"=>"$subSupplyDesc",
"docType"=>"$docType",
"docNo"=>"$docNo",
"docDate"=>"$docDate",
"fromGstin"=>"$fromGstin",
"fromTrdName"=>"$fromTrdName",
"fromAddr1"=>"$fromAddr1",
"fromAddr2"=>"$fromAddr2",
"fromPlace"=>"$fromPlace",
"fromPincode"=>$fromPincode,
"actFromStateCode"=>$actFromStateCode,
"fromStateCode"=>$fromStateCode,
"toGstin"=>"$toGstin",
"toTrdName"=>"$toTrdName",
"toAddr1"=>"$toAddr1",
"toAddr2"=>"$toAddr2",
"toPlace"=>"$toCity",
"toPincode"=>$toPincode,
"actToStateCode"=>$actToStateCode,
"toStateCode"=>$toStateCode,
"transactionType"=>$TransType,
"dispatchFromGSTIN"=>"$dispatchFromGSTIN",
"dispatchFromTradeName"=>"$dispatchFromTradeName",
"shipToGSTIN"=>"$shipToGSTIN",
"shipToTradeName"=>"$shipToTradeName",
"otherValue"=>$Others,
"totalValue"=>$totalValue,
"cgstValue"=>$Cgst,
"sgstValue"=>$Sgst,
"igstValue"=>$Igst,
"cessValue"=>$cessValue,
"cessNonAdvolValue"=>$cessNonAdvolValue,
"totInvValue"=>$TotalInvValue,
"transporterId"=>"$transporterId",
"transporterName"=>"$transporterName",
"transDocNo"=>"$transDocNo",
"transMode"=>"$transMode",
"transDistance"=>"$transDistance",
"transDocDate"=>"$transDocDate",
"vehicleNo"=>"$vehicleNo",
"vehicleType"=>"$vehicleType",
"itemList"=>array([
'productName'=>$productName,
'productDesc'=>$productDesc,
'hsnCode'=>$hsnCode,
'quantity'=>$quantity,
'qtyUnit'=>$qtyUnit,
'cgstRate'=>$cgstRate,
'sgstRate'=>$sgstRate,
'igstRate'=>$igstRate,
'cessRate'=>$cessRate,
'cessNonAdvol'=>$cessNonAdvol,
'taxableAmount'=>$taxableAmount,
])
);

$timestamp=date("Y-m-d H:i:s");

$payload = json_encode($data);

// echo $payload;

$asp_id = $tax_pro_asp_id;
$asp_Passwd = $tax_pro_asp_password;
$myGSTIn = $rrpl_gst_no;
$UserName = $ewb_rrpl_username;
$PassWord = $ewb_rrpl_password;
	
	$get_token=Qry($conn,"SELECT token FROM api_token WHERE company='RRPL' ORDER BY id DESC LIMIT 1");
	if(!$get_token){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}

	$row_token=fetchArray($get_token);
	$authToken=$row_token['token'];
	
	$ch = curl_init("$tax_pro_url/v1.03/dec/ewayapi?action=GENEWAYBILL&aspid=$asp_id&password=$asp_Passwd&gstin=$myGSTIn&username=$UserName&authtoken=$authToken");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLINFO_HEADER_OUT, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Content-Length: ' . strlen($payload)));
	
	$result = curl_exec($ch);
	$err1 = curl_error($ch);
	curl_close($ch);
	
	$EwayArray = json_decode($result, true);
	
	if($err1)
	{
		$insert_error=Qry($conn,"INSERT INTO eway_bill_error(company,error_desc,lrno,branch,timestamp) VALUES 
		('RRPL','cURL Error : $err1','COAL','$branch','$timestamp')");
		
		if(!$insert_error){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","../");
			exit();
		}
				
		echo "<script>
			alert('Error: While Generating Eway-Bill.');
			$('#loadicon').hide();
		</script>";
		exit();
	}
		
	if(@$EwayArray['error'])
	{
		$ErrorMsg=@$EwayArray['error']['message'];
		$Error=@$EwayArray['error']['error_cd'];
		
	if($EwayArray['error']['error_cd']=="GSP102")
	{
		$token_gen_url = "$tax_pro_url/v1.03/dec/auth?action=ACCESSTOKEN&aspid=$asp_id&password=$asp_Passwd&gstin=$myGSTIn&username=$UserName&ewbpwd=$PassWord";
		// echo $token_gen_url;
		$curl_token = curl_init();
		curl_setopt_array($curl_token, array(
		CURLOPT_URL => $token_gen_url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		));

		$response_gen11 = curl_exec($curl_token);
		$err = curl_error($curl_token);
		curl_close($curl_token);
				
		if($err)
		{
			echo "cURL Error #:" . $err; 
			
			$insert_error=Qry($conn,"INSERT INTO eway_bill_error(company,error_desc,lrno,branch,branch_user,timestamp) VALUES 
			('RRPL','cURL Error : $err','$transDocNo','$branch','$branch_sub_user','$timestamp')");
			
			if(!$insert_error){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			}
	
			echo "<script>
				alert('Unable to Get Token contact System Admin.');
				$('#loadicon').hide();
			</script>";
			exit();
		}
		
		$response_gen = json_decode($response_gen11, true);
		// $Token = $response_gen['authtoken'];
		$authToken_new = $response_gen['authtoken'];
		
			if(@$response_gen['error'])
			{
				$error_msg_token = escapeString($conn,$response_gen['error']['message']);
							
				$insert_error = Qry($conn,"INSERT INTO eway_bill_error(company,error_desc,lrno,msg,branch,branch_user,timestamp) VALUES 
				('RRPL','".escapeString($conn,$response_gen11)."','$transDocNo','cURL Error : $error_msg_token','$branch','$branch_user','$timestamp')");
							
				if(!$insert_error){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
				}
			}
			else
			{
				if($authToken_new=='')
				{
					echo "<script>
						alert('Error : Empty token. Please try once again..');
						$('#loadicon').hide();
					</script>";
					exit();
				}
						
					$insert_token=Qry($conn,"INSERT INTO api_token (token,company,user,branch_user,timestamp) VALUES 
					('$authToken_new','RRPL','$branch','$branch_sub_user','$timestamp')");
					
					if(!$insert_token){
						ScriptError($conn,$page_name,__LINE__);
						exit();
					}
				
				echo "<script>
						alert('Token generated. Please try again !!');
						$('#loadicon').hide();
					</script>";
				exit();
			}		
		}
		else
		{
			$insert_error=Qry($conn,"INSERT INTO eway_bill_error(company,error_desc,lrno,msg,branch,branch_user,timestamp) VALUES 
			('RRPL','".escapeString($conn,$result)."','$transDocNo','".escapeString($conn,$ErrorMsg)."','$_SESSION[rrpl_ship]','$branch_sub_user','$timestamp')");
			
			if(!$insert_error){
				ScriptError($conn,$page_name,__LINE__);
				exit();
			}
			
			echo "
				<font color='red'><b>$ErrorMsg</b></font>
				<script>
				alert('Error: While Generating e-Way Bill.');
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
		
		$EWayBillNo=$EwayArray['ewayBillNo'];
		$EWayDate=$EwayArray['ewayBillDate'];
		$EWayValidity=$EwayArray['validUpto'];
		
	if($EWayBillNo=='')
	{
		echo "<script>
				alert('Error: Eway Bill Number is NULL. Please try Again.');
				$('#loadicon').hide();
			</script>";
		exit();
	}		
		
	$InsertLog=Qry($conn,"INSERT INTO eway_bill_log(eway_bill_no,lrno,gen_on,exp_on,branch,branch_user,timestamp) VALUES 
	('$EWayBillNo','$rowLR[lrno]','$EWayDate','$EWayValidity','$branch','$branch_sub_user','$timestamp')");
	
	if(!$InsertLog){
		ScriptError($conn,$page_name,__LINE__);
		exit();
	}
			
	 $curl = curl_init();
	 curl_setopt_array($curl, array(
	  CURLOPT_URL => "$tax_pro_url/v1.03/dec/ewayapi?action=GetEwayBill&aspid=$asp_id&password=$asp_Passwd&gstin=24AAGCR0742P1Z5&username=RRPL@AHD_API_111&authtoken=$authToken&ewbNo=$EWayBillNo",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	));

		$response = curl_exec($curl);
		// echo "<br>Fetch : ".$response;
		$err = curl_error($curl);
		curl_close($curl);
		if($err)
		{ echo "cURL Error #:" . $err; }
	
		$response = json_decode($response, true);
		
		$genMode=$response['genMode'];
		$userGstin=$response['userGstin'];
		$supplyType=$response['supplyType'];
		$subSupplyType=$response['subSupplyType'];
		$docType=$response['docType'];
		$docNo=$response['docNo'];
		$docDate=$response['docDate'];
		$fromGstin=$response['fromGstin'];
		$fromTrdName=$response['fromTrdName'];
		$fromAddr1=$response['fromAddr1'];
		$fromAddr2=$response['fromAddr2'];
		$fromPlace=$response['fromPlace'];
		$fromPincode=$response['fromPincode'];
		$fromStateCode=$response['fromStateCode'];
		$toGstin=$response['toGstin'];
		$toTrdName=$response['toTrdName'];
		$toAddr1=$response['toAddr1'];
		$toAddr2=$response['toAddr2'];
		$toPlace=$response['toPlace'];
		$toPincode=$response['toPincode'];
		$toStateCode=$response['toStateCode'];
		$totalValue=$response['totalValue'];
		$totInvValue=$response['totInvValue'];
		$cgstValue=$response['cgstValue'];
		$sgstValue=$response['sgstValue'];
		$igstValue=$response['igstValue'];
		$cessValue=$response['cessValue'];
		$transporterId=$response['transporterId'];
		$transporterName=$response['transporterName'];
		$status=$response['status'];
		$actualDist=$response['actualDist'];
		$noValidDays=$response['noValidDays'];
		$validUpto=$response['validUpto'];
		$extendedTimes=$response['extendedTimes'];
		$rejectStatus=$response['rejectStatus'];
		$vehicleType=$response['vehicleType'];
		$actFromStateCode=$response['actFromStateCode'];
		$actToStateCode=$response['actToStateCode'];
		$transactionType=$response['transactionType'];
		$otherValue=$response['otherValue'];
		$cessNonAdvolValue=$response['cessNonAdvolValue'];

$ewb_exp_db = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$EWayValidity)));
$ewb_date_db = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$EWayDate)));
		
		foreach ($response['itemList'] as $One_data)
		{
			$itemNo=$One_data['itemNo'];
			$productId=$One_data['productId'];
			$productName=$One_data['productName'];
			$productDesc=$One_data['productDesc'];
			$hsnCode=$One_data['hsnCode'];
			$quantity=$One_data['quantity'];
			$qtyUnit=$One_data['qtyUnit'];
			$cgstRate=$One_data['cgstRate'];
			$sgstRate=$One_data['sgstRate'];
			$igstRate=$One_data['igstRate'];
			$cessRate=$One_data['cessRate'];
			$cessNonAdvol=$One_data['cessNonAdvol'];
			$taxableAmount=$One_data['taxableAmount'];
		}
		
		foreach ($response['VehiclListDetails'] as $Two_Data)
		{
			$updMode=$Two_Data['updMode'];
			$vehicleNo=$Two_Data['vehicleNo'];
			$fromPlace_v=$Two_Data['fromPlace'];
			$fromState=$Two_Data['fromState'];
			$tripshtNo=$Two_Data['tripshtNo'];
			$userGSTINTransin=$Two_Data['userGSTINTransin'];
			$enteredDate=$Two_Data['enteredDate'];
			$transMode=$Two_Data['transMode'];
			$transDocNo=$Two_Data['transDocNo'];
			$transDocDate=$Two_Data['transDocDate'];
			$groupNo=$Two_Data['groupNo'];
		}

StartCommit($conn);
$flag = true;	
		
		$insert_WayBill=Qry($conn,"INSERT INTO eway_bill(ewbNo,branch,ewayBillDate,genMode,userGstin,supplyType,subSupplyType,
		docType,docNo,docDate,fromGstin,fromTrdName,fromAddr1,fromAddr2,fromPlace,fromPincode,fromStateCode,toGstin,toTrdName,
		toAddr1,toAddr2,toPlace,toPincode,toStateCode,totalValue,totInvValue,cgstValue,sgstValue,igstValue,cessValue,
		transporterId,transporterName,status,actualDist,noValidDays,validUpto,extendedTimes,rejectStatus,vehicleType,
		actFromStateCode,actToStateCode,transactionType,otherValue,cessNonAdvolValue,itemNo,productId,productName,productDesc,
		hsnCode,quantity,qtyUnit,cgstRate,sgstRate,igstRate,cessRate,cessNonAdvol,taxableAmount,updMode,vehicleNo,fromPlace_v,
		fromState,tripshtNo,userGSTINTransin,enteredDate,transMode,transDocNo,transDocDate,groupNo,timestamp) VALUES 
		('$EWayBillNo','$branch','$EWayDate','$genMode','$userGstin','$supplyType','$subSupplyType','$docType','$docNo','$docDate',
		'$fromGstin','$fromTrdName','$fromAddr1','$fromAddr2','$fromPlace','$fromPincode','$fromStateCode','$toGstin',
		'$toTrdName','$toAddr1','$toAddr2','$toPlace','$toPincode','$toStateCode','$totalValue','$totInvValue','$cgstValue',
		'$sgstValue','$igstValue','$cessValue','$transporterId','$transporterName','$status','$actualDist','$noValidDays',
		'$validUpto','$extendedTimes','$rejectStatus','$vehicleType','$actFromStateCode','$actToStateCode','$transactionType',
		'$otherValue','$cessNonAdvolValue','$itemNo','$productId','$productName','$productDesc','$hsnCode','$quantity',
		'$qtyUnit','$cgstRate','$sgstRate','$igstRate','$cessRate','$cessNonAdvol','$taxableAmount','$updMode','$vehicleNo',
		'$fromPlace_v','$fromState','$tripshtNo','$userGSTINTransin','$enteredDate','$transMode','$transDocNo','$transDocDate',
		'$groupNo','$timestamp')");
		
		if(!$insert_WayBill){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$updateLR=Qry($conn,"UPDATE lr_entry SET eway_bill='1' WHERE id='$lrid'");
		
		if(!$updateLR){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}

$insert_ewb_data = Qry($conn,"INSERT INTO rrpl_database._eway_bill_lr_wise(lrno,lr_id,branch,ewb_no,ewb_flag_desc,by_pass,ewb_copy,
ewayBillDate,ewb_date,ewb_expiry,ewb_extended,docNo,docDate,fromPincode,totInvValue,totalValue,toPincode,timestamp) VALUES ('$transDocNo',
'$rowLR[main_lr_id]','$branch','$EWayBillNo','','','','$ewb_date_db','$ewb_date_db','$ewb_exp_db','','$docNo','$docDate','$fromPincode',
'$totInvValue','$totalValue','$toPincode','$timestamp')");

if(!$insert_ewb_data){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$ewb_id = getInsertID($conn);

$insert_ewb_data2 = Qry($conn,"INSERT INTO rrpl_database._eway_bill_validity(owner_mobile,driver_mobile,lrno,truck_no,lr_date,from_loc,
to_loc,consignor,consignee,lr_id,branch,ewb_no,ewb_flag_desc,ewb_id,ewb_date,ewb_expiry,timestamp) VALUES 
('$owner_mobile_no','$driver_mobile_no','$transDocNo','$rowLR[tno]','$rowLR[lr_date]','$rowLR[from_station]','$rowLR[to_station]',
'$rowLR[consignor]','$rowLR[consignee]','$rowLR[main_lr_id]','$branch','$EWayBillNo','','$ewb_id','$ewb_date_db','$ewb_exp_db','$timestamp')");

if(!$insert_ewb_data2){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script> 
			alert('E-Way Bill: $EWayBillNo. generated successfully.');
			$('#GenBtn$lrid').attr('disabled',true);
			$('#PrintBtn$lrid').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
	</script>";
	exit();
}
?>