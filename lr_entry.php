<?php
include_once "header.php";

$get_data = Qry($conn,"SELECT * FROM fix_data WHERE branch='$branch'");
				 
if(!$get_data){
	ScriptError($conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_data)==0)
{
	echo "<script>
		alert('Consignor or location data not found !');
		window.location.href='./';
	</script>";
	exit();
}

$row_fix = fetchArray($get_data);
				 
$from_location = $row_fix['from_loc'];
$from_location_pin = $row_fix['from_pincode'];
$from_location_id = $row_fix['from_loc_id'];
	
$con1_readonly = $row_fix['consignor_readonly'];
$con1_name = $row_fix['consignor'];
$con1_gst = $row_fix['consignor_gst'];
$con1_id = $row_fix['consignor_id'];

?>

<script type="text/javascript">
	$(function() {
		$("#from_loc").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/location.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#from_loc').val(ui.item.value);   
               $('#from_loc_id').val(ui.item.id);     
               // $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Station does not exists.');
			$("#from_loc").val('');
			$("#from_loc").focus();
			$("#from_loc_id").val('');
			// $("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
  
 	$(function() {
		$("#to_loc").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/location.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#to_loc').val(ui.item.value);   
               $('#to_loc_id').val(ui.item.id);     
               // $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Station does not exists.');
			$("#to_loc").val('');
			$("#to_loc").focus();
			$("#to_loc_id").val('');
			// $("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
		
 	$(function() {
		$("#con1").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/con1.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#con1').val(ui.item.value);   
               $('#con1_id').val(ui.item.id);     
               $('#con1_gst').val(ui.item.gst);     
               // $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Consignor does not exists.');
			$("#con1").val('');
			$("#con1").focus();
			$("#con1_id").val('');
			$("#con1_gst").val('');
			// $("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
		
		
 	$(function() {
		$("#con2").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/con2.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#con2').val(ui.item.value);   
               $('#con2_id').val(ui.item.id);     
               $('#con2_gst').val(ui.item.gst);     
               // $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Consignee does not exists.');
			$("#con2").val('');
			$("#con2").focus();
			$("#con2_id").val('');
			$("#con2_gst").val('');
			// $("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});		
  
   	$(function() {
		$("#market_tno").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/get_market_vehicle.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#market_tno').val(ui.item.value);   
               $('#market_tno_id').val(ui.item.id);     
               $('#market_wheeler').val(ui.item.wheeler);     
               // $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle does not exists.');
			$("#market_tno").val('');
			$("#market_tno").focus();
			$("#market_tno_id").val('');
			$("#market_wheeler").val('');
			// $("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});	
		
   	$(function() {
		$("#own_tno").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/get_own_vehicle.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#own_tno').val(ui.item.value);   
               $('#own_tno_id').val(ui.item.id);  
				$('#own_wheeler').val(ui.item.wheeler);			   
               // $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle does not exists.');
			$("#own_tno").val('');
			$("#own_tno").focus();
			$("#own_wheeler").focus();
			$("#own_tno_id").val('');
			// $("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});	
</script>	

<div class="content-wrapper">
    <section class="content-header">
       <h4>
		LR ENTRY :
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
         <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#create_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_lr_entry.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>	

<script>
  $(function() {  
      $("#do_no").autocomplete({
			source: function(request, response) { 
			if(document.getElementById('shipment_no2').value!=''){
                 $.ajax({
                  url: "./autofill/get_boe_no.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   shipment_no: document.getElementById('shipment_no2').value
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              } else {
                alert('Shipment not found !');
				$("#do_no").val('');
				$("#do_no_id").val('');
				$("#boe_date").val('');
			 }

              },
              select: function (event, ui) { 
               $('#do_no').val(ui.item.value);   
               $('#do_no_id').val(ui.item.id);   
               $('#boe_date').val(ui.item.be_date);   
				return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
				$("#do_no").val('');
				$("#do_no_id").val('');
				$("#boe_date").val('');
                $(event.target).focus();
                alert('Bill of entry does not exist !'); 
              } 
              },
			}); 
      }); 
	  
 </script> 		

<div id="result_form"></div> 
   
	<form id="Form1" autocomplete="off">

         <div class="row">
			
			 <div class="form-group col-md-12">
		
			
				 <?php
				$getLastLR=Qry($conn,"SELECT lrno FROM rrpl_database.lr_check WHERE branch='$branch' ORDER BY id DESC LIMIT 1");
				 if(!$getLastLR){
					ScriptError($conn,$page_name,__LINE__);
					exit();
				}

				 if(numRows($getLastLR)==0)
				 {
					 $last_lrno="";
					 $last_lrno_new="";
				 }
				 else
				 {
					 $row_last_lr=fetchArray($getLastLR);
					 $last_lrno=$row_last_lr['lrno'];
					 $last_lrno_new=++$row_last_lr['lrno'];
				 }
				 ?>
				 
				 <div class="form-group col-md-3">
                       <label>Last LR No. <font color="red">* </font></label>
                        <input value="<?php echo $last_lrno; ?>" type="text" readonly class="form-control" required />
                 </div>
				 
                 <div class="form-group col-md-3">
                     <label>LR No. <font color="red">* </font></label>
                     <input value="<?php echo $last_lrno_new; ?>" type="number" class="form-control" id="lr_no" name="lr_no" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                       <label>Gen. e-Way Bill <font color="red">*</font></label>
                       <select name="gen_way_bill" class="form-control" required>
							<option value="0">NO</option>
					   </select>
                 </div>
				 
				 <div class="form-group col-md-3">
                       <label>Company <font color="red">*</font></label>
                       <input type="text" value="RRPL" readonly name="company" class="form-control" required>
				</div>
				 
				 <div class="form-group col-md-3">
                       <label>Vessel Name <font color="red">*</font></label>
                       <select id="vessel_name_sel_box" style="font-size:13px;" onchange="FetchBoeDate(this.value);$('#do_no').val('')" name="shipment_id" class="form-control" required>
							<option class="vessel_name_sel_box_opt" value="">---SELECT---</option>
							<?php
							$fetch_shipment=Qry($conn,"SELECT s.id,s.unq_id,s.hsn_code,s.voyage_no,v.name,boe.be_date 
							FROM shipment as s 
							LEFT OUTER JOIN vessel_name AS v ON v.id=s.vessel_name 
							LEFT OUTER JOIN be_data AS boe ON boe.unq_id=s.unq_id 
							WHERE s.complete!='1' AND s.branch='$branch' AND bal_weight>0 GROUP by s.id");
							
							if(!$fetch_shipment){
								ScriptError($conn,$page_name,__LINE__);
								exit();
							}
				
							if(numRows($fetch_shipment)>0)
							{
								while($row_shipment=fetchArray($fetch_shipment))
								{
									echo "<option class='vessel_name_sel_box_opt' id='vessel_name_sel_box_opt_$row_shipment[id]' value='$row_shipment[id]_$row_shipment[unq_id]_$row_shipment[be_date]_$row_shipment[hsn_code]'>$row_shipment[name]-$row_shipment[voyage_no] ($row_shipment[unq_id])</option>";
								}
							}
							?>
					   </select>
                 </div>
				 
				 <div id="result2"></div>
				 
				 <input type="hidden" id="shipment_no2">
				 
				 <script>
				 function FetchBoeDate(shipment)
				 {
					 $('#to_loc_sel').val('');
					 
					 var item_array = shipment.split("_");
					 var shipment_id = item_array[0];
					 var shipment_no = item_array[1];
					 var date1 = item_array[2];
					 var hsn_code = item_array[3];
					 
					 $('#shipment_no2').val(shipment_no);
					 $('#hsn_code').val(hsn_code);
					 
					 if(date1=='')
					 {
						$('#boe_date').val('');
					 }
					 else
					 {
						$('#boe_date').val(date1);
					 }
					 
					 $("#loadicon").show();
						jQuery.ajax({
						url: "get_ship_destinations.php",
						data: 'shipment_no=' + shipment_no + '&id=' + shipment_id,
						type: "POST",
						success: function(data) {
							$("#to_loc_sel").html(data);
						},
						error: function() {}
					});
				 }
				 </script>
				 
				 <div class="form-group col-md-3">
                       <label>Market/Own Truck <font color="red">*</font></label>
                       <select onchange="LRType(this.value)" id="vehicle_type_selection" name="lr_type" class="form-control" required>
							<option class="veh_type_sel_opt" value="">---SELECT---</option>
							<option class="veh_type_sel_opt" id="veh_type_sel_opt_MARKET" value="MARKET">MARKET TRUCK</option>
							<option class="veh_type_sel_opt" id="veh_type_sel_opt_OWN" value="OWN">OWN TRUCK</option>
					   </select>
                 </div>
				 
				 <script>
				 function LRType(elem)
				 {
					 $('#market_tno').val(''); 
					 $('#own_tno').val(''); 
					 
					 if(elem=='OWN')
					 {
						$('#market_truck_div').hide(); 
						$('#market_tno').attr('required',false); 
						
						$('#own_truck_div').show(); 
						$('#own_tno').attr('required',true); 
					 }
					 else
					 {
						$('#own_truck_div').hide(); 
						$('#own_tno').attr('required',false); 
						
						$('#market_truck_div').show(); 
						$('#market_tno').attr('required',true); 
					 }
				}
				 </script>
				 
				 <div class="form-group col-md-3" id="market_truck_div">
                       <label>Market Truck No <font color="red">*</font></label>
                       <input type="text" id="market_tno" class="form-control" name="market_tno" required />
                 </div>
				 
				 <input type="hidden" name="market_tno_id" id="market_tno_id">
				 <input type="hidden" name="own_tno_id" id="own_tno_id">
				 <input type="hidden" name="market_wheeler" id="market_wheeler">
				 <input type="hidden" name="own_wheeler" id="own_wheeler">
				 <input type="hidden" name="from_loc_id" id="from_loc_id" value="<?php echo $from_location_id; ?>">
				 <input type="hidden" name="to_loc_id" id="to_loc_id">
				 
				 <input type="hidden" value="<?php echo $con1_id; ?>" name="con1_id" id="con1_id">
				 <input type="hidden" value="1903" name="con2_id" id="con2_id">
				 
				 <div class="form-group col-md-3" id="own_truck_div" style="display:none">
                       <label>OWN Truck No <font color="red">*</font></label>
                       <input type="text" id="own_tno" class="form-control" name="own_tno" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                       <label>LR Date <font color="red">*</font></label>
                        <input type="date" id="lr_date" class="form-control" name="lr_date" required max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>From Station <font color="red">*</font></label>
                     <input type="text" value="<?php echo $from_location; ?>" readonly id="from_loc" class="form-control" name="from_loc" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>From PinCode <font color="red">*</font></label>
                     <input type="text" value="<?php echo $from_location_pin; ?>" readonly class="form-control" id="from_pincode" name="from_pincode" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>To Station <font color="red">*</font></label>
                     <select onchange="GetCodes(this.value)" id="to_loc_sel" class="form-control" name="to_loc" required>
						<option value="">--select destination--</option>
					 </select>
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>To PinCode <font color="red">*</font></label>
                     <input type="text" readonly class="form-control" id="to_pin_code" name="to_pin_code" required />
                 </div>
				 
				 <script>
				 function GetCodes(loc)
				 {
					 var shipment_no = $('#shipment_no2').val();
					 
					 if(shipment_no=='')
					 {
						 alert('Shipment not found..');
						 $('#to_loc_sel').val('');
					 }
					 else
					 {
						 if(loc!='')
						 {
							 $("#loadicon").show();
								jQuery.ajax({
								url: "get_location_codes.php",
								data: 'loc=' + loc + '&branch=' + '<?php echo $branch; ?>' + '&shipment_no=' + shipment_no,
								type: "POST",
								success: function(data) {
									$("#result_form").html(data);
								},
								error: function() {}
							});
						 }
						 else
						 {
							$('#to_pin_code').val(''); 
							$('#po_no').val('');
							$('#to_loc_id').val('');
							$('#approx_distance').val('');
						 }
					 }
				 }
				 </script>
				 
				 <div class="form-group col-md-3">
                     <label>Consignor <font color="red">*</font> <a onclick="ResetCon1()" href="#0">Reset</a></label>
                     <input type="text" id="con1" value="<?php echo $con1_name; ?>" <?php echo $con1_readonly; ?> class="form-control" name="con1" required />
                 </div>
				 
				 <script>
				 function ResetCon1()
				 {
					 $('#con1').val('');
					 $('#con1_id').val('');
					 $('#con1_gst').val('');
					 $('#con1').attr('readonly',false);
				 }
				 
				 function ResetCon2()
				 {
					 $('#con2').val('');
					 $('#con2_id').val('');
					 $('#con2_gst').val('');
					 $('#con2').attr('readonly',false);
				 }
				 </script>
				 
				  <div class="form-group col-md-3">
                     <label>Consignor GST <font color="red">*</font></label>
                     <input type="text" id="con1_gst" value="<?php echo $con1_gst; ?>" name="con1_gst" class="form-control" readonly required />
                 </div>
				 
				  <div class="form-group col-md-3">
                     <label>Consignee <font color="red">*</font> <a onclick="ResetCon2()" href="#0">Reset</a></label>
                     <input type="text" id="con2" readonly value="HINDUSTAN ZINC LIMITED - RAJASTHAN" class="form-control" name="con2" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Consignee GST <font color="red">*</font></label>
                     <input type="text" id="con2_gst" value="08AAACH7354K1ZB" name="con2_gst" class="form-control" name="" readonly required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Approx Distance <font color="red">*</font></label>
                     <input type="number" readonly class="form-control" id="approx_distance" name="approx_distance" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>P.O. <font color="red">*</font></label>
                     <input type="text" readonly class="form-control" id="po_no" name="po_no" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Allow Slip No. <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9,-]/,'')" type="text" class="form-control" name="allow_slip_no" required />
                 </div>
				 
				  <div class="form-group col-md-3">
                     <label>Trip No. <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-]/,'')" type="text" class="form-control" name="trip_no" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Actual Weight <font color="red">*</font></label>
                     <input type="number" min="1" step="any" class="form-control" name="actual_wt" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Charge Weight <font color="red">*</font></label>
                     <input type="number" step="any" oninput="ResetAll();" min="1" class="form-control" id="charge_wt" name="charge_wt" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>BOE No <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'');CheckShipment()" type="text" id="do_no" class="form-control" name="boe_no" required />
                 </div>
				 
				 <input type="hidden" id="do_no_id" name="do_no_id">
				 
				 <script>
				 function CheckShipment()
				 {
					 if($('#shipment_no2').val()=='') 
					 { 
						alert('select shipment first');
						$('#do_no').val('');	
					 }
				 }
				 </script>
				 
				 <div class="form-group col-md-3">
                     <label>BOE Date <font color="red">*</font></label>
                     <input type="date" readonly id="boe_date" class="form-control" name="boe_date" required max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Invoice No <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-]/,'')" type="text" id="inv_no" class="form-control" name="inv_no" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Shipment No <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9,-]/,'')" type="text" id="ship_no" class="form-control" name="ship_no" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Truck Type <font color="red">*</font></label>
                    <select name="t_type" id="t_type" class="form-control" required="required">
					<option value="">--Select Option--</option>
					<?php
					$get_t_type = Qry($conn,"SELECT title FROM rrpl_database.truck_type WHERE hide=0");
					if(numRows($get_t_type)>0)
					{
						while($row_t_type = fetchArray($get_t_type))
						{
							echo "<option value='$row_t_type[title]'>$row_t_type[title]</option>";	
						}
					}
					?>
				</select>
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Item <font color="red">*</font></label>
                     <select onchange="ItemHsn(this.value)" class="form-control" id="items" required>
						<!--<option value="">--Select Destination--</option>-->
						<option value="NON COKING COAL_33">NON COKING COAL</option>
					 </select>
                 </div>
				 
				 <input type="hidden" name="item_id" id="item_id">
				 <input type="hidden" name="items" id="item_name_val">
				 
				 <script>
				 function ItemHsn(item)
				 {
					 var item_array = item.split("_");
					 var item_name = item_array[0];
					 var item_id = item_array[1];
					 // var item_hsn_code = item_array[2];
					 
					// $('#hsn_code').val(item_hsn_code);
					$('#goods_desc').val(item_name);
					$('#item_name_val').val(item_name);
					$('#item_id').val(item_id);
				}
				 </script>
				 
				 <div class="form-group col-md-3">
                     <label>HSN Code <font color="red">*</font></label>
                     <input type="text" readonly id="hsn_code" class="form-control" name="hsn_code" required />
                 </div>
				 
				 <!--
				 <div class="form-group col-md-3">
                     <label>Articles <font color="red">*</font></label>
                     <input type="text" id="acticles" class="form-control" name="acticles" required />
                 </div>
				 -->
				 
				 <div class="form-group col-md-3">
                     <label>Goods/Taxable Value <font color="red">*</font></label>
                     <input type="number" min="1" oninput="CalInvVal(this.value);" id="goods_value" class="form-control" name="goods_value" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>IGST Rate <font color="red">*</font></label>
                     <input type="number" readonly value="5" id="igst_rate" class="form-control" name="igst_rate" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>IGST Value </label>
                     <input type="number" readonly id="igst_value" class="form-control" name="igst_value" required />
                 </div>
				 
				 <div class="form-group col-md-2">
                     <label>Total Value </label>
                     <input type="number" readonly id="total_inv_value" class="form-control" name="total_inv_value" required />
                 </div>
				 
				 <div class="form-group col-md-2">
                     <label>Bill. Rate</label>
                     <input value="0" readonly type="number" oninput="sum1();" min="" id="bill_rate" class="form-control" name="bill_rate" />
                 </div>
				 
				 <div class="form-group col-md-2">
                     <label>Bill. Amt</label>
                     <input value="0" readonly type="number" oninput="sum2();" min="" id="bill_amount" class="form-control" name="bill_amount" />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Loading Slip <font color="red">*</font></label>
                     <input type="file" accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" class="form-control" name="loading_slip[]" multiple="multiple" required />
                 </div>
				 
				<div class="form-group col-md-3">
                     <label>Goods Description <font color="red">*</font></label>
                     <textarea class="form-control" readonly id="goods_desc" name="goods_desc" required></textarea>
                 </div>
				 
				 <?php
				 if($branch=='MAGDALLA')
				 {
				?>
				<div class="form-group col-md-4">
                     <label>E-way bill number <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^0-9]/,'')" type="text" id="ewb_no" class="form-control" name="ewb_no" required />
                 </div>
				 
				 <div class="form-group col-md-4">
                     <label>&nbsp;</label>
					 <br />
                     <button type="button" id="ewb_verify_btn" class="btn btn-danger" onclick="VerifyEwb()">Verify E-way bill</button>
                 </div>
				<?php				
				 }
				 ?>
				<div class="form-group col-md-12">   
					<button <?php if($branch=='MAGDALLA'){ echo "disabled style='display:none'"; } ?> type="submit" id="create_button" class="btn btn-success">Create new LR</button>
                </div> 
		</div>
          </div>
         
      </form>

</div>
	</div>
			</div> 
          </div>
        </div>       
    </section>
	
<div id="ewb_ver_res"></div>
	
<script>	
function VerifyEwb()
{
	$('#create_button').attr('disabled',true);
	$('#create_button').hide();
	
	var ewb_no = $('#ewb_no').val();
	var vessel_name = $('#vessel_name_sel_box').val();
	var vehicle_type = $('#vehicle_type_selection').val();
	var market_tno = $('#market_tno').val();
	var own_tno = $('#own_tno').val();
	var lr_date = $('#lr_date').val();
	var from_pincode = $('#from_pincode').val();
	var to_loc_sel = $('#to_loc_sel').val();
	var to_pin_code = $('#to_pin_code').val();
	var con1 = $('#con1').val();
	var con1_gst = $('#con1_gst').val();
	var con2 = $('#con2').val();
	var con2_gst = $('#con2_gst').val();
	
	if(ewb_no=='')
	{
		$('#ewb_no').focus();
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "verify_ewb_manually.php",
		data: 'ewb_no=' + ewb_no + '&vessel_name=' + vessel_name + '&vehicle_type=' + vehicle_type + '&market_tno=' + market_tno + '&own_tno=' + own_tno + '&lr_date=' + lr_date + '&from_pincode=' + from_pincode + '&to_loc_sel=' + to_loc_sel + '&to_pin_code=' + to_pin_code + '&con1=' + con1 + '&con1_gst=' + con1_gst + '&con2=' + con2 + '&con2_gst=' + con2_gst,
		type: "POST",
		success: function(data) {
			$("#ewb_ver_res").html(data);
		},
			error: function() {}
		});
	}
}
</script>	
		
		<script type="text/javascript">
					function CalInvVal() 
					 {
						var igst_value = Math.round(Number($("#goods_value").val()) * Number($("#igst_rate").val()) / 100).toFixed(2)
						var total_inv_value = Math.round(Number($("#goods_value").val()) + Number(igst_value)).toFixed(2)
						$("#igst_value").val(igst_value);
						$("#total_inv_value").val(total_inv_value);
					}
					 
                     function sum1() 
					 {
						if($("#charge_wt").val()=='')
						{
							alert('Please enter charge weight.');
							$("#charge_wt").focus();
							$("#bill_rate").val('');
							$("#bill_amount").val('');
						}	
						else
						{
							$("#bill_amount").val(Math.round(Number($("#charge_wt").val()) * Number($("#bill_rate").val())).toFixed(2));
						}
					 }
					 
					 function sum2() 
					 {
						if($("#charge_wt").val()=='')
						{
							alert('Please enter charge weight.');
							$("#charge_wt").focus();
							$("#bill_rate").val('');
							$("#bill_amount").val('');
						}	
						else
						{
							$("#bill_rate").val(Math.round(Number($("#bill_amount").val()) / Number($("#charge_wt").val())).toFixed(2));
						}	
					 }
					 
					 function ResetAll() 
					 {
						$("#bill_amount").val('');
						$("#bill_rate").val('');
                     }
                </script>	
<?php
include "footer.php";
?>

<script>
var items = $('#items').val();
ItemHsn(items);
</script>