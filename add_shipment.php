<?php
include_once "header.php";
?>
<script type="text/javascript">
	$(function() {
		$("#shipment_party").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/shipment_party.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#shipment_party').val(ui.item.value);   
               $('#shipment_party_id').val(ui.item.id);     
               $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Shipment Party does not exists.');
			$("#shipment_party").val('');
			$("#shipment_party").focus();
			$("#shipment_party_id").val('');
			$("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});


$(function() {
		$("#shipper_name").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/shipper.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#shipper_name').val(ui.item.value);   
               $('#shipper_name_id').val(ui.item.id);     
               $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Shipper Party does not exists.');
			$("#shipper_name").val('');
			$("#shipper_name").focus();
			$("#shipper_name_id").val('');
			$("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
		

$(function() {
		$("#vessel_name").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/vessel.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#vessel_name').val(ui.item.value);   
               $('#vessel_name_id').val(ui.item.id);     
               $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vessel does not exists.');
			$("#vessel_name").val('');
			$("#vessel_name").focus();
			$("#vessel_name_id").val('');
			$("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});		
		
$(function() {
		$("#loading_port").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/port.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#loading_port').val(ui.item.value);   
               $('#loading_port_id').val(ui.item.id);     
               $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('PORT does not exists.');
			$("#loading_port").val('');
			$("#loading_port").focus();
			$("#loading_port_id").val('');
			$("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
		
$(function() {
		$("#discharge_port").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/port.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#discharge_port').val(ui.item.value);   
               $('#discharge_port_id').val(ui.item.id);     
               $("#create_button").attr('disabled',false);				   
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('PORT does not exists.');
			$("#discharge_port").val('');
			$("#discharge_port").focus();
			$("#discharge_port_id").val('');
			$("#create_button").attr('disabled',true);
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script>	

<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Create Shipment :
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
         
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#create_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_shipment.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>			

<div id="result_form"></div> 
   
        <form id="Form1" autocomplete="off">

         <div class="row">
			
			 <div class="form-group col-md-12">
			 
<script type="text/javascript">
var room = 1;
function education_fields()
{
	room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group removeclass"+room);
	var rdiv = 'removeclass'+room;
    divtest.innerHTML = '<div class="col-md-4"><div class="form-group"> <label>BL Number <font color="red">*</font></label><input type="text" class="form-control" name="bl_no[]" placeholder="BL Number" required></div></div><div class="col-md-3"><div class="form-group"> <label>BL Date <font color="red">*</font></label><input type="date" class="form-control" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" name="bl_date[]" required></div></div><div class="col-md-3"><div class="form-group"><div class="input-group"> <label>BL Weight (in MTS) <font color="red">*</font></label> <input type="number" step="any" min="0.01" oninput="Func1()" name="bl_weight[]" class="form-control bl_weight" required> <div class="input-group-btn"> <label>&nbsp;</label><br> <button id="remove_button" class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');Func1();"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div>';
    
    objTo.appendChild(divtest)
}
   function remove_education_fields(rid) {
	   $('.removeclass'+rid).remove();
   }
</script>

				<div class="form-group col-md-12">
				 </div>
				 
                 <div class="form-group col-md-3">
                       <label>Shipment Party <font color="red">*</font></label>
                        <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" type="text" class="form-control" id="shipment_party" name="shipment_party" required />
                 </div>
				 <input type="hidden" name="shipment_party_id" id="shipment_party_id">
				 
				 <div class="form-group col-md-3">
                       <label>Shipper Name <font color="red">*</font></label>
                       <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" type="text" id="shipper_name" class="form-control" name="shipper" required />
                 </div>
				 <input type="hidden" name="shipper_name_id" id="shipper_name_id">
				 
				 <div class="form-group col-md-3">
                       <label>Vessel Name <font color="red">*</font></label>
                        <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" type="text" id="vessel_name" class="form-control" name="vessel_name" required />
                 </div>
				  <input type="hidden" name="vessel_name_id" id="vessel_name_id">
				 
				 <div class="form-group col-md-3">
                     <label>Voyage No <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^a-z A-Z0-9-]/,'')" type="text" class="form-control" name="voy_no" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>Port of Loading (POL) <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" type="text" class="form-control" id="loading_port" name="loading_port" required />
                 </div>
				 <input type="hidden" name="loading_port_id" id="loading_port_id">
				 
				 <div class="form-group col-md-3">
                     <label>Port of Discharge (POD) <font color="red">*</font></label>
                     <input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" type="text" class="form-control" id="discharge_port" name="discharge_port" required />
                 </div>
				  <input type="hidden" name="discharge_port_id" id="discharge_port_id">
				 
				 <div class="form-group col-md-3">
                     <label>Total BL Weight <font color="red">*</font></label>
                     <input type="number" min="1" step="any" class="form-control total_bl_weight" name="total_bl_weight" readonly required />
                 </div>
				 
				 <div class="form-group col-md-3">
                     <label>HSN Code <font color="red">*</font></label>
                     <input type="text" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" name="hsn_code" required />
                 </div>
				 
				 <div class="form-group col-md-4">
                     <label>Goods Description <font color="red">*</font></label>
                     <textarea oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,]/,'')" class="form-control" name="goods_desc" required></textarea>
                 </div>
				 
				 <div class="row"></div>
				  
				<div class="col-md-4">
				  <div class="form-group">
					<label>BL Number <font color="red">*</font></label>
					<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9-,]/,'')" type="text" name="bl_no[]" class="form-control" placeholder="BL Number" required>
				  </div>
				</div>

				<div class="col-md-3">
				  <div class="form-group">
					<label>BL Date <font color="red">*</font></label>
					<input type="date" name="bl_date[]" class="form-control" max="<?php echo date("Y-m-d"); ?>" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
				  </div>
				</div>
				
				<div class="col-md-3">
				  <div class="form-group">
					<div class="input-group">
					  <label>BL Weight (in MTS) <font color="red">*</font></label>
						<input type="number" oninput="Func1()" step="any" min="0.01" name="bl_weight[]" class="form-control bl_weight" required>
					  <div class="input-group-btn">
						<label>&nbsp;</label><br>
						<button class="btn btn-success" type="button" id="add_button" onclick="education_fields();Func1();"> 
							<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
						</button>
					  </div>
					</div>
				  </div>
				</div> 
       
				<div id="education_fields">
				</div>
				
				 <div class="form-group col-md-12">   
					<br />
                       <button type="submit" id="create_button" class="btn btn-sm btn-success">Add New Shipment</button>
                 </div> 
				 
          </div>
          </div>
         
      </form>

</div>

		</div>
			</div> 
          </div>
        </div>       
    </section>
	
		<script>
		function Func1()
		{
			var sum = 0;
			$(".bl_weight").each(function(){
				sum += +$(this).val();
			});
			$(".total_bl_weight").val(sum);
		}
		</script>
<?php
include "footer.php";
?>
