<?php
echo "<script>
	window.location.href='./';	
	</script>";
exit();
			
include "header.php";
?>
<style>
.modal-body {
    max-height: calc(100vh - 200px);
    overflow-y: auto;
}
</style>

<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Shipment Summary : Railway : <span style="font-size:20px;" class="fa fa-train"></span>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div id="tab_result"></div>

<div id="result_form"></div> 		
			
<div class="row">
	<div class="form-group col-md-12 table-responsive">
	
	 <table id="example2" class="table table-bordered table-striped" style="font-size:13px;">
          <thead>
		  <tr>
				<th>#</th>
				<th>Vessel name</th>
				<th>Voyage no.</th>
				<th>Party name</th>
				<th>Total BL</th>
				<th>BL weight</th>
				<th>Draft weight</th>
				<th>Balance weight</th>
				<th>Ship by rail</th>
				<th>#details</th>
			</tr>
		</thead>	
		<tbody>	
			
	<?php
			$qry=Qry($conn,"SELECT s.id,s.voyage_no,s.total_bl,s.bl_weight,s.draft_weight,s.bal_weight,s.by_rail,
			v.name as shipname,p.name as shipment_party 
			FROM shipment as s
			LEFT OUTER JOIN vessel_name as v ON v.id=s.vessel_name 
			LEFT OUTER JOIN shipment_party as p ON p.id=s.shipment_party 
			WHERE s.complete!='1' AND s.branch='$branch' AND s.by_rail>0 ORDER BY s.id ASC");
			
            if(!$qry){
				ScriptError($conn,$page_name,__LINE__);
				exit();	
			}
			
			if(numRows($qry)==0)
			{
					echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			}
			else
			{
				$sn=1;
			  while($row = fetchArray($qry))
			  {
				  echo "<tr>
					  <input type='hidden' value='$row[shipname]' id='shipment_name_$row[id]'>
					  <td>$sn</td>
					  <td>$row[shipname]</td>
					  <td>$row[voyage_no]</td>
					  <td>$row[shipment_party]</td>
					  <td>$row[total_bl]</td>
					  <td>$row[bl_weight]</td>
					  <td>$row[draft_weight]</td>
					  <td>$row[bal_weight]</td>
					  <td>$row[by_rail]</td>
					  <td><button type='button' onclick=ViewDetails('$row[id]') class='btn btn-xs btn-primary'>View</button></td>
				 </tr>";	
				$sn++;		
				}
         }
		?>
		</tbody>	
</table>		
      </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
	
<div id="result_func"></div>	
	
<script>	
$(document).ready(function() {
    $('#example2').DataTable();
} );

// function ViewDetails(id)
// {
	// var shipment_name = $('#shipment_name_'+id).val();
	// $('#loadicon').show();
	// jQuery.ajax({
		// url: "_load_shipment_details.php",
		// data: 'id=' + id + '&shipment_name=' + shipment_name,
		// type: "POST",
		// success: function(data){
			// $("#result_func").html(data);
		// },
	// error: function() {}
	// });
// }
</script>

<?php
include "footer.php";
?>