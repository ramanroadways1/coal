<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Freight Memo Advance Pending :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
	
<script type="text/javascript">
$(document).ready(function (e) {
$("#AdvForm").on('submit',(function(e) {
$("#loadicon").show();
$("#submit_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./freight_memo_advance_submit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>			

<div id="result_form"></div> 		
			
<div class="row">
	<div class="form-group col-md-12 table-responsive">
	
        <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
          <thead>
          <tr>
				<th>Id</th>
				<th>FM_No</th>
				<th>TruckNo</th>
				<th>FM_Date</th>
				<th>LRNo</th>
				<th>Weight</th>
				<th>Freight</th>
				<th>Broker</th>
				<th>#</th>
			</tr>
          </thead>
          <tbody>
            <?php
              $sql = Qry($conn,"SELECT f.id,f.fm_no,f.company,f.broker,f.date,f.owner,f.broker,f.weight,f.freight,f.lrnos,
			  b.name as broker_name,b.pan as broker_pan,o.tno,o.name as owner_name,o.pan as owner_pan,up6 as declaration,
			  f.gps_install_type,gps.first_install
			  FROM freight_memo as f
			  LEFT OUTER JOIN rrpl_database.mk_broker as b ON b.id=f.broker 
			  LEFT OUTER JOIN rrpl_database.mk_truck as o ON o.id=f.owner 
			  LEFT OUTER JOIN gps_device_record as gps ON gps.frno=f.fm_no 
			  WHERE f.advance='0' AND f.branch='$branch'");
             
			  if(!$sql){
				 ScriptError($conn,$page_name,__LINE__);
				 exit();
			  }
			  
			 if(numRows($sql)==0)
			 {
				echo "<tr>
						<td colspan='10'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			 }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				  if($row['broker']=='2616'){
					  $commission_amount="400";
				  } else {
					  $commission_amount="0";
				  }
				  
				  if($row['gps_install_type']=="")
				  {
					  $adv_disabled = "disabled";
					  $btn_html = "Gps not found";
				  }
				  else
				  {
					  $adv_disabled = "";
					  $btn_html = "PAY <br>Advance";
				  }
				  
				  if($row['gps_install_type']=="TRIP")
				  {
					  $gps_deposit="3000";
				  }
				  else
				  {
					   if($row['first_install']=="1"){
							$gps_deposit="3000";
					   }
				     	else{
							$gps_deposit="0";
						}
				  }
				  
					$owner_name=$row['owner_name'];
					$broker_name=$row['broker_name'];
					$owner_pan=$row['owner_pan'];
					$declaration=$row['declaration'];
				 
                echo"
				<tr>
				  <td>$sn</td>
				  <td>$row[fm_no]</td>
				  <td>$row[tno]</td>
				   <td>".date("d-m-y",strtotime($row['date']))."</td>
				  <td>$row[lrnos]</td>
				  <td>$row[weight]</td>
				  <td>$row[freight]</td>
				  <td>$row[broker_name]</td>
				  <input type='hidden' id='commission_amount_$row[id]' value='$commission_amount' />
				  <td><button type='button' id='AdvanceId$row[id]' 
				  onclick=AdvanceModal('$row[id]','$row[fm_no]','$row[company]','$row[tno]','$row[freight]','$owner_pan','$row[broker_pan]','$row[owner]','$row[broker]','$row[date]')
				  class='btn btn-xs bg-red' $adv_disabled>$btn_html</button></td>
				<input type='hidden' id='decBox$row[id]' value='$declaration'>		
				<input type='hidden' id='gps_deposit_amount_$row[id]' value='$gps_deposit'>		
				<input type='hidden' id='gps_install_type_$row[id]' value='$row[gps_install_type]'>		
				<input type='hidden' id='owner_name_$row[id]' value='$row[owner_name]'>		
				<input type='hidden' id='broker_name_$row[id]' value='$row[broker_name]'>		
				</tr>
				";
              }
			}
            ?>
	</tbody>		
        </table>
      </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
	
<style>
 .ui-autocomplete { z-index:2147483647; }
 
 .ui-front {
    z-index: 9999999 !important;
}

.modal {
  overflow-y:auto;
}

@media (min-width: 768px) {
  .modal-xl {
    width: 90%;
   max-width:1200px;
  }
}
</style>
	
<script>
  $(function() {  
      $("#card_no").autocomplete({
			source: function(request, response) { 
			if(document.getElementById('fuel_company_man').value!=''){
                 $.ajax({
                  url: "../b5aY6EZzK52NA8F/autofill/get_diesel_cards.php",
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                   company: document.getElementById('fuel_company_man').value
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              } else {
                alert('Please select company first !');
				$("#card_no").val('');
				$("#cardno_id").val('');
				$("#phy_card_no").val('');
				$("#rilpre").val('');
              }

              },
              select: function (event, ui) { 
               $('#card_no').val(ui.item.value);   
               $('#cardno_id').val(ui.item.dbid);     
               $('#phy_card_no').val(ui.item.phy_card_no);     
               $('#rilpre').val(ui.item.rilpre);     

               return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
				$("#cardno_id").val('');
				$("#phy_card_no").val('');
				$("#rilpre").val('');
                $(event.target).focus();
                alert('Card No. does not exist !'); 
              } 
              },
			}); 
      }); 
	  
 </script> 
 
<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#myModal"></button>

<div class="modal" id="myModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
	
      <div class="bg-primary modal-header">
        <h4 class="modal-title"><b>Pay ADVANCE : <span style="color:" id="fm_no_html"></span></h4></b>
		<h5>
			<span><b>OWNER :</b> <span id="owner_name_html"></span> </span>
			 <span class="pull-right"><b>BROKER :</b> <span id="broker_name_html"></span></span>
		</h5>
      </div>

<div id="get_tds_div"></div>

	<form id="AdvForm" autocomplete="off">
      <div class="modal-body">
		 <div class="row">
		 
			<div class="form-group col-md-2">
				<label>Freight <font color="red">*</font></label>
				<input type="number" min="1" id="freight_pay" name="freight" class="form-control" readonly required />
			</div>
			
			<script>
			function AdvTo(elem)
			{
				$('#cash_adv').val('');
				$('#diesel_adv').val('');
				$('#rtgs_adv').val('');
				
				$('#cash_adv').attr('readonly',false);
				$('#diesel_adv').attr('readonly',false);
				$('#rtgs_adv').attr('readonly',false);
				
				$('#diesel_div').hide();
				$('#rtgs_div').hide();
				
				var broker_pan=$('#broker_pan').val();
				var owner_pan=$('#owner_pan').val();
				var freight=$('#freight').val();
					
				if(elem=='BROKER')
				{
					if($('#tds_deduct').val()==1)
					{
						$('#loadicon').show();
						jQuery.ajax({
							url: "get_tds_value.php",
							data: 'adv_bal=' + 'ADV' + '&pan_letter=' + broker_pan.slice(3,4) + '&freight=' + freight + '&pay_to=' + elem,
							type: "POST",
							success: function(data) {
							$("#get_tds_div").html(data);
							},
							error: function() {}
						});
					
						// if((broker_pan.slice(3, 4))=='P' || (broker_pan.slice(3, 4))=='H')
						// {
							// $('#tds_amount').val(Math.round(freight*1/100));
							// $('#tds_amount').val(Math.ceil(freight*0.75/100));
						// }
						// else if((broker_pan.slice(3, 4))=='C' || (broker_pan.slice(3, 4))=='F' || (broker_pan.slice(3, 4))=='E')
						// {
							// $('#tds_amount').val(Math.round(freight*2/100));
							// $('#tds_amount').val(Math.round(freight*1.50/100));
						// }
						// else
						// {
							// alert('Broker PAN Card is not Valid. Select another Option');
							// $('#adv_to').val('');
							// $('#tds_amount').val('0');
						// }
					}
					else
					{
						$('#tds_amount').val('0');
					}
					
					$('#party_name').val($('#broker').val());
					$('#party_pan').val($('#broker_pan').val());
					
					$('#ac_holder').val($('#db_broker_acname').val());
					$('#ac_number').val($('#db_broker_acno').val());
					$('#bank_name').val($('#db_broker_bank').val());
					$('#ifsc_code').val($('#db_broker_ifsc').val());
				}
				else if(elem=='OWNER')
				{
					if($('#tds_deduct').val()==1)
					{
						$('#loadicon').show();
						jQuery.ajax({
							url: "get_tds_value.php",
							data: 'adv_bal=' + 'ADV' + '&pan_letter=' + owner_pan.slice(3,4) + '&freight=' + freight + '&pay_to=' + elem,
							type: "POST",
							success: function(data) {
							$("#get_tds_div").html(data);
							},
							error: function() {}
						});
						
						// if((owner_pan.slice(3, 4))=='P' || (owner_pan.slice(3, 4))=='H')
						// {
							// $('#tds_amount').val(Math.round(freight*1/100));
							// $('#tds_amount').val(Math.round(freight*0.75/100));
						// }
						// else if((owner_pan.slice(3, 4))=='C' || (owner_pan.slice(3, 4))=='F' || (owner_pan.slice(3, 4))=='E')
						// {
							// $('#tds_amount').val(Math.round(freight*2/100));
							// $('#tds_amount').val(Math.round(freight*1.50/100));
						// }
						// else
						// {
							// alert('Owner PAN Card is not Valid. Select another Option');
							// $('#adv_to').val('');
							// $('#tds_amount').val('0');
						// }
					}
					else
					{
						$('#tds_amount').val('0');
					}
					
					$('#party_name').val($('#owner').val());
					$('#party_pan').val($('#owner_pan').val());
					
					$('#ac_holder').val($('#db_owner_acname').val());
					$('#ac_number').val($('#db_owner_acno').val());
					$('#bank_name').val($('#db_owner_bank').val());
					$('#ifsc_code').val($('#db_owner_ifsc').val());
				}
				else if(elem=='NULL')
				{
					$('#loadicon').show();
						jQuery.ajax({
							url: "get_tds_value.php",
							data: 'adv_bal=' + 'ADV' + '&pan_letter=' + owner_pan.slice(3,4) + '&freight=' + freight + '&pay_to=' + elem + '&pan_letter_broker=' + broker_pan.slice(3,4) + '&owner_name=' + $('#owner').val() + '&owner_pan=' + $('#owner_pan').val() + '&broker_name=' + $('#broker').val() + '&broker_pan=' + $('#broker_pan').val() + '&tds_deduct=' + $('#tds_deduct').val(),
							type: "POST",
							success: function(data) {
							$("#get_tds_div").html(data);
						},
						error: function() {}
					});
					
						// if((owner_pan.slice(3, 4))=='P' || (owner_pan.slice(3, 4))=='H')
						// {
							// var tds_amt1 = Math.round(freight*1/100);
							// var tds_amt1 = Math.round(freight*0.75/100);
							// var p_name=$('#owner').val();
							// var p_pan=$('#owner_pan').val();
						// }
						// else if((owner_pan.slice(3, 4))=='C' || (owner_pan.slice(3, 4))=='F' || (owner_pan.slice(3, 4))=='E')
						// {
							// var tds_amt1 = Math.round(freight*2/100);
							// var tds_amt1 = Math.round(freight*1.50/100);
							// var p_name=$('#owner').val();
							// var p_pan=$('#owner_pan').val();
						// }
						// else
						// {
							// if((broker_pan.slice(3, 4))=='P' || (broker_pan.slice(3, 4))=='H')
							// {
								// var tds_amt1 = Math.round(freight*1/100);
								// var tds_amt1 = Math.round(freight*0.75/100);
								// var p_name=$('#broker').val();
								// var p_pan=$('#broker_pan').val();
							// }
							// else if((broker_pan.slice(3, 4))=='C' || (broker_pan.slice(3, 4))=='F' || (broker_pan.slice(3, 4))=='E')
							// {
								// var tds_amt1 = Math.round(freight*2/100);
								// var tds_amt1 = Math.round(freight*1.50/100);
								// var p_name=$('#broker').val();
								// var p_pan=$('#broker_pan').val();
							// }
							// else
							// {
								// alert('Owner and Broker Both PAN Card is not Valid.');
								// $('#adv_to').val('');
								// var p_name="";
								// var p_pan="";
								// var tds_amt1 = 0;
							// }
						// }
					
					if($('#tds_deduct').val()==0)
					{
						$('#tds_amount').val('0');
					}
					
					
					// $('#party_name').val(p_name);
					// $('#party_pan').val(p_pan);
					$('#cash_adv').val('0');
					$('#diesel_adv').val('0');
					$('#rtgs_adv').val('0');
					
					$('#cash_adv').attr('readonly',true);
					$('#diesel_adv').attr('readonly',true);
					$('#rtgs_adv').attr('readonly',true);
					DieselAdv('0');
					RtgsAdv('0');
					
					$('#ac_holder').val('');
					$('#ac_number').val('');
					$('#bank_name').val('');
					$('#ifsc_code').val('');
				}
				else
				{
					$('#party_name').val('');
					$('#party_pan').val('');
					
					$('#ac_holder').val('');
					$('#ac_number').val('');
					$('#bank_name').val('');
					$('#ifsc_code').val('');
					
					$('#tds_amount').val('0');
				}
			}
			</script>
			
			<div class="form-group col-md-3">
				<label>PAY Advance To <font color="red">*</font></label>
				<select name="adv_to" id="adv_to" onchange="AdvTo(this.value)" class="form-control" required>
					<option value="">--SELECT--</option>
					<option value="NULL">ZERO / No ADV</option>
					<option id="broker_sel" value="BROKER">BROKER</option>
					<option id="owner_sel" value="OWNER">OWNER</option>
				</select>
			</div>
			
			<div class="form-group col-md-4">
				<label>Party Name <font color="red">*</font></label>
				<input type="text" id="party_name" name="" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Party PAN <font color="red">*</font></label>
				<input type="text" id="party_pan" name="" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Loading(+) <font color="red">*</font></label>
				<input type="number" min="0" id="loading" name="loading" class="form-control" required />
			</div>
			
			<div class="form-group col-md-2">
				<label>Others(-) <font color="red">*</font></label>
				<input type="number" min="0" id="others" name="others" class="form-control" required />
			</div>
			
			<div class="form-group col-md-2">
				<label>Commission(-) <font color="red">*</font></label>
				<input type="number" min="0" id="commission_model" name="commission_model" readonly class="form-control" required />
			</div>
			
			<div class="form-group col-md-2">
				<label>TDS(-) <font color="red">*</font></label>
				<input type="number" min="0" value="0" readonly id="tds_amount" name="tds_amount" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>GPS Deposit(-) <font color="red">*</font> <sup><span style="color:red" id="install_type_html"></span></sup></label>
				<input type="number" min="0" value="<?php echo $gps_deposit; ?>" readonly id="gps_deposit" name="gps_deposit" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Total Freight <font color="red">*</font></label>
				<input type="number" min="0" id="total_freight" name="total_freight" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Cash Advance <font color="red">*</font></label>
				<input type="number" max="0" min="0" id="cash_adv" name="cash_adv" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Diesel Advance <font color="red">*</font></label>
				<input type="number" min="0" oninput="DieselAdv(this.value);" value="" id="diesel_adv" name="diesel_adv" class="form-control" required />
			</div>
			
			<div class="form-group col-md-3">
				<label>RTGS Advance <font color="red">*</font></label>
				<input type="number" min="0" oninput="RtgsAdv(this.value)" value="" id="rtgs_adv" name="rtgs_adv" class="form-control" required />
			</div>
			
			<div id="ac_result"></div>
			
			<script>
			function FetchAc(owner_id,broker_id)
			{
				var dec = $('#declaration').val();
				
				if(dec=='')
				{
					if($('#freight_pay').val()<=0)
					{
						alert('Freight Amount is not enought for TDS deduction.');
						window.location.href='./';
					}
						
					$('#tds_deduct').val('1');
				}
				else
				{
					$('#tds_deduct').val('0');
				}
				
				jQuery.ajax({
					url: "fetch_ac_details.php",
					data: 'owner_id=' + owner_id + '&broker_id=' + broker_id + '&type=' + 'ADV',
					type: "POST",
					success: function(data) {
					$("#ac_result").html(data);
					},
					error: function() {}
				});
			}
			</script>
			
			<input type="hidden" id="tds_deduct" name="tds_deduct">
			
			
			<input type="hidden" id="db_owner_acname">
			<input type="hidden" id="db_owner_acno">
			<input type="hidden" id="db_owner_bank">
			<input type="hidden" id="db_owner_ifsc">
			
			<input type="hidden" id="db_broker_acname">
			<input type="hidden" id="db_broker_acno">
			<input type="hidden" id="db_broker_bank">
			<input type="hidden" id="db_broker_ifsc">
			
		<script>	
		function DieselAdv(amount)
		{
			if($('#adv_to').val()=='')
			{
				alert('Select Advance to option first.');
				$('#diesel_adv').val('');
			}
			else
			{	
				if(amount>0)
				{
					$('#diesel_div').show();
				}
				else
				{
					$('#diesel_div').hide();
				}
			}
		}
		</script>	
		
		<script>	
		function RtgsAdv(amount)
		{
			if($('#adv_to').val()=='')
			{
				alert('Select Advance to option first.');
				$('#rtgs_adv').val('');
			}
			else
			{	
			if(amount>0)
			{
				$('#ac_holder').attr('required',true);
				$('#ac_number').attr('required',true);
				$('#bank_name').attr('required',true);
				$('#ifsc_code').attr('required',true);
				
				if($('#ac_holder').val()=='')
				{
					$('#ac_holder').attr('readonly',false);
					$('#ac_number').attr('readonly',false);
					$('#bank_name').attr('readonly',false);
					$('#ifsc_code').attr('readonly',false);
				}
				else
				{
					$('#ac_holder').attr('readonly',true);
					$('#ac_number').attr('readonly',true);
					$('#bank_name').attr('readonly',true);
					$('#ifsc_code').attr('readonly',true);
				}
				
				$('#rtgs_div').show();
			}
			else
			{
				$('#ac_holder').attr('required',false);
				$('#ac_number').attr('required',false);
				$('#bank_name').attr('required',false);
				$('#ifsc_code').attr('required',false);
				
				$('#rtgs_div').hide();
			}
			}
		}
		</script>	
		
		<script>
		function LoadDiesel()
		{
			$('#loadicon').show();
			jQuery.ajax({
			url: "fetch_diesel_cache.php",
			data: 'vou_no=' + $('#VouId_for_diesel').val() + '&type=' + "ADV",
			type: "POST",
			success: function(data) {
			$("#diesel_div").html(data);
			},
			error: function() {}
			});
		}
		</script>
		
			
		<div id="diesel_div" style="display:none">
		
		</div>
		
		<script>
		function ResetQtyRate()
		{
			$("#dsl_qty").val('');
			$("#dsl_rate").val('');
		}
		
		function SumRate(rate)
		{
			$("#dsl_qty").val((Number($("#diesel_adv").val()) / rate).toFixed(2));
		}
		
		function SumQty(qty)
		{
			$("#dsl_rate").val((Number($("#diesel_adv").val()) / qty).toFixed(2));
		}
		</script>
		
		<div id="rtgs_div" style="display:none">
		
			<div class="form-group col-md-3">
				<label>Ac Holder <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z]/,'')" type="text" id="ac_holder" name="ac_holder" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Ac Number <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" type="text" id="ac_number" name="ac_number" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Bank Name <font color="red">*</font></label>
				<input oninput="this.value=this.value.replace(/[^a-z A-Z0-9]/,'')" type="text" id="bank_name" name="bank_name" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>IFSC Code <font color="red">*</font></label>
				<input  maxlength="11" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'');ValidateIFSC(this.value)" type="text" id="ifsc_code" name="ifsc_code" class="form-control" readonly required />
			</div>
		</div>
		
			<div class="form-group col-md-3">
				<label>Total Advance <font color="red">*</font></label>
				<input type="number" min="0" id="total_adv" name="total_adv" class="form-control" readonly required />
			</div>
			
			<div class="form-group col-md-3">
				<label>Balance Amount <font color="red">*</font></label>
				<input type="number" min="0" value="0" id="balance_amount" name="balance_amount" class="form-control" readonly required />
			</div>
		
		</div>
		
		<script type="text/javascript">
function ValidateIFSC() {
  var Obj = document.getElementById("ifsc_code");
        if (Obj.value != "") {
            ObjVal = Obj.value;
            var panPat = /^([a-zA-Z]{4})([0])([a-zA-Z0-9]{6})$/;
            if (ObjVal.search(panPat) == -1) {
                //alert("Invalid Pan No");
				document.getElementById("ifsc_code").setAttribute(
   "style","background-color:red;text-transform:uppercase;color:#fff;");
   document.getElementById("submit_button").setAttribute("disabled",true);
                Obj.focus();
                return false;
            }
          else
            {
              //alert("Correct Pan No");
			  document.getElementById("ifsc_code").setAttribute(
   "style","background-color:green;color:#fff;text-transform:uppercase;");
			document.getElementById("submit_button").removeAttribute("disabled");
              }
        }
		else
		{
			document.getElementById("ifsc_code").setAttribute(
   "style","background-color:white;color:#fff;text-transform:uppercase;");
	document.getElementById("submit_button").removeAttribute("disabled");
		//	$("#pan_div").val("1");
		}
  } 			
</script>

			<input type="hidden" id="fm_id" name="fm_id">	
			<input type="hidden" id="fm_number" name="fm_number">	
			<input type="hidden" id="freight" name="freight">	
			<input type="hidden" id="owner" name="owner">	
			<input type="hidden" id="owner_pan" name="owner_pan">	
			<input type="hidden" id="broker" name="broker">	
			<input type="hidden" id="broker_pan" name="broker_pan">	
			<input type="hidden" id="company" name="company">	
			<input type="hidden" id="vou_date" name="vou_date">	
			
			<input type="hidden" id="owner_id" name="owner_id">	
			<input type="hidden" id="broker_id" name="broker_id">	
			<input type="hidden" id="truck_no" name="truck_no">	
			
			<input type="hidden" id="declaration" name="declaration">	
      </div>
	  
	 <script> 
$(function()
{
$("#freight,#loading,#others,#commission_model,#tds_amount,#gps_deposit,#cash_adv,#diesel_adv,#rtgs_adv").on("keydown keyup blur change",sum);
function sum() {
	$("#total_freight").val((Number($("#freight").val()) + Number($("#loading").val()) - Number($("#others").val()) - Number($("#commission_model").val()) - Number($("#tds_amount").val()) - Number($("#gps_deposit").val())).toFixed(2));
	$("#total_adv").val((Number($("#cash_adv").val()) + Number($("#diesel_adv").val()) + Number($("#rtgs_adv").val())).toFixed(2));
	$("#balance_amount").val((Number($("#total_freight").val()) - Number($("#total_adv").val())).toFixed(2));
}});
</script> 

      <div class="modal-footer">
        <button type="submit" id="submit_button" class="btn btn-primary">Submit</button>
        <button type="button" id="close_modal_button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	
	
<input type="hidden" id="VouId_for_diesel">	
	
<?php
include("modal_diesel_adv.php");
?>
	
<script>	
function AdvanceModal(id,fm_no,company,truck_no,freight,owner_pan,broker_pan,owner_id,broker_id,vou_date)
{
	var dec = $('#decBox'+id).val();
	var broker_name = $('#broker_name_'+id).val();
	var owner_name =  $('#owner_name_'+id).val();
	var gps_deposit = $('#gps_deposit_amount_'+id).val();
	var install_type =  $('#gps_install_type_'+id).val();
	var commission_amount = $('#commission_amount_'+id).val();
	
	$('#install_type_html').html("("+install_type+")");
	$('#gps_deposit').val(gps_deposit);
	
	$('#vou_type_diesel2').val("ADV");
	$('#vou_id_for_diesel').val(fm_no);
	$('#VouId_for_diesel').val(fm_no);
	$('#fm_id').val(id);
	
	$('#commission_model').val(commission_amount);
	$('#commission_model').attr("min",commission_amount);
	$('#others').val("300");
	$('#others').attr("min","300");
	
	$('#fm_number').val(fm_no);
	$('#freight').val(freight);
	$('#freight_pay').val(freight);
	$('#owner').val(owner_name);
	$('#owner_pan').val(owner_pan);
	$('#declaration').val(dec);
	$('#broker').val(broker_name);
	$('#broker_pan').val(broker_pan);
	$('#owner_id').val(owner_id);
	$('#broker_id').val(broker_id);
	$('#company').val(company);
	$('#truck_no').val(truck_no);
	$('#vou_date').val(vou_date);
	
	$('#owner_name_html').html(owner_name);
	$('#broker_name_html').html(broker_name);
	$('#fm_no_html').html(fm_no);
	
	FetchAc(owner_id,broker_id);
	LoadDiesel();
	document.getElementById('ModalButton').click();
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
	
<?php
include "footer.php";
?>