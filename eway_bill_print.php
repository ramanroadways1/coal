<?php
include "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Print EWay Bill :
        <small></small>
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		
  
	</section>
    
	<section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

			<div id="tab_result"></div>
	
<div id="result_form"></div> 		
			
<div class="row">
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
        <thead>
		<tr>
				<th>#</th>
				<th>Ewb number</th>
				<th>LR number</th>
				<th>Vehicle no.</th>
				<th>LR date</th>
				<th>PO number</th>
				<th>BOE number</th>
				<th>BOE date</th>
				<th>Taxable value</th>
				<th>Total value</th>
				<th>#</th>
				<th>#</th>
			</tr>
          </thead>
		  <tbody> 
            <?php
              $sql = Qry($conn,"SELECT lr_entry.id,lr_entry.lrno,tno,lr_date,po_no,do_no,boe_date,goods_value,
			  total_value,eway_bill,eway_bill_log.eway_bill_no,eway_bill_log.download_link FROM 
			  lr_entry,eway_bill_log WHERE eway_bill=1 AND lr_entry.branch='$branch' AND lr_entry.lrno=eway_bill_log.lrno AND download_link=''");
              
			  if(!$sql)
			  {
				  ScriptError($conn,$page_name,__LINE__);
				  exit();
			  }
			  
			 if(numRows($sql)==0)
			 {
				echo "<tr>
						<td colspan='12'><b>NO RESULT FOUND..</b></td>
				</tr>";  
			 }
			 else
			{
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 if($row['download_link']=='')
				 {
					 $button_d="";
					 $button_v="disabled";
					 $button_d_name="Download";
				 }	
				else
				{
					$button_d="";
				   $button_v="";
				   $button_d_name="Download<br>Again";
				}
				  
				echo 
                "<tr>
				  <td>$sn</td>
				  <td>$row[eway_bill_no]</td>
				  <td>$row[lrno]</td>
				  <td>$row[tno]</td>
				  <td>".date("d/m/y",strtotime($row['lr_date']))."</td>				 
				 <td>$row[po_no]</td>
				 <td>$row[do_no]</td>
				 <td>".date("d/m/y",strtotime($row['boe_date']))."</td>		
				<td>$row[goods_value]</td>				 
				<td>$row[total_value]</td>				 
				<input type='hidden' id='ShipNameBox$row[id]' value='$row[id]'>
				<td>
					<button $button_d type='button' id='Download$row[id]' onclick=Download('$row[id]','$row[eway_bill_no]') class='btn btn-xs btn-danger'>$button_d_name</button>
				</td>
				<td>
					<a href='./EWayFolder/$row[eway_bill_no].pdf' target='_blank'>
						<button type='button' $button_v id='View$row[id]' class='btn btn-xs btn-primary'>View</button>
					</a>	
				</td>
				</tr>
				";
			$sn++;		
              }
			}
            ?>
		 </tbody>	
        </table>
      </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>
	
<script>	
$(document).ready(function() {
    $('#example').DataTable();
} );

function Download(id,EwbNo)
{
		$('#Download'+id).attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
		url: "eway_bill_download.php",
		data: 'id=' + id + '&EwbNo=' + EwbNo,
		type: "POST",
		success: function(data) {
		$("#tab_result").html(data);
		
		},
		error: function() {}
	});
}
</script>
<?php
include "footer.php";
?>