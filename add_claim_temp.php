<?php
require_once("./connection.php");

$lrno = escapeString($conn,strtoupper(trim($_POST['lrno'])));
$amount = escapeString($conn,strtoupper($_POST['claim_amount']));
$claim_vou_no = escapeString($conn,strtoupper($_POST['claim_vou_no']));
$vou_no = escapeString($conn,strtoupper($_POST['vou_no']));
$vou_type = escapeString($conn,strtoupper($_POST['vou_type']));
$timestamp = date("Y-m-d H:i:s");

if(!preg_match('/^[a-zA-Z0-9\.]*$/', $lrno))
{
	echo "<script>
		alert('Error : Check LR Number !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_lr = Qry($conn,"SELECT id FROM claim_cache WHERE lrno='$lrno' AND vou_no='$vou_no' AND adv_bal='$vou_type'");

if(!$chk_lr){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_lr)>0)
{
	echo "<script>
		alert('Duplicate Record found LR Number : $lrno.');
		$('#loadicon').hide();
	</script>";
	exit();
}

$claim_txn = Qry($conn,"SELECT id FROM claim_book_trans WHERE lrno='$lrno' AND vou_no='$claim_vou_no'");

if(!$claim_txn){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($claim_txn)==0)
{
	echo "<script>
		alert('Claim record not found for LR: $lrno and Voucher: $vou_no.');
		$('#loadicon').hide();
	</script>";
	exit();
} 

$insert = Qry($conn,"INSERT INTO claim_cache(vou_no,claim_vou_no,lrno,amount,adv_bal,timestamp) VALUES ('$vou_no','$claim_vou_no',
'$lrno','$amount','$vou_type','$timestamp')");

if(!$insert){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

echo "<script>
		alert('Claim entry Success.');
		 $('#ClaimForm').trigger('reset');
		LoadClaimLR('$vou_no');
	</script>";
	exit();
?>