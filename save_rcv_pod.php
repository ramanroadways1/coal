<?php
require_once("./connect.php");

$lrno=mysqli_real_escape_string($conn,strtoupper($_POST['lrno']));
$fm_no=mysqli_real_escape_string($conn,strtoupper($_POST['fm_no']));
$unload_date=mysqli_real_escape_string($conn,strtoupper($_POST['unload_date']));

if(count($_FILES['unloading_slip']['name'])>0)
{
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	foreach($_FILES['unloading_slip']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			echo "<script>alert('Error : Only Image Upload Allowed.');</script>";exit();
		}
    }
	
	for($i2=0; $i2<count($_FILES['unloading_slip']['name']);$i2++) 
	{
		$UploadedSize = $_FILES['unloading_slip']['size'][$i2];
		
		if(($UploadedSize/1024/1024)>2)
		{
			echo "<script>alert('Error : MAX Upload size allowed is : 2MB');</script>";exit();	
		}
	}
}
else
{
	echo "<script>alert('Please Upload Un-Loading SLIP.');</script>";exit();
}

if(count($_FILES['pod_copy']['name'])>0)
{
	$valid_types2 = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	foreach($_FILES['pod_copy']['type'] as $file_types1)
	{
		if(!in_array($file_types1, $valid_types2))
		{
			echo "<script>alert('Error : Only Image Upload Allowed.');</script>";exit();
		}
    }
	
	for($i3=0; $i3<count($_FILES['pod_copy']['name']);$i3++) 
	{
		$UploadedSize2 = $_FILES['pod_copy']['size'][$i3];
		
		if(($UploadedSize2/1024/1024)>2)
		{
			echo "<script>alert('Error : MAX Upload size allowed is : 2MB');</script>";exit();	
		}
	}
}
else
{
	echo "<script>alert('Please Upload POD Copy.');</script>";exit();
}

	for($i=0; $i<count($_FILES['unloading_slip']['name']);$i++) 
		{
			$sourcePath = $_FILES['unloading_slip']['tmp_name'][$i];
			
			if($sourcePath!="")
			{
				$fix_name=date('dmYHis').mt_rand();
				$targetPath = "unloading_slip/".$fix_name.".".pathinfo($_FILES['unloading_slip']['name'][$i],PATHINFO_EXTENSION);
				$maxDimW = "1500";
				$maxDimH = "1500";
				include ('./up_func.php');

				if(move_uploaded_file($sourcePath, $targetPath)) 
				{
					$files_unload[] = $targetPath;
				}
				else
				{
					echo "<script>alert('Unable to upload Un-Loading SLIP.');</script>";exit();
				}
			}
			else
			{
				echo "<script>alert('No attachment found Unloading Slip.');</script>";exit();
			}
        }
		
		$file_name_unload=implode(',',$files_unload);
		
		
	for($i4=0; $i4<count($_FILES['pod_copy']['name']);$i4++) 
		{
			$sourcePath1 = $_FILES['pod_copy']['tmp_name'][$i4];
			
			if($sourcePath1!="")
			{
				$fix_name1=date('dmYHis').mt_rand();
				$targetPath1 = "pod_copy/".$fix_name1.".".pathinfo($_FILES['pod_copy']['name'][$i4],PATHINFO_EXTENSION);
				$maxDimW1 = "1500";
				$maxDimH1 = "1500";
				include ('./up_func2.php');

				if(move_uploaded_file($sourcePath1, $targetPath1)) 
				{
					$files_pod[] = $targetPath1;
				}
				else
				{
					echo "<script>alert('Unable to upload POD Copy.');</script>";exit();
				}
			}
			else
			{
				echo "<script>alert('No attachment found : POD Copy.');</script>";exit();
			}
        }
		
		$file_name_pod=implode(',',$files_pod);		
	
$timestamp=date("Y-m-d H:i:s");
$date=date("Y-m-d");

$rcv_pod = mysqli_query($conn,"INSERT INTO rcv_pod(fm_no,lrno,unloading_slip,pod_copy,unload_date,timestamp) VALUES 
('$fm_no','$lrno','$file_name_unload','$file_name_pod','$unload_date','$timestamp')");

if(!$rcv_pod)
{
	echo mysqli_error($conn);
	exit();
}

$update_freight_memo=mysqli_query($conn,"UPDATE freight_memo SET pod='1',pod_date='$date' WHERE fm_no='$fm_no'");

	echo "<script>
			alert('POD Received !');
			window.location.href='./rcv_pod.php';
		</script>";
	exit();

?>