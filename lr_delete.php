<?php
include_once "header.php";
?>
<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Pending Voucher LRs :
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<div id="result_form"></div> 
   
      <div class="row">
		<div class="form-group col-md-12 table-responsive">
		<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		  <tr>
				<th>#</th>
				<th>LR number</th>
				<th>Vehicle type</th>
				<th>Vehicle no.</th>
				<th>LR date</th>
				<th>Locations</th>
				<th>Consignor & Consignee</th>
				<th>Act. wt</th>
				<th>Chrg. wt</th>
				<th>#delete</th>
			</tr>
		</thead>	
		<tbody>	
			
	<?php
			$qry=Qry($conn,"SELECT id,lrno,lr_type,truck_no,date,consignor,consignee,fstation,tstation,wt12,weight 
			FROM rrpl_database.lr_sample_pending WHERE branch='$branch'");
			
            if(!$qry){
				ScriptError($conn,$page_name,__LINE__);
				exit();	
			}
			
			if(numRows($qry)==0)
			{
				echo "<tr>
					<td colspan='12'><b>NO RESULT FOUND..</b></td>
				</tr>";  
			}
			else
			{
				$sn=1;
			  while($row = fetchArray($qry))
			  {
				  echo "<tr>
					  <td>$sn</td>
					  <td>$row[lrno]</td>
					  <td>$row[lr_type]</td>
					  <td>$row[truck_no]</td>
					  <td>$row[date]</td>
					  <td>From: $row[fstation]<br>To: $row[tstation] </td>
					  <td>$row[consignor]<br>$row[consignee] </td>
					  <td>$row[wt12]</td>
					  <td>$row[weight]</td>
					  <td><button type='button' id='delete_button_$row[id]' onclick=DeleteLR('$row[id]') class='btn btn-xs btn-danger'>delete</button></td>
				 </tr>";	
				$sn++;		
				}
         }
		?>
		</tbody>	
</table>		

			</div>
       </div>
       
</div>

		</div>
			</div> 
          </div>
        </div>       
    </section>
	
<div id="result_func"></div>
<script>	
$(document).ready(function() {
    $('#example').DataTable();
} );


function DeleteLR(id)
{
	$('#delete_button_'+id).attr('disabled',true);
	$('#loadicon').show();
	jQuery.ajax({
		url: "./lr_delete_save.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#result_func").html(data);
		},	
		error: function() {}
	});
}


</script>	
<?php
include "footer.php";
?>