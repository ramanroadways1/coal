<?php
require_once("./connect.php");

$name=mysqli_real_escape_string($conn2,strtoupper($_POST['fullName']));
$gst_number=mysqli_real_escape_string($conn2,strtoupper($_POST['gst_number']));
$mobile=mysqli_real_escape_string($conn2,strtoupper($_POST['mobile']));
$pan_no=mysqli_real_escape_string($conn2,strtoupper($_POST['pan_no']));
$addr=mysqli_real_escape_string($conn2,strtoupper($_POST['addr']));

$timestamp=date("Y-m-d H:i:s");

$sourcePath = $_FILES['pan_copy']['tmp_name']; 

if($sourcePath=='')
{
	echo "<script>
	alert('Upload PAN Card Copy.');
	</script>";
	exit();
}

$maxDimW = "1500";
$maxDimH = "1500";

	if($sourcePath!='')
	{
		$targetPath = "customer_pan_copy/".$name."_".date("YmdHis")."_".$_FILES["pan_copy"]["name"];	
		include ('./up_func_bl.php');
		if(!move_uploaded_file($sourcePath,$targetPath))
		{
			echo "<script>
			alert('Unable to upload PAN Card Copy !');
			</script>";
			exit();
		}
	}
	else
	{
		$targetPath='';
	}

$insert = mysqli_query($conn2,"INSERT INTO customer_details(fullName,gst_number,pan_number,mobile_number,address,pan_copy,timestamp) 
VALUES ('$name','$gst_number','$pan_no','$mobile','$addr','$targetPath','$timestamp')");

if(!$insert)
{
	echo mysqli_error($conn2);
	exit();
}
else
{
	echo "<script>
			alert('Party Added Successfully !');
			window.location.href='./add_customer.php';
		</script>";
		exit();
}
?>