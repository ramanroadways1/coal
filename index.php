<?php
include "header.php";

$today_date = date("Y-m-d");

// $get_card_details = Qry($conn,"SELECT 
// (SELECT COUNT(id) FROM dairy.happay_card_inventory) as 'total_cards',
// (SELECT COUNT(id) FROM dairy.happay_card_inventory WHERE card_status='1') as 'active_cards',
// (SELECT COUNT(id) FROM dairy.happay_card_inventory WHERE card_assigned='1') as 'assigned_cards',
// (SELECT COUNT(id) FROM dairy.happay_card_inventory WHERE company='RRPL') as 'rrpl_cards',
// (SELECT COUNT(id) FROM dairy.happay_card_inventory WHERE company='RAMAN_ROADWAYS') as 'rr_cards'");

// if(!$get_card_details){
	// echo getMySQLError($conn);
	// errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	// exit();
// }

// $row_details = fetchArray($get_card_details);

// $get_wallet_details = Qry($conn,"SELECT 
// (SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date') as 'today_transactions',
// (SELECT COALESCE(SUM(credit+debit),0) FROM dairy.happay_webhook_live WHERE date='$today_date') as 'today_transactions_amt',
// (SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Wallet Load Credit') as 'today_load',
// (SELECT COALESCE(SUM(credit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Wallet Load Credit') as 'today_load_amt',
// (SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Wallet Withdraw Debit') as 'today_withdraw',
// (SELECT COALESCE(SUM(debit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Wallet Withdraw Debit') as 'today_withdraw_amt',
// (SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='ATM Expenses') as 'atm_exp',
// (SELECT COALESCE(SUM(debit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='ATM Expenses') as 'atm_exp_amt',
// (SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Fee Expenses') as 'atm_fee',
// (SELECT COALESCE(SUM(debit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Fee Expenses') as 'atm_fee_amt',
// (SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Card Transaction') as 'card_txn',
// (SELECT COALESCE(SUM(debit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type='Card Transaction') as 'card_txn_amt',
// (SELECT COUNT(id) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type IN('ATM Reversal','FEE Reversal')) as 'reversal',
// (SELECT COALESCE(SUM(credit),0) FROM dairy.happay_webhook_live WHERE date='$today_date' AND trans_type IN('ATM Reversal','FEE Reversal')) as 'reversal_amt',
// (SELECT COUNT(id) FROM dairy.happay_webhook_load_wallet WHERE date(txn_date)='$today_date') as 'company_load',
// (SELECT COALESCE(SUM(currency_amount),0) FROM dairy.happay_webhook_load_wallet WHERE date(txn_date)='$today_date') as 'company_load_amt'
// ");

// if(!$get_wallet_details){
	// echo getMySQLError($conn);
	// errorLog(getMySQLError($conn),$conn,$_SERVER['REQUEST_URI'],__LINE__);
	// exit();
// }

// $row_details2 = fetchArray($get_wallet_details);
?>
<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Dashboard
        <small></small>
      </h4>
    </section>

<section class="content">
		
	<div class="row" style="font-size:13px;">
		  
		<div class="col-lg-3 col-xs-6">
			  <div class="small-box bg-green">
				<div class="inner">
				  <h4><?php echo "0"; ?></h4>
				  <p>LR Created</p>
				</div>
				<a href="#0" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
			  </div>
		 </div> 
	 
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4><?php echo "0"; ?></h4>
			<p>Pending e-Way Bill</p>
            </div>
            <a href="#0" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4><?php echo "0"; ?></h4>

              <p>Freight Memo</p>
            </div>
            <a href="#0" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4><?php echo "0"; ?></h4>

              <p>Own Truck Form</p>
            </div>
            <a href="#0" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		
	</div>
	
	<?php /*
	<div class="row" style="font-size:13px;">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<h4>Today's Statistics</h4>
		</div>
	</div>
	
	<div class="row" style="font-size:13px;">
		  
		<div class="col-lg-3 col-xs-6">
			  <div class="small-box bg-green">
				<div class="inner">
				  <h4><?php echo $row_details2['today_transactions_amt']; ?></h4>
				  <p>Transactions: <?php echo $row_details2['today_transactions']; ?></p>
				</div>
				<a href="report/index.php?type=TODAY_TRANS" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
			  </div>
		 </div> 
	 
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4><?php echo $row_details2['today_load_amt']; ?></h4>
			<p>Wallet Load: <?php echo $row_details2['today_load']; ?></p>
            </div>
            <a href="report/index.php?type=WALLET_LOAD" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4><?php echo $row_details2['today_withdraw_amt']; ?></h4>

              <p>Wallet Withdraw: <?php echo $row_details2['today_withdraw']; ?></p>
            </div>
            <a href="report/index.php?type=WALLET_WDL" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4><?php echo $row_details2['company_load_amt']; ?></h4>

              <p>Company Wallet Load: <?php echo $row_details2['company_load']; ?></p>
            </div>
            <a href="report/index.php?type=COMPANY_WALLET" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		
	</div>
	
	
	<div class="row" style="font-size:13px;">
		  
		<div class="col-lg-3 col-xs-6">
			  <div class="small-box bg-green">
				<div class="inner">
				  <h4><?php echo $row_details2['atm_exp_amt']; ?></h4>
				  <p>ATM Expenses: <?php echo $row_details2['atm_exp']; ?></p>
				</div>
				<a href="report/index.php?type=ATM_EXP" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
			  </div>
		 </div> 
	 
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4><?php echo $row_details2['atm_fee_amt']; ?></h4>
			<p>ATM Fee Expenses: <?php echo $row_details2['atm_fee']; ?></p>
            </div>
            <a href="report/index.php?type=FEE_EXP" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h4><?php echo $row_details2['card_txn_amt']; ?></h4>

              <p>Card Transaction: <?php echo $row_details2['card_txn']; ?></p>
            </div>
            <a href="report/index.php?type=CARD_TRANS" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
		<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h4><?php echo $row_details2['reversal_amt']; ?></h4>

              <p>Reversal: <?php echo $row_details2['reversal']; ?></p>
            </div>
            <a href="report/index.php?type=REVERSAL" target="_blank" class="small-box-footer">View <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		
	</div>
	
	*/ ?>
</section>
 </div>

<?php
include "footer.php";
?>