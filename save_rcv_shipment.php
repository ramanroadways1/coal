<?php
require_once("./connect.php");

$ship_id=escapeString($conn,($_POST['ship_id']));
$date_of_arrival=escapeString($conn,($_POST['date_of_arrival']));
$time_of_arrival=escapeString($conn,($_POST['time_of_arrival']));
$draft_weight=escapeString($conn,($_POST['draft_weight']));
$total_bl=escapeString($conn,($_POST['total_bl']));
$unq_id=escapeString($conn,($_POST['unq_id']));
$timestamp=date("Y-m-d H:i:s");
$total_be=count($_POST['be_no']);

if($total_bl!=$total_be)
{
	echo "<script type='text/javascript'>
		alert('Min Value of BE Should be : $total_bl');
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',false);
	</script>";
	exit();
}

if($ship_id=='' || $date_of_arrival=='' || $time_of_arrival=='')
{
	echo "<script type='text/javascript'>
		alert('Unable to fetch Date or Time.');
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',false);
	</script>";
	exit();
}

if($total_be<0)
{
	echo "<script type='text/javascript'>
		alert('BE NOT ADDED YET.');
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',false);
	</script>";
	exit();
}

for($j=0; $j<$total_be;$j++) 
{
	$chk_be_no = Qry($conn,"SELECT id FROM be_data WHERE unq_id='$unq_id' AND be_no='".escapeString($conn,strtoupper($_POST['be_no'][$j]))."'");
		  
	if(!$chk_be_no){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$be_no_db = escapeString($conn,strtoupper($_POST['be_no'][$j]));
	
	if(numRows($chk_be_no)>0)
	{
		echo "<script type='text/javascript'>
			alert('Duplicate BE number : $be_no_db in shipment number : $unq_id.');
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
		exit();
	}
}

if(count($_FILES['be_copy']['name'])>0)
{
	$valid_types = array("application/pdf");
	
	foreach($_FILES['be_copy']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			echo "<script>
				alert('Error : Only PDF Upload Allowed.');
				$('#loadicon').hide();
				$('#submit_button').attr('disabled',false);
			</script>";
			exit();
		}
    }
	
	for($i2=0; $i2<count($_FILES['be_copy']['name']);$i2++) 
	{
		$UploadedSize = $_FILES['be_copy']['size'][$i2];
		
		if(($UploadedSize/1024/1024)>4)
		{
			echo "<script>
				alert('Error : MAX Upload size allowed is : 4MB');
				$('#loadicon').hide();
				$('#submit_button').attr('disabled',false);
			</script>";
			exit();
		}
	}
}
else
{
	echo "<script>
			alert('Error : Please Upload BE Copy.');
			$('#loadicon').hide();
			$('#submit_button').attr('disabled',false);
		</script>";
	exit();
}
   
StartCommit($conn);
$flag = true;	
   
$insert_shipment = Qry($conn,"UPDATE shipment SET draft_weight='$draft_weight',bal_weight='$draft_weight',date_of_arrival='$date_of_arrival',
time_of_arrival='$time_of_arrival',rcv='1',rcv_time='$timestamp',rcv_branch_user='$branch_sub_user' WHERE id='$ship_id'");

if(!$insert_shipment){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

for($i=0; $i<$total_be;$i++) 
{
	$sourcePath = $_FILES['be_copy']['tmp_name'][$i];
	 
	$fix_name=date('dmYHis').mt_rand();
	$targetPath = "be_copy/".$fix_name.".".pathinfo($_FILES['be_copy']['name'][$i],PATHINFO_EXTENSION);
				
	if(pathinfo($_FILES['be_copy']['name'][$i],PATHINFO_EXTENSION)!='pdf')
	{
		ImageUpload(700,900,$sourcePath);
	}

	if(move_uploaded_file($sourcePath, $targetPath)){
		$files = $targetPath;
	}
	else{
		$flag = false;
		errorLog("Error : Unable to upload BE copy.",$conn,$page_name,__LINE__);
	}
	
	$file_name = $files;
	
   $insert_be=Qry($conn,"INSERT INTO be_data(unq_id,bl_id,be_no,be_copy,be_date,branch,branch_user,timestamp) VALUES 
	('".$unq_id."','".$_POST['bl_id'][$i]."','".escapeString($conn,strtoupper($_POST['be_no'][$i]))."',
	'".$file_name."','".$_POST['be_date'][$i]."','$branch','$branch_sub_user','".$timestamp."')");
		  
	if(!$insert_be){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	echo "<script>
			alert('Shipment Marked as Received !');
			$('#RcvButton$ship_id').attr('disabled',true);
			$('#RcvButton$ship_id').html('Received');
			$('#RcvButton$ship_id').attr('class','btn btn-xs bg-danger');
			$('#RcvForm')[0].reset();
			document.getElementById('close_modal_button').click();
			$('#loadicon').hide();
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script type='text/javascript'>
		alert('Error While Processing Request.');
		$('#loadicon').hide();
		$('#submit_button').attr('disabled',false);
	</script>";
	exit();
}

?>