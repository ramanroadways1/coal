<?php
require_once("connect.php");

$frn_no=mysqli_real_escape_string($conn,($_POST['frn_no']));
$rr_no=mysqli_real_escape_string($conn,($_POST['rr_no']));
$files=explode(",",$_POST['uploads']);
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<button onclick="window.close()" type="button" style="margin:10px" class="btn btn-danger">Close</button>

<div class="container">
	
	<div class="row">
		<div class="form-group col-md-4">
			<h4>FRN No : <?php echo $frn_no; ?></h4>
		</div>
		
		<div class="form-group col-md-4">
			<h4>RR No : <?php echo $rr_no; ?></h4>
		</div>
		
		<div class="form-group col-md-4">
			<h4>Total Uploads : <?php echo count($files); ?></h4>
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-12">
			<hr>
		</div>	
	</div>	
	
	<div class="row">	
		<?php
		foreach($files as $my_Array)
		{
			echo "
			<div class='form-group col-md-3'>
				<img style='width:100%;height:250px;' src='$my_Array' />
				<a href='$my_Array' target='_blank' class='btn btn-xs btn-primary'>View Full</a>
			</div>
			";
		}
		?>
	</div>
</div>