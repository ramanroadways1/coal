<?php
include_once "header.php";
?>
<script type="text/javascript">
   	$(function() {
		$("#truck_no").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/get_market_vehicle.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#truck_no').val(ui.item.value);   
               $('#truck_no_id').val(ui.item.id);     
               $('#wheeler').val(ui.item.wheeler);     
               $('#owner_name').val(ui.item.owner_name);     
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Vehicle does not exists.');
			$("#truck_no").val('');
			$("#truck_no").focus();
			$("#truck_no_id").val('');
			$("#wheeler").val('');
			$("#owner_name").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});	
  
     	$(function() {
		$("#broker_name").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/broker.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#broker_name').val(ui.item.value);   
               $('#broker_id').val(ui.item.id);     
               $('#broker_pan').val(ui.item.pan);     
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Broker does not exists.');
			$("#broker_name").val('');
			$("#broker_name").focus();
			$("#broker_id").val('');
			$("#broker_pan").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});

      	$(function() {
		$("#driver_name").autocomplete({
		source: function(request, response) { 
			 $.ajax({ url: 'autofill/driver.php', type: 'post',
                  dataType: "json", data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
		select: function (event, ui) { 
               $('#driver_name').val(ui.item.value);   
               $('#driver_id').val(ui.item.id);     
               $('#driver_lic').val(ui.item.lic);     
               return false;
		},
		change: function (event, ui) {
		if(!ui.item){
		    $(event.target).val("");
			alert('Driver does not exists.');
			$("#driver_name").val('');
			$("#driver_name").focus();
			$("#driver_id").val('');
			$("#driver_lic").val('');
		}}, 
		focus: function (event, ui){
			return false;}
		});});
</script>	
<div class="content-wrapper">
    <section class="content-header">
       <h4>
		Create Feight Memo :
      </h4>
	  
	  <style>
	  label{font-size:13px;}
	  .form-control{text-transform:uppercase;}
	  </style>
	
	</section>

    <section class="content">
        <div class="row">
          <div class=""></div>
            <div class="col-md-12">
            <div class="box"> 
             <div class="box-body pad">
   
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>	   
   
<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
$("#loadicon").show();
$("#create_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_freight_memo.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result_form").html(data);
	},
	error: function() 
	{} });}));});
</script>			

	<form id="Form1" autocomplete="off">

         <div class="row">
			
			 <div class="form-group col-md-12">
			 
				 <div class="form-group col-md-3">
                       <label>Vehicle Number <font color="red">*</font></label>
                        <input type="text" class="form-control" id="truck_no" name="truck_no" required />
                 </div>
				 
				 <div class="form-group col-md-3">
                       <label>Owner Name <font color="red">*</font></label>
                       <input type="text" id="owner_name" class="form-control" name="" required readonly />
                 </div>
				 
				 <input type="hidden" id="truck_no_id" name="truck_id">
				 
				 <div class="form-group col-md-3">
                       <label>Broker Name <font color="red">*</font></label>
                       <input type="text" id="broker_name" class="form-control" name="broker_name" required />
                 </div>
				 
				 <input type="hidden" id="broker_id" name="broker_id">
				 
				 <div class="form-group col-md-3">
                       <label>Broker PAN <font color="red">*</font></label>
                        <input type="text" id="broker_pan" class="form-control" name="" required readonly />
                 </div>
				 
				  <div class="form-group col-md-3">
                       <label>DRIVER Name <font color="red">*</font></label>
                       <input type="text" onblur="FetchDriver(this.value)" id="driver_name" class="form-control" name="driver_name" required />
                 </div>
				 
				 <input type="hidden" id="driver_id" name="driver_id">
				 
				 <div class="form-group col-md-3">
                      <label>Driver LicNo <font color="red">*</font></label>
                      <input type="text" id="driver_lic" class="form-control" name="driver_lic" required readonly />
                 </div>
				 
				<div class="form-group col-md-2">
                     <label>Total LR Wt. <font color="red">*</font></label>
                     <input type="number" value="0" min="1" step="any" class="form-control" id="weight" name="weight" readonly required />
                </div>
				
				<script type="text/javascript">
				$(document).ready(function(){
					$('input[type="checkbox"]').click(function(){
						
						if(this.id=='rate_check')
						{
							$('#fix_check').prop('checked', false);
							$('#freight').prop('readonly', true);
							$('#freight').val('');
							$('#rate').val('');
								
							if($(this).prop("checked") == true)
							{
								$('#rate').prop('readonly', false);
								$('#rate_or_fix').val('RATE');
							}
							else if($(this).prop("checked") == false)
							{
								$('#rate').prop('readonly', true);
								$('#rate_or_fix').val('');
							}
						}
						else if(this.id=='fix_check')
						{
							$('#rate_check').prop('checked', false);
							$('#rate').prop('readonly', true);
							$('#rate').val('');
							$('#freight').val('');
								
							if($(this).prop("checked") == true)
							{
								$('#freight').prop('readonly', false);
								$('#rate_or_fix').val('FIX');
							}
							else if($(this).prop("checked") == false)
							{
								$('#freight').prop('readonly', true);
								$('#rate_or_fix').val('');
							}
						}
					});
				});
				
			function CalcRate(rate)
			{
				var weight = Number($('#weight').val());
				if(weight==0 || weight=="")
				{
					$('#rate').val('')	
					$('#freight').val('')	
				}
				else
				{
					var freight_value=Math.round((Number(rate)*weight).toFixed(2));
					
					if(freight_value>200000)
					{
						alert('MAX Freight Value is : 200000');
						$('#rate').val('')	
						$('#freight').val('')	
					}
					else					
					{
						$('#freight').val(freight_value);
					}
				}	
			}

			function CalcFix(freight)
			{
				var weight = Number($('#weight').val());
				
				if(weight==0 || weight=="")
				{
					$('#rate').val('')	
					$('#freight').val('')	
				}
				else
				{
					if(freight>200000)
					{
						alert('MAX Freight Value is : 200000');
						$('#rate').val('')	
						$('#freight').val('')	
					}
					else
					{
						$('#rate').val(Math.round((Number(freight)/weight).toFixed(2)));
					}	
				}	
			}			
			</script>
				
				<div class="form-group col-md-2">
                     <label>Rate <font color="red">*</font> <input id="rate_check" name="rate_check" type="checkbox" /></label>
                     <input type="number" min="1" oninput="CalcRate(this.value)" class="form-control" id="rate" name="rate" readonly required />
                </div>
				
				<div class="form-group col-md-2">
                     <label>Fix Freight <font color="red">*</font> <input id="fix_check" name="fix_check" type="checkbox" /></label>
                     <input type="number" max="200000" oninput="CalcFix(this.value)" min="1" class="form-control" id="freight" name="freight" readonly required />
                </div>
				 
				<input type="hidden" id="rate_or_fix" name="rate_or_fix"> 
				 
				 <div class="form-group col-md-2">
					<button type="button" id="chk_lr_button" class="btn btn-primary" onclick="FetchLR();">ADD LR</button>
                 </div>
				 
				 <script>
				 function FetchLR()
				 {
					 var tno = $('#truck_no').val();
					 
					 if(tno=='')
					 {
						 alert('Enter Truck Number First !');
						 $('#lr_result').html('');
					 }
					 else
					 {
						 $('#chk_lr_button').attr('disabled',true);
						 
						 $("#loadicon").show();
							jQuery.ajax({
							url: "fetch_lr_for_fm.php",
							data: 'tno=' + tno,
							type: "POST",
							success: function(data) {
								$("#lr_result").html(data);
							},
							error: function() {}
						});
					 }	
				 }
				 
				 function AddLR(shipment_id,id,lrno,weight)
				 {
						$('#fix_check').prop('checked', false);
						$('#rate_check').prop('checked', false);
						$('#rate').prop('readonly', true);
						$('#freight').prop('readonly', true);
						$('#freight').val('');
						$('#rate').val('');
						
					 var ext_shiment_id = $('#shipment_id').val();
					 
					 if(ext_shiment_id!='' && ext_shiment_id!=shipment_id)
					 {
						 alert('LR belongs to another shipment.');
					 }
					 else
					 {
						$('#shipment_id').val(shipment_id);
					
						 var ext_weight = Number($('#weight').val());
						 
						 $('#AddBtn'+id).hide();
						 $('#RemoveBtn'+id).show();
						 $('#weight').val(Number(ext_weight+Number(weight)));
						 $('#lrid'+id).val(id);
						 $('#rightImg'+id).show();
					 }
				 }
				 
				 function RemoveLR(shipment_id,id,lrno,weight)
				 {
					$('#fix_check').prop('checked', false);
					$('#rate_check').prop('checked', false);
					$('#rate').prop('readonly', true);
					$('#freight').prop('readonly', true);
					$('#freight').val('');
					$('#rate').val('');
					
					var ext_weight = Number($('#weight').val());
					 
					 $('#AddBtn'+id).show();
					 $('#RemoveBtn'+id).hide();
					 
					 $('#weight').val(Number(ext_weight-Number(weight)));
					 
					 if(Number(ext_weight-Number(weight))<=0)
					 {
						 $('#shipment_id').val('');
					 }
					 
					 $('#lrid'+id).val('');
					 $('#rightImg'+id).hide();
				 }
				 </script>
				 
				 <input id="shipment_id" type="hidden" name="shipment_id">
				 
				 <div class="form-group col-md-12" id="lr_result">
                 </div>
				
				<div class="form-group col-md-3" style="display:none" id="gps_type_div">   
					<label>GPS Type <font color="red"><sup>*</sup></font></label>
					<select name="gps_type" class="form-control" id="gps_type" required="required">
						<option value="">--select--</option>
						<option value="TRIP">Trip Wise</option>
						<option value="FIX">FIX Install</option>
					</select>
                </div> 
				
				<div class="form-group col-md-3">   
					<label>&nbsp;</label>
					<br>
					<button style="display:none" type="submit" disabled id="create_button" class="btn btn-success">Create Freight Memo</button>
                </div> 
				
				<div class="form-group col-md-6" id="result_form"></div> 
				 
          </div>
          </div>
         
      </form>

</div>
</div>
			</div> 
          </div>
        </div>       
    </section>
	
		<script>
		// function Func1()
		// {
			// var sum = 0;
			// $(".bl_weight").each(function(){
				// sum += +$(this).val();
			// });
			// $(".total_bl_weight").val(sum);
		// }
		</script>
<?php
include "footer.php";
?>
